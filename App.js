import 'react-native-gesture-handler';
import React from 'react';
import {Provider} from 'react-redux';
import store from './store';
import {NavigationContainer} from '@react-navigation/native';
import {Navigator} from './src/navigator/Navigator';

const App = () => {
  return (
    <Provider store={store}>
      
      <NavigationContainer>
        <Navigator />
      </NavigationContainer>
     </Provider>
  );
};

export default App;

// Generar Apk
// cd android
// ./gradlew app:assembleRelease
