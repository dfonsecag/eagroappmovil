import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {MuestreoFrutaScreen} from '../screens/MuestreoFrutaScreen';
import {HomeScreen} from '../screens/HomeScreen';
import {MuestreoRaizScreen} from '../screens/MuestreoRaizScreen';
import {MuestreoMeristemosScreen} from '../screens/MuestreoMeristemosScreen';
import {LoginScreen} from '../screens/LoginScreen';
import {LoadingScreen} from '../screens/LoadingScreen';
import {AvanceObraScreen} from '../screens/AvanceObraScreen';
import {MuestreoPesoScreen} from '../screens/MuestreoPesoScreen';
import {AvanceObraManualScreen} from '../screens/AvanceObraManualScreen';
import {HojaVidaScreen} from '../screens/HojaVidaScreen';
import {DetalleCedulaScreen} from '../screens/DetalleCedulaScreen';
import {MuestreoPremaduracionScreen} from '../screens/MuestreoPremaduracionScreen';
import {MuestreoEstimacionScreen} from '../screens/MuestreoEstimacionScreen';
import {EstimacionScreen} from '../screens/EstimacionScreen';
import { PorcentajeEstimacionScreen } from '../screens/PorcentajeEstimacionScreen';
import { MapaScreen } from '../screens/MapaScreen';
import { AvanceObraAdminScreen } from '../screens/AvanceObraAdminScreen';
import { MuestreoFloracionScreen } from '../screens/MuestreoFloracionScreen';
import { MuestreoSemillaScreen } from '../screens/MuestreoSemillaScreen';
import { MuestreoCalidadSiembraScreen } from '../screens/MuestreoCalidadSiembraScreen';
import { MuestreoFrutaRechazoScreen } from '../screens/MuestreoFrutaRechazoScreen';
import { MuestreoCajasFrutasScreen } from '../screens/MuestreoCajasFrutasScreen';
import { RegistroContratistaTerrazasScreen } from '../screens/RegistroContratistaTerrazasScreen';
import { RegistroPhCloroPlantaScreen } from '../screens/RegistroPhCloroPlantaScreen';

const Stack = createStackNavigator();

export const Navigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="LoadingScreen"
        options={{headerShown: false}}
        component={LoadingScreen}
      />
      <Stack.Screen
        name="LoginScreen"
        options={{headerShown: false}}
        component={LoginScreen}
      />
      <Stack.Screen
        name="HomeScreen"
        options={{
          title: 'MENU PRINCIPAL',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: null,
        }}
        component={HomeScreen}
      />
      <Stack.Screen
        name="MuestreoFrutaScreen"
        options={{
          title: 'MUESTREO PLAGAS EN FRUTA',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 18,
          },
          headerLeft: null,
        }}
        component={MuestreoFrutaScreen}
      />

     <Stack.Screen
        name="MuestreoSemillaScreen"
        options={{
          title: 'MUESTREO PESOS SEMILLAS',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 18,
          },
          headerLeft: null,
        }}
        component={MuestreoSemillaScreen}
      />

     <Stack.Screen
        name="MuestreoCalidadSiembra"
        options={{
          title: 'MUESTREO CALIDAD SIEMBRA',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 18,
          },
          headerLeft: null,
        }}
        component={MuestreoCalidadSiembraScreen}
      />

      <Stack.Screen
        name="DetalleCedulaScreen"
        options={{headerShown: false}}
        component={DetalleCedulaScreen}
      />

      <Stack.Screen
        name="MuestreoRaizScreen"
        options={{
          title: 'MUESTREO PLAGAS EN RAIZ',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 19,
          },
          headerLeft: null,
        }}
        component={MuestreoRaizScreen}
      />

    <Stack.Screen
        name="MuestreoFrutaRechazoScreen"
        options={{
          title: 'MUESTREO FRUTA RECHAZO',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: 'black',
          },
          headerTintColor: 'white',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: null,
        }}
        component={MuestreoFrutaRechazoScreen}
      />

      <Stack.Screen
        name="MuestreoCajasFrutasScreen"
        options={{
          title: 'MUESTREO PESO EN CAJAS',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: null,
        }}
        component={MuestreoCajasFrutasScreen}
      />

      <Stack.Screen
        name="RegistroContratistaTerrazasScreen"
        options={{
          title: 'TERRAZAS CONTRATISTAS',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: null,
        }}
        component={RegistroContratistaTerrazasScreen}
      />

      <Stack.Screen
        name="RegistroPhCloroPlantaScreen"
        options={{
          title: 'REGISTRO PH Y CLORO',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: null,
        }}
        component={RegistroPhCloroPlantaScreen}
      />

      <Stack.Screen
        name="MuestreoMeristemosScreen"
        options={{
          title: 'MUESTREO MERISTEMOS',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: null,
        }}
        component={MuestreoMeristemosScreen}
      />
      <Stack.Screen
        name="MuestreoPesoScreen"
        options={{
          title: 'MUESTREO PESOS',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: null,
        }}
        component={MuestreoPesoScreen}
      />

      <Stack.Screen
        name="AvanceObraScreen"
        options={{
          title: 'AVANCE DE OBRAS',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: null,
        }}
        component={AvanceObraScreen}
      />

    <Stack.Screen
        name="AvanceObraAdminScreen"
        options={{
          title: 'AVANCE DE OBRAS ADMIN',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: null,
        }}
        component={AvanceObraAdminScreen}
      />    

      <Stack.Screen
        name="AvanceObraManualScreen"
        options={{
          title: 'AVANCE DE OBRAS MANUAL',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: null,
        }}
        component={AvanceObraManualScreen}
      />
      <Stack.Screen
        name="MuestreoEstimacionScreen"
        options={{
          title: 'ESTIMACION DE FRUTA',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: null,
        }}
        component={MuestreoEstimacionScreen}
      />
      <Stack.Screen
        name="MuestreoPremaduracionScreen"
        options={{
          title: 'PREMADURACION DE FRUTA',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: null,
        }}
        component={MuestreoPremaduracionScreen}
      />
       <Stack.Screen
        name="MuestreoFloracionScreen"
        options={{
          title: 'MUESTREO FLORACION NATURAL',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: null,
        }}
        component={MuestreoFloracionScreen}
      />
      <Stack.Screen
        name="HojaVidaScreen"
        options={{
          title: 'HOJA DE VIDA',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: 'black',
          },
          headerTintColor: 'white',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: null,
        }}
        component={HojaVidaScreen}
      />
      <Stack.Screen
        name="EstimacionScreen"
        options={{
          title: 'REPORTE ESTIMACION FRUTA',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 18,
          },
          headerLeft: null,
        }}
        component={EstimacionScreen}
      />

      <Stack.Screen
        name="PorcentajeEstimacionScreen"
        options={{
          title: 'PORCENTAJE DE RECOLECCION',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 18,
          },
          headerLeft: null,
        }}
        component={PorcentajeEstimacionScreen}
      />
       <Stack.Screen
        name="MapaScreen"
        options={{
          title: 'MAPA',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#8BC34A',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 18,
          },
          headerLeft: null,
        }}
        component={MapaScreen}
      />
    </Stack.Navigator>
  );
};
