import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  subTitle: {
    alignItems: 'center',
    marginLeft: 10,
    fontSize: 19,
    textAlignVertical: 'center',
    fontWeight: 'bold',
    color: "black"
  },
  viewSubTitle: {
    flexDirection: 'row',
    backgroundColor: '#8BC34A',
    height: 55,
    width: '100%',
  },
  viewSubSeleccionar: {
    flexDirection: 'row',
    height: 55,
    width: '100%',
    marginTop: -5,
  },
  viewCuerpoContador: {
    flexDirection: 'row',
    height: 40,
    width: '100%',
    justifyContent: 'space-around',
  },
  viewCuerpoClima: {
    flex: 1,
    flexDirection: 'row',
    height: 40,
    width: '100%',
    justifyContent: 'space-around',
    marginBottom: 5,
  },
  viewTituloContador: {
    flexDirection: 'row',
    height: 35,
    width: '100%',
  },
  labelContador: {
    alignItems: 'center',
    marginLeft: 10,
    fontSize: 35,
    textAlignVertical: 'center',
    fontWeight: 'bold',
    color:'black'
  },
  botonContenedorEnviar: {
    alignItems: 'center',
    marginTop: 45,
    marginBottom: 20,
  },
  botonEnviar: {
    paddingHorizontal: 20,
    paddingVertical: 5,
    borderRadius: 100,
    backgroundColor: '#8BC34A',
    height: 60,
    width: 125,
  },
  botonSincronizar: {
    paddingHorizontal: 20,
    paddingVertical: 5,
    borderRadius: 100,
    backgroundColor: '#8BC34A',
    height: 60,
    width: 250,
  },
  botonEnviarText: {
    fontSize: 26,
    color: 'black',
    fontWeight: 'bold',
    textAlignVertical: 'center',
    alignItems: 'center',
  },
  inputObservacion: {
    fontSize: 20,
    borderColor: 'black',
    color: 'black',
  },
  inputHora: {
    fontSize: 22,
    borderColor: 'black',
    color: 'black',
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  checkboxContenedor: {
    flexDirection: "row",
  },
  checkboxStyle: {
    alignSelf: "center",
    marginLeft:10
  },
  label: {
    margin: 8,
    fontSize:22,
    fontWeight:'bold',
    textAlign:'center'
  },
});
