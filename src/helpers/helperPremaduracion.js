export const clasificarCalibres = (premaduracion, dataForm) => {
  // Calibre
  let c5 = 0;
  let c6 = 0;
  let c7 = 0;
  let c8 = 0;
  let c9 = 0;
  // Cochinilla
  let total_cochinilla_interna = 0;
  let total_cochinilla_externa = 0;

  premaduracion.map((dato, index) => {
    if (dato.cochinillaInterna) {
      total_cochinilla_interna += 1;
    }
    if (dato.cochinillaExterna) {
      total_cochinilla_externa += 1;
    }

    switch (index) {
      case 0:
        dataForm.b1 = dato.brix;
        dataForm.t1 = dato.translucidez;
        break;
      case 1:
        dataForm.b2 = dato.brix;
        dataForm.t2 = dato.translucidez;
        break;
      case 2:
        dataForm.b3 = dato.brix;
        dataForm.t3 = dato.translucidez;
        break;
      case 3:
        dataForm.b4 = dato.brix;
        dataForm.t4 = dato.translucidez;
        break;
      case 4:
        dataForm.b5 = dato.brix;
        dataForm.t5 = dato.translucidez;
        break;
      case 5:
        dataForm.b6 = dato.brix;
        dataForm.t6 = dato.translucidez;
        break;
      case 6:
        dataForm.b7 = dato.brix;
        dataForm.t7 = dato.translucidez;
        break;
      case 7:
        dataForm.b8= dato.brix;
        dataForm.t8 = dato.translucidez;
        break;
      case 8:
        dataForm.b9 = dato.brix;
        dataForm.t9 = dato.translucidez;
        break;
      case 9:
        dataForm.b10 = dato.brix;
        dataForm.t10 = dato.translucidez;
        break;
      case 10:
        dataForm.b11 = dato.brix;
        dataForm.t11 = dato.translucidez;
         break;
       case 11:
        dataForm.b12 = dato.brix;
        dataForm.t12 = dato.translucidez;
        break;
          
      default:
        break;
    }

    switch (dato.calibre) {
      case 5:
        c5 += 1;
        break;
      case 6:
        c6 += 1;
        break;
      case 7:
        c7 += 1;
        break;
      case 8:
        c8 += 1;
        break;
      case 9:
        c9 += 1;
        break;
      default:
        break;
    }
  });

  dataForm = {
    ...dataForm,
    c5,
    c6,
    c7,
    c8,
    c9,
    total_cochinilla_interna,
    total_cochinilla_externa,
  };
  return dataForm;
};
