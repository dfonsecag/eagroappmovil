import SweetAlert from 'react-native-sweet-alert';

export const mostrarAlerta =  (titulo,mensaje,style) => {
  SweetAlert.showAlertWithOptions({
    title: `${titulo}`,
    subTitle: `${mensaje}`,
    confirmButtonTitle: 'OK',
    confirmButtonColor: '#000',
    otherButtonTitle: 'Cancel',
    otherButtonColor: '#dedede',
    style,
    cancellable: true,
  });
};
