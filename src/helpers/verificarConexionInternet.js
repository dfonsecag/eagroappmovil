import NetInfo from "@react-native-community/netinfo";

export const conexionInternet = async () => {
  let conexion;
  let tipoConexion;

  await NetInfo.fetch().then(state => {
    //  console.log("Connection type", state.type);
    //  console.log("Is connected?", state.isConnected);
    tipoConexion = state.type;
    conexion = state.isConnected;
  });
  if(conexion === true && tipoConexion === "wifi"){
    return true;
  }else{
    return false;
  }
  
};
