export const clasificarCalibres = (pesos, cedula) => {
  let muyPequena = 0;
  let sumaMuyPequena = 0;
  let pequena = 0;
  let sumaPequena = 0;
  let mediano = 0;
  let sumaMediano = 0;
  let grande = 0;
  let sumaGrande = 0;
  let muyGrande = 0;
  let sumaMuyGrande = 0;
  let extraGrande = 0;
  let sumaExtraGrande = 0;
  let jumbo = 0;
  let sumaJumbo = 0;

  pesos.map(p => {
    if (parseInt(p) >= 1100 && parseInt(p) <= 1499) {
      muyPequena += 1;
      sumaMuyPequena += parseInt(p);
    } else if (parseInt(p) >= 1500 && parseInt(p) <= 1899) {
      pequena += 1;
      sumaPequena += parseInt(p);
    } else if (parseInt(p) >= 1900 && parseInt(p) <= 2299) {
      mediano += 1;
      sumaMediano += parseInt(p);
    } else if (parseInt(p) >= 2300 && parseInt(p) <= 2699) {
      grande += 1;
      sumaGrande += parseInt(p);
    } else if (parseInt(p) >= 2700 && parseInt(p) <= 3099) {
      muyGrande += 1;
      sumaMuyGrande += parseInt(p);
    } else if (parseInt(p) >= 3100 && parseInt(p) <= 3499) {
      extraGrande += 1;
      sumaExtraGrande += parseInt(p);
    } else if (parseInt(p) >= 3500 && parseInt(p) <= 3900) {
      jumbo += 1;
      sumaJumbo += parseInt(p);
    }
  });
  if (sumaMuyPequena > 0) {
    cedula.promediomuypequena = Math.ceil(sumaMuyPequena / muyPequena);
    cedula.totalmuypequena = muyPequena;
  } else {
    cedula.promediomuypequena = 0;
    cedula.totalmuypequena = muyPequena;
  }

  if (sumaPequena > 0) {
    cedula.promediopequena = Math.ceil(sumaPequena / pequena);
    cedula.totalpequena = pequena;
  } else {
    cedula.promediopequena = 0;
    cedula.totalpequena = pequena;
  }

  if (sumaMediano > 0) {
    cedula.promediomediano = Math.ceil(sumaMediano / mediano);
    cedula.totalmediano = mediano;
  } else {
    cedula.promediomediano = 0;
    cedula.totalmediano = mediano;
  }
  if (sumaGrande > 0) {
    cedula.promediogrande = Math.ceil(sumaGrande / grande);
    cedula.totalgrande = grande;
  } else {
    cedula.promediogrande = 0;
    cedula.totalgrande = grande;
  }

  if (sumaMuyGrande > 0) {
    cedula.promediomuygrande = Math.ceil(sumaMuyGrande / muyGrande);
    cedula.totalmuygrande = muyGrande;
  } else {
    cedula.promediomuygrande = 0;
    cedula.totalmuygrande = muyGrande;
  }

  if (sumaExtraGrande > 0) {
    cedula.promedioextragrande = Math.ceil(sumaExtraGrande / extraGrande);
    cedula.totalextragrande = extraGrande;
  } else {
    cedula.promedioextragrande = 0;
    cedula.totalextragrande = extraGrande;
  }

  if (sumaJumbo > 0) {
    cedula.promediojumbo = Math.ceil(sumaJumbo / jumbo);
    cedula.totaljumbo = jumbo;
  } else {
    cedula.promediojumbo = 0;
    cedula.totaljumbo = jumbo;
  }

  return cedula;
};


