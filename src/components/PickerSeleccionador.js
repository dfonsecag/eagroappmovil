import React from 'react';
import {Picker} from '@react-native-picker/picker';



export const PickerSeleccionador = ({seleccionar, setSeleccionar, data, placeholder, inicial}) => {

   
  return (
    <Picker
      selectedValue={seleccionar}
     
      onValueChange={( itemValue ) => setSeleccionar(itemValue)}>     
       <Picker.Item style={{fontSize:22, fontWeight:'bold',color:'black'}}  label={` -- ${placeholder} --`} value={0} key={0} />
      {
        (data) ?
        data.map(c => 
          (<Picker.Item style={{fontSize:19,color:'black'}} label={`  **${inicial} ${c.name}   `} value={c.id} key={c.id} />)
          ) :
        null
      }
    </Picker>
  );
};
