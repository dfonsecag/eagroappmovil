import React from 'react';
import { View, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {styles} from '../styles/styles';



export const CerrarSesion = ({cerrarSesion}) => {
  
  return (
    <View style={{...styles.viewSubSeleccionar, justifyContent: 'flex-end'}}>
      <TouchableOpacity onPress={() => cerrarSesion()}>
        <View style={{marginRight: 18, marginTop: 10}}>
          <Icon name="power-outline" size={45} color="black"/>
        </View>
      </TouchableOpacity>
    </View>
  );
};
