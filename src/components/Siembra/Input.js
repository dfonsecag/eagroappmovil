import React from 'react';
import {Text, TextInput, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {styles} from '../../styles/styles';

export const Input = ({dato, setDato, icono = "calculator-outline",subtitulo}) => {
  return (
    <View style={{...styles.viewSubSeleccionar,marginLeft:10, marginTop: 11,height: 70}}>
                  <View style={{justifyContent: 'center'}}>
                    <Icon name={icono} size={30} color="black"/>
                  </View>
                  <Text style={{...styles.subTitle,fontSize:25}}> {`${subtitulo} :`} </Text>
                  <TextInput
                    placeholder="00.0"
                    keyboardType="phone-pad"
                    underlineColorAndroid="black"
                    placeholderTextColor="#6e6e6e"
                    style={{...styles.inputHora,fontSize:33}}
                    selectionColor="black"
                    onChangeText={value => {
                      let numero = value.replace(/[^0-9\.]/g, '');
                      setDato(numero);
                    }}
                    value={dato}
                    autoCapitalize="none"
                    autoCorrect={false}
                  />
                </View>
  );
};
