import React from 'react';
import {Text, View} from 'react-native';
import {styles} from '../../styles/styles';

export const RangoView = ({rango,cantidad}) => {
  return (
    <View style={{...styles.viewSubSeleccionar, height: 65}}>
      <Text style={{...styles.subTitle, fontSize: 22}}>
        {` ${rango}:   ${cantidad > 0 ? cantidad : 0}`}
      </Text>
    </View>
  );
};
