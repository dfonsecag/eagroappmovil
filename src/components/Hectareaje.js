import React from 'react';
import {Text, View} from 'react-native';
import {styles} from '../styles/styles';
import Icon from 'react-native-vector-icons/Ionicons';

export const Hectareaje = ({area}) => {
  return (
    <View style={{...styles.viewSubSeleccionar, height: 60}}>
      <View style={{justifyContent: 'center'}}>
        <Icon name="location-outline" size={35} color='black'/>
      </View>
      <Text style={{...styles.subTitle, fontSize: 22}}>Hectareaje: {area}</Text>
    </View>
  );
};
