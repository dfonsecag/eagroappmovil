import React from 'react';
import SearchableDropdown from 'react-native-searchable-dropdown';

export const DropdownHojaVida = ({seleccionarCedula, setSeleccionarCedula, cedulas}) => {
    return (
       <>
         <SearchableDropdown
          selected
          onTextChange={(text) =>  console.log(text)}
          onItemSelect={(item) =>  setSeleccionarCedula(item)}
          containerStyle={{ padding: 5 }}
          textInputStyle={{
            padding: 12,
            borderWidth: 1,
            borderColor: '#ccc',
            backgroundColor: '#FAF7F6',
            placeholderColor: 'black',
            fontSize:20
          }}
          placeholderTextColor="black"
          itemStyle={{
            padding: 10,
            marginTop: 2,
            backgroundColor: '#FAF9F8',
            borderColor: '#bbb',
            borderWidth: 1,
            
          }}
          itemTextStyle={{
            color: '#000',
            fontSize:20
          }}
          itemsContainerStyle={{
            maxHeight: '100%',
          }}
          items={cedulas}
          placeholder={ (seleccionarCedula.id === undefined) ? 'Seleccionar Lote y Grupo ' : seleccionarCedula.name }
          underlineColorAndroid="transparent"
        />
       </>
    )
}
