import React from 'react';
import {View} from 'react-native';
import {RadioButton} from 'react-native-paper';
import {styles} from '../styles/styles';
import Icon from 'react-native-vector-icons/Ionicons';

export const RadioButtonClima = ({setClima, clima}) => {
  return (
    <View style={styles.viewCuerpoClima}>
       <RadioButton
        value="second"
        status={clima === 'SOLEADO' ? 'checked' : 'unchecked'}
        onPress={() => setClima('SOLEADO')}
      />
     <View style={{justifyContent: 'center'}}>
        <Icon name='sunny-outline' size={40} color='black'/>
      </View>
      <RadioButton
        value="second"
        status={clima === 'NUBLADO' ? 'checked' : 'unchecked'}
        onPress={() => setClima('NUBLADO')}
      />
      <View style={{justifyContent: 'center'}}>
        <Icon name='cloudy-outline' size={40} color='black'/>
      </View>
      <RadioButton
        value="second"
        status={clima === 'LLUVIOSO' ? 'checked' : 'unchecked'}
        onPress={() => setClima('LLUVIOSO')}
      />
      <View style={{justifyContent: 'center'}}>
        <Icon name='rainy-outline' size={40} color='black'/>
      </View>
    
    </View>
  );
};
