import React from 'react';
import {Picker} from '@react-native-picker/picker';

export const Select = ({value, setValue,data,label}) => {
  
  return (
    <Picker
    selectedValue={value}
    onValueChange={itemValue => setValue(itemValue)}>
    <Picker.Item
      style={{fontSize: 20, fontWeight: 'bold'}}
      label={` -- ${label} --`}
      value={0}
      key={0}
    />
    {data
      ? data.map(c => (
          <Picker.Item
            style={{fontSize: 20}}
            label={`  ** ${c.name} `}
            value={c.id}
            key={c.id}
          />
        ))
      : null}
  </Picker>
  );
};
