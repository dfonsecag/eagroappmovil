import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';


export const Contador = ({contador, setContador,muestras,setMuestras, label}) => {
  
  const muestrear = () => {
    setContador(contador+1);
    setMuestras(muestras+1);
  }
  return (
      <View style={estilo.viewBoton}>
              <TouchableOpacity style={estilo.boton} onPress={()=>muestrear()} >
                <View style={{justifyContent: 'center'}}>
                  <Text style={estilo.labelBoton}>{label}</Text>
                </View>
                <View style={{justifyContent: 'center'}}>
                  <Text style={estilo.labelNumero}>{contador}</Text>
                </View>
              </TouchableOpacity>
      </View>
  );
};

const estilo = StyleSheet.create({
  labelBoton: {
    alignItems: 'center',
    marginTop: 6,
    fontSize: 18,
    textAlign:'center',
    textAlignVertical:'center',
    fontWeight: '400',
    color:'black'
  },
  labelNumero: {
    alignItems: 'center',
    marginTop: 6,
    fontSize: 12,
    textAlign:'center',
    textAlignVertical:'center',
    color:'black'
  },
  viewBoton: {
    alignItems: 'center',
    marginTop: 0,
    marginBottom: 0
  },
  boton:{
    paddingHorizontal: 2,
    paddingVertical: 5,
    borderRadius: 20,
    backgroundColor: '#8BC34A',
    height: 70,
    width: 190,
  }
});

