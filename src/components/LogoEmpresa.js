import React from 'react'
import { Image, View } from 'react-native'

export const LogoEmpresaCelular = () => {
    return (
        <View style={{
            alignItems: 'center'
        }}>
            <Image 
                source={ require('../assets/logo.jpg') }
                style={{
                    width: '120%',
                    height: 100 ,
                }}
            />
        </View>
    )
}

export const LogoEmpresaTablet = () => {
  return (
    <>
      <View>
        <View
          style={{
            alignItems: 'center',
            height: '30%',
            marginTop:25,
            paddingHorizontal: 15,
            marginBottom:-90
          }}>
          <Image
            source={require('../assets/logo.jpg')}
            style={{
              width: '120%',
              height: 120
            }}
          />
        </View>
      </View>
    </>
  );
};