import React from 'react';
import {Text, View} from 'react-native';
import {styles} from '../../styles/styles';
import Icon from 'react-native-vector-icons/Ionicons';

export const InfoRecoleccion = ({info}) => {
  return (
    <View style={{...styles.viewSubSeleccionar, height: 45}}>
      <View style={{justifyContent: 'center'}}>
        <Icon name="information-circle-outline" size={35} />
      </View>
      <Text style={{...styles.subTitle, fontSize: 20}}>{info}</Text>
    </View>
  );
};