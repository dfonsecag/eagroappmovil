import React from 'react';
import {Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import { styles } from '../../styles/styles';

export const ContadorEstimacion = ({contador, setContador, contadorRestantes, setContadorRestantes}) => {
  const verificarContador = () => {
  
    if(contador !== 0){
      setContador(contador - 1);
      setContadorRestantes(contadorRestantes + 1)
    }
  
  }
  const incrementar = () => {
     setContador(contador + 1);
     setContadorRestantes(contadorRestantes - 1);
    
  }

  return (
    <View style={styles.viewCuerpoContador}>
      <TouchableOpacity onPress={() => verificarContador()}>
        <View style={{justifyContent: 'center'}}>
          <Icon name="remove-outline" size={50} color="black"/>
        </View>
      </TouchableOpacity>

      <Text style={styles.labelContador}> {contador} </Text>      
       
          <TouchableOpacity disabled={false} onPress={ incrementar }>
          <View style={{justifyContent: 'center', alignItems:'center'}}>
            <Icon name="add-outline" size={50} color='#8BC34A'/>
          </View>
        </TouchableOpacity>
      
    </View>
  );
};
