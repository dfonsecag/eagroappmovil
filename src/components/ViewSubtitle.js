import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {styles} from '../styles/styles';

export const ViewSubtitle = ({title, setEstado,estado = false,}) => {
  return (
    <View style={styles.viewSubTitle}>
    <Text style={styles.subTitle}> {title} </Text>
    <View style={{flex: 1}} />
    <TouchableOpacity onPress={() => setEstado(!estado)}>
      <View style={{justifyContent: 'center'}}>
        {
          (!estado) ?
          ( <Icon name="remove-outline" size={50} color='black'/>):
          ( <Icon name="add-outline" size={50} color='black'/>)
        }
       
      </View>
    </TouchableOpacity>
  </View>

  );
};
