import React from 'react';
import SearchableDropdown from 'react-native-searchable-dropdown';



export const DropdownSelect = ({seleccionarCedula, setSeleccionarCedula, cedulas, placeholder= 'Seleccionar Cédula'}) => {

   
  return (
    <>
    <SearchableDropdown
     selected
     onTextChange={(text) =>  console.log(text)}
     onItemSelect={(item) => setSeleccionarCedula(item)}
     containerStyle={{ padding: 5 }}
     textInputStyle={{
       padding: 12,
       borderWidth: 1,
       borderColor: '#ccc',
       backgroundColor: '#FAF7F6',
       fontSize:20
     }}
   //   textInputProps={
   //       on
   //   }
     itemStyle={{
       padding: 10,
       marginTop: 2,
       backgroundColor: '#FAF9F8',
       borderColor: '#bbb',
       borderWidth: 1,
       
     }}
     itemTextStyle={{
       color: '#000',
       fontSize:20
     }}
     itemsContainerStyle={{
       maxHeight: '100%',
     }}
     items={cedulas}
     placeholder={ (seleccionarCedula.toString() == 0) ? placeholder : seleccionarCedula.name }
     underlineColorAndroid="transparent"
   />
  </>
  );
};
