import React from 'react';
import {TextInput} from 'react-native';
import {styles} from '../styles/styles';

export const InputObservacion = ({observacion, setObservacion}) => {
  return (
    <TextInput
      placeholder="Observaciones"
      multiline
      numberOfLines={5}
      underlineColorAndroid="black"
      placeholderTextColor="#6e6e6e"
      style={styles.inputObservacion}
      selectionColor="black"
      onChangeText={value => {
        let valor = value.replace(/\n|\r/g, " ");;
        setObservacion(valor);
      }}
      value={observacion}
      autoCapitalize="none"
      autoCorrect={false}
    />
  );
};
