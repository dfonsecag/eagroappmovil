import React from 'react';
import {Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {styles} from '../styles/styles';

export const ItemContador = ({titulo}) => {
  return (
    <View style={styles.viewTituloContador}>
      <View style={{justifyContent: 'center'}}>       
      </View>
      <Text style={styles.subTitle}> {titulo} </Text>
    </View>
  );
};
