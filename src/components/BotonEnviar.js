import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {styles} from '../styles/styles';

export const BotonEnviar = ({enviarFormulario, titulo="Enviar"}) => {
  return (
    <View style={styles.botonContenedorEnviar}>
      <TouchableOpacity style={styles.botonEnviar} onPress={ enviarFormulario }>
        <View style={{justifyContent: 'center'}}>
          <Text style={styles.botonEnviarText}>{titulo}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};
