import React from 'react';
import {Text, View} from 'react-native';
import {RadioButton} from 'react-native-paper';
import {styles} from '../styles/styles';
export const RadioButtonQuema = ({setQuema, quema}) => {
  return (
    <View style={styles.viewCuerpoClima}>
       <RadioButton
        value="second"
        status={quema === 'No' ? 'checked' : 'unchecked'}
        onPress={() => setQuema('No')}
      />
     <View style={{justifyContent: 'center'}}>
      <Text style={styles.subTitle}>Sin Quema</Text>
      </View>
      <RadioButton
        value="second"
        status={quema === 'Si' ? 'checked' : 'unchecked'}
        onPress={() => setQuema('Si')}
      />
      <View style={{justifyContent: 'center'}}>
      <Text style={styles.subTitle}>Quemado</Text>
      </View>    
    </View>
  );
};
