import React from 'react';
import {Picker} from '@react-native-picker/picker';



export const PickerSelectCedulasAvanceObra = ({seleccionarCedula, setSeleccionarCedula, cedulas}) => {

   
  return (
    <Picker
      selectedValue={seleccionarCedula}
     
      onValueChange={( itemValue ) => setSeleccionarCedula(itemValue)}>     
       <Picker.Item style={{fontSize:22, fontWeight:'bold'}}  label={'   -- Seleccione la cédula --'} value={0} key={0} />
      {
        (cedulas) ?
        cedulas.map(c => 
          (<Picker.Item style={{fontSize:22}} label={` **  ${c.numerocedula}   `} value={c.numerocedula} key={c.numerocedula} />)
          ) :
        null
      }
    </Picker>
  );
};
