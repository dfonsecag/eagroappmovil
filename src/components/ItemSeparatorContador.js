import React from 'react';
import {View} from 'react-native';

export const ItemSeparatorContador = () => {
  return (
    <View
      style={{
        top: 10,
        borderBottomWidth: 2,
        opacity: 0.7,
        marginVertical: 8,
      }}></View>
  );
};
