import React from 'react'
import { Text, View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/core';



export const ListaMenuHome = ({menuItem} ) => {

   const navigation = useNavigation();
    return (
        <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => navigation.navigate(menuItem.component)}
        >
        <View style={styles.container}>
            <Icon
                name={menuItem.icon}
                color="#8BC34A"
                size={35}
            />
            <Text style={styles.itemText}>{menuItem.name}</Text>

            <View style={{flex:1}}/>

            <Icon
                name="chevron-forward-outline"
                color="#8BC34A"
                size={35}

            />
        </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        height: 50
    },
    itemText:{
        marginLeft:10,
        fontSize:20,
        color: "#4d4d4d"
    }
})
