import React from 'react';
import {Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {styles} from '../styles/styles';


export const FechaHora = ({fecha, hora}) => {

    return (
        <View style={styles.viewSubSeleccionar}>
        <View style={{justifyContent: 'center'}}>
          <Icon name="calendar-outline" size={32} color='black'/>
        </View>
        <Text style={styles.subTitle}> {fecha} </Text>
        <View style={{flex: 1}} />
        <View style={{justifyContent: 'center'}}>
          <Icon name="time-outline" size={32} color='black'/>
        </View>
        <Text style={styles.subTitle}> {hora} </Text>
      </View>
    )
}
