import React from 'react';
import {Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {styles} from '../styles/styles';


export const Ubicacion = ({latitud, longitud}) => {
    return (
        <View style={{...styles.viewSubSeleccionar, marginBottom:5, marginTop:2}}>
        <View style={{justifyContent: 'center'}}>
          <Icon name="locate-outline" size={35} color='black'/>
        </View>
        <Text style={styles.subTitle}>Lat:{latitud}</Text>
        <View/>
        <Text style={styles.subTitle}>Long:{longitud}</Text>
       
      </View>
    )
}
