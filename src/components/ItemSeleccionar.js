import React from 'react';
import {Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {styles} from '../styles/styles';

export const ItemSeleccionar = ({titulo, icono}) => {
  return (
    <View style={styles.viewSubSeleccionar}>
      <View style={{justifyContent: 'center'}}>
        <Icon name={icono} size={35} color="black"/>
      </View>
      <Text style={styles.subTitle}> {titulo} </Text>
    </View>
  );
};
