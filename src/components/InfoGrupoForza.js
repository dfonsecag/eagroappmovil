import React from 'react';
import {Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {styles} from '../styles/styles';
import {useSelector} from 'react-redux';

export const InfoGrupoForza = () => {
  const cedula = useSelector(
    state => state.muestreoMeristemo.cedulaSeleccionada,
  );
  return (
    <View style={styles.viewSubSeleccionar}>
      {cedula ? (
        cedula.muestras ? (
          <>
            <View style={{justifyContent: 'center'}}>
              <Icon name="checkmark-outline" size={35} color='black'/>
            </View>
            <Text style={{...styles.subTitle, fontSize: 19}}>
              Grupo:{cedula.codigo}{' '}
            </Text>

            <Text style={{...styles.subTitle, fontSize: 19}}>
              Muestras:{cedula.muestras}
            </Text>
          </>
        ) : (
          <>
            <View style={{justifyContent: 'center'}}>
              <Icon name="checkmark-outline" size={35} />
            </View>
            <Text style={{...styles.subTitle, fontSize: 19}}>
              Grupo:{cedula.codigo}{' '}
            </Text>
          </>
        )
      ) : (
        <>
          <View style={{justifyContent: 'center'}}>
            <Icon name="close-outline" size={45} color='black'/>
          </View>
          <Text style={{...styles.subTitle, fontSize: 19}}>
            {' '}
            No hay un bloque seleccionado
          </Text>
        </>
      )}
    </View>
  );
};
