import React from 'react';
import {Text, View} from 'react-native';
import {RadioButton} from 'react-native-paper';
import {styles} from '../../styles/styles';

export const RadioButtonPlanta = ({setDato, dato}) => {
  return (
    <View style={styles.viewCuerpoClima}>
       <RadioButton
        value="second"
        status={dato === 'Si' ? 'checked' : 'unchecked'}
        onPress={() => setDato('Si')}
      />
      <View style={{justifyContent: 'center'}}>
      <Text style={{fontSize:24, fontWeight:'bold',color:'black'}}>Si</Text>
      </View>
       <RadioButton
        value="second"
        status={dato === 'No' ? 'checked' : 'unchecked'}
        onPress={() => setDato('No')}
      />
      <View style={{justifyContent: 'center', marginLeft:0}}>
       <Text style={{fontSize:24, fontWeight:'bold',color:'black'}}>No</Text>
      </View>
      
    
    </View>
  );
};
