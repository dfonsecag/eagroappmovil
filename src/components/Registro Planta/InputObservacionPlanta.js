import React from 'react';
import {TextInput} from 'react-native';
import { styles } from '../../styles/styles';

export const InputObservacionPlanta = ({observacion, setObservacion}) => {
  return (
    <TextInput
      placeholder="Observación"
      multiline
      numberOfLines={1}
      underlineColorAndroid="black"
      placeholderTextColor="#6e6e6e"
      maxLength={20}  // Limita a 20 caracteres
      style={{...styles.inputObservacion, fontSize: 33}}
      selectionColor="black"
      onChangeText={value => {
        let valor = value.replace(/\n|\r/g, " ");;
        setObservacion(valor);
      }}
      value={observacion}
      autoCapitalize="none"
      autoCorrect={false}
    />
  );
};
