import React from 'react';
import {TextInput,  View} from 'react-native';
import { styles } from '../../styles/styles';


export const InputPlanta = ({setInput,value,placeholder }) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        height: 55,
        marginLeft: 50,
        width: '100%',
        justifyContent: 'flex-start',
      }}>
      <TextInput
        placeholder={placeholder}
        keyboardType="phone-pad"
        underlineColorAndroid="black"
        placeholderTextColor="#6e6e6e"
        style={{...styles.inputHora, fontSize: 33}}
        selectionColor="black"
        onChangeText={value => {
          let numero = value.replace(/[^0-9.]/g, '');
          setInput(numero);
        }}
        value={value}
        autoCapitalize="none"
        autoCorrect={false}
      />
    </View>
  );
};
