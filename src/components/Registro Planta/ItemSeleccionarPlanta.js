import React from 'react';
import {Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {styles} from '../../styles/styles';

export const ItemSeleccionarPlanta = ({titulo, icono}) => {
  return (
    <View style={styles.viewSubSeleccionar}>
      <View style={{justifyContent: 'center'}}>
        <Icon name={icono} size={35} color="black"/>
      </View>
      <Text style={{...styles.subTitle,fontWeight:'500',fontSize:28}}> {titulo} </Text>
    </View>
  );
};
