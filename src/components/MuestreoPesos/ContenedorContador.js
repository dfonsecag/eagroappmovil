import React from 'react'
import { Text, View } from 'react-native'
import { styles } from '../../styles/styles'
import Icon from 'react-native-vector-icons/Ionicons';

export const ContenedorContador = ({title}) => {
    return (
        <View
          style={{...styles.viewSubSeleccionar, marginTop: 5, marginLeft: 5}}>
          <View style={{justifyContent: 'center'}}>
            <Icon name="barbell-outline" size={35} color='black'/>
          </View>
          <Text style={{...styles.subTitle, fontSize: 20}}> {title} </Text>
        </View>
    )
}
