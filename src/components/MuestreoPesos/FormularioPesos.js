import React from 'react'
import { TextInput } from 'react-native';
import { styles } from '../../styles/styles';

export const FormularioPesos = ({calibre, setCalibre}) => {
    return (
        <TextInput
            placeholder="PESO"
            keyboardType="phone-pad"
            underlineColorAndroid="black"
            placeholderTextColor="#6e6e6e"
            style={{
              ...styles.inputHora,
              fontSize: 28,
              fontWeight: 'bold',
              marginLeft: 55,
              width: 100,
            }}
            selectionColor="black"
            onChangeText={value => {
              let numero = value.replace(/[^0-9]/g, '');
              setCalibre(numero);
            }}
            value={calibre}
            autoCapitalize="none"
            autoCorrect={false}
            maxLength={4}
          />
    )
}
