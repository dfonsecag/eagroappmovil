import React from 'react';
import {Row, Table} from 'react-native-table-component';
import {ScrollView, StyleSheet, Text, View} from 'react-native';

export const Tabla = ({ data, columns, headers }) => {
  return (
    <View style={tabla.container}>
           <ScrollView horizontal={true}>
             <View>
               <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}}>
                 <Row
                   data={headers}
                   widthArr={columns}
                   style={tabla.header}
                   textStyle={tabla.textheader}
                 />
               </Table>
               <ScrollView style={tabla.dataWrapper}>
                 <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}}>
                   {data.map((rowData, index) => (
                     <Row
                       key={index}
                       data={rowData}
                       onPress={() => console.log(rowData[1])}
                       widthArr={columns}
                       // style={[tabla.row, (rowData[9] > 5 && rowData[9] < 21 ) ? ({backgroundColor: '#FDFD96'}) : (rowData[9] > 20 ) ? ({backgroundColor: '#ff6961'}) : ({backgroundColor: '#8BC34A'})]}
                       style={[tabla.row, index%2 && {backgroundColor: '#E0E0E0'}]}
                       textStyle={tabla.text}
                     />
                   ))}
                 </Table>
               </ScrollView>
             </View>
           </ScrollView>
         </View>
  );
};

const tabla = StyleSheet.create({
  container: {padding: 5, paddingTop: 8, paddingBottom:0,justifyContent: 'center',  alignItems: 'center'},
  header: {height: 50, backgroundColor: '#424242'},
  textheader: {textAlign: 'center', fontWeight: 'bold', color:'white'},
  text: {textAlign: 'center', fontWeight: 'normal',fontSize:15 ,color:'black'},
  dataWrapper: {marginTop: -1},
  row: {height: 60, backgroundColor:'#FAFAFA'},
});
