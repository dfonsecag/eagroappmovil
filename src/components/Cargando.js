import React from 'react';
import { ActivityIndicator, Text, View} from 'react-native';

export const Cargando = () => {
  return (
    <View
          style={{ height: 100,
            marginTop:20,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator size={40} color="black" />
          <Text style={{fontSize:18,fontWeight:'600',marginTop:10}}>{`Procesando ...`}</Text>
        </View>
  );
};
