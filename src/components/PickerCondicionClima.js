import React from 'react';
import {Picker} from '@react-native-picker/picker';


export const PickerCondicionClima = ({clima, setClima}) => {
  return (
    <Picker
      selectedValue={clima}
     
      onValueChange={( itemValue ) => setClima(itemValue)}>     
       <Picker.Item label={'--Seleccione el clima --'} value={''} key={0} />
       <Picker.Item label={'SOLEADO'} value={'SOLEADO'} key={1} />
       <Picker.Item label={'LLUVIOSO'} value={'LLUVIOSO'} key={2} />
       <Picker.Item label={'NUBLADO'} value={'NUBLADO'} key={3} />
    </Picker>
  );
};
