import React from 'react'
import { styles } from '../styles/styles'
import Icon from 'react-native-vector-icons/Ionicons';
import { Text, View , TouchableOpacity} from 'react-native';

export const ViewMuestrasRestantes = ({incrementarConteo, validarConteo, contadorRestante}) => {
    return (
        <View
        style={{
          ...styles.viewSubSeleccionar,
          height: 80,
          justifyContent: 'center',
          backgroundColor: '#0a0a0a',
        }}>
         <TouchableOpacity disabled={false} onPress={ incrementarConteo }>
        <View style={{justifyContent: 'center', marginTop:12, marginLeft:8}}>
          <Icon name="remove-outline" color='#FFF' size={55} />
        </View>
        </TouchableOpacity>
        <Text style={{...styles.subTitle, color: '#FFF', fontSize: 25}}>
          {contadorRestante}  plantas restantes
        </Text>
        <TouchableOpacity disabled={false} onPress={ validarConteo }>
        <View style={{justifyContent: 'center', marginTop:12, marginLeft:8}}>
          <Icon name="add-outline" color='#FFF' size={55} />
        </View>
        </TouchableOpacity>
      </View>
    )
}
