import React from 'react';
import {Picker} from '@react-native-picker/picker';



export const PickerSelect = ({seleccionarCedula, setSeleccionarCedula, cedulas}) => {

   
  return (
    <Picker
      selectedValue={seleccionarCedula}
     
      onValueChange={( itemValue ) => setSeleccionarCedula(itemValue)}>     
       <Picker.Item style={{fontSize:22, fontWeight:'bold',color:'black'}}  label={'   -- Seleccione el bloque --'} value={0} key={0} />
      {
        (cedulas) ?
        cedulas.map(c => 
          (<Picker.Item style={{fontSize:19,color:'black'}} label={`  **Lote: ${c.numerolote}   -   **Bloque:${Number.parseFloat(c.numerobloque).toFixed(0)}`} value={c.id} key={c.id} />)
          ) :
        null
      }
    </Picker>
  );
};
