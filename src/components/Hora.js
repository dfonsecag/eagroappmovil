import React, {useState} from 'react';
import {Button, Text, View, TouchableOpacity} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import {styles} from '../styles/styles';
import Icon from 'react-native-vector-icons/Ionicons';
import {ItemSeparator} from './ItemSeparator';

export const Hora = ({horainicio, setHoraInicio, horafinal, setHoraFinal}) => {
  const [date, setDate] = useState(new Date(1598051730000));
  const [show, setShow] = useState(false);
  const [show2, setShow2] = useState(false);
  const onChange = (event, selectedDate) => {
    setShow(false);
    if (selectedDate !== undefined) {
      let hora = selectedDate.getHours();
      if (hora.toString().length === 1) {
        hora = `0${hora}`;
      }
      let minutos = selectedDate.getMinutes();
      if (minutos.toString().length === 1) {
        minutos = `0${minutos}`;
      }
      setHoraInicio(`${hora}:${minutos}`);
    }
  };

  const showTimepicker = () => {
    setShow(true);
  };

  const onChangeTime2 = (event, selectedDate) => {
    setShow2(false);
    if (selectedDate !== undefined) {
      let hora = selectedDate.getHours();
      if (hora.toString().length === 1) {
        hora = `0${hora}`;
      }
      let minutos = selectedDate.getMinutes();
      if (minutos.toString().length === 1) {
        minutos = `0${minutos}`;
      }
      setHoraFinal(`${hora}:${minutos}`);
    }
  };

  const showTimepicker2 = () => {
    setShow2(true);
  };

  return (
    <View>
      <View style={styles.viewSubSeleccionar}>
        <View style={{justifyContent: 'center'}}>
          <Icon name="hourglass-outline" size={35} />
        </View>
        <Text style={styles.subTitle}> Hora Inicial: </Text>

        <TouchableOpacity activeOpacity={0.6} onPress={showTimepicker}>
          <View style={{justifyContent: 'center'}}>
            <Icon
              style={{marginTop: 10, marginLeft: 20}}
              name="time-outline"
              size={32}
            />
          </View>
        </TouchableOpacity>

        <Text style={styles.subTitle}> {horainicio} </Text>
      </View>

      <ItemSeparator />

      <View style={styles.viewSubSeleccionar}>
        <View style={{justifyContent: 'center'}}>
          <Icon name="hourglass-outline" size={35} />
        </View>
        <Text style={styles.subTitle}> Hora Final: </Text>

        <TouchableOpacity activeOpacity={0.6} onPress={showTimepicker2}>
          <View style={{justifyContent: 'center'}}>
            <Icon
              style={{marginTop: 10, marginLeft: 30}}
              name="time-outline"
              size={32}
            />
          </View>
        </TouchableOpacity>

        <Text style={styles.subTitle}> {horafinal} </Text>
      </View>

      {show && (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode="time"
          display="default"
          onChange={onChange}
        />
      )}

      {show2 && (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode="time"
          display="default"
          onChange={onChangeTime2}
        />
      )}
    </View>
  );
};
