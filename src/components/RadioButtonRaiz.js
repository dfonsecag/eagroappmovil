import React from 'react';
import {Text, View} from 'react-native';
import {RadioButton} from 'react-native-paper';
import {styles} from '../styles/styles';
import Icon from 'react-native-vector-icons/Ionicons';

export const RadioButtonRaiz = ({setDato, dato}) => {
  return (
    <View style={styles.viewCuerpoClima}>
       <RadioButton
        value="second"
        status={dato === 'Nada' ? 'checked' : 'unchecked'}
        onPress={() => setDato('Nada')}
      />
     <View style={{justifyContent: 'center'}}>
       <Text style={{fontSize:16, fontWeight:'bold',color:'black'}}>Nada</Text>
      </View>
       <RadioButton
        value="second"
        status={dato === 'Poca' ? 'checked' : 'unchecked'}
        onPress={() => setDato('Poca')}
      />
     <View style={{justifyContent: 'center'}}>
       <Text style={{fontSize:16, fontWeight:'bold',color:'black'}}>Poca</Text>
      </View>
      <RadioButton
        value="second"
        status={dato === 'Regular' ? 'checked' : 'unchecked'}
        onPress={() => setDato('Regular')}
      />
      <View style={{justifyContent: 'center'}}>
      <Text style={{fontSize:16, fontWeight:'bold',color:'black'}}>Regular</Text>
      </View>
      <RadioButton
        value="second"
        status={dato === 'Mucha' ? 'checked' : 'unchecked'}
        onPress={() => setDato('Mucha')}
      />
      <View style={{justifyContent: 'center'}}>
      <Text style={{fontSize:16, fontWeight:'bold',color:'black'}}>Mucha</Text>
      </View>
    
    </View>
  );
};
