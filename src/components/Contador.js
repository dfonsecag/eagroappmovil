import React from 'react';
import {Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import {styles} from '../styles/styles';

export const Contador = ({contador, setContador, contadorRestante}) => {
  const verificarContador = () => {    
    if(contador !== 0){
      setContador(contador - 1);
    }
  
  }
  const incrementar = () => {
     setContador(contador + 1);    
  }

  return (
    <View style={styles.viewCuerpoContador}>
     
      {
        (contadorRestante === 0)?
        (
          <TouchableOpacity disabled={true} onPress={() => verificarContador()}>
          <View style={{justifyContent: 'center', alignItems:'center'}}>
            <Icon name="remove-outline" size={50} color='#8BC34A' />
          </View>
        </TouchableOpacity>
        ):
        (
          <TouchableOpacity disabled={false} onPress={() => verificarContador()}>
          <View style={{justifyContent: 'center'}}>
            <Icon name="remove-outline" size={50} color='black'/>
          </View>
        </TouchableOpacity>
        )
      }

      <Text style={styles.labelContador}> {contador} </Text>
      {
        (contadorRestante === 0)?
        (
          <TouchableOpacity disabled={true} onPress={ incrementar }>
          <View style={{justifyContent: 'center', alignItems:'center'}}>
            <Icon name="add-outline" size={50} color='#8BC34A'/>
          </View>
        </TouchableOpacity>
        ):
        (
          <TouchableOpacity disabled={false} onPress={ incrementar }>
          <View style={{justifyContent: 'center', alignItems:'center'}}>
            <Icon name="add-outline" size={50} color='#8BC34A'/>
          </View>
        </TouchableOpacity>
        )
      }
      
    </View>
  );
};
