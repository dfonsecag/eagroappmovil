import React from 'react';
import DateTimePickerModal from "react-native-modal-datetime-picker";

export const Fecha = ({isVisible, setIsVisible, fecha, setFecha}) => {
  

  const confirmarFecha = date => {
    setFecha(date.toISOString().slice(0,10));
    cancelar();
  };
  const cancelar = () => {
    setIsVisible(false);
  };
  return (
    <>
      
      <DateTimePickerModal
        isVisible={isVisible}
        mode="date"
        value={fecha}
        onConfirm={confirmarFecha}
        onCancel={cancelar}
      />
    </>
  );
};
