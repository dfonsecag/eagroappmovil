import React from 'react';
import {Picker} from '@react-native-picker/picker';



export const PickerSelectMaquinaria = ({seleccionarCedula, setSeleccionarCedula, cedulas, tipo}) => {

   
  return (
    <Picker
      selectedValue={seleccionarCedula}
     
      onValueChange={( itemValue ) => setSeleccionarCedula(itemValue)}>     
       <Picker.Item style={{fontSize:21, fontWeight:'bold'}}  label={`-- Seleccione ${tipo} --`} value={0} key={0} />
      {
        (cedulas) ? 
        (cedulas.map(c => {
          let descripcion = c.descripcion;
           if(c.descripcion.substring(0,7) === 'Tractor'){           
             descripcion = c.descripcion.substring(16, c.descripcion.length)          
           }           
           return (  <Picker.Item style={{fontSize:19}} label={`  ${descripcion}   `} value={c.maquinaid} key={c.maquinaid} />)
        }
        
        )):
        null
      }
    </Picker>
  );
};
