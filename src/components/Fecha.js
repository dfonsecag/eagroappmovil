import React from 'react';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Icon from 'react-native-vector-icons/Ionicons';
import { styles } from '../styles/styles';
import { Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native';

export const Fecha = ({isVisible, setIsVisible, fecha, setFecha}) => {
  

  const confirmarFecha = date => {
    setFecha(date.toISOString().slice(0,10));
    cancelar();
  };
  const cancelar = () => {
    setIsVisible(false);
  };
  return (
    <>
      
      <DateTimePickerModal
        isVisible={isVisible}
        mode="date"
        value={fecha}
        onConfirm={confirmarFecha}
        onCancel={cancelar}
      />

     <View style={{...styles.viewSubSeleccionar, marginTop:5}}>
          {/* <Text style={{...styles.subTitle,color:'black', fontWeight: '500',fontSize:25}}> Fecha:          </Text> */}

          <TouchableOpacity activeOpacity={0.6} onPress={() => setIsVisible(true)}>
            <View style={{justifyContent: 'center'}}>
              <Icon
                style={{marginTop: 10, marginLeft: 20}}
                name="today"
                size={32}
                color="#6e6e6e"
              />
            </View>
          </TouchableOpacity>

          <Text style={{...styles.subTitle,color:'black',fontSize:25}}> {fecha} </Text>
        </View>  
    </>
  );
};