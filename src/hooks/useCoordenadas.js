import React, { useState } from 'react';
import GetLocation from 'react-native-get-location'

export const useCoordenadas = () => {

    const [latitud, setLatitud] = useState('');
    const [longitud, setLongitud] = useState('');
    
    const obtenerUbicacion = async() => {
        try {
            const gps = await GetLocation.getCurrentPosition({
              enableHighAccuracy: true,
              timeout: 15000,
            });
            
            setLatitud(gps.latitude);
            setLongitud(gps.longitude);
            
          } catch (error) {
            console.log(error);
          }
    }

    return {latitud,longitud, obtenerUbicacion }
}
