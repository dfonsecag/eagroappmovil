
import {
  ENVIAR_MUESTREO_SEMILLA,
  ENVIAR_MUESTREO_SEMILLA_EXITO,
  ENVIAR_MUESTREO_SEMILLA_ERROR,
  CARGAR_MUESTREO_SEMILLA,
  CARGAR_MUESTREO_SEMILLA_EXITO,
  CARGAR_BLOQUES,
  SETEAR_BLOQUES,
  AGREGAR_EMPLEADO_EXITO,
  ELIMINAR_EMPLEADO_EXITO,
  SETEAR_EMPLEADO_EXITO,
} from '../types';

// cada reducer tiene su propio state
const initialState = {
  error: false,
  loading: false,
  envioExito: false,
  lotes:[],
  rangos:[],
  tamaniosemillas:[],
  contratistas: [],
  actividades: [],
  bloques: [],
  empleadosSeleccionados:[]
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CARGAR_MUESTREO_SEMILLA:
    case ENVIAR_MUESTREO_SEMILLA:
      return {
        ...state,
        error: false,
        loading: true,
        envioExito: false,
      };
    case ENVIAR_MUESTREO_SEMILLA_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        envioExito: true
      };
    case CARGAR_MUESTREO_SEMILLA_EXITO:
      return {
        ...state,
        error: false,
        loading:false,
        lotes: action.payload.lotes,
        rangos: action.payload.rangos,
        contratistas: action.payload.contratistas,
        tamaniosemillas: action.payload.tamaniosemillas,
        actividades:action.payload.actividades
      };   
    case AGREGAR_EMPLEADO_EXITO:
        return {
          ...state,
          empleadosSeleccionados: [...state.empleadosSeleccionados,action.payload],
          contratistas: state.contratistas.filter(emp => emp.id !== action.payload.id),
        };
    case ELIMINAR_EMPLEADO_EXITO:
          return{
            ...state,
            empleadosSeleccionados: state.empleadosSeleccionados.filter(emp => emp.id !== action.payload.id),
            contratistas: [...state.contratistas, action.payload],
          } 
    case SETEAR_EMPLEADO_EXITO:
          return{
              ...state,
              empleadosSeleccionados: []
            } 
    case CARGAR_BLOQUES:
        return {
          ...state,
          error: false,
          loading:false,
          bloques: action.payload,
        };   
    case SETEAR_BLOQUES:
          return {
            ...state,
            error: false,
            loading:false,
            bloques: [],
          };     
    case ENVIAR_MUESTREO_SEMILLA_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        envioExito: false,
      };
    default:
      return state;
  }
}
