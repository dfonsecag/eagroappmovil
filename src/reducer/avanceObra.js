import {
  CARGAR_AVANCE_OBRA_ERROR,
  CARGAR_CEDULAS_OBRA,
  CARGAR_CEDULAS_OBRA_EXITO,
  CARGAR_IMPLEMENTOS_EXITO,
  CARGAR_MAQUINARIA_EXITO,
  CARGAR_BOQUILLAS_EXITO,
  ENVIAR_AVANCE_OBRA,
  ENVIAR_AVANCE_OBRA_ERROR,
  ENVIAR_AVANCE_OBRA_EXITO,
  CEDULA_AVANCE_OBRA_SELECCIONADA,
  CARGAR_CHOFERES_EXITO,
  CEDULA_AVANCES_OBRA,
} from '../types';
// cada reducer tiene su propio state
const initialState = {
  error: false,
  loading: false,
  envioExito: false,
  formulario: [],
  cedulasPendientes: [],
  maquinaria: [],
  implementos: [],
  boquillas: [],
  choferes:[],
  cedulaSeleccionada: {},
  avances:[]
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CARGAR_CEDULAS_OBRA:
    case ENVIAR_AVANCE_OBRA:
      return {
        ...state,
        error: false,
        loading: true,
        envioExito: false,
      };
    case ENVIAR_AVANCE_OBRA_EXITO:
      return {
        ...state,
        error: false,
        envioExito: true,
        loading: false,
        avances:[],
        formulario: action.payload,
      };
    case CARGAR_CEDULAS_OBRA_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        envioExito: false,
        cedulasPendientes: action.payload,
      };
    case CARGAR_MAQUINARIA_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        envioExito: false,
        maquinaria: action.payload,
      };
    case CARGAR_BOQUILLAS_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        envioExito: false,
        boquillas: action.payload,
      };
    case CARGAR_CHOFERES_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        envioExito: false,
        choferes: action.payload,
      }  
    case CARGAR_IMPLEMENTOS_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        envioExito: false,
        implementos: action.payload,
      };
    case CARGAR_AVANCE_OBRA_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        envioExito: false,
        cedulasPendientes: action.payload,
      };
    case ENVIAR_AVANCE_OBRA_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        envioExito: false,
      };
    case CEDULA_AVANCE_OBRA_SELECCIONADA:
      return {
        ...state,
        loading: false,
        cedulaSeleccionada: action.payload,
      };
    case CEDULA_AVANCES_OBRA:
        return {
          ...state,
          loading: false,
          avances: action.payload,
        };
    default:
      return state;
  }
}
