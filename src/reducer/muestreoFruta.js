
import {
  ENVIAR_MUESTREO_FRUTA,
  ENVIAR_MUESTREO_FRUTA_EXITO,
  ENVIAR_MUESTREO_FRUTA_ERROR,
  CARGAR_MUESTREO_FRUTA,
  CARGAR_MUESTREO_FRUTA_EXITO,
  CARGAR_MUESTREO_FRUTA_ERROR,
} from '../types';

// cada reducer tiene su propio state
const initialState = {
  error: false,
  loading: false,
  envioExito: false,
  cedulasPendientes:[]
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CARGAR_MUESTREO_FRUTA:
    case ENVIAR_MUESTREO_FRUTA:
      return {
        ...state,
        error: false,
        loading: true,
        envioExito: false,
      };
    case ENVIAR_MUESTREO_FRUTA_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        envioExito: true
      };
    case CARGAR_MUESTREO_FRUTA_EXITO:
      return {
        ...state,
        error: false,
        loading:false,
        envioExito: false,
        cedulasPendientes: action.payload,
      };
    case CARGAR_MUESTREO_FRUTA_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        envioExito: false,
        cedulasPendientes: action.payload
      };
    case ENVIAR_MUESTREO_FRUTA_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        envioExito: false,
      };
    default:
      return state;
  }
}
