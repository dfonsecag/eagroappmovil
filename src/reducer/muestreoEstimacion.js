import {
  CARGAR_BLOQUES_ESTIMACION,
  CARGAR_GRUPOS_ESTIMACION_ERROR,
  CARGAR_GRUPOS_ESTIMACION_EXITO,
  CARGAR_MUESTREO_ESTIMACION,
  CARGAR_MUESTREO_ESTIMACION_ERROR,
  CARGAR_MUESTREO_ESTIMACION_EXITO,
  ENVIAR_MUESTREO_ESTIMACION,
  ENVIAR_MUESTREO_ESTIMACION_ERROR,
  ENVIAR_MUESTREO_ESTIMACION_EXITO,
  SETEAR_BLOQUES_ESTIMACION,
} from '../types';

// cada reducer tiene su propio state
const initialState = {
  error: false,
  loading: false,
  envioExito: false,
  cedulasPendientes:[],
  grupos:[],
  bloques:[]
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CARGAR_MUESTREO_ESTIMACION:
    case ENVIAR_MUESTREO_ESTIMACION:
      return {
        ...state,
        error: false,
        loading: true,
        envioExito: false,
      };
    case ENVIAR_MUESTREO_ESTIMACION_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        envioExito: true,
      };
    case CARGAR_MUESTREO_ESTIMACION_EXITO:
      return {
        ...state,
        error: false,
        loading:false,
        envioExito: false,
        cedulasPendientes: action.payload,
      };
    case CARGAR_BLOQUES_ESTIMACION:
      return {
        ...state,
        error: false,
        loading:false,
        bloques: action.payload,
      }
    case CARGAR_MUESTREO_ESTIMACION_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        envioExito: false,
        cedulasPendientes: action.payload
      };
      case CARGAR_GRUPOS_ESTIMACION_EXITO:
      case CARGAR_GRUPOS_ESTIMACION_ERROR:
        return {
          ...state,
          error: true,
          loading: false,
          envioExito: false,
          grupos: action.payload
        };
    case ENVIAR_MUESTREO_ESTIMACION_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        envioExito: false,
      };
    case SETEAR_BLOQUES_ESTIMACION:
      return {
        ...state,
        bloques: []
      }
    default:
      return state;
  }
}
