import {
  OBTENER_REGISTROPHCLORO,
  OBTENER_REGISTROPHCLORO_EXITO,
  OBTENER_REGISTROPHCLORO_ERROR,
  ENVIAR_FORMULARIOPLANTA,
  ENVIAR_FORMULARIOPLANTA_EXITO,
  ENVIAR_FORMULARIOPLANTA_ERROR,
} from '../types';

// cada reducer tiene su propio state
const initialState = {
  error: false,
  loading: false,
  envioExito: false,
  registoPhCloro:[]
};

export default function (state = initialState, action) {
  switch (action.type) {
    case OBTENER_REGISTROPHCLORO:
      return {
        ...state,
        error: false,
        loading: true,
        registoPhCloro:[]
      };
      case OBTENER_REGISTROPHCLORO_EXITO:
        return {
          ...state,
          error: false,
          loading: false,
          registoPhCloro:action.payload
        };
        case OBTENER_REGISTROPHCLORO_ERROR:
          return {
            ...state,
            error: true,
            loading: false,
            registoPhCloro:[]
          };
         
          case ENVIAR_FORMULARIOPLANTA:
            return {
              ...state,
              error: false,
              loading: true,
              envioExito:false
            };
            case ENVIAR_FORMULARIOPLANTA_EXITO:
            return {
              ...state,
              error: false,
              loading: false,
              envioExito:true
            };
            case  ENVIAR_FORMULARIOPLANTA_ERROR:
              return {
                ...state,
                error: true,
                loading: false,
                envioExito:false
              };            
   
    default:
      return state;
  }
}
