import {
  CARGAR_BLOQUES_EXITO,
  CARGAR_CEDULA,
  CARGAR_CEDULA_ERROR,
  CARGAR_CEDULA_EXITO,
  SETEAR_GRUPOID,
  CARGAR_GRUPOS,
  CARGAR_GRUPOS_ERROR,
  CARGAR_GRUPOS_EXITO,
  CARGAR_HOJA_ALL_VIDA_EXITO,
  CARGAR_HOJA_VIDA,
  CARGAR_HOJA_VIDA_ERROR,
  CARGAR_HOJA_VIDA_EXITO,
  CARGAR_AVANCE_EXITO,
} from '../types';

// cada reducer tiene su propio state
const initialState = {
  error: false,
  loading: false,
  grupos: [],
  bloques: '',
  hojaVida: [],
  cedula: [],
  avance:[],
  grupoId: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CARGAR_HOJA_VIDA:
    case CARGAR_GRUPOS:
    case CARGAR_CEDULA:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case CARGAR_GRUPOS_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        grupos: action.payload,
      };
    case CARGAR_HOJA_ALL_VIDA_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
      };
    case SETEAR_GRUPOID:
      return {
        ...state,
        error: false,
        loading: false,
        hojaVida: [],
        bloques: '',
      };
    case CARGAR_HOJA_VIDA_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        hojaVida: action.payload,
      };
    case CARGAR_BLOQUES_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        bloques: action.payload,
      };
    case CARGAR_CEDULA_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        cedula: action.payload,
      };
    case CARGAR_AVANCE_EXITO:
       return {
        ...state,
        error: false,
        loading: false,
        avance: action.payload,
      };
    case CARGAR_HOJA_VIDA_ERROR:
    case CARGAR_GRUPOS_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        grupos: action.payload,
      };
    case CARGAR_CEDULA_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
      };
    default:
      return state;
  }
}
