import {
  OBTENER_BLOQUES,
  OBTENER_BLOQUES_EXITO,
  OBTENER_BLOQUES_ERROR,
  OBTENER_LOTES,
  OBTENER_LOTES_EXITO,
  OBTENER_LOTES_ERROR,
  ENVIAR_MUESTREO_RECHAZO_FRUTA,
  ENVIAR_MUESTREO_RECHAZO_FRUTA_EXITO,
  ENVIAR_MUESTREO_RECHAZO_FRUTA_ERROR,
  OBTENER_CALIBRES,
  ENVIAR_MUESTREO_CAJAS,
  ENVIAR_MUESTREO_CAJAS_EXITO,
  ENVIAR_MUESTREO_CAJAS_ERROR,
  OBTENER_CALIBRES_EXITO,
  OBTENER_CALIBRES_ERROR
} from '../types';

// cada reducer tiene su propio state
const initialState = {
  error: false,
  loading: false,
  envioExito: false,
  lotes:[],
  bloques:[],
  calibre:[]
};

export default function (state = initialState, action) {
  switch (action.type) {
    case OBTENER_LOTES:
      return {
        ...state,
        error: false,
        loading: true,
        lotes:[]
      };
      case OBTENER_LOTES_EXITO:
        return {
          ...state,
          error: false,
          loading: false,
          lotes:action.payload
        };
        case OBTENER_LOTES_ERROR:
          return {
            ...state,
            error: true,
            loading: false,
            lotes:[]
          };
          case OBTENER_CALIBRES:
          return {
            ...state,
            error: false,
            loading: true,
            calibres:[]
          };
          case OBTENER_CALIBRES_EXITO:
            return {
              ...state,
              error: false,
              loading: false,
              calibres:action.payload
            };
            case OBTENER_CALIBRES_ERROR:
              return {
                ...state,
                error: true,
                loading: false,
                calibres:[]
              };
    case OBTENER_BLOQUES:
      return {
        ...state,
        error: false,
        loading: true,
        bloques:[]
      };
      case OBTENER_BLOQUES_EXITO:
        return {
          ...state,
          error: false,
          loading: false,
          bloques:action.payload
        };
        case OBTENER_BLOQUES_ERROR:
          return {
            ...state,
            error: true,
            loading: false,
            bloques:[]
          };
          case ENVIAR_MUESTREO_CAJAS:
          case ENVIAR_MUESTREO_RECHAZO_FRUTA:
            return {
              ...state,
              error: false,
              loading: true,
              envioExito:false
            };
            case ENVIAR_MUESTREO_CAJAS_EXITO:
            case ENVIAR_MUESTREO_RECHAZO_FRUTA_EXITO:
            return {
              ...state,
              error: false,
              loading: false,
              envioExito:true
            };
            case  ENVIAR_MUESTREO_CAJAS_ERROR:
            case ENVIAR_MUESTREO_RECHAZO_FRUTA_ERROR:
              return {
                ...state,
                error: true,
                loading: false,
                envioExito:false
              };

            
   
    default:
      return state;
  }
}
