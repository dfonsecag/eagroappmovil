import {
  CARGAR_MUESTREO_FLORACION,
  CARGAR_MUESTREO_FLORACION_ERROR,
  CARGAR_MUESTREO_FLORACION_EXITO,
  ENVIAR_MUESTREO_FLORACION,
  ENVIAR_MUESTREO_FLORACION_ERROR,
  ENVIAR_MUESTREO_FLORACION_EXITO,
} from '../types';

// cada reducer tiene su propio state
const initialState = {
  error: false,
  loading: false,
  envioExito: false,
  cedulasPendientes: []
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CARGAR_MUESTREO_FLORACION:
    case ENVIAR_MUESTREO_FLORACION:
      return {
        ...state,
        error: false,
        loading: true,
        envioExito: false,
      };
    case ENVIAR_MUESTREO_FLORACION_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        envioExito: true,
      };
    case CARGAR_MUESTREO_FLORACION_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        envioExito: false,
        cedulasPendientes: action.payload,
      };
    case CARGAR_MUESTREO_FLORACION_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        envioExito: false,
        cedulasPendientes: action.payload,
      };
   
    case ENVIAR_MUESTREO_FLORACION_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        envioExito: false,
      };
   
    default:
      return state;
  }
}
