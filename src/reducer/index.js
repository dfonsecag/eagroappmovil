import { combineReducers } from "redux";

import muestreoFruta from "./muestreoFruta";
import muestreoMeristemo from "./muestreoMeristemo";
import muestreoRaiz from "./muestreoRaiz";
import muestreoPeso from "./muestreoPeso";
import avanceObra from "./avanceObra";
import hojaVida from "./hojaVida";
import muestreoEstimacion from "./muestreoEstimacion";
import muestreoPremaduracion from "./muestreoPremaduracion";
import muestreoFloracion from "./muestreoFloracion";
import muestreoSemilla from "./muestreoSemilla";
import sincronizacion from "./sincronizacion";
import registrosPlanta from "./registrosPlanta";

import login from "./login";
import muestreoRechazoFruta from "./muestreoRechazoFruta";

export default combineReducers({
  muestreoFruta: muestreoFruta,
  muestreoMeristemo:muestreoMeristemo,
  muestreoRaiz:muestreoRaiz,
  muestreoPeso:muestreoPeso,
  muestreoEstimacion:muestreoEstimacion,
  muestreoPremaduracion:muestreoPremaduracion,
  muestreoFloracion:muestreoFloracion,
  muestreoSemilla,
  avanceObra:avanceObra,
  login:login,
  hojaVida:hojaVida,
  sincronizacion:sincronizacion,
  muestreoRechazoFruta,
  registrosPlanta
});