import {
  EMPEZAR_LOGIN,
  EMPEZAR_LOGIN_EXITO,
  EMPEZAR_LOGIN_ERROR,
  VERIFICAR_LOGIN_EXITO,
  VERIFICAR_LOGIN_ERROR,
  CERRAR_SESION,
} from '../types';

// cada reducer tiene su propio state
const initialState = {
  error: false,
  loading: false,
  login: false,
  status: null,
  checking: true,
  permisos:{}
};

export default function (state = initialState, action) {
  switch (action.type) {
    case EMPEZAR_LOGIN:
      return {
        ...state,
        error: false,
        loading: true,
        status: false,
        permisos:{}
      };
    case VERIFICAR_LOGIN_EXITO:
    case EMPEZAR_LOGIN_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        status: true,
        checking: false,
        permisos: action.payload
      };
    case VERIFICAR_LOGIN_ERROR:
    case EMPEZAR_LOGIN_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        status: false,
        permisos:{}
      };
    case CERRAR_SESION:
      return {
        ...state,
        loading: false,
        status: false,
        permisos:{}
      };
    default:
      return state;
  }
}
