import {
  ENVIAR_MUESTREO_MERISTEMO,
  ENVIAR_MUESTREO_MERISTEMO_EXITO,
  ENVIAR_MUESTREO_MERISTEMO_ERROR,
  CARGAR_MERISTEMOS,
  CARGAR_MERISTEMOS_EXITO,
  CARGAR_MERISTEMOS_ERROR,
  CEDULA_SELECCIONADA,
  SETEAR_CEDULA_SELECCIONADA,
} from '../types';

// cada reducer tiene su propio state
const initialState = {
  error: false,
  loading: false,
  envioExito: false,
  cedulaSeleccionada: {},
  cedulasPendientes: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CARGAR_MERISTEMOS:
    case ENVIAR_MUESTREO_MERISTEMO:
      return {
        ...state,
        error: false,
        loading: true,
        envioExito: false,
      };
    case ENVIAR_MUESTREO_MERISTEMO_EXITO:
      return {
        ...state,
        error: false,
        envioExito: true,
        loading: false,
      };
    case CEDULA_SELECCIONADA:
      return {
        ...state,
        loading: false,
        cedulaSeleccionada: action.payload,
      };
    case SETEAR_CEDULA_SELECCIONADA:
      return {
        ...state,
        loading: false,
        cedulaSeleccionada: null,
      };
    case CARGAR_MERISTEMOS_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        envioExito: false,
        cedulasPendientes: action.payload,
      };
    case CARGAR_MERISTEMOS_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        envioExito: false,
        cedulasPendientes: action.payload,
      };
    case ENVIAR_MUESTREO_MERISTEMO_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        envioExito: false,
      };
    default:
      return state;
  }
}
