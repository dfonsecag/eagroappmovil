import {EMPEZAR_SINCRONIZACION, FINALIZAR_SINCRONIZACION} from '../types';
// cada reducer tiene su propio state
const initialState = {
  loading: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case EMPEZAR_SINCRONIZACION:
      return {
        ...state,
        loading: true,
      };
    case FINALIZAR_SINCRONIZACION:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
}
