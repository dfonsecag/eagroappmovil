import {
  CARGAR_BLOQUES_PREMADURACION,
  CARGAR_GRUPOS_PREMADURACION_ERROR,
  CARGAR_GRUPOS_PREMADURACION_EXITO,
  CARGAR_MUESTREO_PREMADURACION,
  CARGAR_MUESTREO_PREMADURACION_ERROR,
  CARGAR_MUESTREO_PREMADURACION_EXITO,
  ENVIAR_MUESTREO_PREMADURACION,
  ENVIAR_MUESTREO_PREMADURACION_ERROR,
  ENVIAR_MUESTREO_PREMADURACION_EXITO,
  SETEAR_BLOQUES_PREMADURACION,
} from '../types';

// cada reducer tiene su propio state
const initialState = {
  error: false,
  loading: false,
  envioExito: false,
  cedulasPendientes:[],
  grupos:[],
  bloques:[]
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CARGAR_MUESTREO_PREMADURACION:
    case ENVIAR_MUESTREO_PREMADURACION:
      return {
        ...state,
        error: false,
        loading: true,
        envioExito: false,
      };
    case ENVIAR_MUESTREO_PREMADURACION_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        envioExito: true,
      };
    case CARGAR_MUESTREO_PREMADURACION_EXITO:
      return {
        ...state,
        error: false,
        loading:false,
        envioExito: false,
        cedulasPendientes: action.payload,
      };
    case CARGAR_BLOQUES_PREMADURACION:
      return {
        ...state,
        error: false,
        loading:false,
        bloques: action.payload,
      }
    case CARGAR_MUESTREO_PREMADURACION_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        envioExito: false,
        cedulasPendientes: action.payload
      };
      case CARGAR_GRUPOS_PREMADURACION_EXITO:
      case CARGAR_GRUPOS_PREMADURACION_ERROR:
        return {
          ...state,
          error: true,
          loading: false,
          envioExito: false,
          grupos: action.payload
        };
    case ENVIAR_MUESTREO_PREMADURACION_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        envioExito: false,
      };
    case SETEAR_BLOQUES_PREMADURACION:
      return {
        ...state,
        bloques: []
      }
    default:
      return state;
  }
}
