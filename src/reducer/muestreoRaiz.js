
import {
  ENVIAR_MUESTREO_RAIZ,
  ENVIAR_MUESTREO_RAIZ_EXITO,
  ENVIAR_MUESTREO_RAIZ_ERROR,
  CARGAR_MUESTREO_RAIZ,
  CARGAR_MUESTREO_RAIZ_EXITO,
  CARGAR_MUESTREO_RAIZ_ERROR,
} from '../types';

// cada reducer tiene su propio state
const initialState = {
  error: false,
  loading: false,
  envioExito: false,
  cedulasPendientes:[],
  
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CARGAR_MUESTREO_RAIZ:
    case ENVIAR_MUESTREO_RAIZ:
      return {
        ...state,
        error: false,
        loading: true,
        envioExito: false,
      };
    case ENVIAR_MUESTREO_RAIZ_EXITO:
      return {
        ...state,
        error: false,
        loading: false,
        envioExito: true
      };
    case CARGAR_MUESTREO_RAIZ_EXITO:
      return {
        ...state,
        error: false,
        loading:false,
        envioExito: false,
        cedulasPendientes: action.payload,
      };
    case CARGAR_MUESTREO_RAIZ_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        envioExito: false,
        cedulasPendientes: action.payload
      };
    case ENVIAR_MUESTREO_RAIZ_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        envioExito: false,
      };
    default:
      return state;
  }
}
