
import {
  ENVIAR_MUESTREO_PESO,
  ENVIAR_MUESTREO_PESO_EXITO,
  ENVIAR_MUESTREO_PESO_ERROR,
  CARGAR_MUESTREO_PESO,
  CARGAR_MUESTREO_PESO_EXITO,
  CARGAR_MUESTREO_PESO_ERROR,
} from '../types';

// cada reducer tiene su propio state
const initialState = {
  error: false,
  loading: false,
  envioExito: false,
  cedulasPendientes:[]
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CARGAR_MUESTREO_PESO:
    case ENVIAR_MUESTREO_PESO:
      return {
        ...state,
        error: false,
        loading: true,
        envioExito: false,
      };
    case ENVIAR_MUESTREO_PESO_EXITO:
      return {
        ...state,
        error: false,
        envioExito: true,
        loading: false
      };
    case CARGAR_MUESTREO_PESO_EXITO:
      return {
        ...state,
        error: false,
        loading:false,
        envioExito: false,
        cedulasPendientes: action.payload,
      };
    case CARGAR_MUESTREO_PESO_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        envioExito: false,
        cedulasPendientes: action.payload
      };
    case ENVIAR_MUESTREO_PESO_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
        envioExito: false,
      };
    default:
      return state;
  }
}
