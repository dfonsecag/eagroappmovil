import React, {useEffect, useState} from 'react';
import {
  Alert,
  BackHandler,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/Ionicons';
import SweetAlert from 'react-native-sweet-alert';
import {useCoordenadas} from '../hooks/useCoordenadas';
import {
  getCedulaSeleccionadaFloracion,
  setearCedulaSeleccionada,
} from '../actions/muestreoMeristemoAction';
import {BotonEnviar} from '../components/BotonEnviar';
import {ItemContador} from '../components/ItemContador';
import {ViewSubtitle} from '../components/ViewSubtitle';
import {ItemSeleccionar} from '../components/ItemSeleccionar';
import {ItemSeparator} from '../components/ItemSeparator';
import {FechaHora} from '../components/FechaHora';
import {Ubicacion} from '../components/Ubicacion';
import {getFecha, getHora} from '../helpers/FechaHora';
import {styles} from '../styles/styles';
import {ItemSeparatorContador} from '../components/ItemSeparatorContador';
import {ContadorPesos} from '../components/MuestreoPesos/ContadorPesos';
import {Hectareaje} from '../components/Hectareaje';
import {InputObservacion} from '../components/InputObservacion';
import {enviarFormularioFloracionAction} from '../actions/muestreoFloracionAction';
import { PickerSelect } from '../components/PickerSelect';

export const MuestreoFloracionScreen = ({navigation}) => {
  const dispatch = useDispatch();
  //State ocultar View
  const [ocultarViewContadores, setOcultarViewContadores] = useState(false);
  const [ocultarViewObservacion, setOcultarViewObservacion] = useState(true);
  // Contador General del bloque
  const [contadorRestantes, setContadorRestantes] = useState();
  const [contadorBorrador, setContadorBorrador] = useState(0);
  const [cargarStorage, setCargarStorage] = useState(false);
  const [cargando, setCargando] = useState(true);

  const [seleccionarCedula, setSeleccionarCedula] = useState();
  const [hora_inicio, setHora_inicio] = useState('');

  // State Contadores con Corona
  const [r35dias, setr35dias] = useState(0);
  const [r38dias, setr38dias] = useState(0);
  const [r1, setr1] = useState(0);
  const [r2, setr2] = useState(0);
  const [r3, setr3] = useState(0);
  const [r4, setr4] = useState(0);
  const [r5, setr5] = useState(0);
  const [r6, setr6] = useState(0);
  const [r7, setr7] = useState(0);
  const [r8, setr8] = useState(0);
  const [r9, setr9] = useState(0);
  const [r10, setr10] = useState(0);
  const [r11, setr11] = useState(0);
  const [r12, setr12] = useState(0);
  const [r13, setr13] = useState(0);
  const [r14, setr14] = useState(0);
  const [r140dias, setr140dias] = useState(0);
  const [r147dias, setr147dias] = useState(0);
  const [r154dias, setr154dias] = useState(0);
  const [r161dias, setr161dias] = useState(0);
  const [sin_parir, setSinParir] = useState(0);
  
  const [observacion, setObservacion] = useState('');

  // Custom Hook funcionalidades como coordenadas
  const {latitud, longitud, obtenerUbicacion} = useCoordenadas();
  const cedulas = useSelector(
    state => state.muestreoFloracion.cedulasPendientes,
  );
  const envioExito = useSelector(state => state.muestreoFloracion.envioExito);
  const cedula = useSelector(
    state => state.muestreoMeristemo.cedulaSeleccionada,
  );

  const permisos = useSelector(state => state.login.permisos);

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Advertencia', 'Desea salir de la pagina muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'Si',
          onPress: () =>
            navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid}),
        },
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    dispatch(setearCedulaSeleccionada());
    obtenerUbicacion();
    setHora_inicio(getHora());
    setContadorRestantes();
    verificarFormulario();
  }, []);

  useEffect(() => {
    if (seleccionarCedula === 0 || seleccionarCedula === undefined) {
      setContadorRestantes();
    }
    dispatch(getCedulaSeleccionadaFloracion(seleccionarCedula));
  }, [seleccionarCedula]);

  useEffect(() => {
    if (cedula && cargarStorage) {
      setContadorRestantes(cedula.plantasmuestrear - contadorBorrador);
      setCargarStorage(false);
    } else if (cedula) {
      setContadorRestantes(cedula.plantasmuestrear);
      limpiarContadores();
    }
  }, [cedula]);

  useEffect(() => {
    if (envioExito) {
      BorrarFormularioStorage();
    }
  }, [envioExito]);

  const verificarFormulario = async () => {
    const formularioStorage = await AsyncStorage.getItem('formularioFloracion');
    if (formularioStorage) {
      Alert.alert(
        'Advertencia !',
        'Existe un formulario pendiente a completar, desea seguir completando ?',
        [
          {
            text: 'Borrarlo',
            onPress: () => BorrarFormularioStorage(),
            style: 'cancel',
          },
          {text: 'Si, Completar', onPress: () => cargarFormularioStorage()},
        ],
      );
    } else {
      setCargando(false);
    }
  };

  const BorrarFormularioStorage = async () => {
    await AsyncStorage.setItem('formularioFloracion', '');
    setSeleccionarCedula();
    setContadorBorrador(0);
    setContadorRestantes();
    limpiarContadores();
    setCargando(false);
  };

  const guardarStorageFormulario = async () => {
    const data = await obtenerDatosFormulario();
    data.contadorRestantes = contadorRestantes;
    data.contadorBorrador = cedula.plantasmuestrear - contadorRestantes;
    await AsyncStorage.setItem('formularioFloracion', JSON.stringify(data));
    Alert.alert(
      'Datos guardados exitosamente',
      'Desea salir de la pagina muestreo ?',
      [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'Si',
          onPress: () =>
            navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid}),
        },
      ],
    );
  };

  const cargarFormularioStorage = async () => {
    const formularioStorage = await AsyncStorage.getItem(
      'formularioFloracion',
    );
    const formulario = JSON.parse(formularioStorage);
    setCargarStorage(true);
    setSeleccionarCedula(formulario.id);
    // setHora_inicio(formulario.hora_inicio);
    setr35dias(formulario.r35dias);
    setr38dias(formulario.r38dias);
    setr1(formulario.r1);
    setr2(formulario.r2);
    setr3(formulario.r3);
    setr4(formulario.r4);
    setr5(formulario.r5);
    setr6(formulario.r6);
    setr7(formulario.r7);
    setr8(formulario.r8);
    setr9(formulario.r9);
    setr10(formulario.r10);
    setr11(formulario.r11);
    setr12(formulario.r12);
    setr13(formulario.r13);
    setr14(formulario.r14);
    // Cambios
    setr140dias(formulario.r140dias);
    setr147dias(formulario.r147dias);
    setr154dias(formulario.r154dias);
    setr161dias(formulario.r161dias);

    setSinParir(formulario.sin_parir);   
   
    setObservacion(formulario.observacion);
    setContadorRestantes(formulario.contadorRestantes);
    setContadorBorrador(formulario.contadorBorrador);
    setCargando(false);
  };

  const enviar = () => {
    if (seleccionarCedula === 0 || seleccionarCedula === undefined) {
      SweetAlert.showAlertWithOptions({
        title: 'Seleccionar Bloque',
        subTitle: 'Debes Seleccionar el bloque a muestrear',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    }
     else if (contadorRestantes > 0) {
      SweetAlert.showAlertWithOptions({
        title: 'Aun hay muestras pendientes',
        subTitle: 'Debes de completar las muestras del bloque',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    } 
    else {
      Alert.alert('Advertencia', 'Desea enviar el muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'Si', onPress: () => enviarFormulario()},
      ]);
    }
  };

  const obtenerDatosFormulario = async () => {
    await obtenerUbicacion();
    const dataForm = {
      id: seleccionarCedula,
      fmuestreo: `${getFecha()} ${getHora()}`,
      hora_inicio: `${getFecha()} ${hora_inicio}`,
      hora_fin: `${getFecha()} ${getHora()}`,
      latitud,
      longitud,
      estado: true,
      r35dias,
      r38dias,
      r1,
      r2,
      r3,
      r4,
      r5,
      r6,
      r7,
      r8,
      r9,
      r10,
      r11,
      r12,
      r13,
      r14,
      r140dias,
      r147dias,
      r154dias,
      r161dias,
      sin_parir,
      observacion,
    };
    return dataForm;
  };

  const enviarFormulario = async () => {
    await obtenerUbicacion();
    const dataForm = await obtenerDatosFormulario();
    await dispatch(
      enviarFormularioFloracionAction(dataForm, seleccionarCedula),
    );
  };
  const limpiarContadores = async () => {
    setHora_inicio(getHora());
    setr35dias(0);
    setr38dias(0);
    setr1(0);
    setr2(0);
    setr3(0);
    setr4(0);
    setr5(0);
    setr6(0);
    setr7(0);
    setr8(0);
    setr9(0);
    setr10(0);
    setr11(0);
    setr12(0);
    setr13(0);
    setr14(0);
    // Cambios
    setr140dias(0);
    setr147dias(0);
    setr154dias(0)
    setr161dias(0);

    setSinParir(0); 
    setObservacion('');
  };

  return (
    <>
      {cargando ? (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator size={100} color="black" />
        </View>
      ) : (
        <>
          <ScrollView>
            <View style={{borderBottomWidth: 6, opacity: 0.2}}></View>
            <View style={styles.viewSubTitle}>
              <Text style={styles.subTitle}> Datos Básicos </Text>
              <View style={{flex: 1}} />
            </View>           

            <ItemSeleccionar titulo="Cédulas" icono="reorder-four-outline" />
            <PickerSelect
              seleccionarCedula={seleccionarCedula}
              setSeleccionarCedula={setSeleccionarCedula}
              cedulas={cedulas}
            />

            {cedula ? (
              <>
                <ItemSeparator />
                <View style={styles.viewSubSeleccionar}>
                <View style={{justifyContent: 'center'}}>
                  <Icon name="checkmark-outline" size={35} />
                </View>
                <Text style={{...styles.subTitle, fontSize: 22}}>
                  Grupo: {cedula.codigo}{'   '}
                </Text>
                <Text style={{...styles.subTitle, fontSize: 22}}>
                  Has: { parseFloat(cedula.area).toFixed(2)}
                </Text>
                </View>
              </>
            ) : null}
            <ItemSeparator />

            <FechaHora fecha={getFecha()} hora={getHora()} />
            <ItemSeparator />

            <Ubicacion latitud={latitud} longitud={longitud} />
            {cedula ? (
              <>
              <ItemSeparator />
              <View style={{...styles.viewSubSeleccionar, height: 65}}>
                <View style={{justifyContent: 'center'}}>
                  <Icon name="checkmark-outline" size={35} />
                </View>

                <Text style={{...styles.subTitle, fontSize: 25}}>
                  Muestreo en {cedula.punto_muestreo}
                </Text>
              </View>
                <ItemSeparator />
                <View style={{...styles.viewSubSeleccionar, height: 65}}>
                  <View style={{justifyContent: 'center'}}>
                    <Icon name="checkmark-outline" size={35} />
                  </View>
                  <Text style={{...styles.subTitle, fontSize: 25}}>
                    Plantas a muestrear: {cedula.plantasmuestrear}
                  </Text>
                </View>
                <ItemSeparator />
              <View style={{...styles.viewSubSeleccionar, height: 65}}>
                <View style={{justifyContent: 'center'}}>
                  <Icon name="checkmark-outline" size={35} />
                </View>

                <Text style={{...styles.subTitle, fontSize: 25}}>
                  Plantas por punto: {cedula.plantasxpunto}
                </Text>
              </View>
              </>
            ) : null}

            <ViewSubtitle
              title="Días de Floración"
              setEstado={setOcultarViewContadores}
              estado={ocultarViewContadores}
            />
            {!ocultarViewContadores ? (
              <>
               <ItemContador titulo="35 días" />
                <ContadorPesos
                  contador={r35dias}
                  setContador={setr35dias}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="38 días" />
                <ContadorPesos
                  contador={r38dias}
                  setContador={setr38dias}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="42 días" />
                <ContadorPesos
                  contador={r1}
                  setContador={setr1}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="49 días" />
                <ContadorPesos
                  contador={r2}
                  setContador={setr2}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="56 días" />
                <ContadorPesos
                  contador={r3}
                  setContador={setr3}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="63 días" />
                <ContadorPesos
                  contador={r4}
                  setContador={setr4}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="70 días" />
                <ContadorPesos
                  contador={r5}
                  setContador={setr5}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="77 días" />
                <ContadorPesos
                  contador={r6}
                  setContador={setr6}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="84 días" />
                <ContadorPesos
                  contador={r7}
                  setContador={setr7}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="91 días" />
                <ContadorPesos
                  contador={r8}
                  setContador={setr8}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="98 días" />
                <ContadorPesos
                  contador={r9}
                  setContador={setr9}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="105 días" />
                <ContadorPesos
                  contador={r10}
                  setContador={setr10}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="112 días" />
                <ContadorPesos
                  contador={r11}
                  setContador={setr11}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="119 días" />
                <ContadorPesos
                  contador={r12}
                  setContador={setr12}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="126 días" />
                <ContadorPesos
                  contador={r13}
                  setContador={setr13}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="133 días" />
                <ContadorPesos
                  contador={r14}
                  setContador={setr14}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                {/* Cambios */}
                <ItemSeparatorContador />
                <ItemContador titulo="140 días" />
                <ContadorPesos
                  contador={r140dias}
                  setContador={setr140dias}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                 <ItemSeparatorContador />
                <ItemContador titulo="147 días" />
                <ContadorPesos
                  contador={r147dias}
                  setContador={setr147dias}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                 <ItemSeparatorContador />
                <ItemContador titulo="154 días" />
                <ContadorPesos
                  contador={r154dias}
                  setContador={setr154dias}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                 <ItemSeparatorContador />
                <ItemContador titulo="161 días" />
                <ContadorPesos
                  contador={r161dias}
                  setContador={setr161dias}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />



                <ItemSeparatorContador />
                <ItemContador titulo="Sin Parir" />
                <ContadorPesos
                  contador={sin_parir}
                  setContador={setSinParir}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
               
              </>
            ) : null}

           
            <ViewSubtitle
              title="Observaciones"
              setEstado={setOcultarViewObservacion}
              estado={ocultarViewObservacion}
            />
            {!ocultarViewObservacion ? (
              <InputObservacion
                observacion={observacion}
                setObservacion={setObservacion}
              />
            ) : null}

            <BotonEnviar enviarFormulario={enviar} />
            <ItemContador titulo="" />
          </ScrollView>
          <View
            style={{
              ...styles.viewSubSeleccionar,
              height: 80,
              justifyContent: 'center',
              backgroundColor: '#0a0a0a',
            }}>
            {contadorRestantes || contadorRestantes === 0 ? (
              <>
                <TouchableOpacity onPress={() => guardarStorageFormulario()}>
                  <View style={{marginTop: 17}}>
                    <Icon name="save-outline" color="white" size={40} />
                  </View>
                </TouchableOpacity>
                <Text style={{...styles.subTitle, color: '#FFF', fontSize: 27}}>
                  Plantas restantes: {contadorRestantes}
                </Text>
              </>
            ) : (
              <Text style={{...styles.subTitle, color: '#FFF', fontSize: 27}}>
                Seleccione el bloque
              </Text>
            )}
          </View>
        </>
      )}
    </>
  );
};
