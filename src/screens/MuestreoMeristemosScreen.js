import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  View,
  ScrollView,
  BackHandler,
  Alert,
  TextInput,
  Text,
  TouchableOpacity,
} from 'react-native';
import SweetAlert from 'react-native-sweet-alert';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/Ionicons';

import {BotonEnviar} from '../components/BotonEnviar';
import {Contador} from '../components/Contador';
import {FechaHora} from '../components/FechaHora';
import {ItemContador} from '../components/ItemContador';
import {ItemSeleccionar} from '../components/ItemSeleccionar';
import {ItemSeparator} from '../components/ItemSeparator';
import {PickerSelect} from '../components/PickerSelect';
import {Ubicacion} from '../components/Ubicacion';
import {ViewSubtitle} from '../components/ViewSubtitle';
import {useCoordenadas} from '../hooks/useCoordenadas';
import {
  enviarFormularioAction,
  getCedulaSeleccionada,
  getCedulaSeleccionadaExito,
  setearCedulaSeleccionada,
} from '../actions/muestreoMeristemoAction';
import {ItemSeparatorContador} from '../components/ItemSeparatorContador';
import {InputObservacion} from '../components/InputObservacion';
import {getFecha, getHora} from '../helpers/FechaHora';
import {InfoGrupoForza} from '../components/InfoGrupoForza';
import {styles} from '../styles/styles';
import {Hectareaje} from '../components/Hectareaje';
import { RadioButtonRaiz } from '../components/RadioButtonRaiz';
export const MuestreoMeristemosScreen = ({navigation}) => {
  // Dispatch
  const dispatch = useDispatch();
  const [editar, setEditar] = useState(false);
  const [cedulasEditar, setcedulasEditar] = useState([]);
  const [use, setUse] = useState(false);
  //State ocultar View
  const [ocultarViewContadores, setOcultarViewContadores] = useState(false);
  const [ocultarViewObservacion, setOcultarViewObservacion] = useState(true);
  const [ocultarViewMaleza, setOcultarViewMaleza] = useState(true);
  // State de los contadores
  const [plantasEvaluadas, setPlantasEvaluadas] = useState(0);
  const [plantasInducidas, setPlantasInducidas] = useState(0);
  const [plantasSinInducir, setPlantasSinInducir] = useState(0);
  const [paricionNatural, setParicionNatural] = useState(0);
  const [seleccionarCedula, setSeleccionarCedula] = useState();
  const [observacion, setObservacion] = useState();

     //Malezas
     const [caminadora, setCaminadora] = useState('Nada');
     const [pata_de_gallina, setPata_de_gallina] = useState('Nada');
     const [cizana, setCizana] = useState('Nada');
     const [pepinillo, setPepinillo] = useState('Nada');
     const [cyperus, setCyperus] = useState('Nada');
     const [golondrina, setGolondrina] = useState('Nada');
     const [bledo, setBledo] = useState('Nada');
     const [digitaria, setDigitaria] = useState('Nada');
     const [mombaza, setMombaza] = useState('Nada');
     const [tomatillo, setTomatillo] = useState('Nada');

  // Custom Hook funcionalidades como coordenadas
  const {latitud, longitud, obtenerUbicacion} = useCoordenadas();
  // Selector Redux
  const envioExito = useSelector(state => state.muestreoMeristemo.envioExito);

  const cedulas = useSelector(
    state => state.muestreoMeristemo.cedulasPendientes,
  );
  const cedula = useSelector(
    state => state.muestreoMeristemo.cedulaSeleccionada,
  );

  const permisos = useSelector(state => state.login.permisos);

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Advertencia', 'Desea salir de la pagina muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'Si',
          onPress: () =>
            navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid}),
        },
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    dispatch(setearCedulaSeleccionada());
    obtenerUbicacion();
  }, []);

  useEffect(() => {
    if (editar) {
      obtenerCedulaEditar(seleccionarCedula);
    } else {
      dispatch(getCedulaSeleccionada(seleccionarCedula));
    }
  }, [seleccionarCedula]);

  // Para editar
  useEffect(() => {
    if (editar) {
      dispatch(setearCedulaSeleccionada());
      obtenerMuestreados();
    }
  }, [editar]);

  const obtenerCedulaEditar = async id => {
    let arrCedulasMuestreo = await AsyncStorage.getItem('muestreoMeristemo');
    let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
    const arreglo = arrCedulasMuestreoArreglo.filter(
      bloque => bloque.id === id,
    );
    dispatch(getCedulaSeleccionadaExito(arreglo[0]));
    setUse(false);
    setPlantasEvaluadas(arreglo[0].plantas_evaluadas);
    setPlantasInducidas(arreglo[0].plantas_inducidas);
    setPlantasSinInducir(arreglo[0].plantas_sininducir);
    setParicionNatural(arreglo[0].paricion_natural);
    setSeleccionarCedula(arreglo[0].id);
    setObservacion(arreglo[0].observacion);
    //Maleza
    setCaminadora(arreglo[0].caminadora);
    setCizana(arreglo[0].cizana);
    setPata_de_gallina(arreglo[0].pata_de_gallina);
    setPepinillo(arreglo[0].pepinillo);
    setCyperus(arreglo[0].cyperus);
    setGolondrina(arreglo[0].golondrina);
    setBledo(arreglo[0].bledo);
    setDigitaria(arreglo[0].digitaria);
    setMombaza(arreglo[0].mombaza);
    setTomatillo(arreglo[0].tomatillo);
  };

  const obtenerMuestreados = async () => {
    // await AsyncStorage.setItem('muestreoFruta', '');
    const arrCopia = await AsyncStorage.getItem('muestreoMeristemo');
    const arregloCopia = JSON.parse(arrCopia);
    setcedulasEditar(arregloCopia);
  };
  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity onPress={() => setEditar(!editar)}>
          <View style={{marginTop: 5}}>
            {editar ? (
              <Icon name="arrow-back-outline" color="black" size={45} />
            ) : (
              <Icon name="create-outline" color="black" size={45} />
            )}
          </View>
        </TouchableOpacity>
      ),
    });
  }, [editar]);

  useEffect(() => {
    if (envioExito) {
      setPlantasEvaluadas(0);
      setPlantasInducidas(0);
      setPlantasSinInducir(0);
      setParicionNatural(0);
      setSeleccionarCedula(0);
      setObservacion('');
       //Maleza
     setCaminadora('Nada');
     setPata_de_gallina('Nada');
     setCizana('Nada')
     setPepinillo('Nada');
     setCyperus('Nada');
     setGolondrina('Nada');
     setBledo('Nada');
     setDigitaria('Nada');
     setMombaza('Nada');
     setTomatillo('Nada');
     setOcultarViewMaleza(true);
    }
  }, [envioExito]);

  const enviar = () => {
    if (seleccionarCedula === 0 || seleccionarCedula === undefined) {
      SweetAlert.showAlertWithOptions({
        title: 'Seleccionar Bloque',
        subTitle: 'Debes Seleccionar el bloque a muestrear',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    } else if (
      parseInt(plantasEvaluadas) !==
      parseInt(plantasInducidas) +
        parseInt(plantasSinInducir) +
        parseInt(paricionNatural)
    ) {
      SweetAlert.showAlertWithOptions({
        title: 'Verificar los datos',
        subTitle: 'El conteo de plantas debe ser igual a plantas evaluadas',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    } else {
      Alert.alert('Advertencia', 'Desea enviar el muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'Si', onPress: () => enviarFormulario()},
      ]);
    }
  };

  const enviarFormulario = async () => {
    setEditar(false);
    await obtenerUbicacion();
    const dataForm = {
      plantas_evaluadas: plantasEvaluadas,
      plantas_inducidas: plantasInducidas,
      plantas_sininducir: plantasSinInducir,
      paricion_natural: paricionNatural,
      estado: true,
      id: seleccionarCedula,
      observacion,
      //  fecha: getFecha(),
      //  hora: getHora(),
      latitud,
      longitud,
      codigo: cedula.codigo,
      area: cedula.area,
      numerolote: cedula.numerolote,
      numerobloque: cedula.numerobloque,
       //maleza
       caminadora,
       pata_de_gallina,
       cizana,
       pepinillo,
       cyperus,
       golondrina,
       bledo,
       digitaria,
       mombaza,
       tomatillo,
    };
    await dispatch(enviarFormularioAction(dataForm, seleccionarCedula));
  };

  return (
    <View>
      <ScrollView>
        <View style={{borderBottomWidth: 6, opacity: 0.2}}></View>

        <View style={{borderBottomWidth: 6, opacity: 0.2}}></View>
        <View style={styles.viewSubTitle}>
          {editar ? (
            <Text style={styles.subTitle}> Editar Cédulas</Text>
          ) : (
            <Text style={styles.subTitle}> Datos Básicos </Text>
          )}
          <View style={{flex: 1}} />
        </View>
        {editar ? (
          <>
            <ItemSeleccionar
              titulo="Cédulas completadas"
              icono="reorder-four-outline"
            />
            <PickerSelect
              seleccionarCedula={seleccionarCedula}
              setSeleccionarCedula={setSeleccionarCedula}
              cedulas={cedulasEditar}
            />
          </>
        ) : (
          <>
            <ItemSeleccionar titulo="Cédulas" icono="reorder-four-outline" />
            <PickerSelect
              seleccionarCedula={seleccionarCedula}
              setSeleccionarCedula={setSeleccionarCedula}
              cedulas={cedulas}
            />
          </>
        )}
        <ItemSeparator />

        <InfoGrupoForza />
        {cedula ? (
          <>
            <ItemSeparator />
            <Hectareaje area={cedula.area} />
          </>
        ) : null}
        <ItemSeparator />

        <FechaHora fecha={getFecha()} hora={getHora()} />
        <ItemSeparator />

        <Ubicacion latitud={latitud} longitud={longitud} />

        <ViewSubtitle
          title="Conteo Plantas"
          setEstado={setOcultarViewContadores}
          estado={ocultarViewContadores}
        />
        {!ocultarViewContadores ? (
          <>
            <ItemContador titulo="Plantas Evualadas" />
            <Contador
              contador={plantasEvaluadas}
              setContador={setPlantasEvaluadas}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Plantas Inducidas" />
            <Contador
              contador={plantasInducidas}
              setContador={setPlantasInducidas}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Plantas Sin Inducir" />
            <Contador
              contador={plantasSinInducir}
              setContador={setPlantasSinInducir}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Paricion Natural" />
            <Contador
              contador={paricionNatural}
              setContador={setParicionNatural}
            />
            <ItemContador titulo="" />
          </>
        ) : null}

           <ViewSubtitle
            title="Maleza"
            setEstado={setOcultarViewMaleza}
            estado={ocultarViewMaleza}
            />
            {
              !ocultarViewMaleza ?
              <>
              <ItemSeleccionar
              titulo="Asystasia gangética (Cizaña)"
              icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz dato={cizana} setDato={setCizana} />
              <ItemSeparator />
              
              <ItemSeleccionar
              titulo="R.Cochinchinensis (Caminadora)"
              icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz dato={caminadora} setDato={setCaminadora} />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="E. Digitaria (Pata de Gallina)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={pata_de_gallina}
                setDato={setPata_de_gallina}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Momordica charantia (Pepinillo)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={pepinillo}
                setDato={setPepinillo}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Cyperus luzulae (Cyperus)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={cyperus}
                setDato={setCyperus}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Commelina benghalensis (Golondrina)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={golondrina}
                setDato={setGolondrina}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Agastache rugosa (Bledo)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={bledo}
                setDato={setBledo}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Digitaria sanguinalis (Digitaria)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={digitaria}
                setDato={setDigitaria}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Panicum maximun (Mombaza)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={mombaza}
                setDato={setMombaza}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Solanum torvum (Tomatillo)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={tomatillo}
                setDato={setTomatillo}
              />

              </>
              :null
            }

        <ViewSubtitle
          title="Observaciones"
          setEstado={setOcultarViewObservacion}
          estado={ocultarViewObservacion}
        />
        {!ocultarViewObservacion ? (
          <InputObservacion
            observacion={observacion}
            setObservacion={setObservacion}
          />
        ) : null}

        {editar ? (
          <BotonEnviar enviarFormulario={enviar} titulo="Editar" />
        ) : (
          <BotonEnviar enviarFormulario={enviar} titulo="Enviar" />
        )}
        <ItemContador titulo="" />
      </ScrollView>
    </View>
  );
};
