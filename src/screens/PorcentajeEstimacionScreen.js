import React, {useEffect, useState} from 'react';
import {
  Alert,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  TextInput,
  BackHandler,
} from 'react-native';
import {useDispatch} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/Ionicons';
import SweetAlert from 'react-native-sweet-alert';
import {BotonEnviar} from '../components/BotonEnviar';
import {ItemContador} from '../components/ItemContador';
import {ItemSeparator} from '../components/ItemSeparator';
import {styles} from '../styles/styles';
import {actualizarPorcentajeEstimacionAction} from '../actions/muestreoEstimacionAction';
import {InfoRecoleccion} from '../components/MuestreoEstimacion/InfoRecoleccion';

export const PorcentajeEstimacionScreen = ({navigation, route}) => {
  const {bloqueid} = route.params;

  const dispatch = useDispatch();

  const [bloque, setbloque] = useState({});

  const [porcentaje, setporcentaje] = useState('');

  useEffect(() => {
    obtenerInfoBloque();
  }, []);

  useEffect(() => {
    const backAction = () => {
      navigation.navigate('EstimacionScreen', {cargar: true});
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const obtenerInfoBloque = async () => {
    const arrBloquesStorage = await AsyncStorage.getItem('bloquesEstimacion');
    const bloques = JSON.parse(arrBloquesStorage);
    const bloque = bloques.filter(bloq => bloq.id === bloqueid);
    setbloque(bloque[0]);
  };

  const enviar = () => {
    if (parseInt(porcentaje) < 1 || parseInt(porcentaje) > 100) {
      SweetAlert.showAlertWithOptions({
        title: 'Verifique el porcentaje',
        subTitle: 'El porcentaje de recoleccion debe ser entre 1 a 100',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    } else {
      Alert.alert('Advertencia', 'Desea enviar el muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'Si', onPress: () => enviarFormulario()},
      ]);
    }
  };

  const enviarFormulario = async () => {
    await dispatch(actualizarPorcentajeEstimacionAction(bloque.id, porcentaje));
    obtenerInfoBloque();
  };

  return (
    <>
      <ScrollView>
        <View style={{borderBottomWidth: 6, opacity: 0.2}}></View>
        <View style={styles.viewSubTitle}>
          <Text style={styles.subTitle}> Datos del bloque </Text>
          <View style={{flex: 1}} />
        </View>

        {bloque ? (
          <>
            <View
              style={{
                opacity: 0.7,
                marginVertical: 8,
              }}></View>
            <InfoRecoleccion
              info={` Lote: ${bloque.numerolote}   -    Bloque: ${bloque.numerobloque}`}
            />
            <ItemSeparator />
            <InfoRecoleccion
              info={` Grupo: ${bloque.codigo}  -  Area: ${bloque.area}`}
            />

            <ItemSeparator />
            <InfoRecoleccion info={ ` Semillas: ${bloque.totalsemillas}`} />
            <ItemSeparator />
            <InfoRecoleccion info={ (bloque.cantidad_extraida !== null) ? ` Extraída: ${parseFloat(bloque.cantidad_extraida).toFixed(0)}` : ` Extraida: 0`} />
            <ItemSeparator />
            <InfoRecoleccion info={ (bloque.cantidad_restante !== null) ? ` Total Restante: ${parseFloat(bloque.cantidad_restante).toFixed(0)}` : ` Total Restante: 0`} />
            
            
          </>
        ) : null}
        <View style={styles.viewSubTitle}>
          <Text style={styles.subTitle}>Porcentaje de Recolección {bloque.porcetaje_recoleccion}%</Text>
          <View style={{flex: 1}} />
        </View>
        <View
          style={{
            ...styles.viewSubSeleccionar,
            marginTop: 11,
            marginBottom: 11,
          }}>
          <View style={{justifyContent: 'center'}}>
            <Icon name="calculator-outline" size={35} />
          </View>
          <Text style={{...styles.subTitle, fontSize: 25}}> Porcentaje: </Text>
          <TextInput
            placeholder="100"
            keyboardType="phone-pad"
            underlineColorAndroid="black"
            style={{...styles.inputHora, fontSize: 20}}
            selectionColor="black"
            onChangeText={value => {
              let numero = value.replace(/[^0-9\p]/g, '');
              setporcentaje(numero);
            }}
            value={porcentaje}
            autoCapitalize="none"
            autoCorrect={false}
          />
        </View>
        <View style={styles.viewSubTitle}>
          <Text style={styles.subTitle}> Actualizar </Text>
          <View style={{flex: 1}} />
          <TouchableOpacity onPress={() => setEstado(!estado)}>
            <View style={{justifyContent: 'center'}}></View>
          </TouchableOpacity>
        </View>

        <View style={{...styles.botonContenedorEnviar, marginTop: 20}}>
          <TouchableOpacity
            style={styles.botonEnviar}
            onPress={enviar}>
            <View style={{justifyContent: 'center'}}>
              <Text style={styles.botonEnviarText}>Editar</Text>
            </View>
          </TouchableOpacity>
        </View>
        <ItemContador titulo="" />
      </ScrollView>
    </>
  );
};
