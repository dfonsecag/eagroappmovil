import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  BackHandler,
  TouchableOpacity,
} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import {Table, Row} from 'react-native-table-component';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import {useDimensions} from '@react-native-community/hooks';

import {
  getBloquesAction,
  setBloques,
} from '../actions/muestreoEstimacionAction';
import {styles} from '../styles/styles';

export const EstimacionScreen = ({navigation}) => {
  const dispatch = useDispatch();

  const permisos = useSelector(state => state.login.permisos);

  const grupos = useSelector(state => state.muestreoEstimacion.grupos);

  const bloques = useSelector(state => state.muestreoEstimacion.bloques);
  //   State
  const [seleccionarGrupo, setSeleccionarGrupo] = useState({});

  const [data, setData] = useState([]);

  const [buscar, setBuscar] = useState(true);

  const [mostrarSearch, setMostrarSearch] = useState(false);

  const [cedulas, setCedulas] = useState([]);

  const screen = useDimensions().screen;
  
  useEffect(() => {
    if (screen.width > screen.height) {
      setBuscar(false);
      setMostrarSearch(true);
    } else {
      setBuscar(true);
      setMostrarSearch(false);
    }
  }, [screen]);

  const tableData = [];
  useEffect(() => {
    if (bloques) {
      for (let i = 0; i < bloques.length; i += 1) {
        const rowData = [];
        rowData.push(`  📝  `);
        rowData.push(bloques[i].codigo);
        rowData.push(bloques[i].numerolote);
        rowData.push(bloques[i].numerobloque);
        rowData.push(`${bloques[i].porcetaje_recoleccion} %`);
        rowData.push(`${bloques[i].area} / Ha`);
        rowData.push(bloques[i].totalsemillas);
        if (bloques[i].cantidad_extraida === null) {
          rowData.push(0);
        } else {
          rowData.push(Number.parseInt(bloques[i].cantidad_extraida));
        }

        if (bloques[i].cantidad_restante === null) {
          rowData.push(0);
        } else {
          rowData.push(Number.parseInt(bloques[i].cantidad_restante));
        }
        rowData.push(bloques[i].muestras);
       
        rowData.push(bloques[i].semana_estimacion);
        if (bloques[i].estado === true) {
          rowData.push('Muestreado');
        } else {
          rowData.push('Pendiente');
        }
        rowData.push(bloques[i].id);

        tableData.push(rowData);
      }
      setData(tableData);
    }
  }, [bloques]);

  useEffect(() => {
    dispatch(setBloques());
  }, []);

  useEffect(() => {
    const gruposEstimacion = [];
    grupos.map(gr => {
      gruposEstimacion.push(
        gr.name.substring(gr.name.length - 6, gr.name.length - 4),
      );
    });

    const newArreglo = new Set(gruposEstimacion);
    const gruposConcatenados = [...newArreglo];
    setCedulas(gruposConcatenados);
  }, [grupos]);

  useEffect(() => {
    const backAction = () => {
      navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid});
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    if (seleccionarGrupo) {
      if (mostrarSearch) {
        setBuscar(false);
      }

      dispatch(getBloquesAction(seleccionarGrupo));
    }
  }, [seleccionarGrupo]);

  let state = {
    tableHead: [
      '📝',
      'Grupo',
      'Lote',
      'Bloque',
      '% Recolección',
      'Área',
      'Total semillas',
      'Cantidad Extraída',
      'Cantidad Restante',
      'Muestras',      
      'Semana Estimación',
      'Estado',
      'Id',
    ],
    widthArr: [60,95, 90, 90, 100, 120, 140, 140, 100, 110, 100, 120, 0],
  };

  // Activar metodo Search
  const activarBusqueda = () => {
    setBuscar(!buscar);
  };

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <View style={{...styles.viewSubSeleccionar, marginLeft: 10}}>
          {mostrarSearch ? (
            <TouchableOpacity onPress={() => activarBusqueda()}>
              <View style={{marginTop: 10}}>
                <Icon name="search-outline" color="black" size={40} />
              </View>
            </TouchableOpacity>
          ) : null}
        </View>
      ),
    });
  }, [mostrarSearch, buscar]);

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
            <TouchableOpacity onPress={() =>  dispatch(getBloquesAction(seleccionarGrupo))}>
              <View style={{marginTop: 10}}>
                <Icon name="sync-outline" color="black" size={40} />
              </View>
            </TouchableOpacity>
      ),
    });
  }, [seleccionarGrupo]);

  return (
    <>
     
      {buscar ? (
        <Picker
          selectedValue={seleccionarGrupo}
          onValueChange={itemValue => setSeleccionarGrupo(itemValue)}>
          <Picker.Item
            style={{fontSize: 24, fontWeight: 'bold', color:'black'}}
            label={' -- Seleccione el grupo --'}
            value={0}
            key={0}
          />
          {cedulas
            ? cedulas.map(c => (
                <Picker.Item
                  style={{fontSize: 20, color:'black'}}
                  label={`Grupo: ${c} `}
                  value={c}
                  key={c}
                />
              ))
            : null}
        </Picker>
      ) : null}

      <View style={tabla.container}>
        <ScrollView horizontal={true}>
          <View>
            <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}}>
              <Row
                data={state.tableHead}
                widthArr={state.widthArr}
                style={tabla.header}
                textStyle={tabla.textheader}
              />
            </Table>
            <ScrollView style={tabla.dataWrapper}>
              <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}}>
                {data.map((rowData, index) => (
                  <Row
                    key={index}
                    data={rowData}
                    onPress={() => {
                      navigation.navigate('PorcentajeEstimacionScreen', {bloqueid: rowData[12]})
                    }}
                    widthArr={state.widthArr}
                    style={[
                      tabla.row,
                      rowData[11] === 'Muestreado'
                        ? {backgroundColor: '#8BC34A'}
                        : {backgroundColor: '#FDFD96'},
                    ]}
                    textStyle={tabla.text}
                  />
                ))}
              </Table>
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    </>
  );
};

const tabla = StyleSheet.create({
  container: {flex: 1, padding: 5, paddingTop: 2, paddingBottom: 0},
  header: {height: 50, backgroundColor: '#424242'},
  textheader: {textAlign: 'center', fontWeight: 'bold', color: 'white'},
  text: {
    textAlign: 'center',
    fontWeight: 'normal',
    fontSize: 15,
    color: 'black',
  },
  dataWrapper: {marginTop: -1},
  row: {height: 60},
});
