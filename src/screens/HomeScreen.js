import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  BackHandler,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SweetAlert from 'react-native-sweet-alert';
import {styles} from '../styles/styles';
// Menu
import {ListaMenuHome} from '../components/ListaMenuHome';
// Cedulas  Meristemos
import {
  getCedulasPendientesAction,
  sincronizarMeristemoAction,
} from '../actions/muestreoMeristemoAction';
// Cedulas  Fruta
import {
  getCedulasPendientesFrutaAction,
  sincronizarFrutaAction,
} from '../actions/muestreoFrutaAction';
// Cedulas raiz
import {
  getCedulasPendientesRaizAction,
  sincronizarRaizAction,
} from '../actions/muestreoRaizAction';
// Hoja de Vida y grupos
import {
  getGruposAction,
  getAllHojasVidaAction,
} from '../actions/hojaVidaAction';
import {conexionInternet} from '../helpers/verificarConexionInternet';
import {cerrarSesionAction} from '../actions/loginAction';
import {CerrarSesion} from '../components/CerrarSesion';
import {
  getCedulasPendientesPesoAction,
  sincronizarPesoAction,
} from '../actions/muestreoPesoAction';

// Action de muestreo estimacion
import {
  sincronizarEstimacionAction,
  getCedulasPendientesEstimacionAction,
  getGruposEstimacionAction,
  sincronizarPorcentajeEstimacionAction
} from '../actions/muestreoEstimacionAction';
// Action de muestreo premaduracion
import {
  sincronizarPremaduracionAction,
  getCedulasPendientesPremaduracionAction
} from '../actions/muestreoPremaduracionAction';
// Action de muestreo floracion
import {
  sincronizarFloracionAction,
  getCedulasPendientesFloracionAction
} from '../actions/muestreoFloracionAction';

import {
  getCedulasPendientesAvanceObraAction,
  sincronizarAvanceObraAction,
} from '../actions/avanceObraAction';

import {empezarSincronizacion, finalizarSincronizacionAction} from '../actions/sincronizacionAction';

import {menuItemsObra} from '../data/MenuAvanceObra';
import {MenuAdmin} from '../data/MenuAdmin';
import {MenuMuestreador} from '../data/MenuMuestreador';
import { MenuProduccion } from '../data/MenuProduccion';
import { MenuTrazabilidad } from '../data/MenuTrazabilidad';
import { menuMuestreadorPlanta } from '../data/MenuMuestreadorPlanta';
import { getDatosFormularioSemillaAction, sincronizarMuestreoSemillaAction } from '../actions/muestreoSemillaAction';

export const HomeScreen = ({navigation, route}) => {

  const { rolUsuario } = route.params;
  // Dispatch
  const dispatch = useDispatch();
  const status = useSelector(state => state.login.status);
  const permisos = useSelector(state => state.login.permisos);
  const sincronizando = useSelector(state => state.sincronizacion.loading);
  
  const [menu, setMenu] = useState([]);

  const cerrarSesion = () => {
    Alert.alert('Advertencia', 'Desea cerrar sesion?', [
      {
        text: 'Cancelar',
        onPress: () => null,
        style: 'cancel',
      },
      {text: 'Si', onPress: () => dispatch(cerrarSesionAction())},
    ]);
  };

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => <CerrarSesion cerrarSesion={cerrarSesion} />,
    });
  }, []);

  useEffect(() => {
    const backAction = () => {
      BackHandler.exitApp();
    };
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    if (status === false) {
      navigation.navigate('LoginScreen');
    }
  }, [status]);

  const sincronizar = async () => {
   
    const internet = await conexionInternet();
    if (internet) {
      dispatch(empezarSincronizacion());      

      const meristemo = await AsyncStorage.getItem('muestreoMeristemo');
      const fruta = await AsyncStorage.getItem('muestreoFruta');
      const raiz = await AsyncStorage.getItem('muestreoRaiz');
      const peso = await AsyncStorage.getItem('muestreoPeso');
      const estimacion = await AsyncStorage.getItem('muestreoEstimacion');
      const estimacionPorcentaje = await AsyncStorage.getItem('muestreoPorcentaje');
      const premaduracion = await AsyncStorage.getItem('muestreoPremaduracion');
      const floracion = await AsyncStorage.getItem('muestreoFloracion');
      const avanceObra = await AsyncStorage.getItem('avanceobra');
      const muestreoSemilla = await AsyncStorage.getItem('muestreoSemilla');
      const muestreoCalidadSiembra = await AsyncStorage.getItem('muestreoCalidadSiembra');
      const muestreoDistanciaSiembra = await AsyncStorage.getItem('muestreoDistanciaSiembra');
      const muestreoTerrazas = await AsyncStorage.getItem('terrazasContratista');
      // Sincronizar Avance Obra
      if (
        meristemo !== null ||
        fruta !== null ||
        raiz !== null ||
        peso !== null ||
        avanceObra !== null ||
        estimacion !== null ||
        premaduracion !== null ||
        estimacionPorcentaje !== null ||
        floracion !== null ||
        muestreoSemilla !== null ||
        muestreoCalidadSiembra !== null ||
        muestreoDistanciaSiembra !== null ||
        muestreoTerrazas !== null
      ) {
       await dispatch(sincronizarFrutaAction());
       await dispatch(sincronizarMeristemoAction());
       await dispatch(sincronizarRaizAction());
       await dispatch(sincronizarPesoAction());
       await dispatch(sincronizarAvanceObraAction());
       await dispatch(sincronizarEstimacionAction());
       await dispatch(sincronizarPremaduracionAction());
       await dispatch(sincronizarFloracionAction())
       await dispatch(sincronizarPorcentajeEstimacionAction())
       await dispatch(sincronizarMuestreoSemillaAction())
      }
      await sincronizarDatosUsuarios();
      dispatch(finalizarSincronizacionAction());
    } else {
      SweetAlert.showAlertWithOptions({
        title: 'Verifique su conexion',
        subTitle: 'Los datos no han sido sincronizados, intente nuevamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    }
  };

  useEffect(() => {
    if (permisos.rolid === 1) {
      setMenu(MenuAdmin);
    }else if (permisos.rolid === 2) {
      setMenu(MenuTrazabilidad);
    } 
    else if (permisos.rolid === 4) {
      setMenu(MenuProduccion);
    } else if (permisos.rolid === 10) {
      setMenu(MenuMuestreador);
    } else if (permisos.rolid === 14) {
      setMenu(menuItemsObra);
    } else if (permisos.rolid === 16){
      setMenu(menuMuestreadorPlanta);
    }
  }, [permisos]);

  useEffect(() => {
    dispatch(empezarSincronizacion());

   
    dispatch(sincronizarFrutaAction());
    dispatch(sincronizarMeristemoAction());
    dispatch(sincronizarRaizAction());
    dispatch(sincronizarPesoAction());
    dispatch(sincronizarAvanceObraAction());
    dispatch(sincronizarEstimacionAction());
    dispatch(sincronizarPremaduracionAction());
    dispatch(sincronizarFloracionAction())
    dispatch(sincronizarPorcentajeEstimacionAction());
    dispatch(sincronizarMuestreoSemillaAction())

    sincronizarDatosUsuarios();      

    dispatch(finalizarSincronizacionAction());
  }, []);

  const sincronizarDatosUsuarios = () => {
    if(rolUsuario === 10){
      console.log("Muestreador")
      dispatch(getCedulasPendientesAction());
      dispatch(getCedulasPendientesFrutaAction());
      dispatch(getCedulasPendientesRaizAction());
      dispatch(getCedulasPendientesPesoAction());
      dispatch(getCedulasPendientesEstimacionAction());
      dispatch(getCedulasPendientesPremaduracionAction());
      dispatch(getCedulasPendientesFloracionAction());
      dispatch(getDatosFormularioSemillaAction())

    } else if (rolUsuario === 14){
      console.log("Operador")
      dispatch(getCedulasPendientesAvanceObraAction());
    } else if (rolUsuario === 4){
      console.log("Menu Produccion")
      dispatch(getCedulasPendientesAction());
      dispatch(getCedulasPendientesFrutaAction());
      dispatch(getCedulasPendientesRaizAction());
      dispatch(getCedulasPendientesPesoAction());
      dispatch(getCedulasPendientesFloracionAction());
      dispatch(getDatosFormularioSemillaAction())
      dispatch(getCedulasPendientesAvanceObraAction());
      dispatch(getGruposAction());
      dispatch(getAllHojasVidaAction());
    }else if (rolUsuario === 2){
      console.log("Menu Produccion")
      dispatch(getGruposAction());
      dispatch(getAllHojasVidaAction());
      dispatch(getCedulasPendientesEstimacionAction());
      dispatch(getCedulasPendientesPremaduracionAction());
      dispatch(getCedulasPendientesFloracionAction());
      dispatch(getDatosFormularioSemillaAction())
      dispatch(getGruposEstimacionAction());
      dispatch(getAllHojasVidaAction());
    }
    else {
      console.log("Soy administrador")
      dispatch(getCedulasPendientesAction());
      dispatch(getCedulasPendientesFrutaAction());
      dispatch(getCedulasPendientesRaizAction());
      dispatch(getCedulasPendientesPesoAction());
      dispatch(getCedulasPendientesAvanceObraAction());
      dispatch(getCedulasPendientesEstimacionAction());
      dispatch(getCedulasPendientesPremaduracionAction());
      dispatch(getCedulasPendientesFloracionAction());
      dispatch(getDatosFormularioSemillaAction())
      dispatch(getGruposEstimacionAction());
      // Hoja Vida y grupos
      dispatch(getGruposAction());
      dispatch(getAllHojasVidaAction());
    }
  }

  return (
    <>
      {sincronizando ? (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator size={100} color="black" />
        </View>
      ) : (
        <>
          {/* <CerrarSesion cerrarSesion={cerrarSesion} /> */}
          <View style={{flex: 1, marginHorizontal: 20, marginTop: 30}}>
            <FlatList
              data={menu}
              // data={menuItemsObra}
              renderItem={({item}) => <ListaMenuHome menuItem={item} />}
              keyExtractor={item => item.name} //Debe ser string KeyExtractor
              // ListHeaderComponent={
              //   <View style={{marginTop: 0, marginBottom: 20}}>
              //     <Text
              //       style={{
              //         fontSize: 30,
              //         fontWeight: 'bold',
              //         textAlign: 'center',
              //       }}>
              //       Opciones de Obras
              //     </Text>
              //   </View>
              // } //cabezado de la lista
              ItemSeparatorComponent={() => (
                <View
                  style={{
                    borderBottomWidth: 1,
                    opacity: 0.7,
                    marginVertical: 8,
                  }}></View>
              )} //Separador
            />

            <View
              style={{
                flexDirection: 'row',
                paddingVertical: 30,
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                style={styles.botonSincronizar}
                onPress={sincronizar}>
                <View
                  style={{
                    justifyContent: 'center',
                    flexDirection: 'row',
                    paddingTop: 5,
                  }}>
                  <Icon name="sync-outline" color="black" size={35} />
                  <Text style={styles.botonEnviarText}> Sincronizar</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </>
      )}
    </>
  );
};
