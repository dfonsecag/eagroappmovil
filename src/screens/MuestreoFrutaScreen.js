import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  View,
  ScrollView,
  BackHandler,
  Alert,
  Text,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/Ionicons';
import SweetAlert from 'react-native-sweet-alert';

import {BotonEnviar} from '../components/BotonEnviar';
import {Contador} from '../components/Contador';
import {FechaHora} from '../components/FechaHora';
import {ItemContador} from '../components/ItemContador';
import {ItemSeleccionar} from '../components/ItemSeleccionar';
import {ItemSeparator} from '../components/ItemSeparator';
import {PickerSelect} from '../components/PickerSelect';
import {Ubicacion} from '../components/Ubicacion';
import {ViewSubtitle} from '../components/ViewSubtitle';
import {useCoordenadas} from '../hooks/useCoordenadas';
import {enviarFormularioFrutaAction} from '../actions/muestreoFrutaAction';
import {ItemSeparatorContador} from '../components/ItemSeparatorContador';
import {InputObservacion} from '../components/InputObservacion';
import {getFecha, getHora} from '../helpers/FechaHora';
import {
  getCedulaSeleccionadaFruta,
  getCedulaSeleccionadaExito,
  setearCedulaSeleccionada,
} from '../actions/muestreoMeristemoAction';
import {InfoGrupoForza} from '../components/InfoGrupoForza';
import {RadioButtonClima} from '../components/RadioButtonClima';
import {styles} from '../styles/styles';
import {ViewMuestrasRestantes} from '../components/ViewMuestrasRestantes';
import {Hectareaje} from '../components/Hectareaje';
import { PickerSelectEstimacion } from '../components/MuestreoEstimacion/PickerSelectEstimacion';
import { RadioButtonRaiz } from '../components/RadioButtonRaiz';

export const MuestreoFrutaScreen = ({navigation}) => {
  // Dispatch
  const dispatch = useDispatch();
  const [editar, setEditar] = useState(false);
  //State ocultar View
  const [ocultarViewContadores, setOcultarViewContadores] = useState(false);
  const [ocultarViewObservacion, setOcultarViewObservacion] = useState(true);
  const [ocultarViewMaleza, setOcultarViewMaleza] = useState(true);
  // Contador General del bloque
  const [contadorRestante, setContadorRestantes] = useState();
  // State de los contadores
  const [theclaHuevo, setTheclaHuevo] = useState(0);
  const [theclaLarva, setTheclaLarva] = useState(0);
  const [gusanoSoldadoLarva, setGusanoSoldadoLarva] = useState(0);
  const [gusanoSoldadoPupa, setGusanoSoldadoPupa] = useState(0);
  const [cochinillaExternaB, setCochinillaExternaB] = useState(0);
  const [cochinillaExternaC, setCochinillaExternaC] = useState(0);
  const [hormiga, setHormiga] = useState(0);
  const [gomosisFresca, setGomosisFresca] = useState(0);
  const [gomosisVieja, setGomosisVieja] = useState(0);
  const [danoRoedor, setDanoRoedor] = useState(0);
  const [danoThecla, setDanoThecla] = useState(0);
  const [danoGusanoSoldado, setDanoGusanoSoldado] = useState(0);
  const [babosa, setBabosa] = useState(0);
  const [frutasMuestriadas, setFrutasMuestriadas] = useState(0);
  const [cochinillaExternaC2, setCochinillaExternaC2] = useState(0);
  const [frutasMuestriadas2, setFrutasMuestriadas2] = useState(0);
  const [cochinillaExternaB2, setCochinillaExternaB2] = useState(0);

  const [quema_sol, setQuema_Sol] = useState(0);
  const [escamas, setEscamas] = useState(0);
  const [fusarium, setfusarium] = useState(0);
  const [phytophthora, setphytophthora] = useState(0);
  const [jobotos, setjobotos] = useState(0);
  const [sinfilidos, setsinfilidos] = useState(0);
  const [erwinia, seterwinia] = useState(0);

  const [seleccionarCedula, setSeleccionarCedula] = useState(0);
  const [observacion, setObservacion] = useState();
  const [hora_inicio, setHora_inicio] = useState('');
  const [clima, setClima] = useState('SOLEADO');
  const [use, setUse] = useState(false);

     //Malezas
     const [caminadora, setCaminadora] = useState('Nada');
     const [pata_de_gallina, setPata_de_gallina] = useState('Nada');
     const [cizana, setCizana] = useState('Nada');
     const [pepinillo, setPepinillo] = useState('Nada');
     const [cyperus, setCyperus] = useState('Nada');
     const [golondrina, setGolondrina] = useState('Nada');
     const [bledo, setBledo] = useState('Nada');
     const [digitaria, setDigitaria] = useState('Nada');
     const [mombaza, setMombaza] = useState('Nada');
     const [tomatillo, setTomatillo] = useState('Nada');

  const [cedulasEditar, setcedulasEditar] = useState([]);

  // Custom Hook funcionalidades como coordenadas
  const {latitud, longitud, obtenerUbicacion} = useCoordenadas();
  // Selector Redux
  const envioExito = useSelector(state => state.muestreoFruta.envioExito);

  const cedulas = useSelector(state => state.muestreoFruta.cedulasPendientes);
  const cedula = useSelector(
    state => state.muestreoMeristemo.cedulaSeleccionada,
  );

  const permisos = useSelector(state => state.login.permisos);

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Advertencia', 'Desea salir del muestreo fruta ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'Si',
          onPress: () =>
            navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid}),
        },
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    dispatch(setearCedulaSeleccionada());
    obtenerUbicacion();
    setHora_inicio(getHora());
    setContadorRestantes(0);
  }, []);

  // LLeva el contador de las plantas evaluadas
  useEffect(() => {
    if (cedula && editar === false) {
      limpiarContadores();
      setContadorRestantes(cedula.muestras);
    }
  }, [cedula]);

  useEffect(() => {
    if (envioExito) {
      setSeleccionarCedula(0);
      limpiarContadores();
    }
  }, [envioExito]);

  useEffect(() => {
    if (editar) {
      obtenerCedulaEditar(seleccionarCedula);
    } else {
      dispatch(getCedulaSeleccionadaFruta(seleccionarCedula));
    }
  }, [seleccionarCedula]);

  // Para editar
  useEffect(() => {
    if (editar) {
      obtenerMuestreados();
    }
  }, [editar]);

  const obtenerCedulaEditar = async id => {
    let arrCedulasMuestreo = await AsyncStorage.getItem('muestreoFruta');
    let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
    arreglo = arrCedulasMuestreoArreglo.filter(bloque => bloque.id === id);
    dispatch(getCedulaSeleccionadaExito(arreglo[0]));
    setUse(false);
    setTheclaHuevo(arreglo[0].thechla_huevo);
    setTheclaLarva(arreglo[0].thechla_larva);
    setGusanoSoldadoLarva(arreglo[0].gusano_soldado_larva);
    setGusanoSoldadoPupa(arreglo[0].gusano_soldado_pupa);
    setCochinillaExternaB(arreglo[0].cochinilla_externa_b);
    setCochinillaExternaC(arreglo[0].cochinilla_externa_c);
    setHormiga(arreglo[0].hormiga);
    setGomosisFresca(arreglo[0].gomosis_fresca);
    setGomosisVieja(arreglo[0].gomosis_vieja);
    setDanoRoedor(arreglo[0].danno_roedor);
    setDanoThecla(arreglo[0].danno_thechla);
    setDanoGusanoSoldado(arreglo[0].danno_gusano_soldado);
    setBabosa(arreglo[0].babosa);
    setFrutasMuestriadas(arreglo[0].frutas_muestreadas_cochinilla_c);
    setCochinillaExternaC2(arreglo[0].cochinilla_interna_c);
    setFrutasMuestriadas2(arreglo[0].frutas_muestreadas_cochinilla_b);
    setCochinillaExternaB2(arreglo[0].cochinilla_interna_b);
    setObservacion(arreglo[0].observacion);
    setHora_inicio(getHora());
    setClima(arreglo[0].clima);
    setTheclaHuevo(arreglo[0].thechla_huevo);
    setTheclaLarva(arreglo[0].thechla_larva);
    setGusanoSoldadoLarva(arreglo[0].gusano_soldado_larva);
    setGusanoSoldadoPupa(arreglo[0].gusano_soldado_pupa);
    setCochinillaExternaB(arreglo[0].cochinilla_externa_b);
    setCochinillaExternaC(arreglo[0].cochinilla_externa_c);
    setHormiga(arreglo[0].hormiga);
    setGomosisFresca(arreglo[0].gomosis_fresca);
    setGomosisVieja(arreglo[0].gomosis_vieja);
    setDanoRoedor(arreglo[0].danno_roedor);
    setDanoThecla(arreglo[0].danno_thechla);
    setDanoGusanoSoldado(arreglo[0].danno_gusano_soldado);
    setBabosa(arreglo[0].babosa);
    setFrutasMuestriadas(arreglo[0].frutas_muestreadas_cochinilla_c);
    setCochinillaExternaC2(arreglo[0].cochinilla_interna_c);
    setFrutasMuestriadas2(arreglo[0].frutas_muestreadas_cochinilla_b);
    setCochinillaExternaB2(arreglo[0].cochinilla_interna_b);
    setObservacion(arreglo[0].observacion);
    setHora_inicio(getHora());
    setClima(arreglo[0].clima);
    setContadorRestantes(0);

    setQuema_Sol(arreglo[0].quema_sol);
    setEscamas(arreglo[0].escamas);
    setfusarium(arreglo[0].fusarium);
    setphytophthora(arreglo[0].phytophthora);
    setjobotos(arreglo[0].jobotos);
    setsinfilidos(arreglo[0].sinfilidos);
    seterwinia(arreglo[0].erwinia);
     //Maleza
     setCaminadora(arreglo[0].caminadora);
     setCizana(arreglo[0].cizana);
     setPata_de_gallina(arreglo[0].pata_de_gallina);
     setPepinillo(arreglo[0].pepinillo);
     setCyperus(arreglo[0].cyperus);
     setGolondrina(arreglo[0].golondrina);
     setBledo(arreglo[0].bledo);
     setDigitaria(arreglo[0].digitaria);
     setMombaza(arreglo[0].mombaza);
     setTomatillo(arreglo[0].tomatillo);
  };

  const obtenerMuestreados = async () => {
    // await AsyncStorage.setItem('muestreoFruta', '');
    const arrCopia = await AsyncStorage.getItem('muestreoFruta');
    const arregloCopia = JSON.parse(arrCopia);
    setcedulasEditar(arregloCopia);
  };

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <View
          style={{...styles.viewSubSeleccionar, justifyContent: 'flex-end'}}>
          <TouchableOpacity onPress={() => setEditar(!editar)}>
            <View style={{marginTop: 5}}>
              {editar ? (
                <Icon name="arrow-back-outline" color="black" size={45} />
              ) : (
                <Icon name="create-outline" color="black" size={45} />
              )}
            </View>
          </TouchableOpacity>
        </View>
      ),
    });
  }, [editar]);

  const enviar = () => {
    if (seleccionarCedula === 0 || seleccionarCedula === undefined) {
      SweetAlert.showAlertWithOptions({
        title: 'Seleccionar la cédula',
        subTitle: 'Debes seleccionar la cédula',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    } else if (contadorRestante > 0) {
      SweetAlert.showAlertWithOptions({
        title: 'Aun hay muestras pendientes',
        subTitle: 'Debes de completar las muestras del bloque',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    } else {
      Alert.alert('Advertencia', 'Desea enviar la cédula ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'Si', onPress: () => enviarFormulario()},
      ]);
    }
  };

  const enviarFormulario = async () => {
    await obtenerUbicacion();
    setEditar(false);
    const data = {
      thechla_huevo: theclaHuevo,
      thechla_larva: theclaLarva,
      gusano_soldado_larva: gusanoSoldadoLarva,
      gusano_soldado_pupa: gusanoSoldadoPupa,
      cochinilla_externa_b: cochinillaExternaB,
      cochinilla_externa_c: cochinillaExternaC,
      hormiga,
      gomosis_fresca: gomosisFresca,
      gomosis_vieja: gomosisVieja,
      danno_roedor: danoRoedor,
      danno_thechla: danoThecla,
      danno_gusano_soldado: danoGusanoSoldado,
      babosa,
      frutas_muestreadas_cochinilla_c: frutasMuestriadas,
      cochinilla_interna_c: cochinillaExternaC2,
      frutas_muestreadas_cochinilla_b: frutasMuestriadas2,
      cochinilla_interna_b: cochinillaExternaB2,
      estado: true,
      observacion,
      id: seleccionarCedula,
      dia_fecha: getFecha(),
      hora_inicio: `${getFecha()} ${hora_inicio}`,
      hora_fin: `${getFecha()} ${getHora()}`,

      // hora: time,
      latitud,
      longitud,
      clima,
      muestras: cedula.muestras,
      codigo: cedula.codigo,
      area: cedula.area,
      numerolote: cedula.numerolote,
      numerobloque: cedula.numerobloque,
      quema_sol,
      escamas,
      fusarium,
      phytophthora,
      jobotos,
      sinfilidos,
      erwinia,

        //maleza
        caminadora,
        pata_de_gallina,
        cizana,
        pepinillo,
        cyperus,
        golondrina,
        bledo,
        digitaria,
        mombaza,
        tomatillo,
    };
    await dispatch(enviarFormularioFrutaAction(data, seleccionarCedula));
  };
  const validarConteo = () => {
    if (contadorRestante === 0) {
      SweetAlert.showAlertWithOptions({
        title: 'Muestras completadas',
        subTitle: 'Has completado la cantidad de muestras',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    } else {
      setContadorRestantes(contadorRestante - 1);
    }
  };

  const incrementarConteo = () => {
    if (contadorRestante === cedula.muestras) {
      SweetAlert.showAlertWithOptions({
        title: 'Limite muestras',
        subTitle: 'Las plantas restantes no puede ser mayor a las muestras',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    } else {
      setContadorRestantes(contadorRestante + 1);
    }
  };

  const limpiarContadores = () => {
    setTheclaHuevo(0);
    setTheclaLarva(0);
    setGusanoSoldadoLarva(0);
    setGusanoSoldadoPupa(0);
    setCochinillaExternaB(0);
    setCochinillaExternaC(0);
    setHormiga(0);
    setGomosisFresca(0);
    setGomosisVieja(0);
    setDanoRoedor(0);
    setDanoThecla(0);
    setDanoGusanoSoldado(0);
    setBabosa(0);
    setFrutasMuestriadas(0);
    setCochinillaExternaC2(0);
    setFrutasMuestriadas2(0);
    setCochinillaExternaB2(0);
    setObservacion('');
    setHora_inicio(getHora());
    setClima('SOLEADO');
    //Maleza
    setCaminadora('Nada');
    setPata_de_gallina('Nada');
    setCizana('Nada')
    setPepinillo('Nada');
    setCyperus('Nada');
    setGolondrina('Nada');
    setBledo('Nada');
    setDigitaria('Nada');
    setMombaza('Nada');
    setTomatillo('Nada');
    setOcultarViewMaleza(true);
    setQuema_Sol(0);
    setEscamas(0);
    setfusarium(0);
    setphytophthora(0);
    setjobotos(0);
    setsinfilidos(0);
    seterwinia(0);
  };

  return (
    <>
      <ScrollView>
        <View style={{borderBottomWidth: 6, opacity: 0.2}}></View>

        <View style={{borderBottomWidth: 6, opacity: 0.2}}></View>
        <View style={styles.viewSubTitle}>
          {editar ? (
            <Text style={styles.subTitle}> Editar Cédulas</Text>
          ) : (
            <Text style={styles.subTitle}> Datos Básicos </Text>
          )}

          <View style={{flex: 1}} />
        </View>

        {editar ? (
          <>
            <ItemSeleccionar
              titulo="Cédulas completadas"
              icono="reorder-four-outline"
            />
            <PickerSelect
              seleccionarCedula={seleccionarCedula}
              setSeleccionarCedula={setSeleccionarCedula}
              cedulas={cedulasEditar}
            />
          </>
        ) : (
          <>
            <ItemSeleccionar titulo="Cédulas" icono="reorder-four-outline" />
            <PickerSelectEstimacion
              seleccionarCedula={seleccionarCedula}
              setSeleccionarCedula={setSeleccionarCedula}
              cedulas={cedulas}
            />
          </>
        )}

        <ItemSeparator />

        <InfoGrupoForza />

        {cedula ? (
          <>
            <ItemSeparator />
            <Hectareaje area={cedula.area} />
          </>
        ) : null}

        <ItemSeparator />

        <ItemSeleccionar titulo="Condicion Climatica" icono="cloud-outline" />
        <RadioButtonClima clima={clima} setClima={setClima} />
        <ItemSeparator />

        <FechaHora fecha={getFecha()} hora={getHora()} />
        <ItemSeparator />

        <Ubicacion latitud={latitud} longitud={longitud} />

        <ViewSubtitle
          title="Conteo Plantas"
          setEstado={setOcultarViewContadores}
          estado={ocultarViewContadores}
        />
        {!ocultarViewContadores ? (
          <>
            <ItemContador titulo="Thecla Huevo" />
            <Contador
              contador={theclaHuevo}
              setContador={setTheclaHuevo}
              contadorRestante={contadorRestante}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Thecla Larva" />
            <Contador
              contador={theclaLarva}
              setContador={setTheclaLarva}
              contadorRestante={contadorRestante}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Gusano Soldado Larva" />
            <Contador
              contador={gusanoSoldadoLarva}
              setContador={setGusanoSoldadoLarva}
              contadorRestante={contadorRestante}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Gusano Soldado Pupa" />
            <Contador
              contador={gusanoSoldadoPupa}
              setContador={setGusanoSoldadoPupa}
              contadorRestante={contadorRestante}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Cochinilla Externa B" />
            <Contador
              contador={cochinillaExternaB}
              setContador={setCochinillaExternaB}
              contadorRestante={contadorRestante}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Cochinilla Externa C" />
            <Contador
              contador={cochinillaExternaC}
              setContador={setCochinillaExternaC}
              contadorRestante={contadorRestante}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Hormiga" />
            <Contador
              contador={hormiga}
              setContador={setHormiga}
              contadorRestante={contadorRestante}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Gomosis Fresca" />
            <Contador
              contador={gomosisFresca}
              setContador={setGomosisFresca}
              contadorRestante={contadorRestante}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Gomosis Vieja" />
            <Contador
              contador={gomosisVieja}
              setContador={setGomosisVieja}
              contadorRestante={contadorRestante}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Daño Roedor" />
            <Contador
              contador={danoRoedor}
              setContador={setDanoRoedor}
              contadorRestante={contadorRestante}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Daño Thecla" />
            <Contador
              contador={danoThecla}
              setContador={setDanoThecla}
              contadorRestante={contadorRestante}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Daño Gusano Soldado" />
            <Contador
              contador={danoGusanoSoldado}
              setContador={setDanoGusanoSoldado}
              contadorRestante={contadorRestante}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Babosa" />
            <Contador
              contador={babosa}
              setContador={setBabosa}
              contadorRestante={contadorRestante}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Frutas Muestriadas" />
            <Contador
              contador={frutasMuestriadas}
              setContador={setFrutasMuestriadas}
              contadorRestante={contadorRestante}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Cochinilla Interna C" />
            <Contador
              contador={cochinillaExternaC2}
              setContador={setCochinillaExternaC2}
              contadorRestante={contadorRestante}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Frutas Muestriadas" />
            <Contador
              contador={frutasMuestriadas2}
              setContador={setFrutasMuestriadas2}
              contadorRestante={contadorRestante}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Cochinilla Interna B" />
            <Contador
              contador={cochinillaExternaB2}
              setContador={setCochinillaExternaB2}
              contadorRestante={contadorRestante}
            />

            <ItemSeparatorContador />
            <ItemContador titulo="Quema Sol" />
            <Contador
              contador={quema_sol}
              setContador={setQuema_Sol}
              contadorRestante={contadorRestante}
            />

            <ItemSeparatorContador />
            <ItemContador titulo="Escamas" />
            <Contador
              contador={escamas}
              setContador={setEscamas}
              contadorRestante={contadorRestante}
            />

          <ItemSeparatorContador />
            <ItemContador titulo="Fusarium" />
            <Contador
              contador={fusarium}
              setContador={setfusarium}
              contadorRestante={contadorRestante}
            />

            <ItemSeparatorContador />
            <ItemContador titulo="Phytophthora" />
            <Contador
              contador={phytophthora}
              setContador={setphytophthora}
              contadorRestante={contadorRestante}
            />

            <ItemSeparatorContador />
            <ItemContador titulo="Jobotos" />
            <Contador
              contador={jobotos}
              setContador={setjobotos}
              contadorRestante={contadorRestante}
            />

            <ItemSeparatorContador />
            <ItemContador titulo="Sinfilidos" />
            <Contador
              contador={sinfilidos}
              setContador={setsinfilidos}
              contadorRestante={contadorRestante}
            />

            <ItemSeparatorContador />
            <ItemContador titulo="Erwinia" />
            <Contador
              contador={erwinia}
              setContador={seterwinia}
              contadorRestante={contadorRestante}
            />


            <ItemContador titulo="" />
          </>
        ) : null}


<ViewSubtitle
            title="Maleza"
            setEstado={setOcultarViewMaleza}
            estado={ocultarViewMaleza}
            />
            {
              !ocultarViewMaleza ?
              <>
              <ItemSeleccionar
              titulo="Asystasia gangética (Cizaña)"
              icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz dato={cizana} setDato={setCizana} />
              <ItemSeparator />
              
              <ItemSeleccionar
              titulo="R.Cochinchinensis (Caminadora)"
              icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz dato={caminadora} setDato={setCaminadora} />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="E. Digitaria (Pata de Gallina)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={pata_de_gallina}
                setDato={setPata_de_gallina}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Momordica charantia (Pepinillo)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={pepinillo}
                setDato={setPepinillo}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Cyperus luzulae (Cyperus)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={cyperus}
                setDato={setCyperus}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Commelina benghalensis (Golondrina)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={golondrina}
                setDato={setGolondrina}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Agastache rugosa (Bledo)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={bledo}
                setDato={setBledo}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Digitaria sanguinalis (Digitaria)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={digitaria}
                setDato={setDigitaria}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Panicum maximun (Mombaza)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={mombaza}
                setDato={setMombaza}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Solanum torvum (Tomatillo)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={tomatillo}
                setDato={setTomatillo}
              />

              </>
              :null
            }

        <ViewSubtitle
          title="Observaciones"
          setEstado={setOcultarViewObservacion}
          estado={ocultarViewObservacion}
        />
        {!ocultarViewObservacion ? (
          <InputObservacion
            observacion={observacion}
            setObservacion={setObservacion}
          />
        ) : null}
        {editar ? (
          <BotonEnviar enviarFormulario={enviar} titulo="Editar" />
        ) : (
          <BotonEnviar enviarFormulario={enviar} titulo="Enviar" />
        )}

        <ItemContador titulo="" />
      </ScrollView>

      {(seleccionarCedula === undefined || seleccionarCedula === 0) ? (
        <View
          style={{
            ...styles.viewSubSeleccionar,
            height: 80,
            justifyContent: 'center',
            backgroundColor: '#0a0a0a',
          }}>
          <Text style={{...styles.subTitle, color: '#FFF', fontSize: 27}}>
            Seleccione el bloque
          </Text>
        </View>
      ) : (
        <ViewMuestrasRestantes
          validarConteo={validarConteo}
          contadorRestante={contadorRestante}
          incrementarConteo={incrementarConteo}
        />
      )}
    </>
  );
};
