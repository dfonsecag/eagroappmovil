import React, {useEffect, useState} from 'react';
import {
  Alert,
  BackHandler,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/Ionicons';
import SweetAlert from 'react-native-sweet-alert';
import {useCoordenadas} from '../hooks/useCoordenadas';
import {
  getCedulaSeleccionadaEstimacion,
  setearCedulaSeleccionada,
} from '../actions/muestreoMeristemoAction';
import {BotonEnviar} from '../components/BotonEnviar';
import {ItemContador} from '../components/ItemContador';
import {ViewSubtitle} from '../components/ViewSubtitle';
import {ItemSeleccionar} from '../components/ItemSeleccionar';
import {ItemSeparator} from '../components/ItemSeparator';
import {FechaHora} from '../components/FechaHora';
import {Ubicacion} from '../components/Ubicacion';
import {getFecha, getHora} from '../helpers/FechaHora';
import {styles} from '../styles/styles';
import {ItemSeparatorContador} from '../components/ItemSeparatorContador';
import {ContadorEstimacion} from '../components/MuestreoEstimacion/ContadorEstimacion';
import {Hectareaje} from '../components/Hectareaje';
import {InputObservacion} from '../components/InputObservacion';
import {empezarEnvio, enviarFormularioEstimacionAction} from '../actions/muestreoEstimacionAction';
import {PickerSelectEstimacion} from '../components/MuestreoEstimacion/PickerSelectEstimacion';
import { Cargando } from '../components/Cargando';

export const MuestreoEstimacionScreen = ({navigation}) => {
  const dispatch = useDispatch();
  //State ocultar View
  const [ocultarViewCrownless, setOcultarViewCrownless] = useState(false);
  const [ocultarViewRechazo, setOcultarViewRechazo] = useState(false);
  const [ocultarViewContadores, setOcultarViewContadores] = useState(false);
  const [ocultarViewObservacion, setOcultarViewObservacion] = useState(true);
  // Contador General del bloque
  const [contadorRestantes, setContadorRestantes] = useState();
  const [contadorBorrador, setContadorBorrador] = useState(0);
  const [cargarStorage, setCargarStorage] = useState(false);
  const [cargando, setCargando] = useState(true);

  const [seleccionarCedula, setSeleccionarCedula] = useState();
  const [hora_inicio, setHora_inicio] = useState('');

  // State Contadores con Corona
  const [c4, setc4] = useState(0);
  const [c5, setc5] = useState(0);
  const [c6, setC6] = useState(0);
  const [c7, setC7] = useState(0);
  const [c8, setc8] = useState(0);
  const [c9, setC9] = useState(0);
  const [c10, setC10] = useState(0);
  // State Rechazo
  const [menos, setMenos] = useState(0);
  const [hueco, setHueco] = useState(0);
  const [cochinilla, setCochinilla] = useState(0);
  const [natural, setNatural] = useState(0);
  const [fallas, setFallas] = useState(0);
  const [techla, setTechla] = useState(0);
  const [gomosis, setGomosis] = useState(0);
  const [corcho, setCorcho] = useState(0);
  const [d_sol, setD_sol] = useState(0);
  const [desbraquea, setDesbraquea] = useState(0);
  const [fruta_atrazada, setFrutaAtrazada] = useState(0);
  const [conica, setConica] = useState(0);
  const [sombra, setSombra] = useState(0);
  const [roedor, setRoedor] = useState(0);
  const [otros, setOtros] = useState(0);
  // State Contadores sin Corona
  const [cl4, setCl4] = useState(0);
  const [cl5, setCl5] = useState(0);
  const [cl6, setCl6] = useState(0);
  const [cl7, setCl7] = useState(0);
  const [cl8, setCl8] = useState(0);
  const [observacion, setObservacion] = useState();

  // Custom Hook funcionalidades como coordenadas
  const {latitud, longitud, obtenerUbicacion} = useCoordenadas();
  const cedulas = useSelector(
    state => state.muestreoEstimacion.cedulasPendientes,
  );
  const envioExito = useSelector(state => state.muestreoEstimacion.envioExito);
  const loading = useSelector(state => state.muestreoEstimacion.loading);
  const cedula = useSelector(
    state => state.muestreoMeristemo.cedulaSeleccionada,
  );

  const permisos = useSelector(state => state.login.permisos);

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Advertencia', 'Desea salir de la pagina muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'Si',
          onPress: () =>
            navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid}),
        },
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    dispatch(setearCedulaSeleccionada());
    obtenerUbicacion();
    setHora_inicio(getHora());
    setContadorRestantes();
    verificarFormulario();
  }, []);

  useEffect(() => {
    if (seleccionarCedula === 0 || seleccionarCedula === undefined) {
      setContadorRestantes();
    }
    dispatch(getCedulaSeleccionadaEstimacion(seleccionarCedula));
  }, [seleccionarCedula]);

  useEffect(() => {
    if (cedula && cargarStorage) {
      setContadorRestantes(cedula.muestrasxusuario - contadorBorrador);
      setCargarStorage(false);
    } else if (cedula) {
      setContadorRestantes(cedula.muestrasxusuario);
      limpiarContadores();
    }
  }, [cedula]);

  useEffect(() => {
    if (envioExito) {
      BorrarFormularioStorage();
    }
  }, [envioExito]);

  const verificarFormulario = async () => {
    const formularioStorage = await AsyncStorage.getItem(
      'formularioEstimacion',
    );
    if (formularioStorage) {
      Alert.alert(
        'Advertencia !',
        'Existe un formulario pendiente a completar, desea seguir completando ?',
        [
          {
            text: 'Borrarlo',
            onPress: () => BorrarFormularioStorage(),
            style: 'cancel',
          },
          {text: 'Si, Completar', onPress: () => cargarFormularioStorage()},
        ],
      );
    } else {
      setCargando(false);
    }
  };

  const BorrarFormularioStorage = async () => {
    await AsyncStorage.setItem('formularioEstimacion', '');
    setSeleccionarCedula();
    setContadorBorrador(0);
    setContadorRestantes();
    limpiarContadores();
    setCargando(false);
  };

  const guardarStorageFormulario = async () => {
    const data = await obtenerDatosFormulario();
    data.contadorRestantes = contadorRestantes;
    data.contadorBorrador = cedula.muestrasxusuario - contadorRestantes;
    await AsyncStorage.setItem('formularioEstimacion', JSON.stringify(data));
    Alert.alert(
      'Datos guardados exitosamente',
      'Desea salir de la pagina muestreo ?',
      [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'Si',
          onPress: () =>
            navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid}),
        },
      ],
    );
  };

  const cargarFormularioStorage = async () => {
    const formularioStorage = await AsyncStorage.getItem(
      'formularioEstimacion',
    );
    const formulario = JSON.parse(formularioStorage);
    setCargarStorage(true);
    setSeleccionarCedula(formulario.id);
    // setHora_inicio(formulario.hora_inicio);
    setc4(formulario.c4);
    setc5(formulario.c5);
    setC6(formulario.c6);
    setC7(formulario.c7);
    setc8(formulario.c8);
    setC9(formulario.c9);
    setC10(formulario.c10);
    setMenos(formulario.menos);
    setHueco(formulario.hueco);
    setCochinilla(formulario.cochinilla);
    setNatural(formulario.natural);
    setFallas(formulario.fallas);
    setTechla(formulario.techla);
    setGomosis(formulario.gomosis);
    setCorcho(formulario.corcho);
    setD_sol(formulario.d_sol);
    setDesbraquea(formulario.desbraquea);
    setFrutaAtrazada(formulario.fruta_atrazada);
    setConica(formulario.conica);
    setSombra(formulario.sombra);
    setRoedor(formulario.roedor);
    setOtros(formulario.otros);
    setCl4(formulario.cl4);
    setCl5(formulario.cl5);
    setCl6(formulario.cl6);
    setCl7(formulario.cl7);
    setCl8(formulario.cl8);
    setObservacion(formulario.observacion);
    setContadorRestantes(formulario.contadorRestantes);
    setContadorBorrador(formulario.contadorBorrador);
    console.log('Termino de cargar');
    setCargando(false);
  };

  const enviar = () => {
    if (seleccionarCedula === 0 || seleccionarCedula === undefined) {
      SweetAlert.showAlertWithOptions({
        title: 'Seleccionar Bloque',
        subTitle: 'Debes Seleccionar el bloque a muestrear',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    } 
    // else if (contadorRestantes > 0) {
    //   SweetAlert.showAlertWithOptions({
    //     title: 'Aun hay muestras pendientes',
    //     subTitle: 'Debes de completar las muestras del bloque',
    //     confirmButtonTitle: 'OK',
    //     confirmButtonColor: '#000',
    //     otherButtonTitle: 'Cancel',
    //     otherButtonColor: '#dedede',
    //     style: 'error',
    //     cancellable: true,
    //   });
    // } 
    else {
      Alert.alert('Advertencia', 'Desea enviar el muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'Si', onPress: () => enviarFormulario()},
      ]);
    }
  };

  const obtenerDatosFormulario = async () => {
    await obtenerUbicacion();
    const dataForm = {
      id: seleccionarCedula,
      fmuestreo: `${getFecha()} ${getHora()}`,
      hora_inicio: `${getFecha()} ${hora_inicio}`,
      hora_fin: `${getFecha()} ${getHora()}`,
      // plantasmuestreadas: (cedula.muestrasxusuario + Math.abs(contadorRestantes)),
      plantasmuestreadas: ( c4+c5+c6+c7+c8+c9+c10+cl4+cl5+cl6+cl7+cl8+menos+hueco+cochinilla+natural+fallas+techla+gomosis+corcho+
      d_sol+desbraquea+fruta_atrazada+conica+sombra+roedor+otros),
      latitud,
      longitud,
      estado: true,
      c4,
      c5,
      c6,
      c7,
      c8,
      c9,
      c10,
      // State Contadores sin Corona
      cl4,
      cl5,
      cl6,
      cl7,
      cl8,
      // State Rechazo
      menos,
      hueco,
      cochinilla,
      natural,
      fallas,
      techla,
      gomosis,
      corcho,
      d_sol,
      desbraquea,
      fruta_atrazada,
      conica,
      sombra,
      roedor,
      otros,
      observacion,
    };
    // alert(dataForm.plantasmuestreadas)
    return dataForm;
  };

  const enviarFormulario = async () => {
    await dispatch(empezarEnvio())
    await obtenerUbicacion();
    const dataForm = await obtenerDatosFormulario();
    await dispatch(
      enviarFormularioEstimacionAction(dataForm, seleccionarCedula),
    );
  };
  const limpiarContadores = async () => {
    setHora_inicio(getHora());
    setc4(0);
    setc5(0);
    setC6(0);
    setC7(0);
    setc8(0);
    setC9(0);
    setC10(0);
    setMenos(0);
    setHueco(0);
    setCochinilla(0);
    setNatural(0);
    setFallas(0);
    setTechla(0);
    setGomosis(0);
    setCorcho(0);
    setD_sol(0);
    setDesbraquea(0);
    setFrutaAtrazada(0);
    setConica(0);
    setSombra(0);
    setRoedor(0);
    setOtros(0);
    setCl4(0);
    setCl5(0);
    setCl6(0);
    setCl7(0);
    setCl8(0);
    setObservacion();
  };

  return (
    <>
      {cargando ? (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator size={100} color="black" />
        </View>
      ) : (
        <>
          <ScrollView>
            <View style={{borderBottomWidth: 6, opacity: 0.2}}></View>
            <View style={styles.viewSubTitle}>
              <Text style={styles.subTitle}> Datos Básicos </Text>
              <View style={{flex: 1}} />
            </View>
            <ItemSeleccionar titulo="Cédulas" icono="reorder-four-outline" />

            <PickerSelectEstimacion
              seleccionarCedula={seleccionarCedula}
              setSeleccionarCedula={setSeleccionarCedula}
              cedulas={cedulas}
            />

            {cedula ? (
              <>
                <ItemSeparator />
                <Hectareaje area={cedula.area} />
              </>
            ) : null}
            <ItemSeparator />

            <FechaHora fecha={getFecha()} hora={getHora()} />
            <ItemSeparator />

            <Ubicacion latitud={latitud} longitud={longitud} />
            {cedula ? (
              <>
                <ItemSeparator />
                <View style={{...styles.viewSubSeleccionar, height: 65}}>
                  <View style={{justifyContent: 'center'}}>
                    <Icon name="checkmark-outline" size={35} color="black"/>
                  </View>
                  <Text style={{...styles.subTitle, fontSize: 25}}>
                    Plantas totales: {cedula.muestras}
                  </Text>
                </View>
              </>
            ) : null}

            <ViewSubtitle
              title="Conteo Calibres"
              setEstado={setOcultarViewContadores}
              estado={ocultarViewContadores}
            />
            {!ocultarViewContadores ? (
              <>
                <ItemContador titulo="4s" />
                <ContadorEstimacion
                  contador={c4}
                  setContador={setc4}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="5s" />
                <ContadorEstimacion
                  contador={c5}
                  setContador={setc5}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="6s" />
                <ContadorEstimacion
                  contador={c6}
                  setContador={setC6}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="7s" />
                <ContadorEstimacion
                  contador={c7}
                  setContador={setC7}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="8s" />
                <ContadorEstimacion
                  contador={c8}
                  setContador={setc8}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="9s" />
                <ContadorEstimacion
                  contador={c9}
                  setContador={setC9}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="10s" />
                <ContadorEstimacion
                  contador={c10}
                  setContador={setC10}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
              </>
            ) : null}

            {/* Motivos de rechazo */}
            <ViewSubtitle
              title="Motivos Rechazo"
              setEstado={setOcultarViewRechazo}
              estado={ocultarViewRechazo}
            />
            {!ocultarViewRechazo ? (
              <>
                <ItemContador titulo="Menos" />
                <ContadorEstimacion
                  contador={menos}
                  setContador={setMenos}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                
                  <ItemSeparatorContador />
                  <ItemContador titulo="Hueco" />
                  <ContadorEstimacion
                      contador={hueco}
                      setContador={setHueco}
                      contadorRestantes={contadorRestantes}
                      setContadorRestantes={setContadorRestantes}
                    />
                <ItemSeparatorContador />
                <ItemContador titulo="Cochinilla" />
                <ContadorEstimacion
                  contador={cochinilla}
                  setContador={setCochinilla}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="Natural" />
                <ContadorEstimacion
                  contador={natural}
                  setContador={setNatural}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="Fallas" />
                <ContadorEstimacion
                  contador={fallas}
                  setContador={setFallas}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="Techla" />
                <ContadorEstimacion
                  contador={techla}
                  setContador={setTechla}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="Gomosis" />
                <ContadorEstimacion
                  contador={gomosis}
                  desbraquea
                  setContador={setGomosis}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="Corcho" />
                <ContadorEstimacion
                  contador={corcho}
                  setContador={setCorcho}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="Quema de Sol" />
                <ContadorEstimacion
                  contador={d_sol}
                  setContador={setD_sol}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="Desbraquea" />
                <ContadorEstimacion
                  contador={desbraquea}
                  setContador={setDesbraquea}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo=" Fruta Atrazada" />
                <ContadorEstimacion
                  contador={fruta_atrazada}
                  setContador={setFrutaAtrazada}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="Cónica" />
                <ContadorEstimacion
                  contador={conica}
                  setContador={setConica}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="Sombra" />
                <ContadorEstimacion
                  contador={sombra}
                  setContador={setSombra}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="Daño Roedor" />
                <ContadorEstimacion
                  contador={roedor}
                  setContador={setRoedor}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="Otros" />
                <ContadorEstimacion
                  contador={otros}
                  setContador={setOtros}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
              </>
            ) : null}

            <ViewSubtitle
              title="Crownless"
              setEstado={setOcultarViewCrownless}
              estado={ocultarViewCrownless}
            />
            {!ocultarViewCrownless ? (
              <>
                <ItemContador titulo="4sl" />
                <ContadorEstimacion
                  contador={cl4}
                  setContador={setCl4}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="5sl" />
                <ContadorEstimacion
                  contador={cl5}
                  setContador={setCl5}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="6sl" />
                <ContadorEstimacion
                  contador={cl6}
                  setContador={setCl6}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="7sl" />
                <ContadorEstimacion
                  contador={cl7}
                  setContador={setCl7}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
                <ItemContador titulo="8sl" />
                <ContadorEstimacion
                  contador={cl8}
                  setContador={setCl8}
                  contadorRestantes={contadorRestantes}
                  setContadorRestantes={setContadorRestantes}
                />
                <ItemSeparatorContador />
              </>
            ) : null}
            <ViewSubtitle
              title="Observaciones"
              setEstado={setOcultarViewObservacion}
              estado={ocultarViewObservacion}
            />
            {!ocultarViewObservacion ? (
              <InputObservacion
                observacion={observacion}
                setObservacion={setObservacion}
              />
            ) : null}         
            {
              !loading ? (
                <BotonEnviar enviarFormulario={enviar} />                
              ) :
              (
                <Cargando/>
              )
            }
            <ItemContador titulo="" />
          </ScrollView>
          <View
            style={{
              ...styles.viewSubSeleccionar,
              height: 80,
              justifyContent: 'center',
              backgroundColor: '#0a0a0a',
            }}>
            {contadorRestantes || contadorRestantes === 0 ? (
              <>
                <TouchableOpacity onPress={() => guardarStorageFormulario()}>
                  <View style={{marginTop: 17}}>
                    <Icon name="save-outline" color="white" size={40} />
                  </View>
                </TouchableOpacity>
                <Text style={{...styles.subTitle, color: '#FFF', fontSize: 26}}>
                  Plantas {contadorRestantes >= 0 ? 'Restantes':'Adicionales'}: {Math.abs(contadorRestantes)}
                </Text>
              </>
            ) : (
              <Text style={{...styles.subTitle, color: '#FFF', fontSize: 27}}>
                Seleccione el bloque
              </Text>
            )}
          </View>
        </>
      )}
    </>
  );
};
