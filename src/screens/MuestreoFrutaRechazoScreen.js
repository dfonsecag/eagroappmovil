import React, {useEffect, useState} from 'react';
import {
  Alert,
  BackHandler,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import {BotonEnviar} from '../components/BotonEnviar';
import {ItemContador} from '../components/ItemContador';
import {getFecha, getHora} from '../helpers/FechaHora';
import {styles} from '../styles/styles';
import { Contador } from '../components/MuestreoRechazoFruta/Contador';
import { enviarFormulario, enviarFormularioRechazoFrutaAction, obtenerBloquesAction, obtenerLotesAction } from '../actions/rechazoFrutaAction';
import { Select } from '../components/MuestreoRechazoFruta/Select';
import { mostrarAlerta } from '../helpers/MostrarAlerta';
import { Fecha } from '../components/Fecha';
import { Cargando } from '../components/Cargando';

export const MuestreoFrutaRechazoScreen = ({navigation}) => {

  const dispatch = useDispatch();

  const permisos = useSelector(state => state.login.permisos);
  const lotes = useSelector(state => state.muestreoRechazoFruta.lotes);
  const bloques  = useSelector(state => state.muestreoRechazoFruta.bloques );
  const envioExito  = useSelector(state => state.muestreoRechazoFruta.envioExito );
  const loading  = useSelector(state => state.muestreoRechazoFruta.loading );

  //Ocultar View
  const [ocultarViewEnviar, setOcultarViewEnviar] = useState(true);

  const [loteid, setLoteid] = useState(0);
  const [bloqueid, setBloqueid] = useState(0);
  const [dano_roedor, setdano_roedor] = useState(0);
  const [cochinilla, setcochinilla] = useState(0);
  const [gomosis, setgomosis] = useState(0);
  const [thecla, setthecla] = useState(0);
  const [color_alto, setcolor_alto] = useState(0);
  const [fruta_deforme, setfruta_deforme] = useState(0);
  const [natural, setnatural] = useState(0);
  const [fruta_cronica, setfruta_cronica] = useState(0);
  const [fruta_con_cuello, setfruta_con_cuello] = useState(0);
  const [cicatriz_criple, setcicatriz_criple] = useState(0);
  const [oxidacion_pedunculo, setoxidacion_pedunculo] = useState(0);
  const [quema_sol, setquema_sol] = useState(0);
  const [sombra, setsombra] = useState(0);
  const [mancha_basal, setmancha_basal] = useState(0);
  const [corchosis, setcorchosis] = useState(0);
  const [corona_grande, setcorona_grande] = useState(0);
  const [corona_torcida, setcorona_torcida] = useState(0);
  const [corona_doble, setcorona_doble] = useState(0);
  const [corona_multiple, setcorona_multiple] = useState(0);
  const [corona_con_dano, setcorona_con_dano] = useState(0);
  const [sin_corona, setsin_corona] = useState(0);
  const [corona_pequena, setcorona_pequena] = useState(0);
  const [corona_fasciolada, setcorona_fasciolada] = useState(0);
  const [corona_con_espinas, setcorona_con_espinas] = useState(0);
  const [traslucidez_alta, settraslucidez_alta] = useState(0);
  const [traslucidez_baja, settraslucidez_baja] = useState(0);
  const [gople_agua, setgople_agua] = useState(0);
  const [color_bajo, setcolor_bajo] = useState(0);
  const [fruta_sucia, setfruta_sucia] = useState(0);
  const [fruta_pequena, setfruta_pequena] = useState(0);
  const [fruta_grande, setfruta_grande] = useState(0);
  const [fruta_con_golpe, setfruta_con_golpe] = useState(0);
  const [c8, setc8] = useState(0);
  const [c9, setc9] = useState(0);
  const [c10, setc10] = useState(0);
  const [radiacion, setRadiacion] = useState(0);

  const [fecha, setFecha] = useState(getFecha());
  const [isVisible, setIsVisible] = useState(false);
  
  const [cantidad, setCantidad] = useState(0);
  
  // Obtener Lotes
    useEffect(() => {
      dispatch(obtenerLotesAction(fecha))
    }, [fecha]);

    // Obtener Bloques
  useEffect(() => {
    dispatch(obtenerBloquesAction(fecha,loteid))
  }, [loteid]);

   // Setear Datos
   useEffect(() => {
   if(envioExito){
    setearValores()
   }
  }, [envioExito]);

  const setearValores = () => {
    setCantidad(0);
    setLoteid(0);
   setBloqueid(0);  
    setdano_roedor(0);
    setcochinilla(0);
    setgomosis(0);
    setthecla(0);
    setcolor_alto(0);
    setfruta_deforme(0);
    setnatural(0);
    setfruta_cronica(0);
    setfruta_con_cuello(0);
    setcicatriz_criple (0);
     setoxidacion_pedunculo(0);
     setquema_sol(0);
     setsombra(0);
     setmancha_basal(0);
     setcorchosis(0);
     setcorona_grande(0);
     setcorona_torcida(0);
     setcorona_doble(0);
     setcorona_multiple(0);
     setcorona_con_dano(0);
     setsin_corona(0);
     setcorona_pequena(0);
     setcorona_fasciolada(0);
     setcorona_con_espinas(0);
     settraslucidez_alta(0);
     settraslucidez_baja(0);
     setgople_agua(0);
     setcolor_bajo(0);
     setfruta_sucia(0);
     setfruta_pequena(0);
     setfruta_grande(0);
     setfruta_con_golpe(0);
     setc8(0);
     setc9(0);
     setc10(0);
     setRadiacion(0)
  
  }

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Advertencia', 'Desea salir de la página muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'Si',
          onPress: () =>
            navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid}),
        },
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const enviar = async() => {
    if(loteid == 0)
      mostrarAlerta('Verificar lote','Debes seleccionar el lote','error')
    else if(bloqueid == 0)
      mostrarAlerta('Verificar bloque','Debes seleccionar el bloque','error')
    else {
      await dispatch(enviarFormulario())
      const data = {
      loteid	,
      bloqueid	,
      fecha	: `${fecha} ${getHora()}`,
      dano_roedor	,
      cochinilla	,
      gomosis	,
      thecla	,
      color_alto	,
      fruta_deforme	,
      natural	,
      fruta_cronica	,
      fruta_con_cuello	,
      cicatriz_criple	,
      oxidacion_pedunculo	,
      quema_sol	,
      sombra	,
      mancha_basal	,
      corchosis	,
      corona_grande	,
      corona_torcida	,
      corona_doble	,
      corona_multiple	,
      corona_con_dano	,
      sin_corona	,
      corona_pequena	,
      corona_fasciolada	,
      corona_con_espinas	,
      traslucidez_alta	,
      traslucidez_baja	,
      gople_agua	,
      color_bajo	,
      fruta_sucia	,
      fruta_pequena	,
      fruta_grande	,
      fruta_con_golpe	,
      c8	,
      c9	,
      c10,
      radiacion	}
      // alert('Quitar')
      await dispatch(enviarFormularioRechazoFrutaAction(data))
    }
  }
  

  return (
    <>
      
          <ScrollView>
          <View style={estilo.viewFecha}>
             <Fecha isVisible={isVisible} setIsVisible={setIsVisible} fecha={fecha} setFecha={setFecha}/>  
          </View>
          <View style={{...styles.viewCuerpoContador, height:80}}>          
          <View style={estilo.viewSelect}>
             <Select data={lotes} setValue={setLoteid} value={loteid} label='LOTES'/>
          </View>
          <View style={estilo.viewSelect}>
            <Select data={bloques} setValue={setBloqueid} value={bloqueid} label='BLOQUES'/>
          </View>
          </View>

          <View style={{...styles.viewCuerpoContador, height:80, paddingTop:20}}>
            <Contador label='PEQUEÑA' contador={fruta_pequena} setContador={setfruta_pequena} muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='DAÑO ROEDOR' contador={dano_roedor} setContador={setdano_roedor} muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='GOMOSIS' contador={gomosis} setContador={setgomosis} muestras={cantidad} setMuestras={setCantidad}/>
         </View>
         <View style={{...styles.viewCuerpoContador, height:80, paddingTop:20}}>
            <Contador label='TECHLA' contador={thecla} setContador={setthecla} muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='COCHINILLA' contador={cochinilla} setContador={setcochinilla} muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='COLOR ALTO' contador={color_alto} setContador={setcolor_alto} muestras={cantidad} setMuestras={setCantidad}/>
         </View>
         <View style={{...styles.viewCuerpoContador, height:80, paddingTop:20}}>
            <Contador label='SIN CORONA' contador={sin_corona} setContador={setsin_corona} muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='FRUTA DEFORME' contador={fruta_deforme} setContador={setfruta_deforme} muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='CICATRIZ / CRIPLE' contador={cicatriz_criple} setContador={setcicatriz_criple} muestras={cantidad} setMuestras={setCantidad}/>
         </View>
         <View style={{...styles.viewCuerpoContador, height:80, paddingTop:20}}>
            <Contador label='QUEMA SOL' contador={quema_sol} setContador={setquema_sol} muestras={cantidad} setMuestras={setCantidad} />
            <Contador label='SOMBRA' contador={sombra} setContador={setsombra } muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='CORCHOSIS' contador={corchosis} setContador={setcorchosis} muestras={cantidad} setMuestras={setCantidad}/>
         </View>
         <View style={{...styles.viewCuerpoContador, height:80, paddingTop:20}}>
            <Contador label='FRUTA CÓNICA' contador={fruta_cronica} setContador={setfruta_cronica} muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='MANCHA BASAL' contador={mancha_basal} setContador={setmancha_basal} muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='CORONA TORCIDA' contador={corona_torcida} setContador={setcorona_torcida} muestras={cantidad} setMuestras={setCantidad}/>
         </View>
         <View style={{...styles.viewCuerpoContador, height:80, paddingTop:20}}>
            <Contador label='CORONA DOBLE' contador={corona_doble} setContador={setcorona_doble} muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='CORONA MULTIPLE' contador={corona_multiple} setContador={setcorona_multiple} muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='CORONA PEQUEÑA' contador={corona_pequena} setContador={setcorona_pequena}  muestras={cantidad} setMuestras={setCantidad}/>
         </View>
         <View style={{...styles.viewCuerpoContador, height:80, paddingTop:20}}>
            <Contador label='CORONA GRANDE' contador={corona_grande} setContador={setcorona_grande}  muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='GRANDE' contador={fruta_grande} setContador={setfruta_grande}  muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='FRUTA CON GOLPE' contador={fruta_con_golpe} setContador={setfruta_con_golpe}  muestras={cantidad} setMuestras={setCantidad}/>
         </View>
         <View style={{...styles.viewCuerpoContador, height:80, paddingTop:20}}>
            <Contador label='CALIBRE 8' contador={c8} setContador={setc8}  muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='CALIBRE 9' contador={c9} setContador={setc9}  muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='CALIBRE 10' contador={c10} setContador={setc10}  muestras={cantidad} setMuestras={setCantidad}/>
         </View>
         <View style={{...styles.viewCuerpoContador, height:80, paddingTop:20}}>
            <Contador label='NATURAL' contador={natural} setContador={setnatural}  muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='CORONA SUCIA' contador={corona_con_dano} setContador={setcorona_con_dano}  muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='CORONA FASCIOLADA' contador={corona_fasciolada} setContador={setcorona_fasciolada}  muestras={cantidad} setMuestras={setCantidad}/>
         </View>
         <View style={{...styles.viewCuerpoContador, height:80, paddingTop:20}}>
            <Contador label='CORONA ESPINAS' contador={corona_con_espinas} setContador={setcorona_con_espinas}  muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='TRANSLUCIDEZ ALTA' contador={traslucidez_alta} setContador={settraslucidez_alta}  muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='TRANSLUCIDEZ BAJA' contador={traslucidez_baja} setContador={settraslucidez_baja}  muestras={cantidad} setMuestras={setCantidad}/>
         </View>
         <View style={{...styles.viewCuerpoContador, height:80, paddingTop:20}}>
            <Contador label='GOLPE AGUA' contador={gople_agua} setContador={setgople_agua}  muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='COLOR BAJO' contador={color_bajo} setContador={setcolor_bajo}  muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='FRUTA CON CUELLO' contador={fruta_con_cuello} setContador={setfruta_con_cuello}  muestras={cantidad} setMuestras={setCantidad}/>
         </View>
         <View style={{...styles.viewCuerpoContador, height:80, paddingTop:20}}>
            <Contador label='FRUTA SUCIA' contador={fruta_sucia} setContador={setfruta_sucia}  muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='OXIDACIÓN ' contador={oxidacion_pedunculo} setContador={setoxidacion_pedunculo}  muestras={cantidad} setMuestras={setCantidad}/>
            <Contador label='RADIACIÓN ' contador={radiacion} setContador={setRadiacion}  muestras={cantidad} setMuestras={setCantidad}/>
         </View>
       
         <View style={{height:30}}/>
         <View style={{...styles.viewSubTitle,backgroundColor:'black'}}>
    <Text style={{...styles.subTitle, color:'white'}}> ENVIAR FORMULARIO </Text>
    <View style={{flex: 1}} />
    <TouchableOpacity onPress={() => setOcultarViewEnviar(!ocultarViewEnviar)}>
      <View style={{justifyContent: 'center'}}>
        {
          (!ocultarViewEnviar) ?
          ( <Icon name="remove-outline" size={50} color='white'/>):
          ( <Icon name="add-outline" size={50} color='white'/>)
        }
       
      </View>
    </TouchableOpacity>
  </View>

        {
          (!ocultarViewEnviar && !loading) && (
            <BotonEnviar enviarFormulario={()=> enviar()} titulo="Enviar" />
          )
        }

        {
          (!ocultarViewEnviar && loading) && (
            <Cargando/>
          )
        }

            <ItemContador titulo="" />
          </ScrollView>

          <View
        style={{
          ...styles.viewSubSeleccionar,
          height: 80,
          justifyContent: 'center',
          backgroundColor: '#0a0a0a',
        }}>
        
            <Text style={{...styles.subTitle, color: '#FFF', fontSize: 30}}>
             Frutas Muestreadas:  {cantidad}
            </Text>
       
      </View>
        
    </>
  );
};


const estilo = StyleSheet.create({
 
  viewSelect:{
    paddingHorizontal: 2,
    paddingVertical: 5,
    height: 70,
    width: '50%',
  },

  viewFecha:{
    flexDirection: 'row',
    paddingHorizontal: 2,
    paddingVertical: 5,
    height: 70,
    width: '100%',
    paddingTop:5
  }
});
