import React, {useEffect, useState} from 'react';
import {Picker} from '@react-native-picker/picker';
import SweetAlert from 'react-native-sweet-alert';

import {
  Row,
  Table,
} from 'react-native-table-component';

import {
  Alert,
  BackHandler,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import {
  enviarFormularioAction,
  getCedulaSeleccionada,
} from '../actions/avanceObraAction';
import {BotonEnviar} from '../components/BotonEnviar';
import {Hora} from '../components/Hora';
import {InputObservacion} from '../components/InputObservacion';
import {ItemContador} from '../components/ItemContador';
import {ItemSeleccionar} from '../components/ItemSeleccionar';
import {ItemSeparator} from '../components/ItemSeparator';
import {PickerSelectMaquinaria} from '../components/PickerSelectMaquinaria';
import {RadioButtonClima} from '../components/RadioButtonClima';
import {ViewSubtitle} from '../components/ViewSubtitle';
import {getFecha} from '../helpers/FechaHora';
import {styles} from '../styles/styles';
import {DropdownAvanceobra} from '../components/AvanceObra/DropdownAvanceobra';
import CheckBox from '@react-native-community/checkbox';

export const AvanceObraScreen = ({navigation}) => {
  const dispatch = useDispatch();

  const cedulas = useSelector(state => state.avanceObra.cedulasPendientes);
  const implementos = useSelector(state => state.avanceObra.implementos);
  const envioExito = useSelector(state => state.avanceObra.envioExito);
  const permisos = useSelector(state => state.login.permisos);
  const cedula = useSelector(state => state.avanceObra.cedulaSeleccionada);
  const boquillas = useSelector(state => state.avanceObra.boquillas);
  const maquinaria = useSelector(state => state.avanceObra.maquinaria);
  const avances = useSelector(state => state.avanceObra.avances);
  // Ocultar View
  const [ocultarViewObservacion, setOcultarViewObservacion] = useState(true);
  const [ocultarViewAvances, setOcultarViewAvances] = useState(true);

  //   State
  const [seleccionarCedula, setSeleccionarCedula] = useState(0);
  const [maquinaid, setMaquinaId] = useState(0);
  const [maquinaimplementoid, setImplementoId] = useState(0);
  const [tipoboquillaid, setTipoboquillaid] = useState(0);
  const [clima, setClima] = useState('SOLEADO');
  const [horainicio, setHoraInicio] = useState('--:--');
  const [horafinal, setHoraFinal] = useState('--:--');
  const [observacion, setObservacion] = useState('');
  const [horimetroinicial, setHorimetroinicial] = useState();
  const [horimetrofinal, setHorimertrofinal] = useState();
  const [psi, setPsi] = useState();
  const [rpm, setRpm] = useState();
  const [avance_litros, setAvance_litros] = useState('');
  const [pendiente, setPediente] = useState(false);
  // cargar avances de la cedula 
  const [data, setData] = useState([]);
  const [totalLitros, setTotalLitros] = useState(0);

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Advertencia', 'Desea salir de la pagina avance de obra ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'Si',
          onPress: () =>
            navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid}),
        },
      ]);
      return true;

    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    dispatch(getCedulaSeleccionada(seleccionarCedula));
  }, [seleccionarCedula]);
  
  const tableData = [];
  useEffect(() => {
    if (avances) {
      let total = 0
      for (let i = 0; i < avances.length; i += 1) {
        const rowData = [];
        rowData.push(avances[i].fecha);
        rowData.push(avances[i].inicio);
        rowData.push(avances[i].fin);
        rowData.push(avances[i].avance_litros);
        rowData.push(avances[i].nombre);
        total += parseInt(avances[i].avance_litros)
        tableData.push(rowData);
      }
      setData(tableData);
      setTotalLitros(total)
    }
  }, [avances]);

  useEffect(() => {
    if (envioExito) {
      setSeleccionarCedula(0);
      setMaquinaId(0);
      setImplementoId(0);
      setTipoboquillaid(0);
      setClima('SOLEADO');
      setHoraInicio('--:--');
      setHoraFinal('--:--');
      setObservacion('');
      setHorimetroinicial();
      setHorimertrofinal();
      setPsi();
      setRpm();
      setAvance_litros('');
      setPediente(false);
      setOcultarViewAvances(true);
      setOcultarViewObservacion(true);
    }
  }, [envioExito]);

  const enviar = () => {
    if (seleccionarCedula === 0 || seleccionarCedula === undefined) {
      SweetAlert.showAlertWithOptions({
        title: 'Seleccionar Cedula',
        subTitle: 'Debes selecccionar la cedula',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    } else if (
      maquinaid === 0 ||
      maquinaimplementoid === 0 ||
      tipoboquillaid === 0 ||
      horainicio === '--:--' ||
      horafinal === '--:--' ||
      horimetroinicial === undefined ||
      horimetrofinal === undefined ||
      psi === undefined ||
      rpm === undefined ||
      avance_litros === ''

    ) {
      SweetAlert.showAlertWithOptions({
        title: 'Faltan datos por ingresar',
        subTitle: 'Verifique que haya ingresado todos los datos',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    } else {
      Alert.alert('Advertencia', 'Desea enviar la cédula ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'Si', onPress: () => enviarFormulario()},
      ]);
    }
  };

  const enviarFormulario = async () => {
    let estado_cedula;
    if(!pendiente){
      estado_cedula = 9;
    } else{
      estado_cedula = 3;
    }

    const dataForm = {
      cedulaaplicaciongrupoid: cedula.cedulaaplicaciongrupoid,
      numerocedula: cedula.numerocedula,
      clima,
      horainicio: `${getFecha()} ${horainicio}`,
      horafinal: `${getFecha()} ${horafinal}`,
      observacion,
      horimetroinicial,
      horimetrofinal,
      psi,
      rpm,
      maquinaid,
      maquinaimplementoid,
      tipoboquillaid,
      fcreacion: getFecha(),
      timestamp: `${getFecha()} ${horainicio}`,
      // Verificar estos datos
      // empleadosxcompaniaautorizaid: 4521,
      // empleadosxcompaniaentregaid: 131,
      // empleadosxcompaniasupervisaid: 5323,
      choferid: permisos.choferid,
      creadopor: permisos.nombre,
      avance_litros: parseInt(avance_litros),
      estadoid:estado_cedula
    };
    // console.log(dataForm)
   await dispatch(enviarFormularioAction(dataForm, seleccionarCedula, estado_cedula));
  };

  return (
    <View>
      <ItemSeleccionar titulo="Cédulas" icono="reorder-four-outline" />
      <DropdownAvanceobra
        seleccionarCedula={seleccionarCedula}
        setSeleccionarCedula={setSeleccionarCedula}
        cedulas={cedulas}
      />
      <ItemSeparator />
      <ScrollView>
        {!cedula ? (
          <>
            <View style={{...styles.viewSubSeleccionar, height: 60}}>
              <View style={{justifyContent: 'center'}}>
                <Icon name="close-outline" size={35} />
              </View>
              <Text style={{...styles.subTitle, fontSize: 22}}>
                Seleccione una cédula
              </Text>
            </View>
          </>
        ) : (
          <>
            <View style={{...styles.viewSubSeleccionar, height: 60}}>
              <View style={{justifyContent: 'center'}}>
                <Icon name="document-outline" size={35} />
              </View>
              <Text style={{...styles.subTitle, fontSize: 22}}>
                Cédula: {cedula.numerocedula}
              </Text>
            </View>
           
            <ItemSeparator />
            <View style={{...styles.viewSubSeleccionar, height: 65}}>
              <View style={{justifyContent: 'center'}}>
                <Icon name="information-circle-outline" size={35} />
              </View>

              <Text style={{...styles.subTitle, fontSize: 22}}>
               Lote: {cedula.numerolote}    Grupo: {cedula.codigo ? cedula.codigo : 'N/A'}
              </Text>
            </View>
            <ItemSeparator />
            <View style={{...styles.viewSubSeleccionar, height: 65}}>
              <View style={{justifyContent: 'center'}}>
                <Icon name="information-circle-outline" size={35} />
              </View>

              <Text style={{...styles.subTitle, fontSize: 22}}>
                {cedula.descripcion}
              </Text>
            </View>
          </>
        )}
        <ItemSeparator />
        <ViewSubtitle
          title="Avances"
          setEstado={setOcultarViewAvances}
          estado={ocultarViewAvances}
        />
        {!ocultarViewAvances ? 
        <>
          {
            data.length > 0 ?
            (
              <>
              <View style={tabla.container}>
                 <ScrollView horizontal={true}>
                  <View>
                    <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}}>
                      <Row
                        data={['Fecha','Inicio', 'Fin','Litros','Operador']}
                        widthArr={[90,90,90,90, 180]}
                        style={tabla.header}
                        textStyle={tabla.textheader}
                      />
                    </Table>
                    <ScrollView style={tabla.dataWrapper}>
                      <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}}>
                        {data.map((rowData, index) => (
                          <Row
                            key={index}
                            data={rowData}
                            onPress={() => console.log(rowData[1])}
                            widthArr={[90,90,90,90, 180]}
                            // style={[tabla.row, (rowData[9] > 5 && rowData[9] < 21 ) ? ({backgroundColor: '#FDFD96'}) : (rowData[9] > 20 ) ? ({backgroundColor: '#ff6961'}) : ({backgroundColor: '#8BC34A'})]}
                            style={[tabla.row, index%2 && {backgroundColor: '#E0E0E0'}]}
                            textStyle={tabla.text}
                          />
                        ))}
                        <Row
                        data={['Total','', '',totalLitros,'Operador']}
                        widthArr={[90,90,90,90, 180]}
                        style={tabla.header}
                        textStyle={tabla.textheader}
                      />
                      </Table>
                    </ScrollView>
                  </View>
                </ScrollView> 
              </View>
            </>
            )
            :
            (
              <View style={{...styles.viewSubSeleccionar, height: 75}}>
              <View style={{justifyContent: 'center'}}>
                <Icon name="information-circle-outline" size={40} />
              </View>
              <Text style={{...styles.subTitle, fontSize: 26}}>
                No hay avances
              </Text>
            </View>
            )
          }
          </>
         
        : null}

        <ItemSeparator />
        <ItemSeleccionar titulo="Condición Climatica" icono="cloud-outline" />
        {/* <PickerCondicionClima clima={clima} setClima={setClima}/> */}
        <RadioButtonClima clima={clima} setClima={setClima} />
        <ItemSeparator />
        <ItemSeleccionar titulo="Maquinaria" icono="reorder-four-outline" />
        <PickerSelectMaquinaria
          seleccionarCedula={maquinaid}
          setSeleccionarCedula={setMaquinaId}
          cedulas={maquinaria}
          tipo={`la maquinaria`}
        />
        <ItemSeparator />
        <ItemSeleccionar titulo="Implemento" icono="reorder-four-outline" />
        <PickerSelectMaquinaria
          seleccionarCedula={maquinaimplementoid}
          setSeleccionarCedula={setImplementoId}
          cedulas={implementos}
          tipo={`el implemento`}
        />
        <ItemSeparator />
        <ItemSeleccionar titulo="Tipo Boquilla" icono="reorder-four-outline" />

        <Picker
          selectedValue={tipoboquillaid}
          onValueChange={itemValue => setTipoboquillaid(itemValue)}>
          <Picker.Item
            style={{fontSize: 22, fontWeight: 'bold'}}
            label={' -- Seleccione la boquilla --'}
            value={0}
            key={0}
          />
          {boquillas
            ? boquillas.map(c => (
                <Picker.Item
                  style={{fontSize: 22}}
                  label={`   **  ${c.codigo} `}
                  value={c.tipoboquillaid}
                  key={c.tipoboquillaid}
                />
              ))
            : null}
        </Picker>

        <ItemSeparator />

        <Hora
          horainicio={horainicio}
          setHoraInicio={setHoraInicio}
          horafinal={horafinal}
          setHoraFinal={setHoraFinal}
        />

        <ItemSeparator />
        <ItemSeleccionar titulo="Horimetro" icono="reorder-four-outline" />
        <View style={styles.viewSubSeleccionar}>
          <View style={{justifyContent: 'center'}}>
            <Icon
              style={{marginTop: 10, marginLeft: 45}}
              name="time-outline"
              size={32}
            />
          </View>

          <TextInput
            placeholder="Inicio"
            keyboardType="phone-pad"
            underlineColorAndroid="black"
            style={styles.inputHora}
            selectionColor="black"
            onChangeText={value => {
              let numero = value.replace(/[^0-9\.]/g, '');
              setHorimetroinicial(numero);
            }}
            value={horimetroinicial}
            autoCapitalize="none"
            autoCorrect={false}
          />

          <View style={{justifyContent: 'center', marginLeft: 90}}>
            <Icon style={{marginTop: 5}} name="time-outline" size={32} />
          </View>

          <TextInput
            placeholder="Final"
            keyboardType="phone-pad"
            underlineColorAndroid="black"
            style={styles.inputHora}
            selectionColor="black"
            onChangeText={value => {
              let numero = value.replace(/[^0-9\.]/g, '');
              setHorimertrofinal(numero);
            }}
            value={horimetrofinal}
            autoCapitalize="none"
            autoCorrect={false}
          />
        </View>
        <ItemSeparator />
        <View style={styles.viewSubSeleccionar}>
          <View style={{justifyContent: 'center'}}>
            <Icon name="stopwatch-outline" size={35} />
          </View>
          <Text style={styles.subTitle}> PSI: </Text>
          <TextInput
            placeholder="Presion: 0"
            keyboardType="phone-pad"
            underlineColorAndroid="black"
            style={styles.inputHora}
            selectionColor="black"
            onChangeText={value => {
              let numero = value.replace(/[^0-9]/g, '');
              setPsi(numero);
            }}
            value={psi}
            autoCapitalize="none"
            autoCorrect={false}
          />
        </View>
            
        <ItemSeparator />
        <View style={styles.viewSubSeleccionar}>
          <View style={{justifyContent: 'center'}}>
            <Icon name="speedometer-outline" size={35} />
          </View>
          <Text style={styles.subTitle}> RPM: </Text>
          <TextInput
            placeholder="RPM: 0"
            keyboardType="phone-pad"
            underlineColorAndroid="black"
            style={styles.inputHora}
            selectionColor="black"
            onChangeText={value => {
              let numero = value.replace(/[^0-9]/g, '');
              setRpm(numero);
            }}
            value={rpm}
            autoCapitalize="none"
            autoCorrect={false}
          />
        </View>

        <ItemSeparator />
        <View style={styles.viewSubSeleccionar}>
          <View style={{justifyContent: 'center'}}>
            <Icon name="flask-outline" size={35} />
          </View>
          <Text style={styles.subTitle}> LITROS: </Text>
          <TextInput
            placeholder="99999"
            keyboardType="phone-pad"
            underlineColorAndroid="black"
            style={styles.inputHora}
            selectionColor="black"
            onChangeText={value => {
              let numero = value.replace(/[^0-9]/g,'');
              setAvance_litros(numero);
            }}
            value={avance_litros}
            autoCapitalize="none"
            autoCorrect={false}
          />
        </View>
        <ItemSeparator />
        <ItemSeleccionar titulo="Estado de la Cédula" icono="reorder-four-outline" />
          
             <View style={styles.checkboxContenedor}>
              <CheckBox
                value={pendiente}
                onValueChange={setPediente}
                style={styles.checkboxStyle}
                hideBox
              />
              <Text style={styles.label}> La cédula fue finalizada ?</Text>
            </View>
         <Text style={{...styles.label, fontWeight:'normal', fontSize:22}}> {!pendiente ? "La cédula quedara en un estado pendiente" : "La cédula quedara finalizada completamente"}</Text>
        
        <ItemContador titulo="" />

        <ViewSubtitle
          title="Observaciones"
          setEstado={setOcultarViewObservacion}
          estado={ocultarViewObservacion}
        />
        {!ocultarViewObservacion ? (
          <InputObservacion
            observacion={observacion}
            setObservacion={setObservacion}
          />
        ) : null}
         <BotonEnviar enviarFormulario={enviar} />

         <ItemContador titulo="" />
         <ItemContador titulo="" />
         <ItemContador titulo="" />
         <ItemContador titulo="" />
         <ItemContador titulo="" />
      </ScrollView>
    </View>
  );
};

const tabla = StyleSheet.create({
  container: {flex: 1, padding: 5, paddingTop: 8, paddingBottom:0,justifyContent: 'center',  alignItems: 'center'},
  header: {height: 50, backgroundColor: '#424242'},
  textheader: {textAlign: 'center', fontWeight: 'bold', color:'white'},
  text: {textAlign: 'center', fontWeight: 'normal',fontSize:15 ,color:'black'},
  dataWrapper: {marginTop: -1},
  row: {height: 60, backgroundColor:'#FAFAFA'},
});
