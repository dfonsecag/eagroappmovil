import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  BackHandler,
  Text,
} from 'react-native';
import {
  Table,
  Row,
} from 'react-native-table-component';
import Icon from 'react-native-vector-icons/Ionicons';
import { useSelector} from 'react-redux';
import { ItemContador } from '../components/ItemContador';
import { ItemSeparator } from '../components/ItemSeparator';
import { ViewSubtitle } from '../components/ViewSubtitle';
import { styles } from '../styles/styles';

export const DetalleCedulaScreen = ({navigation, route}) => {

  const { numerocedula } = route.params;

  const cedula = useSelector(state => state.hojaVida.cedula);
  const avance = useSelector(state => state.hojaVida.avance);

   // Ocultar View
   const [ocultarViewDetalle, setOcultarViewDetalle] = useState(false);
   const [ocultarViewAvances, setOcultarViewAvances] = useState(true);

  const [data, setData] = useState([]);
  const [avanceCedula, setAvanceCedula] = useState([]);

  const tableData = [];
  useEffect(() => {
    if (cedula) {
      for (let i = 0; i < cedula.length; i += 1) {
        const rowData = [];
        rowData.push(cedula[i].codigo);
        rowData.push(cedula[i].producto);
        rowData.push(Number.parseFloat(cedula[i].aplicado).toFixed(0));
        rowData.push(cedula[i].descripcion);

        tableData.push(rowData);
      }
      setData(tableData);
    }
  }, [cedula]);

  const tableDataAvance = [];
  useEffect(() => {
    if (avance) {
      for (let i = 0; i < avance.length; i += 1) {
        const rowData = [];
        rowData.push(avance[i].nombre);
        rowData.push(avance[i].fecha);
        rowData.push(avance[i].inicio);
        rowData.push(avance[i].fin);
        rowData.push(avance[i].avance_litros);
        rowData.push(avance[i].clima);

        tableDataAvance.push(rowData);
      }
      setAvanceCedula(tableDataAvance);
    }
  }, [avance]);

//   useEffect(() => {
//     navigation.setOptions({
//         title: `Detalle Productos - Cédula #${numerocedula} `
//     });
// }, [])

  useEffect(() => {
    const backAction = () => {
      navigation.navigate('HojaVidaScreen');
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  let state = {
    tableHead: [
      'Código',
      'Producto',
      'Cantidad',
      'Unidad Medida'
    ],
    widthArr: [100, 250, 85, 120],
  };

  let stateAvance = {
    tableHead: [
      'Operador',
      'Fecha',
      'Inicio',
      'Fin',
      'Litros',
      'Clima'
    ],
    widthArr: [180, 90, 90, 90,90,90],
  };

  return (
    <>
      <ScrollView>
        <ViewSubtitle
          title={`Productos Cédula #${numerocedula} `}
          setEstado={setOcultarViewDetalle}
          estado={ocultarViewDetalle}
        />
        {!ocultarViewDetalle ? (
           <View style={tabla.container}>
           <ScrollView horizontal={true}>
             <View>
               <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}}>
                 <Row
                   data={state.tableHead}
                   widthArr={state.widthArr}
                   style={tabla.header}
                   textStyle={tabla.textheader}
                 />
               </Table>
               <ScrollView style={tabla.dataWrapper}>
                 <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}}>
                   {data.map((rowData, index) => (
                     <Row
                       key={index}
                       data={rowData}
                       onPress={() => console.log(rowData[1])}
                       widthArr={state.widthArr}
                       // style={[tabla.row, (rowData[9] > 5 && rowData[9] < 21 ) ? ({backgroundColor: '#FDFD96'}) : (rowData[9] > 20 ) ? ({backgroundColor: '#ff6961'}) : ({backgroundColor: '#8BC34A'})]}
                       style={[tabla.row, index%2 && {backgroundColor: '#E0E0E0'}]}
                       textStyle={tabla.text}
                     />
                   ))}
                 </Table>
               </ScrollView>
             </View>
           </ScrollView>
         </View>
        ) : null}
       <ItemSeparator />
       <ViewSubtitle
          title={`Avances Cédula #${numerocedula} `}
          setEstado={setOcultarViewAvances}
          estado={ocultarViewAvances}
        />
        {!ocultarViewAvances ? (
          <>
          {
            avanceCedula.length > 0 ?
            (
              <View style={tabla.container}>
              <ScrollView horizontal={true}>
                <View>
                  <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}}>
                    <Row
                      data={stateAvance.tableHead}
                      widthArr={stateAvance.widthArr}
                      style={tabla.header}
                      textStyle={tabla.textheader}
                    />
                  </Table>
                  <ScrollView style={tabla.dataWrapper}>
                    <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}}>
                      {avanceCedula.map((rowData, index) => (
                        <Row
                          key={index}
                          data={rowData}
                          onPress={() => console.log(rowData[1])}
                          widthArr={stateAvance.widthArr}
                          // style={[tabla.row, (rowData[9] > 5 && rowData[9] < 21 ) ? ({backgroundColor: '#FDFD96'}) : (rowData[9] > 20 ) ? ({backgroundColor: '#ff6961'}) : ({backgroundColor: '#8BC34A'})]}
                          style={[tabla.row, index%2 && {backgroundColor: '#E0E0E0'}]}
                          textStyle={tabla.text}
                        />
                      ))}
                    </Table>
                  </ScrollView>
                </View>
              </ScrollView>
            </View>
            )
            :
            (
              <View style={{...styles.viewSubSeleccionar, height: 75}}>
              <View style={{justifyContent: 'center'}}>
                <Icon name="information-circle-outline" size={40} />
              </View>
              <Text style={{...styles.subTitle, fontSize: 26}}>
                No hay avances
              </Text>
            </View>
            )
          }
          
         </>
        ) : null}
         <ItemContador titulo="" />
         <ItemContador titulo="" />
    </ScrollView>
    </>
  );
};

const tabla = StyleSheet.create({
  container: {padding: 5, paddingTop: 8, paddingBottom:0,justifyContent: 'center',  alignItems: 'center'},
  header: {height: 50, backgroundColor: '#424242'},
  textheader: {textAlign: 'center', fontWeight: 'bold', color:'white'},
  text: {textAlign: 'center', fontWeight: 'normal',fontSize:15 ,color:'black'},
  dataWrapper: {marginTop: -1},
  row: {height: 60, backgroundColor:'#FAFAFA'},
});
