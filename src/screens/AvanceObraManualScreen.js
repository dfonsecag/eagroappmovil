import React, {useEffect, useState} from 'react';
import SweetAlert from 'react-native-sweet-alert';
import {
  Alert,
  BackHandler,
  ScrollView,
  Text,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import {
  enviarFormularioManualAction,
  getCedulaSeleccionada,
} from '../actions/avanceObraAction';
import {BotonEnviar} from '../components/BotonEnviar';
import {InputObservacion} from '../components/InputObservacion';
import {ItemContador} from '../components/ItemContador';
import {ItemSeleccionar} from '../components/ItemSeleccionar';
import {ItemSeparator} from '../components/ItemSeparator';
import {ViewSubtitle} from '../components/ViewSubtitle';
import {getFecha, getHora} from '../helpers/FechaHora';
import {styles} from '../styles/styles';
import {DropdownAvanceobra} from '../components/AvanceObra/DropdownAvanceobra';
import { Fecha } from '../components/AvanceObra/Fecha';
import { TouchableOpacity } from 'react-native-gesture-handler';

export const AvanceObraManualScreen = ({navigation}) => {
  const dispatch = useDispatch();

  const cedulas = useSelector(state => state.avanceObra.cedulasPendientes);
  const envioExito = useSelector(state => state.avanceObra.envioExito);
  const permisos = useSelector(state => state.login.permisos);
  const cedula = useSelector(state => state.avanceObra.cedulaSeleccionada);
  // Ocultar View
  const [ocultarViewObservacion, setOcultarViewObservacion] = useState(true);

  //   State
  const [seleccionarCedula, setSeleccionarCedula] = useState(0); 
  const [observacion, setObservacion] = useState('');
  // State Fecha Inicio
  const [isVisibleInicio, setIsVisibleInicio] = useState(false);
  const [fechaInicio, setFechaInicio] = useState('');
  // State Fecha Fin
  const [isVisibleFin, setIsVisibleFin] = useState(false);
  const [fechaFin, setFechaFin] = useState('');

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Advertencia', 'Desea salir de la pagina muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'Si', onPress: () => navigation.navigate('HomeScreen', {rolUsuario:permisos.rolid})},
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    dispatch(getCedulaSeleccionada(seleccionarCedula));
  }, [seleccionarCedula]);
  

  useEffect(() => {
    if (envioExito) {
      setSeleccionarCedula(0);  
      setObservacion('');
    }
  }, [envioExito]);

  const enviar = () => {
    if (seleccionarCedula === 0 || seleccionarCedula === undefined) {
      SweetAlert.showAlertWithOptions({
        title: 'Seleccionar Cedula',
        subTitle: 'Debes selecccionar la cedula',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    } else if(fechaInicio === '' || fechaFin === ''){
      SweetAlert.showAlertWithOptions({
        title: 'Faltan datos por ingresar',
        subTitle: 'Debes ingresar la fecha inicio y fecha fin',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    } else {
      Alert.alert('Advertencia', 'Desea enviar la cédula ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'Si', onPress: () => enviarFormulario()},
      ]);
    }
  };

  const enviarFormulario = async () => {
    const dataForm = {
      cedulaaplicaciongrupoid: cedula.cedulaaplicaciongrupoid,
      numerocedula: cedula.numerocedula,
      observacion,
      fcreacion: getFecha(),
      timestamp: `${getFecha()} ${getHora()}`,
      // Verificar estos datos
      // empleadosxcompaniaautorizaid: 4521,
      // empleadosxcompaniaentregaid: 131,
      // empleadosxcompaniasupervisaid: 5323,
      creadopor: permisos.nombre,
      finicioaplicacion: fechaInicio,
      ffinaplicion:fechaFin,
      estadoid:3
    };
     await dispatch(enviarFormularioManualAction(dataForm, seleccionarCedula));
  };

  return (
    <View>
      <ItemSeleccionar titulo="Cédulas" icono="reorder-four-outline" />
      <DropdownAvanceobra
        seleccionarCedula={seleccionarCedula}
        setSeleccionarCedula={setSeleccionarCedula}
        cedulas={cedulas}
      />
      <ItemSeparator />
      <ScrollView>
        {!cedula ? (
          <>
            <View style={{...styles.viewSubSeleccionar, height: 60}}>
              <View style={{justifyContent: 'center'}}>
                <Icon name="close-outline" size={35} />
              </View>
              <Text style={{...styles.subTitle, fontSize: 22}}>
                Seleccione una cédula
              </Text>
            </View>
          </>
        ) : (
          <>
            <View style={{...styles.viewSubSeleccionar, height: 60}}>
              <View style={{justifyContent: 'center'}}>
                <Icon name="document-outline" size={35} />
              </View>
              <Text style={{...styles.subTitle, fontSize: 22}}>
                Cédula: {cedula.numerocedula}
              </Text>
            </View>
           
            <ItemSeparator />
            <View style={{...styles.viewSubSeleccionar, height: 65}}>
              <View style={{justifyContent: 'center'}}>
                <Icon name="information-circle-outline" size={35} />
              </View>

              <Text style={{...styles.subTitle, fontSize: 22}}>
                Lote: {cedula.numerolote}    Grupo: {cedula.codigo ? cedula.codigo : 'N/A'}
              </Text>
            </View>
            <ItemSeparator />
            <View style={{...styles.viewSubSeleccionar, height: 65}}>
              <View style={{justifyContent: 'center'}}>
                <Icon name="information-circle-outline" size={35} />
              </View>

              <Text style={{...styles.subTitle, fontSize: 22}}>
                {cedula.descripcion}
              </Text>
            </View>
          </>
        )}

        <ItemSeparator />

        <Fecha isVisible={isVisibleInicio} setIsVisible={setIsVisibleInicio} fecha={fechaInicio} setFecha={setFechaInicio}/>
        <View style={styles.viewSubSeleccionar}>
        <View style={{justifyContent: 'center'}}>
          <Icon name="hourglass-outline" size={35} />
        </View>
        <Text style={styles.subTitle}> Fecha Inicio: </Text>

        <TouchableOpacity activeOpacity={0.6} onPress={() => setIsVisibleInicio(true)}>
          <View style={{justifyContent: 'center'}}>
            <Icon
              style={{marginTop: 10, marginLeft: 20}}
              name="calendar-outline"
              size={32}
            />
          </View>
        </TouchableOpacity>

        <Text style={styles.subTitle}> {fechaInicio} </Text>
      </View>
      <ItemSeparator />
      <Fecha isVisible={isVisibleFin} setIsVisible={setIsVisibleFin} fecha={fechaFin} setFecha={setFechaFin}/>
        <View style={styles.viewSubSeleccionar}>
        <View style={{justifyContent: 'center'}}>
          <Icon name="hourglass-outline" size={35} />
        </View>
        <Text style={styles.subTitle}> Fecha Fin:     </Text>

        <TouchableOpacity activeOpacity={0.6} onPress={() => setIsVisibleFin(true)}>
          <View style={{justifyContent: 'center'}}>
            <Icon
              style={{marginTop: 10, marginLeft: 20}}
              name="calendar-outline"
              size={32}
            />
          </View>
        </TouchableOpacity>

        <Text style={styles.subTitle}> {fechaFin} </Text>
      </View>
      
        <ViewSubtitle
          title="Observaciones"
          setEstado={setOcultarViewObservacion}
          estado={ocultarViewObservacion}
        />
        {!ocultarViewObservacion ? (
          <InputObservacion
            observacion={observacion}
            setObservacion={setObservacion}
          />
        ) : null}
         <BotonEnviar enviarFormulario={enviar} />

         <ItemContador titulo="" />
         <ItemContador titulo="" />
         <ItemContador titulo="" />
         <ItemContador titulo="" />
         <ItemContador titulo="" />
         </ScrollView>
    </View>
  );
};
