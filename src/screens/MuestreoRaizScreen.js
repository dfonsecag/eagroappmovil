import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {View, ScrollView, BackHandler, Alert, Text} from 'react-native';
import SweetAlert from 'react-native-sweet-alert';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/Ionicons';

import {BotonEnviar} from '../components/BotonEnviar';
import {Contador} from '../components/Contador';
import {FechaHora} from '../components/FechaHora';
import {ItemContador} from '../components/ItemContador';
import {ItemSeleccionar} from '../components/ItemSeleccionar';
import {ItemSeparator} from '../components/ItemSeparator';
import {PickerSelect} from '../components/PickerSelect';
import {Ubicacion} from '../components/Ubicacion';
import {ViewSubtitle} from '../components/ViewSubtitle';

import {useCoordenadas} from '../hooks/useCoordenadas';

import {enviarFormularioRaizAction} from '../actions/muestreoRaizAction';
import {ItemSeparatorContador} from '../components/ItemSeparatorContador';
import {getFecha, getHora} from '../helpers/FechaHora';
import {RadioButtonClima} from '../components/RadioButtonClima';
import {InputObservacion} from '../components/InputObservacion';
import {RadioButtonRaiz} from '../components/RadioButtonRaiz';
import {getCedulaSeleccionadaExito, getCedulaSeleccionadaRaiz, setearCedulaSeleccionada} from '../actions/muestreoMeristemoAction';
import {InfoGrupoForza} from '../components/InfoGrupoForza';
import {styles} from '../styles/styles';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { ViewMuestrasRestantes } from '../components/ViewMuestrasRestantes';
import { Hectareaje } from '../components/Hectareaje';
import { RadioButtonQuema } from '../components/RadioButtonQuema';

export const MuestreoRaizScreen = ({navigation}) => {
  // Dispatch
  const dispatch = useDispatch();

  const [editar, setEditar] = useState(false);
  const [cedulasEditar, setcedulasEditar] = useState([])
  const [use, setUse] = useState(false)
  //State ocultar View
  const [ocultarViewContadores, setOcultarViewContadores] = useState(false);
  const [ocultarViewPicudo, setOcultarViewPicudo] = useState(false);
  const [ocultarViewDano, setOcultarViewDano] = useState(false);
  const [ocultarViewRaiz, setOcultarViewRaiz] = useState(false);
  const [ocultarViewMaleza, setOcultarViewMaleza] = useState(false);
  const [ocultarViewObservacion, setOcultarViewObservacion] = useState(true);

  // Contador General del bloque
  const [contadorRestante, setContadorRestantes] = useState();
  // State de los contadores
  const [joboto, setJoboto] = useState(0);
  const [caracol, setCaracol] = useState(0);
  const [sinfilio, setSinfilio] = useState(0);
  const [cochinilla, setCochinilla] = useState(0);
  const [hormiga, setHormiga] = useState(0);
  const [acylophus, setAcylophus] = useState(0);
  const [erwinia, setErwinia] = useState(0);
  const [phytopthora, setPhytopthora] = useState(0);
  const [fusarium_spp, setFusarium_spp] = useState(0);
  const [mortalidad, setMortalidad] = useState(0);
  const [thielaviopsis, setThielaviopsis] = useState(0);
  const [gusano_soldado, setGusanoSoldado] = useState(0);
  const [escamas, setEscamas] = useState(0);
  const [voluntarios, setVoluntarios] = useState(0);
  const [danno_roedores, setDannoroedores] = useState(0);
  const [larva_picudo, setLarvapicudo] = useState(0);
  const [pupa_picudo, setpupapicudo] = useState(0);
  const [adulto_picudo, setadultopicudo] = useState(0);
  const [danno_hoja, setDannoHoja] = useState(0);
  const [danno_tallo, setDannoTallo] = useState(0);
  const [raiz_buena, setRaiz_buena] = useState(0);
  const [raiz_regular, setRaiz_regular] = useState(0);
  const [raiz_mala, setRaiz_mala] = useState(0);
  //Malezas
  const [caminadora, setCaminadora] = useState('Nada');
  const [pata_de_gallina, setPata_de_gallina] = useState('Nada');
  const [cizana, setCizana] = useState('Nada');
  const [pepinillo, setPepinillo] = useState('Nada');
  const [cyperus, setCyperus] = useState('Nada');
  const [golondrina, setGolondrina] = useState('Nada');
  const [bledo, setBledo] = useState('Nada');
  const [digitaria, setDigitaria] = useState('Nada');
  const [mombaza, setMombaza] = useState('Nada');
  const [tomatillo, setTomatillo] = useState('Nada');

  const [seleccionarCedula, setSeleccionarCedula] = useState();
  const [observacion, setObservacion] = useState();
  const [hora_inicio, setHora_inicio] = useState(getHora());
  const [clima, setClima] = useState('SOLEADO');
  const [quema_herbicida, setQuema_herbicida] = useState('No');

  const {latitud, longitud, obtenerUbicacion} = useCoordenadas();
  // Selector Redux
  const envioExito = useSelector(state => state.muestreoRaiz.envioExito);
  // Aqui va cedulas de raiz
  const cedulas = useSelector(state => state.muestreoRaiz.cedulasPendientes);
  const cedula = useSelector(
    state => state.muestreoMeristemo.cedulaSeleccionada,
  );

  const permisos = useSelector(state => state.login.permisos);

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Advertencia', 'Desea salir de la pagina muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'Si', onPress: () => navigation.navigate('HomeScreen', {rolUsuario:permisos.rolid})},
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  // Para mostrar la informacion de la cedula seleccionada
  useEffect(() => {
    if(editar){   
      obtenerCedulaEditar(seleccionarCedula);     
    }else{
      dispatch(getCedulaSeleccionadaRaiz(seleccionarCedula));
    }
    
  }, [seleccionarCedula]);
   // Para editar
   useEffect(() => {
    if (editar) {
      obtenerMuestreados();
    }
  }, [editar]);

  const obtenerCedulaEditar = async (id) => {
    let arrCedulasMuestreo = await AsyncStorage.getItem('muestreoRaiz');
    let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
    let arreglo = arrCedulasMuestreoArreglo.filter((bloque) => bloque.id === id);
    dispatch(getCedulaSeleccionadaExito(arreglo[0]));
    setUse(false);
    setJoboto(arreglo[0].joboto);
    setCaracol(arreglo[0].caracol);
    setSinfilio(arreglo[0].sinfilio);
    setCochinilla(arreglo[0].cochinilla);
    setHormiga(arreglo[0].hormiga);
    setAcylophus(arreglo[0].acylophus);
    setErwinia(arreglo[0].erwinia);
    setPhytopthora(arreglo[0].phytopthora);
    setFusarium_spp(arreglo[0].fusarium_spp);
    setMortalidad(arreglo[0].mortalidad);
    setThielaviopsis(arreglo[0].thielaviopsis)
    setGusanoSoldado(arreglo[0].gusano_soldado)
    setEscamas(arreglo[0].escamas)
    setVoluntarios(arreglo[0].voluntarios);
    setDannoroedores(arreglo[0].danno_roedores);
    setLarvapicudo(arreglo[0].larva_picudo);
    setpupapicudo(arreglo[0].pupa_picudo);
    setadultopicudo(arreglo[0].adulto_picudo);
    setDannoHoja(arreglo[0].danno_hoja);
    setDannoTallo(arreglo[0].danno_tallo);
    setRaiz_buena(arreglo[0].raiz_buena);
    setRaiz_regular(arreglo[0].raiz_regular);
    setRaiz_mala(arreglo[0].raiz_mala);
    //Maleza
    setCaminadora(arreglo[0].caminadora);
    setCizana(arreglo[0].cizana);
    setPata_de_gallina(arreglo[0].pata_de_gallina);
    setPepinillo(arreglo[0].pepinillo);
    setCyperus(arreglo[0].cyperus);
    setGolondrina(arreglo[0].golondrina);
    setBledo(arreglo[0].bledo);
    setDigitaria(arreglo[0].digitaria);
    setMombaza(arreglo[0].mombaza);
    setTomatillo(arreglo[0].tomatillo);

    setClima(arreglo[0].clima);
    setQuema_herbicida((arreglo[0].quema_herbicida == true) ? 'Si': 'No' )
    setObservacion(arreglo[0].observacion);      
    setHora_inicio(getHora());

    setContadorRestantes(0);
  }

  const obtenerMuestreados = async () => {
    // await AsyncStorage.setItem('muestreoFruta', '');
    const arrCopia = await AsyncStorage.getItem('muestreoRaiz');
    const arregloCopia = JSON.parse(arrCopia);
    setcedulasEditar(arregloCopia);

  };

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <View
          style={{...styles.viewSubSeleccionar, justifyContent: 'flex-end'}}>
          <TouchableOpacity onPress={() => setEditar(!editar)}>
            <View style={{marginTop: 5}}>
              {
                editar?
                (
                  <Icon name="arrow-back-outline" color="black" size={45} />
                )
                :
                (
                  <Icon name="create-outline" color="black" size={45} />
                )
              }
              
            </View>
          </TouchableOpacity>
        </View>
      ),
    });
  }, [editar]);

  // LLeva el contador de las plantas evaluadas
  useEffect(() => {
    if (cedula && editar === false) {
       limpiarContadores();
       setContadorRestantes(cedula.muestras);
    }
  }, [cedula]);

  // Para obtener la ubicacion
  useEffect(() => {
    dispatch(setearCedulaSeleccionada());
    obtenerUbicacion();
    setContadorRestantes(0)
  }, []);

  const enviar = () => {
    if (seleccionarCedula === 0 || seleccionarCedula === undefined) {
      SweetAlert.showAlertWithOptions({
        title: 'Seleccionar Bloque',
        subTitle: 'Debes Seleccionar el bloque a muestrear',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    }
     else if (contadorRestante > 0) {
      SweetAlert.showAlertWithOptions({
        title: 'Aun hay muestras pendientes',
        subTitle: 'Debes de completar las muestras del bloque',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    }
     else {
      Alert.alert('Advertencia', 'Desea enviar el muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'Si', onPress: () => enviarFormulario()},
      ]);
    }
  };

  useEffect(() => {
    if (envioExito) {
      limpiarContadores();   
      setSeleccionarCedula(0);   
    }
    
  }, [envioExito]);

  const enviarFormulario = async () => {
    setEditar(false);
    await obtenerUbicacion();
    const data = {
      id: seleccionarCedula,
      joboto,
      caracol,
      sinfilio,
      cochinilla,
      acylophus,
      erwinia,
      phytopthora,
      fusarium_spp,
      mortalidad,
      voluntarios,
      danno_roedores,
      larva_picudo,
      pupa_picudo,
      adulto_picudo,
      danno_hoja,
      danno_tallo,
      raiz_buena,
      raiz_regular,
      raiz_mala,
      //maleza
      caminadora,
      pata_de_gallina,
      cizana,
      pepinillo,
      cyperus,
      golondrina,
      bledo,
      digitaria,
      mombaza,
      tomatillo,

      thielaviopsis,
      gusano_soldado,
      escamas,
      quema_herbicida: quema_herbicida == 'Si' ? true : false,
      hormiga,
      latitud,
      longitud,
      estado: true,
      observacion,
      hora_inicio: `${getFecha()} ${hora_inicio}`,
      hora_fin: `${getFecha()} ${getHora()}`,
      clima,
      muestras: cedula.muestras,
      codigo: cedula.codigo,
      area:cedula.area,
      numerolote: cedula.numerolote,
      numerobloque:cedula.numerobloque
    };
     await dispatch(enviarFormularioRaizAction(data, seleccionarCedula));
  };

  const validarConteo = () => {
    if(contadorRestante === 0){
      SweetAlert.showAlertWithOptions({
        title: 'Muestras completadas',
        subTitle: 'Has completado la cantidad de muestras',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });

    }else{
      setContadorRestantes(contadorRestante-1)
    }
  
  }

  const incrementarConteo = () => {
    if (contadorRestante === cedula.muestras) {
      SweetAlert.showAlertWithOptions({
        title: 'Limite muestras',
        subTitle: 'Las plantas restantes no puede ser mayor a las muestras',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    } else {
      setContadorRestantes(contadorRestante + 1);
    }
  };

  const limpiarContadores = ()=> {
    setThielaviopsis(0);
    setGusanoSoldado(0);
    setEscamas(0);
      setJoboto(0);
      setCaracol(0);
      setSinfilio(0);
      setCochinilla(0);
      setHormiga(0);
      setAcylophus(0);
      setErwinia(0);
      setPhytopthora(0);
      setFusarium_spp(0);
      setMortalidad(0);
      setVoluntarios(0);
      setDannoroedores(0);
      setLarvapicudo(0);
      setpupapicudo(0);
      setadultopicudo(0);
      setDannoHoja(0);
      setDannoTallo(0);
      setRaiz_buena(0);
      setRaiz_regular(0);
      setRaiz_mala(0);
      //Maleza
      setCaminadora('Nada');
      setPata_de_gallina('Nada');
      setCizana('Nada')
      setPepinillo('Nada');
      setCyperus('Nada');
      setGolondrina('Nada');
      setBledo('Nada');
      setDigitaria('Nada');
      setMombaza('Nada');
      setTomatillo('Nada');

      setQuema_herbicida('No')
      setClima('SOLEADO');
      setObservacion('');      
      setHora_inicio(getHora());
  }

  return (
    <>
      <ScrollView>
        <View style={{borderBottomWidth: 6, opacity: 0.2}}></View>

        <View style={{borderBottomWidth: 6, opacity: 0.2}}></View>
        <View style={styles.viewSubTitle}>
        {
            editar ?
            ( <Text style={styles.subTitle}> Editar Cédulas</Text>)
            :
            ( <Text style={styles.subTitle}> Datos Básicos </Text>)
          }
          <View style={{flex: 1}} />
        </View>
        {editar ? (
          <>
            <ItemSeleccionar titulo="Cédulas completadas" icono="reorder-four-outline" />
            <PickerSelect
              seleccionarCedula={seleccionarCedula}
              setSeleccionarCedula={setSeleccionarCedula}
              cedulas={cedulasEditar}
            />
          </>
        ) : (
          <>
            <ItemSeleccionar titulo="Cédulas" icono="reorder-four-outline" />
            <PickerSelect
              seleccionarCedula={seleccionarCedula}
              setSeleccionarCedula={setSeleccionarCedula}
              cedulas={cedulas}
            />
          </>
        )}
       
        <ItemSeparator />

        <InfoGrupoForza />
        {cedula ? (
          <>
            <ItemSeparator />           
            <Hectareaje area={cedula.area}/>          
          </>
        ) : null}
        <ItemSeparator />

        <ItemSeleccionar titulo="Condicion Climatica" icono="cloud-outline" />
        <RadioButtonClima clima={clima} setClima={setClima} />
        <ItemSeparator />
        <ItemSeleccionar titulo="Bloque Quemado" icono="leaf-outline" />
        <RadioButtonQuema quema={quema_herbicida} setQuema={setQuema_herbicida} />
        <ItemSeparator />
        <FechaHora fecha={getFecha()} hora={getHora()} />
        <ItemSeparator />

        <Ubicacion latitud={latitud} longitud={longitud} />

        <ViewSubtitle
          title="Conteo Plantas"
          setEstado={setOcultarViewContadores}
          estado={ocultarViewContadores}
        />
        {!ocultarViewContadores ? (
          <>
            <ItemContador titulo="Joboto" />
            <Contador contador={joboto} setContador={setJoboto} contadorRestante={contadorRestante}/>
            <ItemSeparatorContador />

            <ItemContador titulo="Physidae (Caracol)" />
            <Contador contador={caracol} setContador={setCaracol} contadorRestante={contadorRestante}/>
            <ItemSeparatorContador />

            <ItemContador titulo="Sinfilido" />
            <Contador contador={sinfilio} setContador={setSinfilio}  contadorRestante={contadorRestante}/>
            <ItemSeparatorContador />

            <ItemContador titulo="D.Neibrevipes (Cochinilla)" />
            <Contador contador={cochinilla} setContador={setCochinilla} contadorRestante={contadorRestante}/>
            <ItemSeparatorContador />

            <ItemContador titulo="Hormiga" />
            <Contador contador={hormiga} setContador={setHormiga} contadorRestante={contadorRestante}/>
            <ItemSeparatorContador />

            <ItemContador titulo="Acylophus" />
            <Contador contador={acylophus} setContador={setAcylophus} contadorRestante={contadorRestante}/>
            <ItemSeparatorContador />

            <ItemContador titulo="Erwinia" />
            <Contador contador={erwinia} setContador={setErwinia} contadorRestante={contadorRestante}/>
            <ItemSeparatorContador />

            <ItemContador titulo="Phytopthora" />
            <Contador contador={phytopthora} setContador={setPhytopthora} contadorRestante={contadorRestante}/>
            <ItemSeparatorContador />

            <ItemContador titulo="Furasium spp" />
            <Contador contador={fusarium_spp} setContador={setFusarium_spp} contadorRestante={contadorRestante}/>
            <ItemSeparatorContador />

            <ItemContador titulo="Mortalidad" />
            <Contador contador={mortalidad} setContador={setMortalidad} contadorRestante={contadorRestante}/>
            <ItemSeparatorContador />

            <ItemContador titulo="Voluntarios" />
            <Contador contador={voluntarios} setContador={setVoluntarios} contadorRestante={contadorRestante}/>
            <ItemSeparatorContador />
           
            <ItemContador titulo="Daño Roedor" />
            <Contador
              contador={danno_roedores}
              setContador={setDannoroedores}
              contadorRestante={contadorRestante}
            />
            <ItemSeparatorContador />
            <ItemContador titulo="Thielaviopsis" />
            <Contador contador={thielaviopsis} setContador={setThielaviopsis} contadorRestante={contadorRestante}/>
            <ItemSeparatorContador />
            <ItemContador titulo="Gusano Soldado" />
            <Contador contador={gusano_soldado} setContador={setGusanoSoldado} contadorRestante={contadorRestante}/>
            <ItemSeparatorContador />
            <ItemContador titulo="Escamas" />
            <Contador contador={escamas} setContador={setEscamas} contadorRestante={contadorRestante}/>
            <ItemSeparatorContador />
            </>
            ) : null}

            <ViewSubtitle
            title="M.Dimidiatipennis (Picudo)"
            setEstado={setOcultarViewPicudo}
            estado={ocultarViewPicudo}
            />
            {
              !ocultarViewPicudo ? 
              <>
                <ItemContador titulo="Larva" />
                <Contador contador={larva_picudo} setContador={setLarvapicudo} contadorRestante={contadorRestante}/>
                <ItemSeparatorContador />

                <ItemContador titulo="Pupa" />
                <Contador contador={pupa_picudo} setContador={setpupapicudo} contadorRestante={contadorRestante}/>
                <ItemSeparatorContador />

                <ItemContador titulo="Adulto" />
                <Contador contador={adulto_picudo} setContador={setadultopicudo} contadorRestante={contadorRestante}/>
                <ItemSeparatorContador />
              </>
              : null
            }
          
            <ViewSubtitle
            title="Daño"
            setEstado={setOcultarViewDano}
            estado={ocultarViewDano}
            />
            {
              !ocultarViewDano ? 
              <>
              <ItemContador titulo="Hoja" />
              <Contador contador={danno_hoja} setContador={setDannoHoja} contadorRestante={contadorRestante}/>
              <ItemSeparatorContador />

              <ItemContador titulo="Tallo" />
              <Contador contador={danno_tallo} setContador={setDannoTallo} contadorRestante={contadorRestante}/>
              <ItemSeparatorContador />
              </>
              :null
            } 
            
            <ViewSubtitle
            title="Raiz"
            setEstado={setOcultarViewRaiz}
            estado={ocultarViewRaiz}
            />
            {
              !ocultarViewRaiz ?
              <>
              <ItemContador titulo="Mala" />
              <Contador contador={raiz_mala} setContador={setRaiz_mala} contadorRestante={contadorRestante}/>
              <ItemSeparatorContador />

              <ItemContador titulo="Regular" />
              <Contador contador={raiz_regular} setContador={setRaiz_regular} contadorRestante={contadorRestante}/>
              <ItemSeparatorContador />
              <ItemContador titulo="Buena" />
              <Contador contador={raiz_buena} setContador={setRaiz_buena} contadorRestante={contadorRestante}/>
              <ItemSeparatorContador />
              </>
              :null
            }

            <ViewSubtitle
            title="Maleza"
            setEstado={setOcultarViewMaleza}
            estado={ocultarViewMaleza}
            />
            {
              !ocultarViewMaleza ?
              <>
              <ItemSeleccionar
              titulo="Asystasia gangética (Cizaña)"
              icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz dato={cizana} setDato={setCizana} />
              <ItemSeparator />
              
              <ItemSeleccionar
              titulo="R.Cochinchinensis (Caminadora)"
              icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz dato={caminadora} setDato={setCaminadora} />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="E. Digitaria (Pata de Gallina)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={pata_de_gallina}
                setDato={setPata_de_gallina}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Momordica charantia (Pepinillo)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={pepinillo}
                setDato={setPepinillo}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Cyperus luzulae (Cyperus)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={cyperus}
                setDato={setCyperus}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Commelina benghalensis (Golondrina)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={golondrina}
                setDato={setGolondrina}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Agastache rugosa (Bledo)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={bledo}
                setDato={setBledo}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Digitaria sanguinalis (Digitaria)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={digitaria}
                setDato={setDigitaria}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Panicum maximun (Mombaza)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={mombaza}
                setDato={setMombaza}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Solanum torvum (Tomatillo)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={tomatillo}
                setDato={setTomatillo}
              />

              </>
              :null
            }
            
        <ViewSubtitle
          title="Observaciones"
          setEstado={setOcultarViewObservacion}
          estado={ocultarViewObservacion}
        />
        {!ocultarViewObservacion ? (
          <InputObservacion
            observacion={observacion}
            setObservacion={setObservacion}
          />
        ) : null}

        {
          editar ? 
          ( <BotonEnviar enviarFormulario={enviar} titulo="Editar" />)
          :
          ( <BotonEnviar enviarFormulario={enviar} titulo="Enviar" />)
        }
      </ScrollView>

      {(seleccionarCedula === undefined || seleccionarCedula === 0) ? (
        <View
          style={{
            ...styles.viewSubSeleccionar,
            height: 80,
            justifyContent: 'center',
            backgroundColor: '#0a0a0a',
          }}>
          <Text style={{...styles.subTitle, color: '#FFF', fontSize: 27}}>
            Seleccione el bloque
          </Text>
        </View>
      ) : (
        <ViewMuestrasRestantes
          validarConteo={validarConteo}
          contadorRestante={contadorRestante}
          incrementarConteo={incrementarConteo}
        />
      )}
    </>
  );
};
