import React, { useEffect } from 'react'
import { ActivityIndicator, View } from 'react-native';
import { validarExpiracionToken} from '../actions/loginAction';
import {useSelector, useDispatch} from 'react-redux';


export const LoadingScreen = ({navigation}) => {

    const dispatch = useDispatch();
    const status = useSelector(state => state.login.status);
    const permisos = useSelector(state => state.login.permisos);
    useEffect(() => {
        if(status){
          navigation.navigate('HomeScreen', {rolUsuario:permisos.rolid});
        } else if(status === false){
            navigation.navigate('LoginScreen');
        }
                 
      }, [status]);
      
      useEffect(() => {
        dispatch(validarExpiracionToken());         
      }, [])

    return (
        <View style={{ 
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        }}>
            <ActivityIndicator 
                size={ 100 }
                color="black"
            />
        </View>
    )
}
