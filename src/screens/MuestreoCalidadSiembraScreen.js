import React, {useEffect, useState} from 'react';
import {
  Alert,
  BackHandler,
  Text,
  View,
  ScrollView,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import SweetAlert from 'react-native-sweet-alert';
import {BotonEnviar} from '../components/BotonEnviar';
import {styles} from '../styles/styles';
import { ItemSeleccionar } from '../components/ItemSeleccionar';
import {  empezarEnvio, enviarFormularioMuestreoCalidadSiembraAction } from '../actions/muestreoSemillaAction';
import { ItemContador } from '../components/ItemContador';
import { Contador } from '../components/Contador';
import { ItemSeparatorContador } from '../components/ItemSeparatorContador';
import { getFecha, getHora } from '../helpers/FechaHora';
import { ViewSubtitle } from '../components/ViewSubtitle';
import { Input } from '../components/Siembra/Input';
import { Cargando } from '../components/Cargando';

export const MuestreoCalidadSiembraScreen = ({navigation}) => {

  // Muestras
  
  const dispatch = useDispatch();

 
  // Contador General del bloque
  const envioExito = useSelector(state => state.muestreoSemilla.envioExito);
  const lotes = useSelector(state => state.muestreoSemilla.lotes);
  const tamaniosemillas = useSelector(state => state.muestreoSemilla.tamaniosemillas);
  const loading = useSelector(state => state.muestreoSemilla.loading);
  const permisos = useSelector(state => state.login.permisos);
  
  // State
  const [loteid, setLoteid] = useState();
  const [tamaniosemillaid, setTamaniosemillaid] = useState();
  const [terraza, setTerraza] = useState('');
  const [tamanio, setTamanio] = useState(0);
  const [anclaje, setAnclaje] = useState(0);
  const [enfermedad, setEnfermedad] = useState(0);
  const [plantas_paridas, setPlantasParidas] = useState(0);
  const [suciedad, setSuciedad] = useState(0);
  const [quemado, setQuemado] = useState(0);
  const [distancia_planta, setdistancia_planta] = useState('')
  const [distancia_hielera, setdistancia_hielera] = useState('')
  const [muestras, setMuestras] = useState([]);

  // State ocultar
  const [ocultarViewContadores, setOcultarViewContadores] = useState(false);
  const [ocultarViewDistancia, setOcultarViewDistancia] = useState(false);
  const [ocultarViewEnviar, setOcultarViewEnviar] = useState(true);

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Advertencia', 'Desea salir de la pagina muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'Si',
          onPress: () =>
            navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid}),
        },
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  
  const agregarMuestra = () => {   
    if(distancia_hielera == '' || distancia_planta == '')
    mostrarAlerta('Digite la distancia','Debes digitar la distancia entre plantas e hieleras')
   
    else{
      setMuestras([...muestras,{distancia_planta,distancia_hielera}])
      setdistancia_hielera('');  
      setdistancia_planta('');     
    }
 
  }


  const enviar = () => {
    if(loteid < 1 || loteid == undefined)
      mostrarAlerta('Lote de siembra', 'Debes seleccionar el lote de siembra')
    else if(tamaniosemillaid < 1 || tamaniosemillaid == undefined)
      mostrarAlerta('Tipo de semilla','Debes seleccionar el tipo de semilla')
    else if(terraza == '')
      mostrarAlerta('Terraza','Debes digitar el número de terraza')
    // else if(muestras.length <= 0)
    //   mostrarAlerta('Faltan datos','No se ha digitado las distancias de siembras')
    else {
      Alert.alert('Advertencia', 'Desea enviar el muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'Si', onPress: () => enviarFormulario()},
      ]);
    }
  };

  const mostrarAlerta = (titulo, mensaje) => {
    SweetAlert.showAlertWithOptions({
      title: `${titulo}`,
      subTitle: `${mensaje}`,
      confirmButtonTitle: 'OK',
      confirmButtonColor: '#000',
      otherButtonTitle: 'Cancel',
      otherButtonColor: '#dedede',
      style: 'error',
      cancellable: true,
    });
  };

  useEffect(() => {
    if (envioExito) {
      limpiarContadores()
    }
  }, [envioExito]);

  const enviarFormulario = async () => {
    await dispatch(empezarEnvio())
    const data = {
      loteid,
      terraza,
      tamanio,
      anclaje,
      enfermedad,
      plantas_paridas,
      suciedad,
      quemado,
      tamaniosemillaid,
      fecha:`${getFecha()} ${getHora()}`
    };
    const distanciaEntreSiembra = {
      loteid,
      terraza,
      tamaniosemillaid,
      fecha:`${getFecha()} ${getHora()}`,
      muestras
    };
     await dispatch(enviarFormularioMuestreoCalidadSiembraAction(data,distanciaEntreSiembra));
  };

  const limpiarContadores = () => {
     setLoteid();
     setTamaniosemillaid();
     setTerraza('')
     setTamanio(0);
     setAnclaje(0);
     setEnfermedad(0);
     setPlantasParidas(0)
     setSuciedad(0);
     setQuemado(0);
     setdistancia_hielera('');
     setdistancia_planta('');
     setMuestras([]);
     setOcultarViewEnviar(true)
  };

  return (
    <>
      <ScrollView>
        <View style={{borderBottomWidth: 6, opacity: 0.2}}></View>
        <View style={styles.viewSubTitle}>
        <Text style={styles.subTitle}> Datos básicos</Text>
          <View style={{flex: 1}} />
        </View>

        <ItemSeleccionar titulo="Lote " icono="reorder-four-outline" />
        <Picker
          selectedValue={loteid}
          onValueChange={itemValue => setLoteid(itemValue)}>
          <Picker.Item
            style={{fontSize: 20, fontWeight: 'bold'}}
            label={' -- Seleccione el lote --'}
            value={0}
            key={0}
          />
          {lotes
            ? lotes.map(c => (
                <Picker.Item
                  style={{fontSize: 20}}
                  label={`  ** Lote ${c.name} `}
                  value={c.id}
                  key={c.id}
                />
              ))
            : null}
        </Picker>

        <ItemSeleccionar titulo="Tipo Semilla " icono="reorder-four-outline" />
        <Picker
          selectedValue={tamaniosemillaid}
          onValueChange={itemValue => setTamaniosemillaid(itemValue)}>
          <Picker.Item
            style={{fontSize: 20, fontWeight: 'bold'}}
            label={' -- Seleccione tipo semilla --'}
            value={0}
            key={0}
          />
          {tamaniosemillas
            ? tamaniosemillas.map(c => (
                <Picker.Item
                  style={{fontSize: 20}}
                  label={`  **  ${c.name} `}
                  value={c.id}
                  key={c.id}
                />
              ))
            : null}
        </Picker>

        <ItemSeleccionar titulo="Terraza" icono="reorder-four-outline" />
        <View style={{  flexDirection: 'row',height: 55,marginLeft:50,width: '100%',justifyContent:'flex-start'}}>
                  
                  <TextInput
                    placeholder="0000"
                    keyboardType="phone-pad"
                    underlineColorAndroid="black"
                    placeholderTextColor="#6e6e6e"
                    style={{...styles.inputHora,fontSize:33}}
                    selectionColor="black"
                    onChangeText={value => {
                      let numero = value.replace(/[^0-9]/g, '');
                      setTerraza(numero);
                    }}
                    value={terraza}
                    autoCapitalize="none"
                    autoCorrect={false}
                  />
                </View>     

        <ViewSubtitle
          title="Defectos en siembra"
          setEstado={setOcultarViewContadores}
          estado={ocultarViewContadores}
        />
         {!ocultarViewContadores && (
          <>
          
          <View style={{height:5}}/>

          <ItemContador titulo=" Tamaño" />
            <Contador contador={tamanio} setContador={setTamanio}/>
          <ItemSeparatorContador />

          <ItemContador titulo=" Anclaje" />
            <Contador contador={anclaje} setContador={setAnclaje}/>
          <ItemSeparatorContador />

          <ItemContador titulo=" Enfermedad" />
            <Contador contador={enfermedad} setContador={setEnfermedad}/>
          <ItemSeparatorContador />

          <ItemContador titulo=" Plantas Paridas" />
            <Contador contador={plantas_paridas} setContador={setPlantasParidas}/>
          <ItemSeparatorContador />

          <ItemContador titulo=" Suciedad" />
            <Contador contador={suciedad} setContador={setSuciedad}/>
          <ItemSeparatorContador />

          <ItemContador titulo=" Quemado" />
            <Contador contador={quemado} setContador={setQuemado}/>

          <View style={{height:25}}/>
          </>

         )}

        <ViewSubtitle
        title={`Distancia de siembra  (${muestras.length})`}
        setEstado={setOcultarViewDistancia}
        estado={ocultarViewDistancia}
        />
        {!ocultarViewDistancia && (
         <>
          <Input dato={distancia_planta} setDato={setdistancia_planta} subtitulo="Entre plantas" icono="analytics-outline"/>
          <Input dato={distancia_hielera} setDato={setdistancia_hielera} subtitulo="Entre hieleras" icono="analytics-outline"/>
          <View
                  style={{
                    flexDirection: 'row',
                    paddingTop: 40,
                    justifyContent: 'center',
                  }}>
                  <TouchableOpacity
                    style={{
                      paddingHorizontal: 20,
                      paddingVertical: 5,
                      borderRadius: 100,
                      backgroundColor: '#8BC34A',
                      height: 60,
                      width: 100,
                    }}
                    onPress={()=> agregarMuestra()}>
                    <View
                      style={{
                        justifyContent: 'center',
                        flexDirection: 'row',
                        paddingTop: 0,
                      }}>
                      <Icon name="add-outline" color="black" size={50} />
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={{height:25}}/>
         </>

        )}
         <ViewSubtitle
          title="Enviar formulario"
          setEstado={setOcultarViewEnviar}
          estado={ocultarViewEnviar}
          />
        {
          (!ocultarViewEnviar && !loading) && (
            <BotonEnviar enviarFormulario={enviar} titulo="Enviar" />
          )
        }

        {
          (!ocultarViewEnviar && loading) && (
            <Cargando/>
          )
        }
         <View style={{height:25}}/>

      </ScrollView>
    
    </>
  );
};
