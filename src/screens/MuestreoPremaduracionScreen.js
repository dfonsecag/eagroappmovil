import React, {useEffect, useState} from 'react';
import {launchCamera} from 'react-native-image-picker';
import {
  Alert,
  BackHandler,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
  Image,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SweetAlert from 'react-native-sweet-alert';
import Slider from '@react-native-community/slider';
import ToggleSwitch from 'toggle-switch-react-native';
import {useCoordenadas} from '../hooks/useCoordenadas';
import {
  getCedulaSeleccionadaPremaduracion,
  setearCedulaSeleccionada,
} from '../actions/muestreoMeristemoAction';
import {BotonEnviar} from '../components/BotonEnviar';
import {ItemContador} from '../components/ItemContador';
import {ViewSubtitle} from '../components/ViewSubtitle';
import {ItemSeleccionar} from '../components/ItemSeleccionar';
import {ItemSeparator} from '../components/ItemSeparator';
import {FechaHora} from '../components/FechaHora';
import {Ubicacion} from '../components/Ubicacion';
import {getFecha, getHora} from '../helpers/FechaHora';
import {styles} from '../styles/styles';
import {ItemSeparatorContador} from '../components/ItemSeparatorContador';
import {Hectareaje} from '../components/Hectareaje';
import {InputObservacion} from '../components/InputObservacion';
import {enviarFormularioPremaduracionAction} from '../actions/muestreoPremaduracionAction';
import {PickerSelectEstimacion} from '../components/MuestreoEstimacion/PickerSelectEstimacion';

export const MuestreoPremaduracionScreen = ({navigation}) => {
  const dispatch = useDispatch();
  //State ocultar View
  const [ocultarViewContadores, setOcultarViewContadores] = useState(false);
  const [ocultarViewObservacion, setOcultarViewObservacion] = useState(true);
  const [ocultarViewImagen, setOcultarViewImagen] = useState(true);
  const [ocultarViewEnviar, setOcultarViewEnviar] = useState(true);
  // Contador General del bloque
  const [contadorRestantes, setContadorRestantes] = useState();
  const [cargando, setCargando] = useState(true);
  const [cargarStorage, setCargarStorage] = useState(false);

  const [seleccionarCedula, setSeleccionarCedula] = useState();
  const [hora_inicio, setHora_inicio] = useState('');

  // State Contadores con Corona

  // State de Formulario
  const [calibre, setCalibre] = useState(5);
  const [brix, setBrix] = useState('');
  const [translucidez, setTranslucidez] = useState(0);
  const [cochinillaInterna, setCochinillaInterna] = useState(false);
  const [cochinillaExterna, setCochinillaExterna] = useState(false);
  const [datos, setDatos] = useState([]);
  const [observacion, setObservacion] = useState('');

  // State imagenes
  const [image1, setimage1] = useState('');
  const [image2, setimage2] = useState('');

  // Custom Hook funcionalidades como coordenadas
  const {latitud, longitud, obtenerUbicacion} = useCoordenadas();
  const cedulas = useSelector(
    state => state.muestreoPremaduracion.cedulasPendientes,
  );
  const envioExito = useSelector(
    state => state.muestreoPremaduracion.envioExito,
  );
  const cedula = useSelector(
    state => state.muestreoMeristemo.cedulaSeleccionada,
  );

  const permisos = useSelector(state => state.login.permisos);

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Advertencia', 'Desea salir de la pagina muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'Si',
          onPress: () =>
            navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid}),
        },
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    if (seleccionarCedula === 0 || seleccionarCedula === undefined) {
      setContadorRestantes();
    }
    limpiarContadores();
    dispatch(getCedulaSeleccionadaPremaduracion(seleccionarCedula));
  }, [seleccionarCedula]);

  useEffect(() => {
    if (cedula && cargarStorage) {
      setContadorRestantes(cedula.muestras - datos.length);
      setCargarStorage(false);
    } else if (cedula) {
      setDatos([]);
      setContadorRestantes(cedula.muestras);
      limpiarContadores();
    }
  }, [cedula]);

  useEffect(() => {
    if (contadorRestantes === 0) {      
      if(image1 == '' && image2 == ''){
        Alert.alert('Advertencia', 'Desea enviar el muestreo sin adjuntar imágenes?', [
          {
            text: 'Cancelar',
            onPress: () => null,
            style: 'cancel',
          },
          {text: 'Si', onPress: () => enviarFormulario()},
        ]);    
      } else {
        Alert.alert('Advertencia', 'Desea enviar el muestreo ?', [
          {
            text: 'Cancelar',
            onPress: () => null,
            style: 'cancel',
          },
          {text: 'Si', onPress: () => enviarFormulario()},
        ]);   
      }
    }
  }, [contadorRestantes]);

  useEffect(() => {
    dispatch(setearCedulaSeleccionada());
    obtenerUbicacion();
    setHora_inicio(getHora());
    verificarFormulario();
  }, []);

  // Adjuntar Imagen
  const takePhoto = (state) => {
    launchCamera(
      {
        mediaType: 'photo',
        quality: 0.5,
        maxWidth: 800,
        maxHeight: 800,
        includeBase64: true
      },
      resp => {
        if (resp.didCancel) return;
        if(state == 'imagen1'){
          setimage1(resp.assets[0].base64)
        }else{
          setimage2(resp.assets[0].base64)
        }
      },
    );
  };

  const verificarFormulario = async () => {
    const formularioStorage = await AsyncStorage.getItem(
      'formularioPremaduracion',
    );
    if (formularioStorage) {
      Alert.alert(
        'Advertencia !',
        'Existe un formulario pendiente a completar, desea seguir completando ?',
        [
          {
            text: 'Borrarlo',
            onPress: () => BorrarFormularioStorage(),
            style: 'cancel',
          },
          {text: 'Si, Completar', onPress: () => cargarFormularioStorage()},
        ],
      );
    } else {
      setCargando(false);
    }
  };

  const BorrarFormularioStorage = async () => {
    await AsyncStorage.setItem('formularioPremaduracion', '');
    setSeleccionarCedula();
    setContadorRestantes();
    limpiarContadores();
    setCargando(false);
    setimage1('');
    setimage2('');
    setOcultarViewImagen(true);
    setOcultarViewEnviar(true);
    setOcultarViewObservacion(true);
  };

  const guardarStorageFormulario = async () => {
    let data = datos;
    data.push(seleccionarCedula);
    await AsyncStorage.setItem('formularioPremaduracion', JSON.stringify(data));

    Alert.alert(
      'Datos guardados exitosamente',
      'Desea salir de la pagina muestreo ?',
      [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'Si',
          onPress: () =>
            navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid}),
        },
      ],
    );
  };

  const cargarFormularioStorage = async () => {
    const formularioStorage = await AsyncStorage.getItem(
      'formularioPremaduracion',
    );
    const formulario = JSON.parse(formularioStorage);
    let arreglo = [];
    formulario.map((d, index) => {
      if (index !== formulario.length - 1) {
        arreglo.push(d);
      }
    });

    const id = formulario[formulario.length - 1];
    setCargarStorage(true);
    setDatos(arreglo);
    setSeleccionarCedula(id);

    setCargando(false);
  };

  const enviar = () => {
    if (seleccionarCedula === 0 || seleccionarCedula === undefined) {
      mostrarAdvertencia(
        'Seleccionar Bloque',
        'Debes Seleccionar el bloque a muestrear',
      );
    } else if (contadorRestantes > 0) {
      mostrarAdvertencia(
        'Aun hay muestras pendientes',
        'Debes de completar las muestras del bloque',
      );
    }
    else {     
        if(image1 == '' && image2 == ''){
          Alert.alert('Advertencia', 'Desea enviar el muestreo sin adjuntar imágenes?', [
            {
              text: 'Cancelar',
              onPress: () => null,
              style: 'cancel',
            },
            {text: 'Si', onPress: () => enviarFormulario()},
          ]);    
        } else {
          Alert.alert('Advertencia', 'Desea enviar el muestreo ?', [
            {
              text: 'Cancelar',
              onPress: () => null,
              style: 'cancel',
            },
            {text: 'Si', onPress: () => enviarFormulario()},
          ]);   
        }
         
    }
  };

  useEffect(() => {
    if (envioExito) {
      BorrarFormularioStorage();
      console.log('Se envio exitosamente');
    }
  }, [envioExito]);

  const enviarFormulario = async () => {
    await obtenerUbicacion();
    const dataForm = {
      id: seleccionarCedula,
      fmuestreo: `${getFecha()} ${getHora()}`,
      hora_inicio: `${getFecha()} ${hora_inicio}`,
      hora_fin: `${getFecha()} ${getHora()}`,
      latitud,
      longitud,
      estado: true,
      observacion,
    };
    await dispatch(
      enviarFormularioPremaduracionAction(dataForm, datos, seleccionarCedula, image1, image2),
    );
  };
  const limpiarContadores = () => {
    setCalibre(5);
    setBrix('');
    setTranslucidez(0);
    setCochinillaInterna(false);
    setCochinillaExterna(false);
  };

  const capturarDatosFormulario = () => {
    if (seleccionarCedula === 0 || seleccionarCedula === undefined) {
      mostrarAdvertencia(
        'Seleccionar Bloque',
        'Debes Seleccionar el bloque a muestrear',
      );
    } else if (contadorRestantes === 0) {
      mostrarAdvertencia(
        'Muestras completadas',
        'Has completado la cantidad de muestras',
      );
    } else if (brix === '') {
      mostrarAdvertencia('Verifique los datos', 'El brix debe ser digitado');
    }  else {
      const formulario = {
        calibre,
        brix,
        translucidez,
        cochinillaInterna,
        cochinillaExterna,
      };
      setContadorRestantes(contadorRestantes - 1);
      setDatos([...datos, formulario]);
      limpiarContadores();
    }
  };

  const mostrarAdvertencia = (header, message) => {
    SweetAlert.showAlertWithOptions({
      title: `${header}`,
      subTitle: `${message}`,
      confirmButtonTitle: 'OK',
      confirmButtonColor: '#000',
      otherButtonTitle: 'Cancel',
      otherButtonColor: '#dedede',
      style: 'error',
      cancellable: true,
    });
  };

  return (
    <>
      {cargando ? (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator size={100} color="black" />
        </View>
      ) : (
        <>
          <ScrollView>
            <View style={{borderBottomWidth: 6, opacity: 0.2}}></View>
            <View style={styles.viewSubTitle}>
              <Text style={styles.subTitle}> Datos Básicos </Text>
              <View style={{flex: 1}} />
            </View>
            <ItemSeleccionar titulo="Cédulas" icono="reorder-four-outline" />
            <PickerSelectEstimacion
              seleccionarCedula={seleccionarCedula}
              setSeleccionarCedula={setSeleccionarCedula}
              cedulas={cedulas}
            />
            {cedula ? (
              <>
                <ItemSeparator />
                <Hectareaje area={cedula.area} />
              </>
            ) : null}
            <ItemSeparator />

            <FechaHora fecha={getFecha()} hora={getHora()} />
            <ItemSeparator />

            <Ubicacion latitud={latitud} longitud={longitud} />
            {cedula ? (
              <>
                <ItemSeparator />
                <View style={{...styles.viewSubSeleccionar, height: 65}}>
                  <View style={{justifyContent: 'center'}}>
                    <Icon name="checkmark-outline" size={35} />
                  </View>
                  <Text style={{...styles.subTitle, fontSize: 25}}>
                    Muestreo: {cedula.punto_muestreo}
                  </Text>
                </View>
              </>
            ) : null}

            <ViewSubtitle
              title="Muestreo"
              setEstado={setOcultarViewContadores}
              estado={ocultarViewContadores}
            />
            {!ocultarViewContadores ? (
              <>
                <ItemContador titulo="Calibre" />
                <Slider
                  style={{width: '100%', height: 30}}
                  minimumValue={5}
                  maximumValue={9}
                  minimumTrackTintColor="#8BC34A"
                  maximumTrackTintColor="black"
                  onValueChange={value => setCalibre(value)}
                  step={1}
                  value={calibre}
                />
                <View style={{alignItems: 'center'}}>
                  <Text style={{fontSize: 22, fontWeight: 'bold',color:"black"}}>
                    {calibre}
                  </Text>
                </View>
                <ItemSeparatorContador />

                <ItemContador titulo="Translucidez" />
                <Slider
                  style={{width: '100%', height: 30}}
                  minimumValue={0}
                  maximumValue={2}
                  minimumTrackTintColor="#8BC34A"
                  maximumTrackTintColor="black"
                  onValueChange={value => setTranslucidez(value)}
                  step={0.25}
                  value={translucidez}
                />
                <View style={{alignItems: 'center'}}>
                  <Text style={{fontSize: 22, fontWeight: 'bold',color:"black"}}>
                    {translucidez}
                  </Text>
                </View>
                <ItemSeparatorContador />

                <View style={{...styles.viewSubSeleccionar, marginTop: 11}}>
                  <View style={{justifyContent: 'center'}}>
                    <Icon name="calculator-outline" size={35} color="black"/>
                  </View>
                  <Text style={styles.subTitle}> Brix: </Text>
                  <TextInput
                    placeholder="0"
                    keyboardType="phone-pad"
                    underlineColorAndroid="black"
                    placeholderTextColor="#6e6e6e"
                    style={styles.inputHora}
                    selectionColor="black"
                    onChangeText={value => {
                      let numero = value.replace(/[^0-9\.]/g, '');
                      setBrix(numero);
                    }}
                    value={brix}
                    autoCapitalize="none"
                    autoCorrect={false}
                  />
                </View>
                <ItemSeparatorContador />

                <ItemContador titulo="Cochinilla Interna" />
                <View
                  style={{alignItems: 'center', marginTop: 6, marginBottom: 2}}>
                  <ToggleSwitch
                    isOn={cochinillaInterna}
                    onColor="#8BC34A"
                    offColor="#E0E0E0"
                    size="medium"
                    onToggle={isOn => setCochinillaInterna(!cochinillaInterna)}
                  />
                </View>

                <ItemSeparatorContador />
                <ItemContador titulo="Cochinilla Externa" />
                <View
                  style={{alignItems: 'center', marginTop: 6, marginBottom: 2}}>
                  <ToggleSwitch
                    isOn={cochinillaExterna}
                    onColor="#8BC34A"
                    offColor="#E0E0E0"
                    size="medium"
                    onToggle={isOn => setCochinillaExterna(!cochinillaExterna)}
                  />
                </View>
                <ItemSeparatorContador />
                {/* <ItemSeleccionar titulo="Agregar Muestra" icono="add-outline" /> */}
                <View
                  style={{
                    flexDirection: 'row',
                    paddingTop: 20,
                    justifyContent: 'center',
                  }}>
                  <TouchableOpacity
                    style={{
                      paddingHorizontal: 20,
                      paddingVertical: 5,
                      borderRadius: 100,
                      backgroundColor: '#8BC34A',
                      height: 60,
                      width: 100,
                    }}
                    onPress={capturarDatosFormulario}>
                    <View
                      style={{
                        justifyContent: 'center',
                        flexDirection: 'row',
                        paddingTop: 0,
                      }}>
                      <Icon name="add-outline" color="black" size={50} />
                    </View>
                  </TouchableOpacity>
                </View>
                <ItemSeparatorContador />
              </>
            ) : null}
            <ViewSubtitle
              title="Imagenes"
              setEstado={setOcultarViewImagen}
              estado={ocultarViewImagen}
            />
            {!ocultarViewImagen ? (
              <>
                <View style={{...styles.viewCuerpoContador, height: 60}}>
                  <TouchableOpacity
                    disabled={false}
                    onPress={() => takePhoto('imagen1')}>
                    <View style={{justifyContent: 'center'}}>
                      <Icon name="camera-outline" size={50} color="black"/>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    disabled={false}
                    onPress={() => takePhoto('imagen2')}>
                    <View style={{justifyContent: 'center'}}>
                      <Icon name="camera-outline" size={50} color="black"/>
                    </View>
                  </TouchableOpacity>
                </View>
                {
                  (image1 !== '' || image2 !== '') &&
                  (
                    <>
                       <View style={{...styles.viewCuerpoContador, height: 180}}>
                        <Image
                          source={{
                            uri: `data:image/jpeg;base64,'${image1}'`,
                          }}
                          style={{height: 170, width: 170}}
                          onPress={()=> console.log("Agregar imagen")}
                        />

                        <Image
                          source={{
                            uri: `data:image/jpeg;base64,'${image2}'`,
                          }}
                          style={{height: 170, width: 170}}
                        />
                      </View>
                    </>
                  )
                }
               
              </>
            ) : null}

            <ViewSubtitle
              title="Observaciones"
              setEstado={setOcultarViewObservacion}
              estado={ocultarViewObservacion}
            />
            {!ocultarViewObservacion ? (
              <InputObservacion
                observacion={observacion}
                setObservacion={setObservacion}
              />
            ) : null}

              <BotonEnviar enviarFormulario={enviar} />
          
            <ItemContador titulo="" />
          </ScrollView>
          <View
            style={{
              ...styles.viewSubSeleccionar,
              height: 80,
              justifyContent: 'center',
              backgroundColor: '#0a0a0a',
            }}>
            {contadorRestantes ? (
              <>
                <TouchableOpacity onPress={() => guardarStorageFormulario()}>
                  <View style={{marginTop: 17}}>
                    <Icon name="save-outline" color="white" size={40} />
                  </View>
                </TouchableOpacity>
                <Text style={{...styles.subTitle, color: '#FFF', fontSize: 27}}>
                  Plantas restantes: {contadorRestantes}
                </Text>
              </>
            ) : (
              <Text style={{...styles.subTitle, color: '#FFF', fontSize: 27}}>
                Seleccione el bloque
              </Text>
            )}
          </View>
        </>
      )}
    </>
  );
};
