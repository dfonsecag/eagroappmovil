import React, {useEffect, useState} from 'react';
import {
  Alert,
  BackHandler,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import SweetAlert from 'react-native-sweet-alert';
import {BotonEnviar} from '../components/BotonEnviar';
import {styles} from '../styles/styles';
import { ItemSeleccionar } from '../components/ItemSeleccionar';
import { ItemSeparator } from '../components/ItemSeparator';
import { empezarEnvio, enviarFormularioAction, getDatosFormularioSemillaAction } from '../actions/muestreoSemillaAction';
import { RangoView } from '../components/Siembra/RangoView';
import { getFecha, getHora } from '../helpers/FechaHora';
import { Cargando } from '../components/Cargando';

export const MuestreoSemillaScreen = ({navigation}) => {

  // Muestras
  
  const dispatch = useDispatch();

 
  // Contador General del bloque
  const envioExito = useSelector(state => state.muestreoSemilla.envioExito);
  const lotes = useSelector(state => state.muestreoSemilla.lotes);
  const rangos = useSelector(state => state.muestreoSemilla.rangos);
  const permisos = useSelector(state => state.login.permisos);
  const loading = useSelector(state => state.muestreoSemilla.loading);
  
  // State
  const [seleccionarLoteProcedencia, setSeleccionarLoteProcedencia] = useState();
  const [seleccionarLoteSiembra, setSeleccionarLoteSiembra] = useState();
  const [seleccionarLotePatio, setSeleccionarLotePatio] = useState();
  const [seleccionarRango, setSeleccionarRango] = useState();
  const [muestras, setMuestras] = useState([]);
  const [peso, setPeso] = useState('')
  const [contador, setcontador] = useState(0);

  useEffect(() => {
    dispatch(getDatosFormularioSemillaAction())
  }, []);

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Advertencia', 'Desea salir de la pagina muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'Si',
          onPress: () =>
            navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid}),
        },
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  
  const agregarMuestra = () => {   
    if(seleccionarRango < 1 || seleccionarRango == undefined)
    mostrarAlerta('Seleccione el rango','Debes seleccionar el rango')
    else if(peso == '')
    mostrarAlerta('Digitar el peso', 'Debes digitar el peso')
    else{
      setMuestras([...muestras,{peso,rangosemillaid:seleccionarRango}])
      setPeso('');
      setcontador(contador + 1);
      rangos.map(r => (r.id == seleccionarRango && (r.cantidad = r.cantidad + 1)) )    
    }
 
  }

  const enviar = () => {
    if(seleccionarLoteProcedencia < 1 || seleccionarLoteProcedencia == undefined)
      mostrarAlerta('Lote de procedencia', 'Debes seleccionar el lote de procedencia')
    else if(seleccionarLoteSiembra < 1 || seleccionarLoteSiembra == undefined) 
      mostrarAlerta('Lote de siembra', 'Debes seleccionar el lote de siembra')
    else if(seleccionarLotePatio < 1 || seleccionarLotePatio == undefined) 
      mostrarAlerta('Lote de patio', 'Debes seleccionar el lote de patio')
    else if(contador < 1)
      mostrarAlerta('Cantidad de muestras','Plantas muestreadas debe ser igual o mayor a 1')
    else {
      Alert.alert('Advertencia', 'Desea enviar el muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'Si', onPress: () => enviarFormulario()},
      ]);
    }
  };

  const mostrarAlerta = (titulo, mensaje) => {
    SweetAlert.showAlertWithOptions({
      title: `${titulo}`,
      subTitle: `${mensaje}`,
      confirmButtonTitle: 'OK',
      confirmButtonColor: '#000',
      otherButtonTitle: 'Cancel',
      otherButtonColor: '#dedede',
      style: 'error',
      cancellable: true,
    });
  };

  useEffect(() => {
    if (envioExito) {
      limpiarContadores()
    }
  }, [envioExito]);

  const enviarFormulario = async () => {
    await dispatch(empezarEnvio())
    const data = {loteid_patio:seleccionarLotePatio,loteid_procedencia:seleccionarLoteProcedencia,fecha:getFecha(),hora:getHora(),loteid_siembra:seleccionarLoteSiembra,muestras};
    await dispatch(enviarFormularioAction(data));
  };

  const limpiarContadores = () => {
     setSeleccionarLoteProcedencia();
     setSeleccionarLoteSiembra();
     setSeleccionarRango();
     setMuestras([]);
     setPeso('')
     setcontador(0);
     dispatch(getDatosFormularioSemillaAction())
  };

  return (
    <>
      <ScrollView>
        <View style={{borderBottomWidth: 6, opacity: 0.2}}></View>
        <View style={styles.viewSubTitle}>
        <Text style={styles.subTitle}> Datos Básicos</Text>
          <View style={{flex: 1}} />
        </View>

        <ItemSeleccionar titulo="Lote Procedencia" icono="reorder-four-outline" />
        <Picker
          selectedValue={seleccionarLoteProcedencia}
          onValueChange={itemValue => setSeleccionarLoteProcedencia(itemValue)}>
          <Picker.Item
            style={{fontSize: 20, fontWeight: 'bold'}}
            label={' -- Seleccione el lote --'}
            value={0}
            key={0}
          />
          {lotes
            ? lotes.map(c => (
                <Picker.Item
                  style={{fontSize: 20}}
                  label={`  ** Lote ${c.name} `}
                  value={c.id}
                  key={c.id}
                />
              ))
            : null}
        </Picker>
 
        <ItemSeleccionar titulo="Lote Siembra" icono="reorder-four-outline" />
        <Picker
          selectedValue={seleccionarLoteSiembra}
          onValueChange={itemValue => setSeleccionarLoteSiembra(itemValue)}>
          <Picker.Item
            style={{fontSize: 20, fontWeight: 'bold'}}
            label={' -- Seleccione el lote --'}
            value={0}
            key={0}
          />
          {lotes
            ? lotes.map(c => (
                <Picker.Item
                  style={{fontSize: 20}}
                  label={`  ** Lote ${c.name} `}
                  value={c.id}
                  key={c.id}
                />
              ))
            : null}
        </Picker>

        <ItemSeleccionar titulo="Lote Patio" icono="reorder-four-outline" />
        <Picker
          selectedValue={seleccionarLotePatio}
          onValueChange={itemValue => setSeleccionarLotePatio(itemValue)}>
          <Picker.Item
            style={{fontSize: 20, fontWeight: 'bold'}}
            label={' -- Seleccione el lote --'}
            value={0}
            key={0}
          />
          {lotes
            ? lotes.map(c => (
                <Picker.Item
                  style={{fontSize: 20}}
                  label={`  ** Lote ${c.name} `}
                  value={c.id}
                  key={c.id}
                />
              ))
            : null}
        </Picker>

        <View style={{borderBottomWidth: 6, opacity: 0.2}}></View>
        <View style={styles.viewSubTitle}>
        <Text style={styles.subTitle}> Toma de muestras</Text>
          <View style={{flex: 1}} />
        </View>

        
        <ItemSeleccionar titulo="Rango" icono="reorder-four-outline" />
        <Picker
          selectedValue={seleccionarRango}
          onValueChange={itemValue => setSeleccionarRango(itemValue)}>
          <Picker.Item
            style={{fontSize: 22, fontWeight: 'bold'}}
            label={' -- Seleccione el rango --'}
            value={0}
            key={0}
          />
          {rangos
            ? rangos.map(c => (
                <Picker.Item
                  style={{fontSize: 26}}
                  label={`${c.name} `}
                  value={c.id}
                  key={c.id}
                />
              ))
            : null}
        </Picker>

        <View style={{...styles.viewSubSeleccionar, marginTop: 11,height: 70}}>
                  <View style={{justifyContent: 'center'}}>
                    <Icon name="calculator-outline" size={40} color="black"/>
                  </View>
                  <Text style={{...styles.subTitle,fontSize:38}}> Peso: </Text>
                  <TextInput
                    placeholder="0000"
                    keyboardType="phone-pad"
                    underlineColorAndroid="black"
                    placeholderTextColor="#6e6e6e"
                    style={{...styles.inputHora,fontSize:33}}
                    selectionColor="black"
                    onChangeText={value => {
                      let numero = value.replace(/[^0-9]/g, '');
                      setPeso(numero);
                    }}
                    value={peso}
                    autoCapitalize="none"
                    autoCorrect={false}
                  />
                </View>

                 <View
                  style={{
                    flexDirection: 'row',
                    paddingTop: 40,
                    justifyContent: 'center',
                  }}>
                  <TouchableOpacity
                    style={{
                      paddingHorizontal: 20,
                      paddingVertical: 5,
                      borderRadius: 100,
                      backgroundColor: '#8BC34A',
                      height: 60,
                      width: 100,
                    }}
                    onPress={()=> agregarMuestra()}>
                    <View
                      style={{
                        justifyContent: 'center',
                        flexDirection: 'row',
                        paddingTop: 0,
                      }}>
                      <Icon name="add-outline" color="black" size={50} />
                    </View>
                  </TouchableOpacity>
                </View>
        
        <View style={{height:50}}/>

        <View style={{borderBottomWidth: 6, opacity: 0.2}}></View>
        <View style={styles.viewSubTitle}>
        <Text style={styles.subTitle}> Muestras Tomadas </Text>
          <View style={{flex: 1}} />
        </View>
        <>
          {rangos.map(item => (
          <RangoView rango={item.name} cantidad={item.cantidad}/>
        ))}
        </>
         
        <ItemSeparator />
        {
              !loading ? (
                <BotonEnviar enviarFormulario={enviar} />                
              ) :
              (
                <Cargando/>
              )
        }
       
      </ScrollView>
      <View
        style={{
          ...styles.viewSubSeleccionar,
          height: 80,
          justifyContent: 'center',
          backgroundColor: '#0a0a0a',
        }}>
         <Text style={{...styles.subTitle, color: '#FFF', fontSize: 28}}>
              Plantas muestreadas: {contador}
            </Text>
      </View>
    </>
  );
};
