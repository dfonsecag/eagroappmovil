import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  BackHandler,
  Text,
  Button,
  Image,
} from 'react-native';
import {launchCamera} from 'react-native-image-picker';

export const MapaScreen = ({navigation, route}) => {
  const [image, setimage] = useState();
  const takePhoto = () => {
    launchCamera(
      {
        mediaType: 'photo',
        quality: 0.5,
        maxWidth: 900,
        maxHeight: 900,
        includeBase64: true,
        // saveToPhotos:true
      },
      resp => {
        if (resp.didCancel) return;
        setimage(resp.assets[0].base64)
        // setTempUri( resp.uri );
        // uploadImage( resp, _id );
      },
    );
  };

  return (
    <>
      <Text>Hola Mapa</Text>
      <View
        style={{flexDirection: 'row', justifyContent: 'center', marginTop: 10}}>
        <Button title="Cámara" onPress={takePhoto} color="#5856D6" />

        <View style={{width: 10}} />
        <Image
          source={{
            uri: `data:image/jpeg;base64,'${image}'`,
          }}
          style={{height: 100, width: 100}}
        />
          <Image
          source={{
            uri: `data:image/jpeg;base64,'${image}'`,
          }}
          style={{height: 100, width: 100}}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  logo: {
    width: 66,
    height: 58,
  },
});
