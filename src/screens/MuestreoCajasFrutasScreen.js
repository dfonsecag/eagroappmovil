import React, {useEffect, useState} from 'react';
import {
  Alert,
  BackHandler,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import {useDispatch, useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import {getFecha, getHora} from '../helpers/FechaHora';
import {styles} from '../styles/styles';
import { mostrarAlerta } from '../helpers/MostrarAlerta';
import { ItemSeleccionar } from '../components/ItemSeleccionar';
import { enviarFormularioPesosCajaAction, obtenerCalibresAction } from '../actions/rechazoFrutaAction';

export const MuestreoCajasFrutasScreen = ({navigation}) => {

  const dispatch = useDispatch();

  const permisos = useSelector(state => state.login.permisos);
  const calibres = useSelector(state => state.muestreoRechazoFruta.calibres);
  const envioExito  = useSelector(state => state.muestreoRechazoFruta.envioExito );

  const [calibreid, setCalibreid] = useState(0);
  const [peso_neto, setPesoNeto] = useState('');

  // Obtener Calibre
    useEffect(() => {
       dispatch(obtenerCalibresAction())
    }, []);

   // Setear Datos
   useEffect(() => {
   if(envioExito){
    setearValores()
   }
  }, [envioExito]);

  const setearValores = () => {
    setCalibreid(0);
    setPesoNeto('');
  }

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Advertencia', 'Desea salir de la página muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'Si',
          onPress: () =>
            navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid}),
        },
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const enviar = async() => {
    if(calibreid == 0)
      mostrarAlerta('Verificar calibre','Debes seleccionar el calibre','error')
    else if(peso_neto == '')
      mostrarAlerta('Verificar peso','Debes digitar el peso de la caja','error')
    else {
      const data = {
      calibreid	,
      peso_neto	,
      fecha	: `${getFecha()} ${getHora()}`
    	}
       await dispatch(enviarFormularioPesosCajaAction(data))
    }
  }
  

  return (
    <>
      
          <ScrollView>
          <View style={{height:80}}/>
          <ItemSeleccionar titulo="CALIBRES" icono="reorder-four-outline" />
        <Picker
          selectedValue={calibreid}
          onValueChange={itemValue => setCalibreid(itemValue)}>
          <Picker.Item
            style={{fontSize: 22, fontWeight: 'bold'}}
            label={'  -- SELECCIONE EL CALIBRE --'}
            value={0}
            key={0}
          />
          {calibres
            ? calibres.map(c => (
                <Picker.Item
                  style={{fontSize: 26}}
                  label={` * ${c.name} `}
                  value={c.id}
                  key={c.id}
                />
              ))
            : null}
        </Picker>

        <View style={{...styles.viewSubSeleccionar, marginTop: 11,height: 70}}>
                  <View style={{justifyContent: 'center'}}>
                    <Icon name="calculator-outline" size={40} color="black"/>
                  </View>
                  <Text style={{...styles.subTitle,fontSize:38}}> Peso: </Text>
                  <TextInput
                    placeholder="0000"
                    keyboardType="phone-pad"
                    underlineColorAndroid="black"
                    placeholderTextColor="#6e6e6e"
                    style={{...styles.inputHora,fontSize:33}}
                    selectionColor="black"
                    onChangeText={value => {
                      let numero = value.replace(/[^0-9\.]/g, '');
                      setPesoNeto(numero);
                    }}
                    value={peso_neto}
                    autoCapitalize="none"
                    autoCorrect={false}
                  />
                </View>

                 <View
                  style={{
                    flexDirection: 'row',
                    paddingTop: 85,
                    justifyContent: 'center',
                  }}>
                  <TouchableOpacity
                    style={{
                      paddingHorizontal: 20,
                      paddingTop:18,
                      borderRadius: 100,
                      backgroundColor: '#8BC34A',
                      height: 90,
                      width: 120,
                    }}
                    onPress={()=> enviar()}>
                    <View
                      style={{
                        justifyContent: 'center',
                        flexDirection: 'row',
                        paddingTop: 0,
                      }}>
                      <Icon name="save-outline" color="black" size={50} />
                    </View>
                  </TouchableOpacity>
                </View>

          
          </ScrollView>
        
    </>
  );
};
