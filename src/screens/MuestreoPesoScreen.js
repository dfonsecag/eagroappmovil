import React, {useEffect, useState} from 'react';
import {
  Alert,
  BackHandler,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/Ionicons';
import SweetAlert from 'react-native-sweet-alert';
import {useCoordenadas} from '../hooks/useCoordenadas';
import {
  getCedulaSeleccionadaPeso,
  setearCedulaSeleccionada,
  getCedulaSeleccionadaExito
} from '../actions/muestreoMeristemoAction';
import {BotonEnviar} from '../components/BotonEnviar';
import {ItemContador} from '../components/ItemContador';
import {ViewSubtitle} from '../components/ViewSubtitle';
import {ItemSeleccionar} from '../components/ItemSeleccionar';
import {PickerSelect} from '../components/PickerSelect';
import {ItemSeparator} from '../components/ItemSeparator';
import {FechaHora} from '../components/FechaHora';
import {Ubicacion} from '../components/Ubicacion';
import {getFecha, getHora} from '../helpers/FechaHora';
import {styles} from '../styles/styles';
import {InputObservacion} from '../components/InputObservacion';
import {enviarFormularioPesoAction} from '../actions/muestreoPesoAction';
import {ItemSeparatorContador} from '../components/ItemSeparatorContador';
import {ContenedorContador} from '../components/MuestreoPesos/ContenedorContador';
import {FormularioPesos} from '../components/MuestreoPesos/FormularioPesos';
import {ContadorPesos} from '../components/MuestreoPesos/ContadorPesos';
import {Hectareaje} from '../components/Hectareaje';
import { RadioButtonRaiz } from '../components/RadioButtonRaiz';

export const MuestreoPesoScreen = ({navigation}) => {
  const dispatch = useDispatch();

  const [editar, setEditar] = useState(false);
  const [cedulasEditar, setcedulasEditar] = useState([]);
  const [use, setUse] = useState(false);

  //State ocultar View
  const [ocultarViewCalibre, setOcultarViewCalibre] = useState(false);
  const [ocultarViewContadores, setOcultarViewContadores] = useState(false);
  const [ocultarViewObservacion, setOcultarViewObservacion] = useState(true);
  const [ocultarViewMaleza, setOcultarViewMaleza] = useState(true);
  // Contador General del bloque
  const [contadorRestantes, setContadorRestantes] = useState();

  const [seleccionarCedula, setSeleccionarCedula] = useState();

  const [observacion, setObservacion] = useState('');
  const [hora_inicio, setHora_inicio] = useState(getHora());
  // State Contadores
  const [totalmuypequena, setTotalmuypequena] = useState(0);
  const [totalpequena, setTotalpequena] = useState(0);
  const [totalmediano, setTotalmediano] = useState(0);
  const [totalgrande, setTotalgrande] = useState(0);
  const [totalmuygrande, setTotalmuygrande] = useState(0);
  const [totalextragrande, setTotalextragrande] = useState(0);
  const [totaljumbo, setTotaljumbo] = useState(0);
  // State calibracion pesos
  const [muypequena1, setMuypequena1] = useState('');
  const [muypequena2, setMuypequena2] = useState('');
  const [pequena1, setPequena1] = useState('');
  const [pequena2, setPequena2] = useState('');
  const [mediano1, setMediano1] = useState('');
  const [mediano2, setMediano2] = useState('');
  const [grande1, setGrande1] = useState('');
  const [grande2, setGrande2] = useState('');
  const [muygrande1, setmuyGrande1] = useState('');
  const [muygrande2, setmuyGrande2] = useState('');
  const [extraGrande1, setExtraGrande1] = useState('');
  const [extraGrande2, setExtraGrande2] = useState('');
  const [jumbo1, setJumbo1] = useState('');
  const [jumbo2, setJumbo2] = useState('');

   //Malezas
   const [caminadora, setCaminadora] = useState('Nada');
   const [pata_de_gallina, setPata_de_gallina] = useState('Nada');
   const [cizana, setCizana] = useState('Nada');
   const [pepinillo, setPepinillo] = useState('Nada');
   const [cyperus, setCyperus] = useState('Nada');
   const [golondrina, setGolondrina] = useState('Nada');
   const [bledo, setBledo] = useState('Nada');
   const [digitaria, setDigitaria] = useState('Nada');
   const [mombaza, setMombaza] = useState('Nada');
   const [tomatillo, setTomatillo] = useState('Nada');

  // Custom Hook funcionalidades como coordenadas
  const {latitud, longitud, obtenerUbicacion} = useCoordenadas();
  const cedulas = useSelector(state => state.muestreoPeso.cedulasPendientes);
  const envioExito = useSelector(state => state.muestreoPeso.envioExito);
  const cedula = useSelector(
    state => state.muestreoMeristemo.cedulaSeleccionada,
  );

  const permisos = useSelector(state => state.login.permisos);

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Advertencia', 'Desea salir de la pagina muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'Si',
          onPress: () =>
            navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid}),
        },
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    if(editar){ 
      obtenerCedulaEditar(seleccionarCedula);     
    }else{
      setContadorRestantes();
      dispatch(getCedulaSeleccionadaPeso(seleccionarCedula));
    }
   
   
  }, [seleccionarCedula]);

   // Para editar
   useEffect(() => {
    if (editar) {
      obtenerMuestreados();
    }
  }, [editar]);

  const obtenerCedulaEditar = async (id) => {
    let arrCedulasMuestreo = await AsyncStorage.getItem('muestreoPeso');
    let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
    const arreglo = arrCedulasMuestreoArreglo.filter((bloque) => bloque.id === id);
    dispatch(getCedulaSeleccionadaExito(arreglo[0]));
    setUse(false);
    setObservacion(arreglo[0].observacion);
    setHora_inicio(getHora());
    // State Contadores
    setTotalmuypequena(arreglo[0].totalmuypequena);
    setTotalpequena(arreglo[0].totalpequena);
    setTotalmediano(arreglo[0].totalmediano);
    setTotalgrande(arreglo[0].totalgrande);
    setTotalmuygrande(arreglo[0].totalmuygrande);
    setTotalextragrande(arreglo[0].totalextragrande);
    setTotaljumbo(arreglo[0].totaljumbo);
    // State calibracion pesos
    setMuypequena1(arreglo[0].muypequena1);
    setMuypequena2(arreglo[0].muypequena2);
    setPequena1(arreglo[0].pequena1);
    setPequena2(arreglo[0].pequena2);
    setMediano1(arreglo[0].mediano1);
    setMediano2(arreglo[0].mediano2);
    setGrande1(arreglo[0].grande1);
    setGrande2(arreglo[0].grande2);
    setmuyGrande1(arreglo[0].muygrande1);
    setmuyGrande2(arreglo[0].muygrande2);
    setExtraGrande1(arreglo[0].extraGrande1);
    setExtraGrande2(arreglo[0].extraGrande2);
    setJumbo1(arreglo[0].jumbo1);
    setJumbo2(arreglo[0].jumbo2);
     //Maleza
     setCaminadora(arreglo[0].caminadora);
     setCizana(arreglo[0].cizana);
     setPata_de_gallina(arreglo[0].pata_de_gallina);
     setPepinillo(arreglo[0].pepinillo);
     setCyperus(arreglo[0].cyperus);
     setGolondrina(arreglo[0].golondrina);
     setBledo(arreglo[0].bledo);
     setDigitaria(arreglo[0].digitaria);
     setMombaza(arreglo[0].mombaza);
     setTomatillo(arreglo[0].tomatillo);

    setContadorRestantes(0);
  }

  const obtenerMuestreados = async () => {
    // await AsyncStorage.setItem('muestreoFruta', '');
    const arrCopia = await AsyncStorage.getItem('muestreoPeso');
    const arregloCopia = JSON.parse(arrCopia);
    setcedulasEditar(arregloCopia);

  };

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
       
          <TouchableOpacity onPress={() => setEditar(!editar)}>
            <View style={{marginTop: 5}}>
              {
                editar?
                (
                  <Icon name="arrow-back-outline" color="black" size={45} />
                )
                :
                (
                  <Icon name="create-outline" color="black" size={45} />
                )
              }
              
            </View>
          </TouchableOpacity>
      
      ),
    });
  }, [editar]);

  useEffect(() => {
    if (cedula && editar === false) {
      limpiarContadores();
      setContadorRestantes(cedula.plantasmuestrear);
    }
  }, [cedula]);

  useEffect(() => {
    dispatch(setearCedulaSeleccionada());
    obtenerUbicacion();
    setContadorRestantes();
  }, []);

  const enviar = () => {
    if (seleccionarCedula === 0 || seleccionarCedula === undefined) {
      SweetAlert.showAlertWithOptions({
        title: 'Seleccionar Bloque',
        subTitle: 'Debes Seleccionar el bloque a muestrear',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    } else if (contadorRestantes > 0) {
      SweetAlert.showAlertWithOptions({
        title: 'Aun hay muestras pendientes',
        subTitle: 'Debes de completar las muestras del bloque',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    } else if (
      totalmuypequena > 0 &&
      (muypequena2 === '' || muypequena1 === '')
    ) {
      mostrarAlerta(
        'Verifique los calibres muy pequeñas',
        'Existe conteo de plantas muy pequeñas',
      );
    } else if (totalpequena > 0 && (pequena1 === '' || pequena2 === '')) {
      mostrarAlerta(
        'Verifique los calibres pequeñas',
        'Existe conteo de plantas pequeñas',
      );
    } else if (totalmediano > 0 && (mediano1 === '' || mediano2 === '')) {
      mostrarAlerta(
        'Verifique los calibres medianos',
        'Existe conteo de plantas medianas',
      );
    } else if (totalgrande > 0 && (grande1 === '' || grande2 === '')) {
      mostrarAlerta(
        'Verifique los calibres grandes',
        'Existe conteo de plantas grandes',
      );
    } else if (totalmuygrande > 0 && (muygrande1 === '' || muygrande2 === '')) {
      mostrarAlerta(
        'Verifique los calibres muy grandes',
        'Existe conteo de plantas muy grandes',
      );
    } else if (
      totalextragrande > 0 &&
      (extraGrande1 === '' || extraGrande2 === '')
    ) {
      mostrarAlerta(
        'Verifique los calibres extra grandes',
        'Existe conteo de plantas extra grandes',
      );
    } else if (totaljumbo > 0 && (jumbo1 === '' || jumbo2 === '')) {
      mostrarAlerta(
        'Verifique los calibres jumbo',
        'Existe conteo de plantas jumbo',
      );
    } else {
      Alert.alert('Advertencia', 'Desea enviar el muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'Si', onPress: () => enviarFormulario()},
      ]);
    }
  };

  const mostrarAlerta = (titulo, mensaje) => {
    SweetAlert.showAlertWithOptions({
      title: `${titulo}`,
      subTitle: `${mensaje}`,
      confirmButtonTitle: 'OK',
      confirmButtonColor: '#000',
      otherButtonTitle: 'Cancel',
      otherButtonColor: '#dedede',
      style: 'error',
      cancellable: true,
    });
  };

  useEffect(() => {
    if (editar) {
      obtenerCedulaEditar(seleccionarCedula);
    } else {
      dispatch(getCedulaSeleccionadaPeso(seleccionarCedula));
    }
  }, [seleccionarCedula]);

  useEffect(() => {
    if (envioExito) {
      setSeleccionarCedula(0);
      setContadorRestantes(0);
      limpiarContadores();
    }
  }, [envioExito]);

  const enviarFormulario = async () => {
    await obtenerUbicacion();
    setEditar(false);
    const dataForm = {
      id: seleccionarCedula,
      observacion,
      latitud,
      longitud,
      estado: true,
      observacion,
      hora_inicio: `${getFecha()} ${hora_inicio}`,
      hora_fin: `${getFecha()} ${getHora()}`,
      promediomuypequena: Math.ceil(
        (parseInt(muypequena1) + parseInt(muypequena2)) / 2,
      ),
      totalmuypequena,
      promediopequena: Math.ceil((parseInt(pequena1) + parseInt(pequena2)) / 2),
      totalpequena,
      promediomediano: Math.ceil((parseInt(mediano1) + parseInt(mediano2)) / 2),
      totalmediano,
      promediogrande: Math.ceil((parseInt(grande1) + parseInt(grande2)) / 2),
      totalgrande,
      promediomuygrande: Math.ceil(
        (parseInt(muygrande1) + parseInt(muygrande2)) / 2,
      ),
      totalmuygrande,
      promedioextragrande: Math.ceil(
        (parseInt(extraGrande1) + parseInt(extraGrande2)) / 2,
      ),
      totalextragrande,
      promediojumbo: Math.ceil((parseInt(jumbo1) + parseInt(jumbo2)) / 2),
      totaljumbo,
      plantasmuestrear: cedula.plantasmuestrear,
      plantasxpunto:cedula.plantasxpunto,
      codigo: cedula.codigo,
      area:cedula.area,
      numerolote: cedula.numerolote,
      numerobloque:cedula.numerobloque,
      muypequena1,
      muypequena2,
      pequena1,
      pequena2,
      mediano1,
      mediano2,
      grande1,
      grande2,
      muygrande1,
      muygrande2,
      extraGrande1,
      extraGrande2,
      jumbo1,
      jumbo2,
      //maleza
      caminadora,
      pata_de_gallina,
      cizana,
      pepinillo,
      cyperus,
      golondrina,
      bledo,
      digitaria,
      mombaza,
      tomatillo,
    };
    await dispatch(enviarFormularioPesoAction(dataForm, seleccionarCedula));
  };
  const limpiarContadores = () => {
    setObservacion('');
    setHora_inicio(getHora());
    // State Contadores
    setTotalmuypequena(0);
    setTotalpequena(0);
    setTotalmediano(0);
    setTotalgrande(0);
    setTotalmuygrande(0);
    setTotalextragrande(0);
    setTotaljumbo(0);
    // State calibracion pesos
    setMuypequena1('');
    setMuypequena2('');
    setPequena1('');
    setPequena2('');
    setMediano1('');
    setMediano2('');
    setGrande1('');
    setGrande2('');
    setmuyGrande1('');
    setmuyGrande2('');
    setExtraGrande1('');
    setExtraGrande2('');
    setJumbo1('');
    setJumbo2('');

     //Maleza
     setCaminadora('Nada');
     setPata_de_gallina('Nada');
     setCizana('Nada')
     setPepinillo('Nada');
     setCyperus('Nada');
     setGolondrina('Nada');
     setBledo('Nada');
     setDigitaria('Nada');
     setMombaza('Nada');
     setTomatillo('Nada');
     setOcultarViewMaleza(true);
  };

  return (
    <>
      <ScrollView>
        <View style={{borderBottomWidth: 6, opacity: 0.2}}></View>
        <View style={styles.viewSubTitle}>
          {
            editar ?
            ( <Text style={styles.subTitle}> Editar Cédulas</Text>)
            :
            ( <Text style={styles.subTitle}> Datos Básicos </Text>)
          }
          <View style={{flex: 1}} />
        </View>
        {editar ? (
          <>
            <ItemSeleccionar titulo="Cédulas completadas" icono="reorder-four-outline" />
            <PickerSelect
              seleccionarCedula={seleccionarCedula}
              setSeleccionarCedula={setSeleccionarCedula}
              cedulas={cedulasEditar}
            />
          </>
        ) : (
          <>
            <ItemSeleccionar titulo="Cédulas" icono="reorder-four-outline" />
            <PickerSelect
              seleccionarCedula={seleccionarCedula}
              setSeleccionarCedula={setSeleccionarCedula}
              cedulas={cedulas}
            />
          </>
        )}
        <ItemSeparator />

        <FechaHora fecha={getFecha()} hora={getHora()} />
        <ItemSeparator />

        <Ubicacion latitud={latitud} longitud={longitud} />

        {/* <InfoGrupoForza /> */}
        {/* <ItemSeparator /> */}
        {cedula ? (
          <>
            <ItemSeparator />
            <View style={{...styles.viewSubSeleccionar, height: 65}}>
              <View style={{justifyContent: 'center'}}>
                <Icon name="checkmark-outline" size={35} color='black'/>
              </View>
              <Text style={{...styles.subTitle, fontSize: 25}}>
                Plantas a muestrear: {cedula.plantasmuestrear}
              </Text>
            </View>
            <ItemSeparator />
            <View style={{...styles.viewSubSeleccionar, height: 65}}>
              <View style={{justifyContent: 'center'}}>
                <Icon name="checkmark-outline" size={35} color='black'/>
              </View>

              <Text style={{...styles.subTitle, fontSize: 25}}>
                Plantas por punto: {cedula.plantasxpunto}
              </Text>
            </View>
          </>
        ) : null}

        <ViewSubtitle
          title="Conteo Calibres"
          setEstado={setOcultarViewContadores}
          estado={ocultarViewContadores}
        />
        {!ocultarViewContadores ? (
          <>
            <ItemContador titulo="Muy Pequeña (1100gr a 1500gr)" />
            <ContadorPesos
              contador={totalmuypequena}
              setContador={setTotalmuypequena}
              contadorRestantes={contadorRestantes}
              setContadorRestantes={setContadorRestantes}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Pequeña (1500gr a 1900gr)" />
            <ContadorPesos
              contador={totalpequena}
              setContador={setTotalpequena}
              contadorRestantes={contadorRestantes}
              setContadorRestantes={setContadorRestantes}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Mediano (1900 a 2300gr)" />
            <ContadorPesos
              contador={totalmediano}
              setContador={setTotalmediano}
              contadorRestantes={contadorRestantes}
              setContadorRestantes={setContadorRestantes}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Grande (2300 a 2700gr)" />
            <ContadorPesos
              contador={totalgrande}
              setContador={setTotalgrande}
              contadorRestantes={contadorRestantes}
              setContadorRestantes={setContadorRestantes}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Muy Grande (2700 a 3100gr)" />
            <ContadorPesos
              contador={totalmuygrande}
              setContador={setTotalmuygrande}
              contadorRestantes={contadorRestantes}
              setContadorRestantes={setContadorRestantes}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Extra Grande (3100 a 3500gr)" />
            <ContadorPesos
              contador={totalextragrande}
              setContador={setTotalextragrande}
              contadorRestantes={contadorRestantes}
              setContadorRestantes={setContadorRestantes}
            />
            <ItemSeparatorContador />

            <ItemContador titulo="Jumbo (3500 a 3900gr)" />
            <ContadorPesos
              contador={totaljumbo}
              setContador={setTotaljumbo}
              contadorRestantes={contadorRestantes}
              setContadorRestantes={setContadorRestantes}
            />
            <ItemSeparatorContador />
          </>
        ) : null}

        <View style={styles.viewSubTitle}>
          <Text style={styles.subTitle}> Calibre en Plantas </Text>
          <View style={{flex: 1}} />
          <TouchableOpacity
            onPress={() => setOcultarViewCalibre(!ocultarViewCalibre)}>
            <View style={{justifyContent: 'center'}}>
              {!ocultarViewCalibre ? (
                <Icon name="remove-outline" size={50} color='black'/>
              ) : (
                <Icon name="add-outline" size={50} color='black'/>
              )}
            </View>
          </TouchableOpacity>
        </View>
        {!ocultarViewCalibre ? (
          <>
            <ContenedorContador title="Muy Pequeña (1100gr a 1500gr)" />
            <View
              style={{...styles.viewSubSeleccionar, height: 50, marginTop: 15}}>
              <FormularioPesos
                calibre={muypequena1}
                setCalibre={setMuypequena1}
              />
              <FormularioPesos
                calibre={muypequena2}
                setCalibre={setMuypequena2}
              />
            </View>

            <ItemSeparator />
            <ContenedorContador title="Pequeña (1500gr a 1900gr)" />
            <View
              style={{...styles.viewSubSeleccionar, height: 50, marginTop: 15}}>
              <FormularioPesos calibre={pequena1} setCalibre={setPequena1} />
              <FormularioPesos calibre={pequena2} setCalibre={setPequena2} />
            </View>
            <ItemSeparator />
            <ContenedorContador title="Mediano (1900 a 2300gr)" />
            <View
              style={{...styles.viewSubSeleccionar, height: 50, marginTop: 15}}>
              <FormularioPesos calibre={mediano1} setCalibre={setMediano1} />
              <FormularioPesos calibre={mediano2} setCalibre={setMediano2} />
            </View>
            <ItemSeparator />
            <ContenedorContador title="Grande (2300 a 2700gr)" />
            <View
              style={{...styles.viewSubSeleccionar, height: 50, marginTop: 15}}>
              <FormularioPesos calibre={grande1} setCalibre={setGrande1} />
              <FormularioPesos calibre={grande2} setCalibre={setGrande2} />
            </View>
            <ItemSeparator />
            <ContenedorContador title="Muy Grande (2700 a 3100gr)" />
            <View
              style={{...styles.viewSubSeleccionar, height: 50, marginTop: 15}}>
              <FormularioPesos
                calibre={muygrande1}
                setCalibre={setmuyGrande1}
              />
              <FormularioPesos
                calibre={muygrande2}
                setCalibre={setmuyGrande2}
              />
            </View>
            <ItemSeparator />
            <ContenedorContador title="Extra Grande (3100 a 3500gr)" />
            <View
              style={{...styles.viewSubSeleccionar, height: 50, marginTop: 15}}>
              <FormularioPesos
                calibre={extraGrande1}
                setCalibre={setExtraGrande1}
              />
              <FormularioPesos
                calibre={extraGrande2}
                setCalibre={setExtraGrande2}
              />
            </View>
            <ItemSeparator />
            <ContenedorContador title="Jumbo (3500 a 3900gr)" />
            <View
              style={{...styles.viewSubSeleccionar, height: 50, marginTop: 15}}>
              <FormularioPesos calibre={jumbo1} setCalibre={setJumbo1} />
              <FormularioPesos calibre={jumbo2} setCalibre={setJumbo2} />
            </View>
            <ItemSeparator />
          </>
        ) : null}

<ViewSubtitle
            title="Maleza"
            setEstado={setOcultarViewMaleza}
            estado={ocultarViewMaleza}
            />
            {
              !ocultarViewMaleza ?
              <>
              <ItemSeleccionar
              titulo="Asystasia gangética (Cizaña)"
              icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz dato={cizana} setDato={setCizana} />
              <ItemSeparator />
              
              <ItemSeleccionar
              titulo="R.Cochinchinensis (Caminadora)"
              icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz dato={caminadora} setDato={setCaminadora} />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="E. Digitaria (Pata de Gallina)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={pata_de_gallina}
                setDato={setPata_de_gallina}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Momordica charantia (Pepinillo)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={pepinillo}
                setDato={setPepinillo}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Cyperus luzulae (Cyperus)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={cyperus}
                setDato={setCyperus}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Commelina benghalensis (Golondrina)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={golondrina}
                setDato={setGolondrina}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Agastache rugosa (Bledo)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={bledo}
                setDato={setBledo}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Digitaria sanguinalis (Digitaria)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={digitaria}
                setDato={setDigitaria}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Panicum maximun (Mombaza)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={mombaza}
                setDato={setMombaza}
              />
              <ItemSeparator />

              <ItemSeleccionar
                titulo="Solanum torvum (Tomatillo)"
                icono="checkmark-circle-outline"
              />
              <RadioButtonRaiz
                dato={tomatillo}
                setDato={setTomatillo}
              />

              </>
              :null
            }

        <ViewSubtitle
          title="Observaciones"
          setEstado={setOcultarViewObservacion}
          estado={ocultarViewObservacion}
        />
        {!ocultarViewObservacion ? (
          <InputObservacion
            observacion={observacion}
            setObservacion={setObservacion}
          />
        ) : null}
         {editar ? (
          <BotonEnviar enviarFormulario={enviar} titulo="Editar" />
        ) : (
          <BotonEnviar enviarFormulario={enviar} titulo="Enviar" />
        )}
        <ItemContador titulo="" />
      </ScrollView>
      <View
        style={{
          ...styles.viewSubSeleccionar,
          height: 80,
          justifyContent: 'center',
          backgroundColor: '#0a0a0a',
        }}>
        {(contadorRestantes || contadorRestantes === 0) ? (
          <>
            <Text style={{...styles.subTitle, color: '#FFF', fontSize: 30}}>
              Plantas restantes: {contadorRestantes}
            </Text>
          </>
        ) : (
          <Text style={{...styles.subTitle, color: '#FFF', fontSize: 27}}>
            Seleccione el bloque
          </Text>
        )}
      </View>
    </>
  );
};
