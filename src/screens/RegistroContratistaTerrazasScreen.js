import React, {useEffect, useState} from 'react';
import {
  Alert,
  BackHandler,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import {getFecha, getHora} from '../helpers/FechaHora';
import {styles} from '../styles/styles';
import { mostrarAlerta } from '../helpers/MostrarAlerta';
import { ItemSeleccionar } from '../components/ItemSeleccionar';
import { ItemSeparator } from '../components/ItemSeparator';
import { Hora } from '../components/Hora';
import { DropdownSelect } from '../components/DropdownSelect';
import { agregarEmpleado, eliminarEmpleadoAction, eliminarPersonaAction, enviarFormularioTerrazasAction, getBloquesAction, getDatosFormularioSemillaAction, setearBloques, setearEmpleados } from '../actions/muestreoSemillaAction';
import { PickerSeleccionador } from '../components/PickerSeleccionador';
import { Tabla } from '../components/Tablas/Tabla';

export const RegistroContratistaTerrazasScreen = ({navigation}) => {

  const dispatch = useDispatch();

  const permisos = useSelector(state => state.login.permisos);
  const lotes = useSelector(state => state.muestreoSemilla.lotes);
  const bloques = useSelector(state => state.muestreoSemilla.bloques);
  const contratistas = useSelector(state => state.muestreoSemilla.contratistas);
  const actividades = useSelector(state => state.muestreoSemilla.actividades);
  const empleadosSeleccionados = useSelector(state => state.muestreoSemilla.empleadosSeleccionados);
  const envioExito  = useSelector(state => state.muestreoSemilla.envioExito );

  const [loteid, setLoteid] = useState(0);
  const [actividadid, setActividadId] = useState(0);
  const [bloqueid, setBloqueId] = useState(0);
  const [terraza, setTerraza] = useState('');
  const [horainicio, setHoraInicio] = useState('--:--');
  const [horafinal, setHoraFinal] = useState('--:--');
  const [contratista, setContratista] = useState(0)  


   // State tabla empleados
   const [data, setData] = useState([]);

   let state = {
    tableHead: ['🗑', 'Código', 'Nombre'],
    widthArr: [60, 80, 200],
  };

   // Arreglo temporal para recorrer los empleados seleccionados
   const tableData = [];

    // Recorre cada vez que hay empleados seleccionados, para luego mostrar en la tabla
  useEffect(() => {
    if (empleadosSeleccionados) {
      for (let i = 0; i < empleadosSeleccionados.length; i += 1) {
        const rowData = [];
        rowData.push(botonEliminar(empleadosSeleccionados[i]));
        rowData.push(empleadosSeleccionados[i].id);
        rowData.push(empleadosSeleccionados[i].nombre_contratista);
        tableData.push(rowData);
      }
      setData(tableData);
    }
  }, [empleadosSeleccionados]);

   // Componente para mostar icono y funcion de eliminar empleado
   const botonEliminar = (data) => {
    return ( 
    <View style={{alignItems: 'center'}}> 
    <TouchableOpacity onPress={() => eliminarEmpleado( data )}>     
      <Icon name="trash-outline" size={30} color='red'/>     
    </TouchableOpacity>
    </View>)
  }
  
   // Setear Datos
   useEffect(() => {
   if(envioExito){
    setearValores()
   }
  }, [envioExito]);

   // Agregar Empleado
   useEffect(() => {
    if(contratista !== 0){
      setContratista(0);
     dispatch(agregarEmpleado({id:contratista.id, nombre_contratista:contratista.name,name:contratista.name,contratista:contratista.contratista}))
    }
   }, [contratista]);

   // Si desea eliminar un empleado o contratista del avance de obras
   const eliminarEmpleado = ( data ) => {
    Alert.alert('⚠️  Advertencia', `Desea eliminar el ${ data.nombre_contratista }?`, [
      {
        text: 'Cancelar',
        onPress: () => null,
        style: 'cancel',
      },
      {
        text: 'Si',
        onPress: () => dispatch(eliminarPersonaAction(data)),
      },
    ]);
  };

   // Setear Datos
   useEffect(() => {
    if(loteid !== 0)
      dispatch(getBloquesAction(loteid))
     else
      dispatch(setearBloques())
   }, [loteid]);

  const setearValores = async() => {
     dispatch(getDatosFormularioSemillaAction());
     dispatch(setearEmpleados())
     setTerraza('');
     setHoraInicio('--:--');
     setHoraFinal('--:--');
     setContratista(0);
  }

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Advertencia', 'Desea salir de la página ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'Si',
          onPress: () =>
            navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid}),
        },
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const enviar = async() => {
    let inicio = `${getFecha()} ${horainicio}`;
    let termino = `${getFecha()} ${horafinal}`;
    //quita (-),(:), espacios y los comvierte en entero
    inicio = parseInt(inicio.replace(/-|:|\s/g, ''));
    termino = parseInt(termino.replace(/-|:|\s/g, ''));

    if(actividadid == 0)
      mostrarAlerta('Verificar actividad','Debes seleccionar la actividad','error')
    else if(loteid == 0)
      mostrarAlerta('Verificar lote','Debes seleccionar el lote','error')
    else if(bloqueid == 0)
      mostrarAlerta('Verificar bloque','Debes seleccionar el bloque','error')
    else if(empleadosSeleccionados.length < 0)
      mostrarAlerta('Verificar empleados','Debes seleccionar al menos un empleado','error')
    else if(horainicio == '--:--')
      mostrarAlerta('Verifique hora inicio','Debes seleccionar hora inicial','error')
    else if(horafinal == '--:--')
      mostrarAlerta('Verifique hora fin','Debes seleccionar hora final','error')
    else if (inicio > termino || inicio === termino) 
      mostrarAlerta('Verifique las horas','Hora de inicio no puede ser mayor o igual a la hora final','error')   
    else if(terraza == '')
      mostrarAlerta('Verificar terraza','Debes digitar la terraza','error')
    else {
      const data = {
      loteid,
      actividadid,
      bloqueid,
      empleados: empleadosSeleccionados,
      horainicio:`${getFecha()} ${horainicio}`,
      horafinal: `${getFecha()} ${horafinal}`,
      fecha	: `${getFecha()} ${getHora()}`,
      terraza
    	}
      
      await dispatch(enviarFormularioTerrazasAction(data))
    }
  }

  return (
      <ScrollView keyboardShouldPersistTaps="always">
        <View style={{height:20}}/>
        

        <ItemSeleccionar titulo="Actividad " icono="reorder-four-outline" />
        <PickerSeleccionador seleccionar={actividadid} setSeleccionar={setActividadId} data={actividades} placeholder='Seleccione la actividad' inicial=''/>
        <ItemSeparator />

        <ItemSeleccionar titulo="Lote " icono="reorder-four-outline" />
        <PickerSeleccionador seleccionar={loteid} setSeleccionar={setLoteid} data={lotes} placeholder='Seleccione el lote' inicial='Lote'/>
      
        <ItemSeparator />
        <ItemSeleccionar titulo="Bloque " icono="reorder-four-outline" />
        <PickerSeleccionador seleccionar={bloqueid} setSeleccionar={setBloqueId} data={bloques} placeholder='Seleccione el bloque' inicial='Bloque'/>

        <ItemSeparator />
        <ItemSeleccionar titulo="Personal " icono="reorder-four-outline" />
       
        <DropdownSelect cedulas={contratistas} seleccionarCedula={contratista} setSeleccionarCedula={setContratista} placeholder='Seleccione personal'/>
        {
          data.length > 0 &&(
            <Tabla columns={state.widthArr} headers={state.tableHead} data={data}/>
          )
        }
        

        <ItemSeparator />
        <Hora
          horainicio={horainicio}
          setHoraInicio={setHoraInicio}
          horafinal={horafinal}
          setHoraFinal={setHoraFinal}
        />
        <ItemSeparator />
        <View style={{...styles.viewSubSeleccionar, marginTop: 11,height: 70}}>
                  <View style={{justifyContent: 'center'}}>
                    <Icon name="code-outline" size={40} color="black"/>
                  </View>
                  <Text style={{...styles.subTitle,fontSize:34}}> Terraza: </Text>
                  <TextInput
                    placeholder="0000"
                    keyboardType="phone-pad"
                    underlineColorAndroid="black"
                    placeholderTextColor="#6e6e6e"
                    style={{...styles.inputHora,fontSize:33}}
                    selectionColor="black"
                    onChangeText={value => {
                      let numero = value.replace(/[^0-9]/g, '');
                      setTerraza(numero);
                    }}
                    value={terraza}
                    autoCapitalize="none"
                    autoCorrect={false}
                  />
                </View>

                 <View
                  style={{
                    flexDirection: 'row',
                    paddingTop: 55,
                    justifyContent: 'center',
                  }}>
                  <TouchableOpacity
                    style={{
                      paddingHorizontal: 20,
                      paddingTop:8,
                      borderRadius: 100,
                      backgroundColor: '#8BC34A',
                      height: 70,
                      width: 110,
                    }}
                    onPress={()=> enviar()}>
                    <View
                      style={{
                        justifyContent: 'center',
                        flexDirection: 'row',
                        paddingTop: 0,
                      }}>
                      <Icon name="save-outline" color="black" size={50} />
                    </View>
                  </TouchableOpacity>
                </View>
    </ScrollView>
  );
};



