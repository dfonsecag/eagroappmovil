import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  BackHandler,
  TouchableOpacity,
} from 'react-native';
import {Table, Row} from 'react-native-table-component';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import {
  getHojaVidaAction,
  getDetalleCedulaAction,
  setGrupoId,
  getDetalleSinCedulaAction
} from '../actions/hojaVidaAction';
import {DropdownHojaVida} from '../components/HojaVida/DropdownHojaVida';
import {styles} from '../styles/styles';
import {ItemSeparator} from '../components/ItemSeparator';

export const HojaVidaScreen = ({navigation}) => {
  const dispatch = useDispatch();

  const permisos = useSelector(state => state.login.permisos);

  const grupos = useSelector(state => state.hojaVida.grupos);

  const hojaVida = useSelector(state => state.hojaVida.hojaVida);

  const bloques = useSelector(state => state.hojaVida.bloques);
  //   State
  const [seleccionarGrupo, setSeleccionarGrupo] = useState({});

  const [data, setData] = useState([]);

  const [estado, setEstado] = useState(false);

  const [buscar, setBuscar] = useState(true);

  const consultarCedula = cedula => {
    dispatch(getDetalleCedulaAction(cedula));
    navigation.navigate('DetalleCedulaScreen', {numerocedula: cedula});
  };

  const consultarSinCedula = (opcionAplicaion) => {
    dispatch(getDetalleSinCedulaAction(opcionAplicaion, seleccionarGrupo.name));
    navigation.navigate('DetalleCedulaScreen', {numerocedula: 'N/A'});
  };

  const tableData = [];
  useEffect(() => {
    if (hojaVida) {
      for (let i = 0; i < hojaVida.length; i += 1) {
        const rowData = [];
        rowData.push(`  👁  `);
        rowData.push(hojaVida[i].cédula);
        rowData.push(hojaVida[i].opcion_aplicación);
        rowData.push(hojaVida[i].fecha_estimada);
        rowData.push(hojaVida[i].fecha_aplicación);
        rowData.push(hojaVida[i].días_avanzados);
        rowData.push(hojaVida[i].estado);
        rowData.push(hojaVida[i].opcionaplicacionid);

        tableData.push(rowData);
      }
      setData(tableData);
    }
  }, [hojaVida]);

  useEffect(() => {
    dispatch(setGrupoId());
  }, []);
  // Filtra por las cedulas que estan aplicadas y las que estan anuladas, generadas
  const cedulasAplicadas = () => {
    if (estado) {
      dispatch(getHojaVidaAction(seleccionarGrupo.id, 'Ejecutada'));
    } else {
      dispatch(getHojaVidaAction(seleccionarGrupo.id, 'Generada'));
    }
    setEstado(!estado);
  };

  useEffect(() => {
    const backAction = () => {
      navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid});
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    if (seleccionarGrupo.id) {
      setBuscar(false);
      setEstado(false);
      dispatch(getHojaVidaAction(seleccionarGrupo.id, 'Ejecutada'));
    }
  }, [seleccionarGrupo]);

  useEffect(() => {
    navigation.setOptions({
      title: seleccionarGrupo.id
        ? `LOTE ${seleccionarGrupo.name}`
        : 'HOJA DE VIDA',
    });
  }, [seleccionarGrupo]);

  let state = {
    tableHead: [
      '👁',
      'Cédula',
      'Opcion Aplicación',
      'Fecha Estimada',
      'Fecha Aplicación',
      'Días',
      'Estado',
      'OpcionAplicacion',
    ],
    widthArr: [60, 80, 230, 120, 120, 80, 120,0],
  };

  // Activar metodo Search
  const activarBusqueda = () => {
    setBuscar(!buscar);
  };

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        // <View
        //   style={{...styles.viewSubSeleccionar, justifyContent: 'flex-end'}}>
        <TouchableOpacity onPress={() => activarBusqueda()}>
          <View style={{marginTop: 10}}>
            <Icon name="search-outline" color="white" size={45} />
          </View>
        </TouchableOpacity>
        // </View>
      ),
    });
  }, [buscar]);

  return (
    <>
      {buscar ? (
        <View>
          <DropdownHojaVida
            seleccionarCedula={seleccionarGrupo}
            setSeleccionarCedula={setSeleccionarGrupo}
            cedulas={grupos}
          />
        </View>
      ) : null}

      <View style={tabla.container}>
        <ScrollView horizontal={true}>
          <View>
            <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}}>
              <Row
                data={state.tableHead}
                widthArr={state.widthArr}
                style={tabla.header}
                textStyle={tabla.textheader}
              />
            </Table>
            <ScrollView style={tabla.dataWrapper}>
              <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}}>
                {data.map((rowData, index) => (
                  <Row
                    key={index}
                    data={rowData}
                    onPress={() => {
                      if(rowData[1]!=='N/A'){
                        consultarCedula(rowData[1]);
                      }else{
                        consultarSinCedula(rowData[7]);
                      }
                    }}
                    widthArr={state.widthArr}
                    style={[
                      tabla.row,
                      rowData[6] === 'Anulada'
                        ? {backgroundColor: '#E0E0E0'}
                        : rowData[5] > 5 && rowData[5] < 21
                        ? {backgroundColor: '#FDFD96'}
                        : rowData[5] > 20
                        ? {backgroundColor: '#ff6961'}
                        : rowData[6] === 'Por Generarse'
                        ? {backgroundColor: '#E0E0E0'}
                        : {backgroundColor: '#8BC34A'}
                    ]}
                    textStyle={tabla.text}
                  />
                ))}
              </Table>
              {bloques ? (
                <>
                  <ItemSeparator />
                  <View style={styles.viewSubSeleccionar}>
                    <TouchableOpacity onPress={() => cedulasAplicadas()}>
                      <View style={{marginRight: 18, marginTop: 10}}>
                        {estado ? (
                          <Icon name="square-outline" size={40} color="black"/>
                        ) : (
                          <Icon name="checkbox-outline" size={40} color="black"/>
                        )}
                      </View>
                    </TouchableOpacity>
                    <Text
                      style={{
                        ...styles.subTitle,
                        fontSize: 20,
                        paddingHorizontal: 8,
                        marginLeft: 0,
                      }}>
                      Bloques: {bloques.bloques}
                    </Text>
                  </View>
                  <ItemSeparator />
                  <View style={styles.viewSubSeleccionar}>
                  <View style={{marginRight: 18, marginTop: 10}}>
                    <Icon name="information-circle-outline" size={40} color="black"/>
                  </View>                   
                    <Text
                      style={{
                        ...styles.subTitle,
                        fontSize: 20,
                        paddingHorizontal: 8,
                        marginLeft: 0,
                      }}>Paquete Tecnológico: {bloques.paquete}</Text>
                  </View>
                  <ItemSeparator />
                  <View style={styles.viewSubSeleccionar}>
                  <View style={{marginRight: 18, marginTop: 10}}>
                    <Icon name="information-circle-outline" size={40} color="black"/>
                  </View>                   
                    <Text
                      style={{
                        ...styles.subTitle,
                        fontSize: 20,
                        paddingHorizontal: 8,
                        marginLeft: 0,
                      }}>
                      Fecha Agrupación: {bloques.fechaAgrupacion}     -     Días Hito: {bloques.diashito}
                    </Text>
                  </View>

                  <ItemSeparator />
                </>
              ) : null}
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    </>
  );
};

const tabla = StyleSheet.create({
  container: {flex: 1, padding: 5, paddingTop: 0, paddingBottom: 0},
  header: {height: 50, backgroundColor: '#424242'},
  textheader: {textAlign: 'center', fontWeight: 'bold', color: 'white'},
  text: {
    textAlign: 'center',
    fontWeight: 'normal',
    fontSize: 15,
    color: 'black',
  },
  dataWrapper: {marginTop: -1},
  row: {height: 60},
});
