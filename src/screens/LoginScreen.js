import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Keyboard,
  BackHandler,
} from 'react-native';
import {useDimensions} from '@react-native-community/hooks';

import Icon from 'react-native-vector-icons/Ionicons';
import {styles} from '../styles/styles';
import {loginAction, loginSinConexionAction} from '../actions/loginAction';
import {LogoEmpresaCelular, LogoEmpresaTablet} from '../components/LogoEmpresa';
import {conexionInternet} from '../helpers/verificarConexionInternet';

export const LoginScreen = ({navigation}) => {
  // Dispatch
  const dispatch = useDispatch();
  const status = useSelector(state => state.login.status);
  const loading = useSelector(state => state.login.loading);
  const permisos = useSelector(state => state.login.permisos);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [isCelular, setIsCelular] = useState(true);

  useEffect(() => {
    const backAction = () => {
      BackHandler.exitApp();
    };
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    return () => backHandler.remove();
  }, []);

  const onLogin = async () => {
    Keyboard.dismiss();

    const internet = await conexionInternet();
    if (!internet) {
      dispatch(loginSinConexionAction(email, password));
    } else {
      dispatch(loginAction(email, password));
    }
  };

  useEffect(() => {
    if (status) {
      navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid});
    }
  }, [status]);

  const screen = useDimensions().screen;

  useEffect(() => {
    let width = 0;
    if (screen.width > screen.height) {
      width = screen.height;
    } else {
      width = screen.width;
    }

    if (width > 500) {
      setIsCelular(false);
    }
  }, [screen]);

  return (
    <>
      <KeyboardAvoidingView
        style={{flex: 1}}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <View style={loginStyles.formContainer}>
          {isCelular ? <LogoEmpresaCelular /> : <LogoEmpresaTablet />}

          {/* Si vas a usar Celular use este logo */}

          {/* <LogoEmpresaCelular /> */}
          {/* Si vas a usar tablet use este logo  */}
          {/* <LogoEmpresaTablet/> */}

          <View style={loginStyles.viewLogin}>
            <View style={{justifyContent: 'center'}}>
              <Icon name="person-outline" size={40} color="#8BC34A" />
            </View>
            <TextInput
              placeholder="Digite su usuario"
              keyboardType="email-address"
              underlineColorAndroid="black"
              style={loginStyles.inputField}
              selectionColor="black"
              placeholderTextColor="#6e6e6e"
              onChangeText={value => setEmail(value)}
              value={email}
              onSubmitEditing={onLogin}
              autoCapitalize="none"
              autoCorrect={false}
            />
          </View>

          <View style={loginStyles.viewLogin}>
            <View style={{justifyContent: 'center'}}>
              <Icon name="key-outline" size={40} color="#8BC34A" />
            </View>

            <TextInput
              placeholder="Digite su contraseña"
              underlineColorAndroid="black"
              secureTextEntry
              style={loginStyles.inputField}
              selectionColor="black"
              placeholderTextColor="#6e6e6e"
              onChangeText={value => setPassword(value)}
              value={password}
              onSubmitEditing={onLogin}
              autoCapitalize="none"
              autoCorrect={false}
            />
          </View>

          {/* Boton login */}
          <View style={styles.botonContenedorEnviar}>
            <TouchableOpacity
              disabled={loading}
              style={loginStyles.botonLogin}
              onPress={onLogin}>
              <View style={{justifyContent: 'center'}}>
                <Text style={loginStyles.botonLoginText}>Iniciar Sesion</Text>
              </View>
            </TouchableOpacity>
          </View>

          {/* Crear una nueva cuenta */}
          <View style={loginStyles.newUserContainer}></View>
        </View>
        <View style={{alignItems: 'center', marginBottom: 15}}>
          <Text style={{fontSize: 18}}>
            Copyright © Agricola Industrial La Lydia
          </Text>
        </View>
      </KeyboardAvoidingView>
    </>
  );
};

const loginStyles = StyleSheet.create({
  formContainer: {
    flex: 1,
    paddingHorizontal: 30,
    justifyContent: 'center',
    height: 600,
    marginBottom: 50,
  },
  title: {
    color: 'black',
    fontSize: 40,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 20,
    textAlign: 'center',
  },
  inputField: {
    fontSize: 30,
    borderColor: 'black',
    color: 'black',
  },

  buttonText: {
    fontSize: 18,
    color: 'black',
  },
  newUserContainer: {
    alignItems: 'flex-end',
    marginTop: 10,
  },
  botonLogin: {
    paddingHorizontal: 20,
    paddingVertical: 5,
    borderRadius: 100,
    backgroundColor: '#8BC34A',
    height: 60,
    width: 200,
  },
  botonLoginText: {
    fontSize: 25,
    color: 'black',
    fontWeight: 'bold',
    textAlignVertical: 'center',
    textAlign: 'center',
    paddingTop: 5,
  },

  viewLogin: {
    marginTop: 20,
  },
});
