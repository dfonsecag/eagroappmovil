import React, {useEffect, useState} from 'react';
import {
  Alert,
  BackHandler,
  View,
  ScrollView,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {BotonEnviar} from '../components/BotonEnviar';
import { obtenerPhClorPlantaAction,enviarFormulario, enviarFormularioPhCloroPlantaAction } from '../actions/registrosPlantaAction';
import { getFecha, getHora } from '../helpers/FechaHora';
import { ViewSubtitle } from '../components/ViewSubtitle';
import { Cargando } from '../components/Cargando';
import { InputPlanta } from '../components/Registro Planta/InputPlanta';
import { ItemSeleccionarPlanta } from '../components/Registro Planta/ItemSeleccionarPlanta';
import { RadioButtonPlanta } from '../components/Registro Planta/RadioButtonPlanta';
import { ItemSeparator } from '../components/ItemSeparator';
import { mostrarAlerta } from '../helpers/MostrarAlerta';
import { InputObservacionPlanta } from '../components/Registro Planta/InputObservacionPlanta';

export const RegistroPhCloroPlantaScreen = ({navigation}) => {

  // Muestras  
  const dispatch = useDispatch();
 
  // Contador General del bloque
  const envioExito = useSelector(state => state.registrosPlanta.envioExito);
  const loading = useSelector(state => state.registrosPlanta.loading);
  const registoPhCloro = useSelector(state => state.registrosPlanta.registoPhCloro);
  const permisos = useSelector(state => state.login.permisos);
  
  // State
  const [fotometro_cloro, setfotometro_cloro] = useState('No');
  const [ph_inicial, setPhInicial] = useState('');
  const [ph_final, setphfinal] = useState('');
  const [acido_citrico, setacido_citrico] = useState('');
  const [ppm_incial, setppm_incial] = useState('');
  const [ppm_final, setppm_final] = useState('');
  const [cantidad_ci, setcantidad_ci] = useState('');
  const [reforzar_cloro, setreforzar_cloro] = useState('No');
  const [limpieza_filtro, setlimpieza_filtro] = useState('No');
  const [observacion, setObservacio] = useState('');

  // State ocultar
  const [ocultarViewEnviar, setOcultarViewEnviar] = useState(true);

  useEffect(() => {
    dispatch(obtenerPhClorPlantaAction(getFecha()))
  }, []);

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Advertencia', 'Desea salir de la pagina ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'Si',
          onPress: () =>
            navigation.navigate('HomeScreen', {rolUsuario: permisos.rolid}),
        },
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  
  const enviar = () => {
    if(registoPhCloro.length == 0 && ph_inicial == '')
      mostrarAlerta('Digite el pH inicial agua', 'Debes digitar el pH inicial agua')
    else if(ph_final == '')
      mostrarAlerta('Digite el pH final agua', 'Debes digitar el pH final agua')
    else if(ppm_incial == '')
      mostrarAlerta('Digite el ppm inicial cloro','Debes digitar el ppm inicial cloro')
    else if(reforzar_cloro == 'Si' && cantidad_ci == '')
      mostrarAlerta('Digite el CI agregado', 'Debes digitar el CI agregado')
    else if(ppm_final == '')
      mostrarAlerta('Digite el ppm final cloro','Debes digitar el ppm final cloro')  
    else {
      Alert.alert('Advertencia', 'Desea enviar el muestreo ?', [
        {
          text: 'Cancelar',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'Si', onPress: () => enviarFormularioData()},
      ]);
    }
  };

   useEffect(() => {
    if (envioExito) {
      dispatch(obtenerPhClorPlantaAction(getFecha()))
      limpiarContadores()
    }
  }, [envioExito]);

  const enviarFormularioData = async () => {
    await dispatch(enviarFormulario())
    const data = {
      medicion_phcloroid: registoPhCloro.length > 0 ? registoPhCloro[0].medicion_phcloroid:null,
      fotometro_cloro:fotometro_cloro == 'No'? false : true,
      cinta:fotometro_cloro == 'No'? true : false,
      hora_calibracion:fotometro_cloro == 'No'? null : `${getHora()}`,
      fecha:`${getFecha()} ${getHora()}`,
      ph_inicial: ph_inicial == '' ? null : ph_inicial,
      ph_final:ph_final == '' ? null : ph_final,
      acido_citrico:acido_citrico == '' ? null : acido_citrico,
      ppm_incial:ppm_incial == '' ? null : ppm_incial,
      ppm_final:ppm_final == '' ? null : ppm_final,
      cantidad_ci:cantidad_ci == '' ? null : cantidad_ci,
      hora_revision:`${getHora()}`,
      reforzar_cloro: reforzar_cloro == 'No'? false : true,
      limpieza_filtro: limpieza_filtro == 'No'? false : true,
      observacion
    };
    await dispatch(enviarFormularioPhCloroPlantaAction(data));
  };

  const limpiarContadores = () => {
   setfotometro_cloro('No');
   setPhInicial('');
   setphfinal('');
   setacido_citrico('');
   setppm_incial('');
   setppm_final('');
   setcantidad_ci('');
   setreforzar_cloro('No');
   setlimpieza_filtro('No');
   setObservacio('');
  };

  return (
    <>
      <ScrollView>
        <View style={{borderBottomWidth: 6, opacity: 0.2}}></View>
       
        {
           (registoPhCloro.length == 0) && (
            <>
               <ItemSeleccionarPlanta titulo="Fotómetro para Cloro" icono="biotech" />
               <RadioButtonPlanta dato={fotometro_cloro} setDato={setfotometro_cloro}/>
               <ItemSeparator /> 
               <ItemSeleccionarPlanta titulo="PH Inicial Agua" icono="opacity" />
               <InputPlanta placeholder="0000" setInput={setPhInicial} value={ph_inicial} />
               <ItemSeparator />                    
            </>
          )
        }
        <ItemSeleccionarPlanta titulo="Ácido Cítrico (kg)" icono="timeline" />
        <InputPlanta placeholder="0000" setInput={setacido_citrico} value={acido_citrico} />
         <ItemSeparator />
        <ItemSeleccionarPlanta titulo="PH Final Agua" icono="crop" />
        <InputPlanta placeholder="0000" setInput={setphfinal} value={ph_final} />
          <ItemSeparator />
        <ItemSeleccionarPlanta titulo="Ppm Inicial Cloro" icono="science" />
        <InputPlanta placeholder="0000" setInput={setppm_incial} value={ppm_incial} />
          <ItemSeparator />
        <ItemSeleccionarPlanta titulo="Reforzar con Cloro" icono="verified" />
        <RadioButtonPlanta dato={reforzar_cloro} setDato={setreforzar_cloro}/>
         
        {
           (reforzar_cloro == 'Si') && (
            <>
            <ItemSeleccionarPlanta titulo="CI Agregada (Kg)" icono="science" />
            <InputPlanta placeholder="0000" setInput={setcantidad_ci} value={cantidad_ci} />
            </>
          )
        }
         <ItemSeparator /> 
        <ItemSeleccionarPlanta titulo="Cloro Ppm Final" icono="spa" />
        <InputPlanta placeholder="0000" setInput={setppm_final} value={ppm_final} />
        <ItemSeparator /> 
        <ItemSeleccionarPlanta titulo="Limpieza Filtro" icono="verified" />
        <RadioButtonPlanta dato={limpieza_filtro} setDato={setlimpieza_filtro}/>
        <ItemSeparator />
        <ItemSeleccionarPlanta titulo="Observación" icono="visibility" />
        <InputObservacionPlanta setObservacion={setObservacio} observacion={observacion}/>
         <ViewSubtitle
          title="Enviar formulario"
          setEstado={setOcultarViewEnviar}
          estado={ocultarViewEnviar}
          />
        {
          (!ocultarViewEnviar && !loading) && (
            <BotonEnviar enviarFormulario={enviar} titulo="Enviar" />
          )
        }

        {
          (!ocultarViewEnviar && loading) && (
            <Cargando/>
          )
        }
         <View style={{height:25}}/>

      </ScrollView>
    
    </>
  );
};
