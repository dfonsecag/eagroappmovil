import AsyncStorage from '@react-native-async-storage/async-storage';
import SweetAlert from 'react-native-sweet-alert';
import clienteAxios from '../config/api';
import {conexionInternet} from '../helpers/verificarConexionInternet';
// import BcryptReactNative from 'bcrypt-react-native';

import {
  EMPEZAR_LOGIN,
  EMPEZAR_LOGIN_EXITO,
  EMPEZAR_LOGIN_ERROR,
  VERIFICAR_LOGIN_EXITO,
  VERIFICAR_LOGIN_ERROR,
  CERRAR_SESION
} from '../types';
import { getFecha } from '../helpers/FechaHora';

// Login usuario
export function loginAction(usuario, contrasena) { 
  return async dispatch => {
      dispatch(enviarLogin())
      try {
         // Se verifica si posee internet, se decide si se envia a la Api o queda almacenado localStorage
      const internet = await conexionInternet();
      if (internet) {
        const choferes = await clienteAxios.get('/usuarios/choferes');
        await AsyncStorage.setItem('usuarios',JSON.stringify(choferes.data));

        const {data} = await clienteAxios.post('/usuarios/login', { usuario, contrasena } );
        await AsyncStorage.setItem('token', data.token );
        await AsyncStorage.setItem('logueado', 'true' );
        await AsyncStorage.setItem('permisos', JSON.stringify({rolid:data.rolid, choferid:data.choferid, nombre:data.nombre}) );
        dispatch(loginExito({rolid:data.rolid, choferid:data.choferid ,nombre:data.nombre}))
      } else{
          dispatch(loginError())
          SweetAlert.showAlertWithOptions({
            title: 'Verifique su conexion',
            subTitle:
              'Es necesario tener internet',
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: 'error',
            cancellable: true,
          });
        }
      } catch (err) {    
        dispatch(loginError())
          SweetAlert.showAlertWithOptions({
            title: 'Hubo un error',
            subTitle:
              'La contraseña o usuario es incorrecto',
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: 'error',
            cancellable: true,
          });
      }
 
  };
}

// Login usuario sin conexion
export function loginSinConexionAction(user, contrasena) { 
  return async dispatch => {
      //dispatch(enviarLogin())
      try {
        const usuarios = await AsyncStorage.getItem('usuarios');
        const arregloUsuarios = JSON.parse(usuarios);
        const usuario =  arregloUsuarios.filter( d => d.usuario === user);
        //  const valido = await BcryptReactNative.compareSync(contrasena, usuario[0].contrasena);
        //  if(!valido){
        //   SweetAlert.showAlertWithOptions({
        //     title: 'Hubo un error',
        //     subTitle:
        //       'La contraseña o usuario es incorrecto',
        //     confirmButtonTitle: 'OK',
        //     confirmButtonColor: '#000',
        //     otherButtonTitle: 'Cancel',
        //     otherButtonColor: '#dedede',
        //     style: 'error',
        //     cancellable: true,
        //   });
        //  }else {
          await AsyncStorage.setItem('logueado', 'true' );
          await AsyncStorage.setItem('permisos', JSON.stringify({rolid:usuario[0].rolid, choferid:usuario[0].choferid, nombre:usuario[0].nombre}) );
          dispatch(loginExito({rolid:usuario[0].rolid, choferid:usuario[0].choferid ,nombre:usuario[0].nombre}))
      //   }
      } catch (err) {    
        console.log(err);
        //dispatch(loginError())
          SweetAlert.showAlertWithOptions({
            title: 'Hubo un error',
            subTitle:
              'La contraseña o usuario es incorrecto',
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: 'error',
            cancellable: true,
          });
      }
 
  };
}

// Verificar Token
export function validarExpiracionToken() { 
    return async dispatch => {
        // dispatch(enviarLogin())
        let token = await AsyncStorage.getItem('token');
        const logueado = await AsyncStorage.getItem('logueado');;
       
       try {
         if(logueado == 'true'){
           if(token){
            let permisos = await AsyncStorage.getItem('permisos');
            permisos = JSON.parse(permisos);
            dispatch(verificarLoginExito(permisos))
           } else{
             dispatch(verifcarLoginError())
           }
         } else {
          dispatch(verifcarLoginError())
         }
       
       } catch (error) {
         console.log(error)
       }
  }
}

// Cerrar la sesion del usuario
export function cerrarSesionAction() { 
  return async dispatch => {
  await AsyncStorage.setItem('logueado', 'false');
   dispatch(cerrarSesion())

}
}

const enviarLogin = () => ({
  type: EMPEZAR_LOGIN,  
});
const loginExito = (data) => ({
  type: EMPEZAR_LOGIN_EXITO,
  payload:data
});
const loginError = () => ({
  type: EMPEZAR_LOGIN_ERROR,
});


const verificarLoginExito = (data) => ({
  type: VERIFICAR_LOGIN_EXITO,
  payload:data
});
const verifcarLoginError = () => ({
  type: VERIFICAR_LOGIN_ERROR,
});

const cerrarSesion = () => ({
  type: CERRAR_SESION,
});
