
import clienteAxios from '../config/api';
import {
  OBTENER_BLOQUES,
  OBTENER_BLOQUES_EXITO,
  OBTENER_BLOQUES_ERROR,
  OBTENER_LOTES,
  OBTENER_LOTES_EXITO,
  OBTENER_LOTES_ERROR,
  ENVIAR_MUESTREO_RECHAZO_FRUTA,
  ENVIAR_MUESTREO_RECHAZO_FRUTA_EXITO,
  ENVIAR_MUESTREO_RECHAZO_FRUTA_ERROR,
  OBTENER_CALIBRES,
  ENVIAR_MUESTREO_CAJAS,
  ENVIAR_MUESTREO_CAJAS_EXITO,
  ENVIAR_MUESTREO_CAJAS_ERROR,
  OBTENER_CALIBRES_EXITO,
  OBTENER_CALIBRES_ERROR,
} from '../types';
import { mostrarAlerta } from '../helpers/MostrarAlerta';

//Obtener Lotes
export function obtenerLotesAction(fecha) {
  return async dispatch => {
    
    dispatch(obtenerLotes());

    try {
      const lotes = await clienteAxios.get(`/muestreorechazosfruta/lotes/${fecha}`);  
      dispatch(obtenerLotesExito(lotes.data));
    } catch (error) {
      mostrarAlerta('Verificar Conexión Internet','Intente nuevamente','error')
      dispatch(obtenerLotesError());
    }
  };
}


//Obtener Bloques
export function obtenerBloquesAction(fecha,loteid) {
  return async dispatch => {
    
    dispatch(obtenerBloques());

    try {
      const bloques = await clienteAxios.get(`/muestreorechazosfruta/bloques/${fecha}/${loteid}`); 
      dispatch(obtenerBloquesExito(bloques.data));
    } catch (error) {
      dispatch(obtenerBloquesError());
    }
  };
}

//Obtener Calibre
export function obtenerCalibresAction() {
  return async dispatch => {
    
    dispatch(obtenerCalibres());

    try {
      const calibres = await clienteAxios.get(`/muestreocajasfruta/calibres`); 
       dispatch(obtenerCalibresExito(calibres.data));
    } catch (error) {
      mostrarAlerta('Verificar Conexión Internet','Intente nuevamente','error')
      dispatch(obtenerCalibresError());
    }
  };
}

//Enviar Formulario Fruta Rechazo
export function enviarFormularioRechazoFrutaAction(data) {
  return async dispatch => {
    
    dispatch(enviarFormulario());

    try {
     
      await clienteAxios.post(`/muestreorechazosfruta`,data); 
      mostrarAlerta('Envio exitoso','Los datos fueron guardados.','success')
      dispatch(enviarFormularioExito());
    } catch (error) {
      mostrarAlerta('Verificar Conexión Internet','Intente nuevamente','error')
      dispatch(enviarFormularioError());
    }
  };
}

//Enviar Formulario Pesos Caja
export function enviarFormularioPesosCajaAction(data) {
  return async dispatch => {
    
    dispatch(enviarFormularioPesos());

    try {
     
      await clienteAxios.post(`/muestreocajasfruta`,data); 
      mostrarAlerta('Envio exitoso','Los datos fueron guardados.','success')
      dispatch(enviarFormularioPesosExito());
    } catch (error) {
      mostrarAlerta('Verificar Conexión Internet','Intente nuevamente','error')
      dispatch(enviarFormularioPesosError());
    }
  };
}

//BLOQUES
const obtenerBloques = () => ({
  type: OBTENER_BLOQUES,
});
const obtenerBloquesExito = (data) => ({
  type: OBTENER_BLOQUES_EXITO,
  payload:data
});
const obtenerBloquesError = () => ({
  type: OBTENER_BLOQUES_ERROR,
});

//LOTES
const obtenerLotes = () => ({
  type: OBTENER_LOTES,
});
const obtenerLotesExito = (data) => ({
  type: OBTENER_LOTES_EXITO,
  payload:data
});
const obtenerLotesError = () => ({
  type: OBTENER_LOTES_ERROR,
});

//CALIBRES
const obtenerCalibres = () => ({
  type: OBTENER_CALIBRES,
});
const obtenerCalibresExito = (data) => ({
  type: OBTENER_CALIBRES_EXITO,
  payload:data
});
const obtenerCalibresError = () => ({
  type: OBTENER_CALIBRES_ERROR,
});


//ENVIO MUESTREO RECHAZO
export const enviarFormulario = () => ({
  type: ENVIAR_MUESTREO_RECHAZO_FRUTA,
});
const enviarFormularioExito = () => ({
  type: ENVIAR_MUESTREO_RECHAZO_FRUTA_EXITO
});
const enviarFormularioError = () => ({
  type: ENVIAR_MUESTREO_RECHAZO_FRUTA_ERROR,
});


//ENVIO MUESTREO PESOS CAJA
const enviarFormularioPesos = () => ({
  type: ENVIAR_MUESTREO_CAJAS,
});
const enviarFormularioPesosExito = () => ({
  type: ENVIAR_MUESTREO_CAJAS_EXITO
});
const enviarFormularioPesosError = () => ({
  type: ENVIAR_MUESTREO_CAJAS_ERROR,
});