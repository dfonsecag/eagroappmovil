import AsyncStorage from '@react-native-async-storage/async-storage';
import SweetAlert from 'react-native-sweet-alert';
import clienteAxios from '../config/api';
import {conexionInternet} from '../helpers/verificarConexionInternet';
import {
  ENVIAR_MUESTREO_RAIZ,
  ENVIAR_MUESTREO_RAIZ_EXITO,
  ENVIAR_MUESTREO_RAIZ_ERROR,
  CARGAR_MUESTREO_RAIZ,
  CARGAR_MUESTREO_RAIZ_EXITO,
  CARGAR_MUESTREO_RAIZ_ERROR,
} from '../types';

// Actualizar el bloque de la cedula que se trabajo
export function enviarFormularioRaizAction(data, id) {
  let arreglo = [];
  return async dispatch => {
    dispatch(empezarEnvio());

   
    try {     
      // Se elimina del localStorage el bloque muestreado      
      let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoRaiz');
      let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      arrCedulasMuestreoArreglo = arrCedulasMuestreoArreglo.filter((bloque) => bloque.id !== id);
      await AsyncStorage.setItem('cedulasmuestreoRaiz', JSON.stringify(arrCedulasMuestreoArreglo));
      dispatch(getCedulasPedientesExito(arrCedulasMuestreoArreglo));
      
      // Se almacena en el localStorage las cedulas de muestreo 
      let arrLocalStorage = await AsyncStorage.getItem('muestreoRaiz');
      arreglo = JSON.parse(arrLocalStorage);
      if (arrLocalStorage) {
        // Se elimina registros duplicados
        arreglo = arreglo.filter((bloque) => bloque.id !== id);
        arreglo = [...arreglo, data];
        await AsyncStorage.setItem('muestreoRaiz',JSON.stringify(arreglo));
      } else {
        await AsyncStorage.setItem('muestreoRaiz', JSON.stringify([data]));
      }

      // Se verifica si posee internet, se decide si se envia a la Api o queda almacenado localStorage
      const internet = await conexionInternet();
      if (internet) {
        const jsonLocalStorage = await AsyncStorage.getItem('muestreoRaiz');
        const arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.put(`/muestreoPlagas/updateRaiz`, arrData);       
        
        await AsyncStorage.setItem('muestreoRaiz', '');
      } 

      SweetAlert.showAlertWithOptions({
        title: 'Envio exitoso',
        subTitle:
          'El bloque de la cédula sea guardado exitosamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'success',
        cancellable: true,
      });
      // Desarrollo ver en consola los bloques que no han sido enviados
      const arrCopia = await AsyncStorage.getItem('muestreoRaiz');
      const arregloCopia = JSON.parse(arrCopia);
      if(arregloCopia){
        arregloCopia.map( a => {
          console.log(a)
        })
      }


       dispatch(enviarFormulario());

    } catch (error) {
      console.log(error)
      dispatch(envioError());
      SweetAlert.showAlertWithOptions({
        title: 'Hubo un error',
        subTitle: 'Revise los datos del formulario, e intente nuevamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    }
  };
}
// Sincrnizar Raiz
export function sincronizarRaizAction() { 
  return async dispatch => {
    dispatch(empezarEnvio());
    try {
      const jsonLocalStorage = await AsyncStorage.getItem('muestreoRaiz');
      if(jsonLocalStorage !== null){
      const arrData = JSON.parse(jsonLocalStorage);  
      await clienteAxios.put(`/muestreoPlagas/updateRaiz`, arrData); 
      
      await AsyncStorage.setItem('muestreoRaiz', '');
      } 
      dispatch(enviarFormulario());
    } catch (error) {
      console.log(error)
      dispatch(envioError());
    }
  
  }
}

// OBTENER CEDULAS PENDIENTES RAIZ
export function getCedulasPendientesRaizAction() { 
  return async dispatch => {
    let arrCedulasMuestreoArreglo = [];
    dispatch(getCedulasPedientes());
    try {
      const internet = await conexionInternet();
      if (internet) {
      const respuesta = await clienteAxios.get('/muestreoPlagas/cedulasRaiz');
      arrCedulasMuestreoArreglo = respuesta.data; 
      } else{
        let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoRaiz');
        arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      }
       await AsyncStorage.setItem('cedulasmuestreoRaiz', JSON.stringify(arrCedulasMuestreoArreglo));      
      dispatch(getCedulasPedientesExito(arrCedulasMuestreoArreglo));

    } catch (error) {
      let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoRaiz');
      arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      dispatch(getCedulasPedientesError(arrCedulasMuestreoArreglo))
    }
  };
}



const empezarEnvio = () => ({
  type: ENVIAR_MUESTREO_RAIZ,
});
const enviarFormulario = () => ({
  type: ENVIAR_MUESTREO_RAIZ_EXITO
});
const envioError = () => ({
  type: ENVIAR_MUESTREO_RAIZ_ERROR,
});

// Obtener cedulas Fruta pendientes a muestrear
const getCedulasPedientes = () => ({
  type: CARGAR_MUESTREO_RAIZ
});
const getCedulasPedientesExito = (data) => ({
  type: CARGAR_MUESTREO_RAIZ_EXITO,
  payload: data
});
const getCedulasPedientesError = (data) => ({
  type: CARGAR_MUESTREO_RAIZ_ERROR,
  payload:data
});
