
import clienteAxios from '../config/api';
import {
  OBTENER_REGISTROPHCLORO,
  OBTENER_REGISTROPHCLORO_EXITO,
  OBTENER_REGISTROPHCLORO_ERROR,
  ENVIAR_FORMULARIOPLANTA,
  ENVIAR_FORMULARIOPLANTA_EXITO,
  ENVIAR_FORMULARIOPLANTA_ERROR,
} from '../types';
import { mostrarAlerta } from '../helpers/MostrarAlerta';

//Obtener Ph Cloro en platan del día
export function obtenerPhClorPlantaAction(fecha) {
  return async dispatch => {
    
    dispatch(obtenerPhCloroDia());

    try {
      const registro = await clienteAxios.get(`/registroMedicionCloro/${fecha}`);  
      dispatch(obtenerPhCloroDiaExito(registro.data));
    } catch (error) {
      mostrarAlerta('Verificar Conexión Internet','Intente nuevamente','error')
      dispatch(obtenerPhCloroDiaError());
    }
  };
}

//Enviar Formulario Fruta Rechazo
export function enviarFormularioPhCloroPlantaAction(data) {
  return async dispatch => {
    
    dispatch(enviarFormulario());

    try {
     
      await clienteAxios.post(`/registroMedicionCloro`,data); 
      mostrarAlerta('Envio exitoso','Los datos fueron guardados.','success')
      dispatch(enviarFormularioExito());
    } catch (error) {
      mostrarAlerta('Verificar Conexión Internet','Intente nuevamente','error')
      dispatch(enviarFormularioError());
    }
  };
}

//OBTENER
const obtenerPhCloroDia = () => ({
  type: OBTENER_REGISTROPHCLORO,
});
const obtenerPhCloroDiaExito = (data) => ({
  type: OBTENER_REGISTROPHCLORO_EXITO,
  payload:data
});
const obtenerPhCloroDiaError = () => ({
  type: OBTENER_REGISTROPHCLORO_ERROR,
});

//ENVIO FORMULARIO
export const enviarFormulario = () => ({
  type: ENVIAR_FORMULARIOPLANTA,
});
const enviarFormularioExito = () => ({
  type: ENVIAR_FORMULARIOPLANTA_EXITO
});
const enviarFormularioError = () => ({
  type: ENVIAR_FORMULARIOPLANTA_ERROR,
});
