import AsyncStorage from '@react-native-async-storage/async-storage';
import SweetAlert from 'react-native-sweet-alert';
import clienteAxios from '../config/api';
import {conexionInternet} from '../helpers/verificarConexionInternet';
import {
  ENVIAR_MUESTREO_FRUTA,
  ENVIAR_MUESTREO_FRUTA_EXITO,
  ENVIAR_MUESTREO_FRUTA_ERROR,
  CARGAR_MUESTREO_FRUTA,
  CARGAR_MUESTREO_FRUTA_EXITO,
  CARGAR_MUESTREO_FRUTA_ERROR,
} from '../types';

// Actualizar el bloque de la cedula que se trabajo
export function enviarFormularioFrutaAction(data, id) {
  let arreglo = [];
  return async dispatch => {
    dispatch(empezarEnvio());


    try {
     
      
      // Se elimina del localStorage el bloque muestreado      
      let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoFruta');
      let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      arrCedulasMuestreoArreglo = arrCedulasMuestreoArreglo.filter((bloque) => bloque.id !== id);
      await AsyncStorage.setItem('cedulasmuestreoFruta', JSON.stringify(arrCedulasMuestreoArreglo));
      dispatch(getCedulasPedientesExito(arrCedulasMuestreoArreglo));

      //Se almacena en el localStorage las cedulas de muestreo 
      let arrLocalStorage = await AsyncStorage.getItem('muestreoFruta');
      arreglo = JSON.parse(arrLocalStorage);
      if (arrLocalStorage) {
         // Se elimina registros duplicados
        arreglo = arreglo.filter((bloque) => bloque.id !== id);
        arreglo = [...arreglo, data];
        await AsyncStorage.setItem('muestreoFruta',JSON.stringify(arreglo));
      } else {
        await AsyncStorage.setItem('muestreoFruta', JSON.stringify([data]));
      }

      // Se verifica si posee internet, se decide si se envia a la Api o queda almacenado localStorage
      const internet = await conexionInternet();
      if (internet) {
        const jsonLocalStorage = await AsyncStorage.getItem('muestreoFruta');
        const arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.put(`/muestreoPlagas/fruta`, arrData);       
        
        await AsyncStorage.setItem('muestreoFruta', '');
      } 

      SweetAlert.showAlertWithOptions({
        title: 'Envio exitoso',
        subTitle:
          'El bloque de la cédula sea guardado exitosamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'success',
        cancellable: true,
      });
      // Desarrollo ver en consola los bloques que no han sido enviados
      const arrCopia = await AsyncStorage.getItem('muestreoFruta');
      const arregloCopia = JSON.parse(arrCopia);
      if(arregloCopia){
        arregloCopia.map( a => {
          console.log(a)
        })
      }


      dispatch(enviarFormulario());

    } catch (error) {
      console.log(error)
      dispatch(envioError());
      SweetAlert.showAlertWithOptions({
        title: 'Hubo un error',
        subTitle: 'Revise los datos del formulario, e intente nuevamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    }
  };
}
// Sincrnizar Fruta
export function sincronizarFrutaAction() { 
  return async dispatch => {
    dispatch(empezarEnvio());
    try {

      const jsonLocalStorage = await AsyncStorage.getItem('muestreoFruta');
      if(jsonLocalStorage !== null){
      const arrData = JSON.parse(jsonLocalStorage);
      await clienteAxios.put(`/muestreoPlagas/fruta`, arrData);     
      
      await AsyncStorage.setItem('muestreoFruta', '');
      }       
      dispatch(enviarFormulario());

    } catch (error) {
      console.log(error)
      dispatch(envioError());
    }
  
  }
}

// OBTENER CEDULAS PENDIENTES FRUTA
export function getCedulasPendientesFrutaAction() { 
  return async dispatch => {
    let arrCedulasMuestreoArreglo = [];
    dispatch(getCedulasPedientes());
    try {
      const internet = await conexionInternet();
      if (internet) {
      const respuesta = await clienteAxios.get('/muestreoPlagas/cedulasFruta');
      arrCedulasMuestreoArreglo = respuesta.data; 
      } else{
        let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoFruta');
        arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      }
       await AsyncStorage.setItem('cedulasmuestreoFruta', JSON.stringify(arrCedulasMuestreoArreglo));      
      dispatch(getCedulasPedientesExito(arrCedulasMuestreoArreglo));

    } catch (error) {
      let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoFruta');
      arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      dispatch(getCedulasPedientesError(arrCedulasMuestreoArreglo))
    }
  };
}



const empezarEnvio = () => ({
  type: ENVIAR_MUESTREO_FRUTA,
});
const enviarFormulario = () => ({
  type: ENVIAR_MUESTREO_FRUTA_EXITO
});
const envioError = () => ({
  type: ENVIAR_MUESTREO_FRUTA_ERROR,
});

// Obtener cedulas Fruta pendientes a muestrear
const getCedulasPedientes = () => ({
  type: CARGAR_MUESTREO_FRUTA
});
const getCedulasPedientesExito = (data) => ({
  type: CARGAR_MUESTREO_FRUTA_EXITO,
  payload: data
});
const getCedulasPedientesError = (data) => ({
  type: CARGAR_MUESTREO_FRUTA_ERROR,
  payload:data
});
