import AsyncStorage from '@react-native-async-storage/async-storage';
import SweetAlert from 'react-native-sweet-alert';
import clienteAxios from '../config/api';
import { clasificarCalibres } from '../helpers/helperPremaduracion';
import {conexionInternet} from '../helpers/verificarConexionInternet';
import {
  CARGAR_BLOQUES_PREMADURACION,
  CARGAR_GRUPOS_PREMADURACION,
  CARGAR_GRUPOS_PREMADURACION_ERROR,
  CARGAR_GRUPOS_PREMADURACION_EXITO,
  CARGAR_MUESTREO_PREMADURACION,
  CARGAR_MUESTREO_PREMADURACION_ERROR,
  CARGAR_MUESTREO_PREMADURACION_EXITO,
  ENVIAR_MUESTREO_PREMADURACION,
  ENVIAR_MUESTREO_PREMADURACION_ERROR,
  ENVIAR_MUESTREO_PREMADURACION_EXITO,
  SETEAR_BLOQUES_PREMADURACION,
} from '../types';

// Actualizar el bloque de la cedula que se trabajo
export function enviarFormularioPremaduracionAction(formulario, datos ,id, imagen1, imagen2) {
  let arreglo = [];
  return async dispatch => {
    dispatch(empezarEnvio());

    try {
     const data =  clasificarCalibres(datos,formulario);
     
      // Se elimina del localStorage el bloque muestreado
      let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoPremaduracion');
      let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      arrCedulasMuestreoArreglo = arrCedulasMuestreoArreglo.filter(bloque => bloque.id !== id);
      await AsyncStorage.setItem('cedulasmuestreoPremaduracion',JSON.stringify(arrCedulasMuestreoArreglo));
      dispatch(getCedulasPedientesExito(arrCedulasMuestreoArreglo));

      //Se almacena en el localStorage las cedulas de muestreo
      let arrLocalStorage = await AsyncStorage.getItem('muestreoPremaduracion');
      arreglo = JSON.parse(arrLocalStorage);
      if (arrLocalStorage) {
        arreglo = [...arreglo, data];
        await AsyncStorage.setItem('muestreoPremaduracion',JSON.stringify(arreglo));
      } else {
        await AsyncStorage.setItem('muestreoPremaduracion',JSON.stringify([data]));
      }

      //Se guarda las imagenes de premaduracion
        let arrStorageImagenes = await AsyncStorage.getItem('imagenesPremaduracion');
        let arregloImagenes = JSON.parse(arrStorageImagenes);
        if (arregloImagenes) {
          arregloImagenes = [...arregloImagenes, {bloque: id, imagen:imagen1},{bloque: id, imagen:imagen2}];
          await AsyncStorage.setItem('imagenesPremaduracion',JSON.stringify(arregloImagenes));
        } else {
          await AsyncStorage.setItem('imagenesPremaduracion',JSON.stringify([{bloque: id, imagen:imagen1},{bloque: id, imagen:imagen2}]));
        }
      
      // Se verifica si posee internet, se decide si se envia a la Api o queda almacenado localStorage
      const internet = await conexionInternet();
      if (internet) {
        const jsonLocalStorage = await AsyncStorage.getItem('muestreoPremaduracion');
        const arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.put(
          `/muestreoEstimacion/actualizarCedulaPremaduraciones`,
          arrData,
        );  
        sincronizarImagenes();
        await AsyncStorage.setItem('muestreoPremaduracion', '');
      }

      SweetAlert.showAlertWithOptions({
        title: 'Envio exitoso',
        subTitle: 'El bloque de la cédula sea guardado exitosamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'success',
        cancellable: true,
      });
     

      dispatch(enviarFormulario());
    } catch (error) {
      console.log(error);
      dispatch(envioError());
      SweetAlert.showAlertWithOptions({
        title: 'Hubo un error',
        subTitle: 'Revise los datos del formulario, e intente nuevamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    }
  };
}
// Sincronizar las imagenes
const sincronizarImagenes = async() => {

  const jsonLocalStorageImagen = await AsyncStorage.getItem('imagenesPremaduracion');
  if (jsonLocalStorageImagen !== null) {
  const arrDataImagenes = JSON.parse(jsonLocalStorageImagen);
  for (const imagen of arrDataImagenes) {
    if(imagen.imagen !== ''){
      await clienteAxios.post(`/muestreoEstimacion/imagenes`,imagen);
    }    
  }
  await AsyncStorage.setItem('imagenesPremaduracion', '');
}

}

// Sincronizar Premaduracion
export function sincronizarPremaduracionAction() {
  return async dispatch => {
    dispatch(empezarEnvio());
    try {
      const jsonLocalStorage = await AsyncStorage.getItem('muestreoPremaduracion');
      if (jsonLocalStorage !== null) {
        const arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.put(
          `/muestreoEstimacion/actualizarCedulaPremaduraciones`,
          arrData,
        );
        await AsyncStorage.setItem('muestreoPremaduracion', '');
        sincronizarImagenes();
      }

      dispatch(enviarFormulario());
    } catch (error) {
      console.log(error);
      dispatch(envioError());
    }
  };
}

// OBTENER CEDULAS PENDIENTES PREMADURACION
export function getCedulasPendientesPremaduracionAction() {
  return async dispatch => {
    let arrCedulasMuestreoArreglo = [];
    dispatch(getCedulasPedientes());
    try {
      const internet = await conexionInternet();
      if (internet) {
        const respuesta = await clienteAxios.get('/muestreoEstimacion/premaduraciones');
        arrCedulasMuestreoArreglo = respuesta.data;
      } else {
        let arrCedulasMuestreo = await AsyncStorage.getItem(
          'cedulasmuestreoPremaduracion',
        );
        arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      }
      await AsyncStorage.setItem(
        'cedulasmuestreoPremaduracion',
        JSON.stringify(arrCedulasMuestreoArreglo),
      );
      dispatch(getCedulasPedientesExito(arrCedulasMuestreoArreglo));
    } catch (error) {
      let arrCedulasMuestreo = await AsyncStorage.getItem(
        'cedulasmuestreoPremaduracion',
      );
      arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      dispatch(getCedulasPedientesError(arrCedulasMuestreoArreglo));
    }
  };
}

const empezarEnvio = () => ({
  type: ENVIAR_MUESTREO_PREMADURACION,
});
const enviarFormulario = () => ({
  type: ENVIAR_MUESTREO_PREMADURACION_EXITO,
});
const envioError = () => ({
  type: ENVIAR_MUESTREO_PREMADURACION_ERROR,
});

// Obtener cedulas PREMADURACION pendientes a muestrear
const getCedulasPedientes = () => ({
  type: CARGAR_MUESTREO_PREMADURACION,
});
const getCedulasPedientesExito = data => ({
  type: CARGAR_MUESTREO_PREMADURACION_EXITO,
  payload: data,
});
const getCedulasPedientesError = data => ({
  type: CARGAR_MUESTREO_PREMADURACION_ERROR,
  payload: data,
});

