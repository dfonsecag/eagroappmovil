import AsyncStorage from '@react-native-async-storage/async-storage';
import SweetAlert from 'react-native-sweet-alert';
import clienteAxios from '../config/api';
import {conexionInternet} from '../helpers/verificarConexionInternet';
import {
  CARGAR_AVANCE_OBRA_ERROR,
  CARGAR_BOQUILLAS_EXITO,
  CARGAR_CEDULAS_OBRA,
  CARGAR_CEDULAS_OBRA_EXITO,
  CARGAR_CHOFERES_EXITO,
  CARGAR_IMPLEMENTOS_EXITO,
  CARGAR_MAQUINARIA_EXITO,
  CEDULA_AVANCES_OBRA,
  CEDULA_AVANCE_OBRA_SELECCIONADA,
  ENVIAR_AVANCE_OBRA,
  ENVIAR_AVANCE_OBRA_ERROR,
  ENVIAR_AVANCE_OBRA_EXITO,
} from '../types';


// Actualizar el bloque de la cedula que se trabajo
export function enviarFormularioAction(data, id, estado) {
  let arreglo = [];
  return async dispatch => {
    dispatch(empezarEnvio());

    try {
      if(estado === 3){
        // Se elimina del localStorage el bloque muestreado
        let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasAvanceObra');
        let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
        arrCedulasMuestreoArreglo = arrCedulasMuestreoArreglo.filter(
          bloque => bloque.numerocedula !== id,
        );
        await AsyncStorage.setItem(
          'cedulasAvanceObra',
          JSON.stringify(arrCedulasMuestreoArreglo),
        );
        dispatch(getCedulasPedientesExito(arrCedulasMuestreoArreglo));
      } 

      //Se almacena en el localStorage las cedulas de muestreo
      let arrLocalStorage = await AsyncStorage.getItem('avanceobra');
      arreglo = JSON.parse(arrLocalStorage);
      if (arrLocalStorage) {
        arreglo = [...arreglo, data];
        await AsyncStorage.setItem('avanceobra', JSON.stringify(arreglo));
      } else {
        await AsyncStorage.setItem('avanceobra', JSON.stringify([data]));
      }

      // Se verifica si posee internet, se decide si se envia a la Api o queda almacenado localStorage
      const internet = await conexionInternet();
      if (internet) {
        const jsonLocalStorage = await AsyncStorage.getItem('avanceobra');
        const arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.put(`/avanceCedulas`, arrData);
        await AsyncStorage.setItem('avanceobra', '');
      }

      SweetAlert.showAlertWithOptions({
        title: 'Envio exitoso',
        subTitle: 'La cédula sea guardado exitosamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'success',
        cancellable: true,
      });
      // Desarrollo ver en consola los bloques que no han sido enviados
      const arrCopia = await AsyncStorage.getItem('avanceobra');
      const arregloCopia = JSON.parse(arrCopia);
      if (arregloCopia) {
        arregloCopia.map(a => {
          console.log(a);
        });
      }

      dispatch(enviarFormulario());
    } catch (error) {
      console.log(error);
      dispatch(envioError());
      SweetAlert.showAlertWithOptions({
        title: 'Hubo un error',
        subTitle: 'Revise los datos del formulario, e intente nuevamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    }
  };
}

// Acrualizar el avance de obra Manual
export function enviarFormularioManualAction(data, id) {
  let arreglo = [];
  return async dispatch => {
    dispatch(empezarEnvio());

    try {
      // Se elimina del localStorage el bloque muestreado
      let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasAvanceObra');
      let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      arrCedulasMuestreoArreglo = arrCedulasMuestreoArreglo.filter(
        bloque => bloque.numerocedula !== id,
      );
      await AsyncStorage.setItem(
        'cedulasAvanceObra',
        JSON.stringify(arrCedulasMuestreoArreglo),
      );
      dispatch(getCedulasPedientesExito(arrCedulasMuestreoArreglo));

      //Se almacena en el localStorage las cedulas de muestreo
      let arrLocalStorage = await AsyncStorage.getItem('avanceObraManual');
      arreglo = JSON.parse(arrLocalStorage);
      if (arrLocalStorage) {
        arreglo = [...arreglo, data];
        await AsyncStorage.setItem('avanceObraManual', JSON.stringify(arreglo));
      } else {
        await AsyncStorage.setItem('avanceObraManual', JSON.stringify([data]));
      }

      // Se verifica si posee internet, se decide si se envia a la Api o queda almacenado localStorage
      const internet = await conexionInternet();
      if (internet) {
        const jsonLocalStorage = await AsyncStorage.getItem('avanceObraManual');
        const arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.put(`/avanceCedulas/manual`, arrData);
        await AsyncStorage.setItem('avanceObraManual', '');
      }

      SweetAlert.showAlertWithOptions({
        title: 'Envio exitoso',
        subTitle: 'La cédula sea guardado exitosamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'success',
        cancellable: true,
      });
      // Desarrollo ver en consola los bloques que no han sido enviados
      const arrCopia = await AsyncStorage.getItem('avanceObraManual');
      const arregloCopia = JSON.parse(arrCopia);
      if (arregloCopia) {
        arregloCopia.map(a => {
          console.log(a);
        });
      }

      dispatch(enviarFormulario(data));
    } catch (error) {
      console.log(error);
      dispatch(envioError());
      SweetAlert.showAlertWithOptions({
        title: 'Hubo un error',
        subTitle: 'Revise los datos del formulario, e intente nuevamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    }
  };
}

// Sincronizar avance obra
export function sincronizarAvanceObraAction() {
  return async dispatch => {
    
    dispatch(enviarFormulario());

    try {
      const jsonLocalStorage = await AsyncStorage.getItem('avanceobra');
      if (jsonLocalStorage !== null) {
        const arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.put(`/avanceCedulas`, arrData);
        await AsyncStorage.setItem('avanceobra', '');
      }
      dispatch(empezarEnvio());
    } catch (error) {
      console.log(error);
      dispatch(envioError());
    }
  };
}

// OBTENER CEDULAS PENDIENTES AVANCE OBRAR
export function getCedulasPendientesAvanceObraAction() {
  return async dispatch => {
    let arrCedulasMuestreoArreglo = [];
    let arrAvancesArreglo = [];
    let arrImplementosArreglo = [];
    let arrMaquinariaArreglo = [];
    let arrBoquillasArreglo = [];
    let arrChoferesArreglo = [];
    
    dispatch(getCedulasPedientes());
    try {
      const internet = await conexionInternet();
      if (internet) {
        // Obtiene las cedulas pendientes
        const cedulas = await clienteAxios.get('/avanceCedulas');
        arrCedulasMuestreoArreglo = cedulas.data.respuesta;
        arrAvancesArreglo = cedulas.data.avances;

        // Obtiene los implementos
        const implementos = await clienteAxios.get(
          '/avanceCedulas/obtenerImplementos',
        );
        arrImplementosArreglo = implementos.data;
        // Obtiene la maquinaria
        const maquinaria = await clienteAxios.get(
          '/avanceCedulas/obtenerMaquinaria',
        );
        arrMaquinariaArreglo = maquinaria.data;
        // Obtiene la boquillas
        const boquillas = await clienteAxios.get(
          '/avanceCedulas/obtenerBoquillas',
        );
        arrBoquillasArreglo = boquillas.data;
        // Obtiene los choferes
        const choferes = await clienteAxios.get(
          '/avanceCedulas/obtenerChoferes',
        );
        arrChoferesArreglo = choferes.data;
      } else {
        let arrCedulasMuestreo = await AsyncStorage.getItem(
          'cedulasAvanceObra',
        );
        arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);

        let arrAvancesObra = await AsyncStorage.getItem(
          'avancesCedula',
        );
        arrAvancesArreglo = JSON.parse(arrAvancesObra);

        let arrImplemento = await AsyncStorage.getItem('implementos');
        arrImplementosArreglo = JSON.parse(arrImplemento);

        let arrMaquinaria = await AsyncStorage.getItem('maquinaria');
        arrMaquinariaArreglo = JSON.parse(arrMaquinaria);

        let arrBoquillas = await AsyncStorage.getItem('boquillas');
        arrBoquillasArreglo = JSON.parse(arrBoquillas);

        let arrChoferes = await AsyncStorage.getItem('choferes');
        arrChoferesArreglo = JSON.parse(arrChoferes);
      }
      await AsyncStorage.setItem(
        'cedulasAvanceObra',
        JSON.stringify(arrCedulasMuestreoArreglo),
      );
      await AsyncStorage.setItem(
        'avancesCedula',
        JSON.stringify(arrAvancesArreglo),
      );
      await AsyncStorage.setItem(
        'implementos',
        JSON.stringify(arrImplementosArreglo),
      );
      await AsyncStorage.setItem(
        'maquinaria',
        JSON.stringify(arrMaquinariaArreglo),
      );
      await AsyncStorage.setItem(
        'boquillas',
        JSON.stringify(arrBoquillasArreglo),
      );
      await AsyncStorage.setItem(
        'choferes',
        JSON.stringify(arrChoferesArreglo),
      );

      dispatch(getCedulasPedientesExito(arrCedulasMuestreoArreglo));
      dispatch(getImplementosExito(arrImplementosArreglo));
      dispatch(getMaquinariaExito(arrMaquinariaArreglo));
      dispatch(getBoquillasExito(arrBoquillasArreglo));
      dispatch(getChoferesExito(arrChoferesArreglo));
    } catch (error) {
      let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasAvanceObra');
      arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      dispatch(getCedulasPedientesError(arrCedulasMuestreoArreglo));

      let arrCedulasAvances = await AsyncStorage.getItem('avancesCedula');
      arrAvancesArreglo = JSON.parse(arrCedulasAvances);
      dispatch(getCedulasPedientesError(arrAvancesArreglo));

      let arrImplemento = await AsyncStorage.getItem('implementos');
      arrImplementosArreglo = JSON.parse(arrImplemento);
      dispatch(getImplementosExito(arrImplementosArreglo));

      let arrMaquinaria = await AsyncStorage.getItem('maquinaria');
      arrMaquinariaArreglo = JSON.parse(arrMaquinaria);
      dispatch(getMaquinariaExito(arrMaquinariaArreglo));

      let arrBoquillas = await AsyncStorage.getItem('boquillas');
      arrBoquillasArreglo = JSON.parse(arrBoquillas);
      dispatch(getBoquillasExito(arrBoquillasArreglo));

      let arrChoferes = await AsyncStorage.getItem('choferes');
      arrChoferesArreglo = JSON.parse(arrChoferes);
      dispatch(getBoquillasExito(arrChoferesArreglo));
    }
  };
}

// Obtiene la cedula seleccionada Peso
export function getCedulaSeleccionada(cedula) {
  return async dispatch => {
    if (cedula !== 0 || cedula !== '') {
      let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasAvanceObra');
      let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      arrCedulasMuestreoArreglo = arrCedulasMuestreoArreglo.filter(
        ced => ced.numerocedula === cedula,
      );

      let arrAvancesCedula = await AsyncStorage.getItem('avancesCedula');
      let arrAvancesCedulaArreglo = JSON.parse(arrAvancesCedula);
      arrAvancesCedulaArreglo = arrAvancesCedulaArreglo.filter(
        ced => ced.cedula === cedula,
      );
      let arrAvanceObra = await AsyncStorage.getItem('avanceobra');
      if(arrAvanceObra){
        let arrAvancesObraArreglo = JSON.parse(arrAvanceObra);
        const avancesAgregar = arrAvancesObraArreglo.filter(
          ced => ced.numerocedula === cedula,
        );
        if(avancesAgregar.length > 0){
          avancesAgregar.map( a => {
            console.log(a)
            const horaInicio = a.horainicio.split(' ',2);
            const horaFin = a.horafinal.split(' ',2);
            arrAvancesCedulaArreglo.push({nombre:a.creadopor, inicio:`${horaInicio[1]}:00`, fin:`${horaFin[1]}:00`, fecha:a.fcreacion, cedula:a.numerocedula, avance_litros:a.avance_litros})
          })
        }
        
      }
      dispatch(getCedulaSeleccionadaExito(arrCedulasMuestreoArreglo[0]));
      dispatch(getCedulaAvanceExito(arrAvancesCedulaArreglo));
    }
  };
}

const enviarFormulario = () => ({
  type: ENVIAR_AVANCE_OBRA,
});
const empezarEnvio = () => ({
  type: ENVIAR_AVANCE_OBRA_EXITO,
});
const envioError = () => ({
  type: ENVIAR_AVANCE_OBRA_ERROR,
});

// Obtener Meristemos pendientes a muestrear
const getCedulasPedientes = () => ({
  type: CARGAR_CEDULAS_OBRA,
});
const getCedulasPedientesExito = data => ({
  type: CARGAR_CEDULAS_OBRA_EXITO,
  payload: data,
});
const getImplementosExito = data => ({
  type: CARGAR_IMPLEMENTOS_EXITO,
  payload: data,
});
const getMaquinariaExito = data => ({
  type: CARGAR_MAQUINARIA_EXITO,
  payload: data,
});

const getBoquillasExito = data => ({
  type: CARGAR_BOQUILLAS_EXITO,
  payload: data,
});

const getChoferesExito = data => ({
  type: CARGAR_CHOFERES_EXITO,
  payload: data,
});

const getCedulasPedientesError = data => ({
  type: CARGAR_AVANCE_OBRA_ERROR,
  payload: data,
});

// Cedula seleccionada
const getCedulaSeleccionadaExito = data => ({
  type: CEDULA_AVANCE_OBRA_SELECCIONADA,
  payload: data,
});

const getCedulaAvanceExito = data => ({
  type: CEDULA_AVANCES_OBRA,
  payload: data,
});