
import { EMPEZAR_SINCRONIZACION, FINALIZAR_SINCRONIZACION } from '../types';


// Sincronizar avance obra
export function finalizarSincronizacionAction() {
  return async dispatch => {
    
    setTimeout(() => {
      dispatch(finalizarSincronizacion())
    }, 500);
  };
}

export const empezarSincronizacion = () => ({
  type: EMPEZAR_SINCRONIZACION
});

// Cedula seleccionada
export const finalizarSincronizacion = () => ({
  type: FINALIZAR_SINCRONIZACION
});
