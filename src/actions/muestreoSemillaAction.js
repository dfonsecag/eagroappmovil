import AsyncStorage from '@react-native-async-storage/async-storage';
import SweetAlert from 'react-native-sweet-alert';
import clienteAxios from '../config/api';
import {conexionInternet} from '../helpers/verificarConexionInternet';
import {
  ENVIAR_MUESTREO_SEMILLA,
  ENVIAR_MUESTREO_SEMILLA_EXITO,
  ENVIAR_MUESTREO_SEMILLA_ERROR,
  CARGAR_MUESTREO_SEMILLA,
  CARGAR_MUESTREO_SEMILLA_EXITO,
  CARGAR_BLOQUES,
  SETEAR_BLOQUES,
  AGREGAR_EMPLEADO_EXITO,
  ELIMINAR_EMPLEADO_EXITO,
  SETEAR_EMPLEADO_EXITO,
} from '../types';
import { mostrarAlerta } from '../helpers/MostrarAlerta';

/**
 * Envia el formulario con la información
 * @param {*} data 
 * @returns 
 */
export function enviarFormularioAction(data) {
  let arreglo = [];
  return async dispatch => {
    dispatch(empezarEnvio());

   
    try {     
           
      // Se almacena en el localStorage las muestras
      let arrLocalStorage = await AsyncStorage.getItem('muestreoSemilla');
      arreglo = JSON.parse(arrLocalStorage);
      if (arrLocalStorage) {
        arreglo = [...arreglo, data];
        await AsyncStorage.setItem('muestreoSemilla',JSON.stringify(arreglo));
      } else {
        await AsyncStorage.setItem('muestreoSemilla', JSON.stringify([data]));
      }

      // Se verifica si posee internet, se decide si se envia a la Api o queda almacenado localStorage
      const internet = await conexionInternet();
      if (internet) {
        const jsonLocalStorage = await AsyncStorage.getItem('muestreoSemilla');
        const arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.post(`/muestreoSemilla`, arrData);       
        
        await AsyncStorage.setItem('muestreoSemilla', '');
      } 

      SweetAlert.showAlertWithOptions({
        title: 'Envio exitoso',
        subTitle: 'El formulario de muestreo fue enviado exitosamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'success',
        cancellable: true,
      });
    
       dispatch(enviarFormulario());

    } catch (error) {
      dispatch(enviarFormulario());
      SweetAlert.showAlertWithOptions({
        title: 'Envio exitoso',
        subTitle: 'El formulario de muestreo fue enviado exitosamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'success',
        cancellable: true,
      });
    }
  };
}

/**
 * Envia el formulario con la información
 * @param {*} data 
 * @returns 
 */
export function enviarFormularioTerrazasAction(data) {
  let arreglo = [];
  return async dispatch => {
    dispatch(empezarEnvio());

   
    try {     
           
      // Se almacena en el localStorage las muestras
      let arrLocalStorage = await AsyncStorage.getItem('terrazasContratista');
      arreglo = JSON.parse(arrLocalStorage);
      if (arrLocalStorage) {
        arreglo = [...arreglo, data];
        await AsyncStorage.setItem('terrazasContratista',JSON.stringify(arreglo));
      } else {
        await AsyncStorage.setItem('terrazasContratista', JSON.stringify([data]));
      }

      // Se verifica si posee internet, se decide si se envia a la Api o queda almacenado localStorage
      const internet = await conexionInternet();
      if (internet) {
        const jsonLocalStorage = await AsyncStorage.getItem('terrazasContratista');
        const arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.post(`/registroTerrazas`, arrData);       
        
        await AsyncStorage.setItem('terrazasContratista', '');
      } 

      SweetAlert.showAlertWithOptions({
        title: 'Envio exitoso',
        subTitle: 'El formulario fue enviado exitosamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'success',
        cancellable: true,
      });
    
       dispatch(enviarFormulario());

    } catch (error) {
      dispatch(enviarFormulario());
      SweetAlert.showAlertWithOptions({
        title: 'Envio exitoso',
        subTitle: 'El formulario fue enviado exitosamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'success',
        cancellable: true,
      });
    }
  };
}


/**
 * Envia el formulario de muestreo calidad siembra
 * @param {*} data 
 * @returns 
 */
export function enviarFormularioMuestreoCalidadSiembraAction(data,distanciaEntreSiembra) {
  let arreglo = [];
  let arregloDistancia = [];
  let arrLocalStorage;
  return async dispatch => {
    dispatch(empezarEnvio());
    
    try {     
           
      // Se almacena en el localStorage las muestras
      arrLocalStorage = await AsyncStorage.getItem('muestreoCalidadSiembra');
      arreglo = JSON.parse(arrLocalStorage);
      if (arrLocalStorage) {
        arreglo = [...arreglo, data];
        await AsyncStorage.setItem('muestreoCalidadSiembra',JSON.stringify(arreglo));
      } else {
        await AsyncStorage.setItem('muestreoCalidadSiembra', JSON.stringify([data]));
      }

       // Se almacena en el localStorage las muestras
       arrLocalStorage = await AsyncStorage.getItem('muestreoDistanciaSiembra');
       arregloDistancia = JSON.parse(arrLocalStorage);
       if (arrLocalStorage) {
         arregloDistancia = [...arregloDistancia, distanciaEntreSiembra];
         await AsyncStorage.setItem('muestreoDistanciaSiembra',JSON.stringify(arregloDistancia));
       } else {
         await AsyncStorage.setItem('muestreoDistanciaSiembra', JSON.stringify([distanciaEntreSiembra]));
       }
 

      // Se verifica si posee internet, se decide si se envia a la Api o queda almacenado localStorage
      const internet = await conexionInternet();
      if (internet) {
        let jsonLocalStorage = await AsyncStorage.getItem('muestreoCalidadSiembra');
        let arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.post(`/muestreocalidadsiembra`, arrData);    
        await AsyncStorage.setItem('muestreoCalidadSiembra', '');
        
        let jsonLocalStorageDistancia = await AsyncStorage.getItem('muestreoDistanciaSiembra');
        let arrDataDistancia = JSON.parse(jsonLocalStorageDistancia);
        console.log(arrDataDistancia)
        await clienteAxios.post(`/muestreodistanciasiembra`, arrDataDistancia);    
        await AsyncStorage.setItem('muestreoDistanciaSiembra', '');


      } 
      mostrarAlerta('Envio exitoso','El formulario de control calidad siembra fue enviado exitosamente','succes');
       
      dispatch(enviarFormulario());

    } catch (error) {
      dispatch(enviarFormulario());
      mostrarAlerta('Envio exitoso','El formulario de control calidad siembra fue enviado exitosamente','succes');
    }
  };
}

export function getBloquesAction(loteid) { 
  return async dispatch => {
    dispatch(empezarEnvio());
    try {
      let bloques = await AsyncStorage.getItem('bloquesSemillas');
      bloques = JSON.parse(bloques);
      bloques = bloques.filter( b => b.loteid == loteid)
      dispatch(getBloques(bloques))
    } catch (error) {
      dispatch(envioError());
    }
  
  }
}

export function eliminarPersonaAction(data) { 
  return async dispatch => {
    dispatch(empezarEnvio());
    try {
     dispatch(eliminarEmpleadoAction(data))
    } catch (error) {
      dispatch(envioError());//
    }  
  }
}


/**
 * Sincroniza datos pendientes de enviar del muestreo semilla
 * @returns 
 */
export function sincronizarMuestreoSemillaAction() { 
  return async dispatch => {
    dispatch(empezarEnvio());
    try {
      const jsonLocalStorage = await AsyncStorage.getItem('muestreoSemilla');
      if(jsonLocalStorage !== null){
      const arrData = JSON.parse(jsonLocalStorage);  
      await clienteAxios.post(`/muestreoSemilla`, arrData);       
      await AsyncStorage.setItem('muestreoSemilla', '');
      } 

      const jsonLocalStorageTerrazas = await AsyncStorage.getItem('terrazasContratista');
      if(jsonLocalStorageTerrazas !== null){
      const arrData = JSON.parse(jsonLocalStorageTerrazas);  
      await clienteAxios.post(`/registroTerrazas`, arrData);       
      await AsyncStorage.setItem('terrazasContratista', '');
      } 

      const jsonLocalControlCalidadSiembra = await AsyncStorage.getItem('muestreoCalidadSiembra');
      if(jsonLocalControlCalidadSiembra !== null){
      const arrData = JSON.parse(jsonLocalControlCalidadSiembra);  
      await clienteAxios.post(`/muestreocalidadsiembra`, arrData);       
      await AsyncStorage.setItem('muestreoCalidadSiembra', '');
      } 

      const jsonLocalDistanciaSiembra = await AsyncStorage.getItem('muestreoDistanciaSiembra');
      if(jsonLocalDistanciaSiembra !== null){
      const arrData = JSON.parse(jsonLocalDistanciaSiembra); 
      await clienteAxios.post(`/muestreodistanciasiembra`, arrData);       
      await AsyncStorage.setItem('muestreoDistanciaSiembra', '');
      } 


      dispatch(enviarFormulario());
    } catch (error) {
      dispatch(envioError());
    }
  
  }
}

/**
 * 
 * @returns Obtiene los datos para el formulario semilla
 */
export function getDatosFormularioSemillaAction() { 
  return async dispatch => {
    let arrLotesArreglo = [];
    let arrRangosArreglo = [];
    let arrTamanioSemmillasArreglo = [];
    let arrContratistasArreglo = [];
    let arrBloquesArreglo = [];
    let arrActividadesArreglo = [];
    dispatch(getDatos());
    try {
      const internet = await conexionInternet();
      if (internet) {
      const respuesta = await clienteAxios.get('/muestreoSemilla');
      arrLotesArreglo = respuesta.data.lotes; 
      arrRangosArreglo = respuesta.data.rangos; 
      arrTamanioSemmillasArreglo = respuesta.data.tamaniosemillas;
      arrContratistasArreglo =  respuesta.data.contratistas;
      arrBloquesArreglo =  respuesta.data.bloques;
      arrActividadesArreglo =  respuesta.data.actividades;
      } else{
        let arrLotes = await AsyncStorage.getItem('lotesSemilla');
        arrLotesArreglo = JSON.parse(arrLotes);
        let arrRangos = await AsyncStorage.getItem('rangos');
        arrRangosArreglo = JSON.parse(arrRangos);
        let arrContratistas = await AsyncStorage.getItem('contratistas');
        arrContratistasArreglo = JSON.parse(arrContratistas);
        let arrTamanioSemillas = await AsyncStorage.getItem('tamanioSemillas');
        arrTamanioSemmillasArreglo = JSON.parse(arrTamanioSemillas);
        let arrBloques = await AsyncStorage.getItem('bloquesSemillas');
        arrBloquesArreglo = JSON.parse(arrBloques);
        let arrActividades = await AsyncStorage.getItem('actividades');
        arrActividadesArreglo = JSON.parse(arrActividades);
      }
       await AsyncStorage.setItem('lotesSemilla', JSON.stringify(arrLotesArreglo));  
       await AsyncStorage.setItem('rangos', JSON.stringify(arrRangosArreglo)); 
       await AsyncStorage.setItem('contratistas', JSON.stringify(arrContratistasArreglo)); 
       await AsyncStorage.setItem('tamanioSemillas', JSON.stringify(arrTamanioSemmillasArreglo)); 
       await AsyncStorage.setItem('bloquesSemillas', JSON.stringify(arrBloquesArreglo)); 
       await AsyncStorage.setItem('actividades', JSON.stringify(arrActividadesArreglo)); 
       dispatch(getDatosExito({lotes:arrLotesArreglo,rangos:arrRangosArreglo,tamaniosemillas:arrTamanioSemmillasArreglo,contratistas:arrContratistasArreglo,actividades:arrActividadesArreglo}));

    } catch (error) {
      let arrLotes = await AsyncStorage.getItem('lotesSemilla');
      arrLotesArreglo = JSON.parse(arrLotes);
      let arrRangos = await AsyncStorage.getItem('rangos');
      arrRangosArreglo = JSON.parse(arrRangos);
      let arrContratistas = await AsyncStorage.getItem('contratistas');
      arrContratistasArreglo = JSON.parse(arrContratistas);
      let arrTamanioSemillas = await AsyncStorage.getItem('tamanioSemillas');
      arrTamanioSemmillasArreglo = JSON.parse(arrTamanioSemillas);
      let arrBloques = await AsyncStorage.getItem('bloquesSemillas');
      arrBloquesArreglo = JSON.parse(arrBloques);
      let arrActividades = await AsyncStorage.getItem('actividades');
      arrActividadesArreglo = JSON.parse(arrActividades);
      dispatch(getDatosExito({lotes:arrLotesArreglo,rangos:arrRangosArreglo,tamaniosemillas:arrTamanioSemmillasArreglo,contratistas:arrContratistasArreglo,actividades:arrActividadesArreglo}));
    }
  };
}



export const empezarEnvio = () => ({
  type: ENVIAR_MUESTREO_SEMILLA,
});
const enviarFormulario = () => ({
  type: ENVIAR_MUESTREO_SEMILLA_EXITO
});
const envioError = () => ({
  type: ENVIAR_MUESTREO_SEMILLA_ERROR,
});

// Obtener datos para formulario
const getDatos = () => ({
  type: CARGAR_MUESTREO_SEMILLA
});
const getDatosExito = (data) => ({
  type: CARGAR_MUESTREO_SEMILLA_EXITO,
  payload: data
});

// Obtener datos para formulario
const getBloques = (data) => ({
  type: CARGAR_BLOQUES,
  payload: data
});
export const setearBloques = () => ({
  type: SETEAR_BLOQUES
});

//Empleado
export const agregarEmpleado = (data) => ({
  type: AGREGAR_EMPLEADO_EXITO,
  payload: data
});

export const eliminarEmpleadoAction = (data) => ({
  type: ELIMINAR_EMPLEADO_EXITO,
  payload: data
});

export const setearEmpleados = () => ({
  type: SETEAR_EMPLEADO_EXITO
});


