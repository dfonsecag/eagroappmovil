import AsyncStorage from '@react-native-async-storage/async-storage';
import SweetAlert from 'react-native-sweet-alert';
import clienteAxios from '../config/api';
import {conexionInternet} from '../helpers/verificarConexionInternet';
import {
  CARGAR_BLOQUES_ESTIMACION,
  CARGAR_GRUPOS_ESTIMACION,
  CARGAR_GRUPOS_ESTIMACION_ERROR,
  CARGAR_GRUPOS_ESTIMACION_EXITO,
  CARGAR_MUESTREO_ESTIMACION,
  CARGAR_MUESTREO_ESTIMACION_ERROR,
  CARGAR_MUESTREO_ESTIMACION_EXITO,
  ENVIAR_MUESTREO_ESTIMACION,
  ENVIAR_MUESTREO_ESTIMACION_ERROR,
  ENVIAR_MUESTREO_ESTIMACION_EXITO,
  SETEAR_BLOQUES_ESTIMACION,
} from '../types';

// Actualizar el bloque de la cedula que se trabajo
export function enviarFormularioEstimacionAction(data, id) {
  let arreglo = [];
  return async dispatch => {
    dispatch(empezarEnvio());

    try {
      // Se elimina del localStorage el bloque muestreado
      let arrCedulasMuestreo = await AsyncStorage.getItem(
        'cedulasmuestreoEstimacion',
      );
      let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      arrCedulasMuestreoArreglo = arrCedulasMuestreoArreglo.filter(
        bloque => bloque.id !== id,
      );
      await AsyncStorage.setItem(
        'cedulasmuestreoEstimacion',
        JSON.stringify(arrCedulasMuestreoArreglo),
      );
      dispatch(getCedulasPedientesExito(arrCedulasMuestreoArreglo));

      //Se almacena en el localStorage las cedulas de muestreo
      let arrLocalStorage = await AsyncStorage.getItem('muestreoEstimacion');
      arreglo = JSON.parse(arrLocalStorage);
      if (arrLocalStorage) {
        arreglo = [...arreglo, data];
        await AsyncStorage.setItem(
          'muestreoEstimacion',
          JSON.stringify(arreglo),
        );
      } else {
        await AsyncStorage.setItem(
          'muestreoEstimacion',
          JSON.stringify([data]),
        );
      }

      // Se verifica si posee internet, se decide si se envia a la Api o queda almacenado localStorage
      const internet = await conexionInternet();
      if (internet) {
        const jsonLocalStorage = await AsyncStorage.getItem(
          'muestreoEstimacion',
        );
        const arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.put(
          `/muestreoEstimacion/actualizarCedulasEstimacion`,
          arrData,
        );

        await AsyncStorage.setItem('muestreoEstimacion', '');
      }

      SweetAlert.showAlertWithOptions({
        title: 'Envio exitoso',
        subTitle: 'El bloque de la cédula sea guardado exitosamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'success',
        cancellable: true,
      });
      // Desarrollo ver en consola los bloques que no han sido enviados
      const arrCopia = await AsyncStorage.getItem('muestreoEstimacion');
      const arregloCopia = JSON.parse(arrCopia);
      if (arregloCopia) {
        arregloCopia.map(a => {
          console.log(a);
        });
      }

      dispatch(enviarFormulario());
    } catch (error) {
      console.log(error);
      dispatch(envioError());
      SweetAlert.showAlertWithOptions({
        title: 'Hubo un error',
        subTitle: 'Revise los datos del formulario, e intente nuevamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    }
  };
}
// Sincronizar Estimacion
export function sincronizarEstimacionAction() {
  return async dispatch => {
    dispatch(empezarEnvio());
    try {
      const jsonLocalStorage = await AsyncStorage.getItem('muestreoEstimacion');
      if (jsonLocalStorage !== null) {
        const arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.put(
          `/muestreoEstimacion/actualizarCedulasEstimacion`,
          arrData,
        );
        await AsyncStorage.setItem('muestreoEstimacion', '');
      }

      dispatch(enviarFormulario());
    } catch (error) {
      console.log(error);
      dispatch(envioError());
    }
  };
}

// Sincronizar Estimacion
export function sincronizarPorcentajeEstimacionAction() {
  return async dispatch => {
    dispatch(empezarEnvio());
    try {
      const jsonLocalStorage = await AsyncStorage.getItem('muestreoPorcentaje');
      if (jsonLocalStorage !== null) {
        const arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.put(
          `/muestreoEstimacion/actualizarPorcentajeRecoleccion`,
          arrData,
        );
        await AsyncStorage.setItem('muestreoPorcentaje', '');
      }

      dispatch(enviarFormulario());
    } catch (error) {
      console.log(error);
      dispatch(envioError());
    }
  };
}

// OBTENER CEDULAS PENDIENTES ESTIMACION
export function getCedulasPendientesEstimacionAction() {
  return async dispatch => {
    let arrCedulasMuestreoArreglo = [];
    dispatch(getCedulasPedientes());
    try {
      const internet = await conexionInternet();
      if (internet) {
        const respuesta = await clienteAxios.get('/muestreoEstimacion');
        arrCedulasMuestreoArreglo = respuesta.data;
      } else {
        let arrCedulasMuestreo = await AsyncStorage.getItem(
          'cedulasmuestreoEstimacion',
        );
        arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      }
      await AsyncStorage.setItem(
        'cedulasmuestreoEstimacion',
        JSON.stringify(arrCedulasMuestreoArreglo),
      );
      dispatch(getCedulasPedientesExito(arrCedulasMuestreoArreglo));
    } catch (error) {
      let arrCedulasMuestreo = await AsyncStorage.getItem(
        'cedulasmuestreoEstimacion',
      );
      arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      dispatch(getCedulasPedientesError(arrCedulasMuestreoArreglo));
    }
  };
}

// Actualizar porcentaje de recoleccion
export function actualizarPorcentajeEstimacionAction(id, porcetaje_recoleccion) {
  let arreglo = [];
  return async dispatch => {
    dispatch(empezarEnvio());

    try {
      // Se elimina registros duplicados
      arreglo = arreglo.filter((bloque) => bloque.id !== id);

      //Se almacena en el localStorage las cedulas de muestreo
      let arrLocalStorage = await AsyncStorage.getItem('muestreoPorcentaje');
      arreglo = JSON.parse(arrLocalStorage);
      if (arrLocalStorage) {
         // Se elimina registros duplicados
        arreglo = arreglo.filter((bloque) => bloque.id !== id);
        arreglo = [...arreglo, {id, porcetaje_recoleccion}];
        await AsyncStorage.setItem('muestreoPorcentaje',JSON.stringify(arreglo));
      } else {
        await AsyncStorage.setItem('muestreoPorcentaje',JSON.stringify([{id, porcetaje_recoleccion}]));
      }

      // Se verifica si posee internet, se decide si se envia a la Api o queda almacenado localStorage
      const internet = await conexionInternet();
      if (internet) {
        const jsonLocalStorage = await AsyncStorage.getItem(
          'muestreoPorcentaje',
        );
        const arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.put(
          `/muestreoEstimacion/actualizarPorcentajeRecoleccion`,
          arrData,
        );

        await AsyncStorage.setItem('muestreoPorcentaje', '');
      }

      const arrBloquesStorage = await AsyncStorage.getItem('bloquesEstimacion');
      const bloques = JSON.parse(arrBloquesStorage);
      bloques.map( bloq => {
        if(bloq.id === id){
          bloq.porcetaje_recoleccion = porcetaje_recoleccion;
        }
      });
      await AsyncStorage.setItem('bloquesEstimacion',JSON.stringify(bloques));

      SweetAlert.showAlertWithOptions({
        title: 'Envio exitoso',
        subTitle: 'El porcentaje de recolección sea actualizado exitosamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'success',
        cancellable: true,
      });
      

      dispatch(enviarFormulario());
    } catch (error) {
      console.log(error);
      dispatch(envioError());
      SweetAlert.showAlertWithOptions({
        title: 'Hubo un error',
        subTitle: 'Revise los datos del formulario, e intente nuevamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    }
  };
}

// OBTENER CEDULAS PENDIENTES ESTIMACION
export function getGruposEstimacionAction() {
  return async dispatch => {
    let arrGruposArreglo = [];
    let arrBloquesArreglo = [];
    dispatch(getGrupos());
    try {
      const internet = await conexionInternet();
      if (internet) {
        const respuesta = await clienteAxios.get(
          '/muestreoEstimacion/bloquesPorSemana',
        );
        arrGruposArreglo = respuesta.data.grupos;
        arrBloquesArreglo = respuesta.data.bloques;
      } else {
        let arrGrupos = await AsyncStorage.getItem('grupoEstimacion');
        arrGruposArreglo = JSON.parse(arrGrupos);
        let arrBloques = await AsyncStorage.getItem('bloquesEstimacion');
        arrBloquesArreglo = JSON.parse(arrBloques);
      }
      await AsyncStorage.setItem(
        'grupoEstimacion',
        JSON.stringify(arrGruposArreglo),
      );
      await AsyncStorage.setItem(
        'bloquesEstimacion',
        JSON.stringify(arrBloquesArreglo),
      );
      dispatch(getGruposExito(arrGruposArreglo));
    } catch (error) {
      let arrGrupos = await AsyncStorage.getItem('grupoEstimacion');
      arrGruposArreglo = JSON.parse(arrGrupos);

      dispatch(getGruposError(arrGruposArreglo));
    }
  };
}

//Obtiene los bloques de una determinada cedula
export function getBloquesAction(cedulaid) {
  return async dispatch => {

    const arrBloquesStorage = await AsyncStorage.getItem('bloquesEstimacion');
    const bloques = JSON.parse(arrBloquesStorage);
    const bloquesGrupo = bloques.filter( bloq => bloq.codigo.substring(bloq.codigo.length - 6 ,bloq.codigo.length-4) === cedulaid );
    
    dispatch(getBloquesExito(bloquesGrupo));

  };
}

export const empezarEnvio = () => ({
  type: ENVIAR_MUESTREO_ESTIMACION,
});
const enviarFormulario = () => ({
  type: ENVIAR_MUESTREO_ESTIMACION_EXITO,
});
const envioError = () => ({
  type: ENVIAR_MUESTREO_ESTIMACION_ERROR,
});

// Obtener cedulas estimacion pendientes a muestrear
const getCedulasPedientes = () => ({
  type: CARGAR_MUESTREO_ESTIMACION,
});
const getCedulasPedientesExito = data => ({
  type: CARGAR_MUESTREO_ESTIMACION_EXITO,
  payload: data,
});
const getCedulasPedientesError = data => ({
  type: CARGAR_MUESTREO_ESTIMACION_ERROR,
  payload: data,
});

// Obtener grupos de estimacion
const getGrupos = () => ({
  type: CARGAR_GRUPOS_ESTIMACION,
});
const getGruposExito = data => ({
  type: CARGAR_GRUPOS_ESTIMACION_EXITO,
  payload: data,
});
const getGruposError = data => ({
  type: CARGAR_GRUPOS_ESTIMACION_ERROR,
  payload: data,
});

const getBloquesExito = data => ({
  type: CARGAR_BLOQUES_ESTIMACION,
  payload: data,
});

export const setBloques = () => ({
  type: SETEAR_BLOQUES_ESTIMACION
});
