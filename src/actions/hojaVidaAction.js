import AsyncStorage from '@react-native-async-storage/async-storage';
import {conexionInternet} from '../helpers/verificarConexionInternet';
import clienteAxios from '../config/api';
import {
  CARGAR_AVANCE_EXITO,
  CARGAR_BLOQUES_EXITO,
  CARGAR_CEDULA,
  CARGAR_CEDULA_ERROR,
  CARGAR_CEDULA_EXITO,
  CARGAR_GRUPOS,
  CARGAR_GRUPOS_ERROR,
  CARGAR_GRUPOS_EXITO,
  CARGAR_HOJA_ALL_VIDA_EXITO,
  CARGAR_HOJA_VIDA,
  CARGAR_HOJA_VIDA_ERROR,
  CARGAR_HOJA_VIDA_EXITO,
  SETEAR_GRUPOID
} from '../types';
import { getFecha } from '../helpers/FechaHora';

// OBTENER CEDULAS PENDIENTES RAIZ
export function getGruposAction() {
  return async dispatch => {
    let arrGrupos = [];
    dispatch(getGrupos());
    try {
      const internet = await conexionInternet();
      if (internet) {
        const respuesta = await clienteAxios.get('/hojaVida');
        arrGrupos = respuesta.data;
      } else {
        let arrGruposStorage = await AsyncStorage.getItem('grupos');
        arrGrupos = JSON.parse(arrGruposStorage);
      }
      await AsyncStorage.setItem('grupos', JSON.stringify(arrGrupos));
      dispatch(getGruposExito(arrGrupos));
    } catch (error) {
      let arrGruposStorage = await AsyncStorage.getItem('grupos');
      arrGrupos = JSON.parse(arrGruposStorage);
      console.log(error)
      dispatch(getGruposError(arrGrupos));
    }
  };
}

// Obtiene todas la hojas de vida
export function getAllHojasVidaAction() {
  return async dispatch => {
    let hojasVida = [];
    let bloquesGrupos = [];
    let cedulas = [];
    let opcionesAplicaciones = [];
    let avances = [];
    dispatch(getHojaVida());
    try {
      const internet = await conexionInternet();
      if (internet) {
        const respuesta = await clienteAxios.get('/hojaVida/allhojaVida');
        hojasVida = respuesta.data.respuesta;
        bloquesGrupos = respuesta.data.bloques;
        cedulas = respuesta.data.cedulas;
        opcionesAplicaciones = respuesta.data.opcionesAplicacion;
        avances = respuesta.data.avances;
      } else {
        let arrHojasVidaStorage = await AsyncStorage.getItem('hojasVida');
        hojasVida = JSON.parse(arrHojasVidaStorage);

        let arrBloqueStorage = await AsyncStorage.getItem('bloques');
        bloquesGrupos = JSON.parse(arrBloqueStorage);

        let arrCedulasStorage = await AsyncStorage.getItem('productosCedulas');
        cedulas = JSON.parse(arrCedulasStorage);

        let arrOpcionesAplicacionStorage = await AsyncStorage.getItem('opcionesAplicaciones');
        opcionesAplicaciones = JSON.parse(arrOpcionesAplicacionStorage);

        let arrAvancesStorage = await AsyncStorage.getItem('avancesHojaVida');
        avances = JSON.parse(arrAvancesStorage);

      }
      await AsyncStorage.setItem('productosCedulas', JSON.stringify(cedulas));
      await AsyncStorage.setItem('hojasVida', JSON.stringify(hojasVida));
      await AsyncStorage.setItem('bloques', JSON.stringify(bloquesGrupos));
      await AsyncStorage.setItem('opcionesAplicaciones', JSON.stringify(opcionesAplicaciones));  
      await AsyncStorage.setItem('avancesHojaVida', JSON.stringify(avances));
      dispatch(getAllHojasVidaExito());
    } catch (error) {
      let arrHojasStorage = await AsyncStorage.getItem('hojasVida');
      hojasVida = JSON.parse(arrHojasStorage);
      await AsyncStorage.setItem('hojasVida', JSON.stringify(hojasVida));

      let arrBloqueStorage = await AsyncStorage.getItem('bloques');
      bloquesGrupos = JSON.parse(arrBloqueStorage);
      await AsyncStorage.setItem('bloques', JSON.stringify(bloquesGrupos));

      let arrCedulasStorage = await AsyncStorage.getItem('productosCedulas');
      cedulas = JSON.parse(arrCedulasStorage);
      await AsyncStorage.setItem('productosCedulas', JSON.stringify(cedulas));

      let arrOpcionesAplicacionStorage = await AsyncStorage.getItem('opcionesAplicaciones');
      opcionesAplicaciones = JSON.parse(arrOpcionesAplicacionStorage);
      await AsyncStorage.setItem('opcionesAplicaciones', JSON.stringify(opcionesAplicaciones));

      let arrAvancesStorage = await AsyncStorage.getItem('avancesHojaVida');
      avances = JSON.parse(arrAvancesStorage);
      await AsyncStorage.setItem('avancesHojaVida', JSON.stringify(avances));

      dispatch(getGruposError());
    }
  };
}

// Obtiene una hoja de vida de un grupo en especifico en el localstorage
export function getHojaVidaAction(grupo, estado) {
  return async dispatch => {
    dispatch(getHojaVida());
    let hojaVida;
    try {
      // Obtiene la hoja de vida de un grupo en especifico
      let arrGruposStorage = await AsyncStorage.getItem('hojasVida');
      const hojasVida = JSON.parse(arrGruposStorage);
      if(estado === 'Ejecutada'){
        hojaVida = hojasVida.filter( hoja => hoja.grupoid === grupo && (hoja.estado === 'Ejecutada' || hoja.estado === 'Por Generarse') );
      } else {
        hojaVida = hojasVida.filter( hoja => hoja.grupoid === grupo && (hoja.estado !== 'Ejecutada' && hoja.estado !== 'Por Generarse' ) );
      }
     

      hojaVida.map( res => {   
        if(res.estado === 'Generada' || res.estado === 'En Ejecución'){
          let dias = 0;
          let date1 = new Date(res.fecha_estimada);
          let date2 = new Date(getFecha());  
          if(date1 > date2){
            dias = 0;
          }else{
            dias = Math.abs(date2 - date1);
            dias = dias/(1000 * 3600 * 24);  
          }          
          res.días_avanzados = dias;          
          res.fecha_aplicación = 'Sin Aplicar'
        } else if(res.estado === 'Anulada'){
          let date1 = new Date(res.fecha_estimada);
          let date2 = new Date(getFecha());  
          let dias = Math.abs(date2 - date1);
          dias = dias/(1000 * 3600 * 24);  
          res.días_avanzados = dias;          
          res.fecha_aplicación = 'Sin Aplicar'
        } else if(res.estado === 'Por Generarse'){
          res.días_avanzados = 'N/A';          
          res.fecha_aplicación = 'N/A';
        } else {
          let date1 = new Date(res.fecha_estimada);
          let date2 = new Date(res.fecha_aplicación);  
          let dias = Math.abs(date2 - date1);
          dias = dias/(1000 * 3600 * 24);  
          res.días_avanzados = dias;              
        } 
      })


      dispatch(getHojaVidaExito(hojaVida));
      // Obtener los bloques de un grupo
      let arrBloquesStorage = await AsyncStorage.getItem('bloques');
      const bloques = JSON.parse(arrBloquesStorage);
      const bloque = bloques.filter( hoja => hoja.grupoid === grupo);
      dispatch(getBloquesExito(bloque[0]));

    } catch (error) {
      dispatch(getHojaVidaError());
    }
  };
}

// Obtiene una hoja de vida de un grupo en especifico en el localstorage
export function getDetalleCedulaAction(numerocedula) {
  
  return async dispatch => {
    dispatch(getCedula());
    try {   
      // Obtiene la hoja de vida de un grupo en especifico
      let arrCedulasStorage = await AsyncStorage.getItem('productosCedulas');
      const cedulas = JSON.parse(arrCedulasStorage);
      const cedula = cedulas.filter( ced => ced.numerocedula === numerocedula);      
      
      // Obtiene los avances de la cedula
      let arrAvancesStorage = await AsyncStorage.getItem('avancesHojaVida');
      const avances = JSON.parse(arrAvancesStorage);
      const avance = avances.filter( ced => ced.numerocedula === numerocedula);
      dispatch(getAvanceExito(avance))

      dispatch(getCedulaExito(cedula));

    } catch (error) {
      dispatch(getCedulaError());
    }
  };
}



// Obtiene detalle de una cedula que no sea generado
export function getDetalleSinCedulaAction(opcionaplicacion, hectareas) {
  return async dispatch => {
    dispatch(getCedula());
    try {
      let hectareaje = hectareas.split(':');
      hectareaje = hectareaje[1];
      hectareaje = parseFloat(hectareaje.trim());
      // Obtiene la hoja de vida de un grupo en especifico
       let arrOpcionesStorage = await AsyncStorage.getItem('opcionesAplicaciones');
       const opciones = JSON.parse(arrOpcionesStorage);
       const cedula = opciones.filter( opc => opc.opcionaplicacionid === opcionaplicacion);
       if(cedula.length > 0){
         cedula.map(c => {
           c.aplicado = c.cantidad * hectareaje;
         })
       }
       dispatch(getCedulaExito(cedula));
       dispatch(getAvanceExito([]))

    } catch (error) {
      dispatch(getCedulaError());
    }
  };
}

//Obtener los grupos
const getGrupos = () => ({
  type: CARGAR_GRUPOS,
});
const getGruposExito = data => ({
  type: CARGAR_GRUPOS_EXITO,
  payload: data,
});
const getGruposError = data => ({
  type: CARGAR_GRUPOS_ERROR,
  payload: data,
});

//Cargar Hoja de Vida
const getHojaVida = () => ({
  type: CARGAR_HOJA_VIDA,
});
const getAllHojasVidaExito = () => ({
  type: CARGAR_HOJA_ALL_VIDA_EXITO
});
const getHojaVidaError = () => ({
  type: CARGAR_HOJA_VIDA_ERROR,
});

const getHojaVidaExito= (data) => ({
  type: CARGAR_HOJA_VIDA_EXITO,
  payload: data,
});

const getBloquesExito= (data) => ({
  type: CARGAR_BLOQUES_EXITO,
  payload: data,
});

//Cargar Hoja de Vida
const getCedula = () => ({
  type: CARGAR_CEDULA,
});
const getCedulaExito = (data) => ({
  type: CARGAR_CEDULA_EXITO,
  payload: data
});
const getAvanceExito = (data) => ({
  type: CARGAR_AVANCE_EXITO,
  payload: data
});
const getCedulaError = () => ({
  type: CARGAR_CEDULA_ERROR,
});

export const setGrupoId = () => ({
  type: SETEAR_GRUPOID
});