import AsyncStorage from '@react-native-async-storage/async-storage';
import SweetAlert from 'react-native-sweet-alert';
import clienteAxios from '../config/api';
import {conexionInternet} from '../helpers/verificarConexionInternet';
import {
  ENVIAR_MUESTREO_MERISTEMO,
  ENVIAR_MUESTREO_MERISTEMO_EXITO,
  ENVIAR_MUESTREO_MERISTEMO_ERROR,
  CARGAR_MERISTEMOS,
  CARGAR_MERISTEMOS_EXITO,
  CARGAR_MERISTEMOS_ERROR,
  CEDULA_SELECCIONADA,
  SETEAR_CEDULA_SELECCIONADA
} from '../types';

// Actualizar el bloque de la cedula que se trabajo
export function enviarFormularioAction(data, id) {
  let arreglo = [];
  return async dispatch => {
    dispatch(empezarEnvio());


    try {
      // Se elimina del localStorage el bloque muestreado      
      let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoMeristemo');
      let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      arrCedulasMuestreoArreglo = arrCedulasMuestreoArreglo.filter((bloque) => bloque.id !== id);
      await AsyncStorage.setItem('cedulasmuestreoMeristemo', JSON.stringify(arrCedulasMuestreoArreglo));
      dispatch(getCedulasPedientesExito(arrCedulasMuestreoArreglo));

      //Se almacena en el localStorage las cedulas de muestreo 
      let arrLocalStorage = await AsyncStorage.getItem('muestreoMeristemo');
      arreglo = JSON.parse(arrLocalStorage);
      if (arrLocalStorage) {
        // Se elimina registros duplicados
        arreglo = arreglo.filter((bloque) => bloque.id !== id);
        arreglo = [...arreglo, data];
        await AsyncStorage.setItem('muestreoMeristemo',JSON.stringify(arreglo));
      } else {
        await AsyncStorage.setItem('muestreoMeristemo', JSON.stringify([data]));
      }

      // Se verifica si posee internet, se decide si se envia a la Api o queda almacenado localStorage
      const internet = await conexionInternet();
      if (internet) {
        const jsonLocalStorage = await AsyncStorage.getItem('muestreoMeristemo');
        const arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.put(`/muestreoPlagas/update`, arrData);       
        
        await AsyncStorage.setItem('muestreoMeristemo', '');
      } 

      SweetAlert.showAlertWithOptions({
        title: 'Envio exitoso',
        subTitle:
          'El bloque de la cédula sea guardado exitosamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'success',
        cancellable: true,
      });
      // Desarrollo ver en consola los bloques que no han sido enviados
      const arrCopia = await AsyncStorage.getItem('muestreoMeristemo');
      const arregloCopia = JSON.parse(arrCopia);
      if(arregloCopia){
        arregloCopia.map( a => {
          console.log(a)
        })
      }


      dispatch(enviarFormulario());

    } catch (error) {
      console.log(error)
      dispatch(envioError());
      SweetAlert.showAlertWithOptions({
        title: 'Hubo un error',
        subTitle: 'Revise los datos del formulario, e intente nuevamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    }
  };
}

// OBTENER CEDULAS PENDIENTES MERISTEMOS
export function getCedulasPendientesAction() { 
  return async dispatch => {
    let arrCedulasMuestreoArreglo = [];
    dispatch(getCedulasPedientes());
    try {
      const internet = await conexionInternet();
      if (internet) {
      const respuesta = await clienteAxios.get('/muestreoPlagas/cedulasMeristemos');
      arrCedulasMuestreoArreglo = respuesta.data;     
      } else{
        let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoMeristemo');
        arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      }
       await AsyncStorage.setItem('cedulasmuestreoMeristemo', JSON.stringify(arrCedulasMuestreoArreglo));      
      dispatch(getCedulasPedientesExito(arrCedulasMuestreoArreglo));

    } catch (error) {
      let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoMeristemo');
      arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      dispatch(getCedulasPedientesError(arrCedulasMuestreoArreglo))
    }
  };
}


// Sincronizar Meristemo
export function sincronizarMeristemoAction() { 
  return async dispatch => {
    dispatch(empezarEnvio());
    try {
      const jsonLocalStorage = await AsyncStorage.getItem('muestreoMeristemo');
      if(jsonLocalStorage !== null){
      const arrData = JSON.parse(jsonLocalStorage);
      await clienteAxios.put(`/muestreoPlagas/update`, arrData);      
      await AsyncStorage.setItem('muestreoMeristemo', '');
      } 
      dispatch(enviarFormulario());
    } catch (error) {
      console.log(error)
      dispatch(envioError());
    }
  
  }
}

// Obtiene la cedula seleccionada Meristemos
export function getCedulaSeleccionada(cedula) { 
  return async dispatch => {
   if(cedula !== 0 || cedula !== ''){ let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoMeristemo');
    let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
    arrCedulasMuestreoArreglo = arrCedulasMuestreoArreglo.filter((bloque) => bloque.id === cedula); 
    dispatch(getCedulaSeleccionadaExito(arrCedulasMuestreoArreglo[0]));
   }
  
  };
  
}

// Obtiene la cedula seleccionada Fruta
export function getCedulaSeleccionadaFruta(cedula) { 
  return async dispatch => {
   if(cedula !== 0 || cedula !== ''){ let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoFruta');
    let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
    arrCedulasMuestreoArreglo = arrCedulasMuestreoArreglo.filter((bloque) => bloque.id === cedula);
    dispatch(getCedulaSeleccionadaExito(arrCedulasMuestreoArreglo[0]));
   }
  
  };
  
}

// Obtiene la cedula seleccionada Raiz
export function getCedulaSeleccionadaRaiz(cedula) { 
  return async dispatch => {
   if(cedula !== 0 || cedula !== ''){ let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoRaiz');
    let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
    arrCedulasMuestreoArreglo = arrCedulasMuestreoArreglo.filter((bloque) => bloque.id === cedula); 
    dispatch(getCedulaSeleccionadaExito(arrCedulasMuestreoArreglo[0]));
   }
  
  };
  
}

// Obtiene la cedula seleccionada Peso
export function getCedulaSeleccionadaPeso(cedula) { 
  return async dispatch => {
   if(cedula !== 0 || cedula !== ''){ let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoPeso');
    let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
    arrCedulasMuestreoArreglo = arrCedulasMuestreoArreglo.filter((bloque) => bloque.id === cedula); 
    dispatch(getCedulaSeleccionadaExito(arrCedulasMuestreoArreglo[0]));
   }
  
  };
  
}

// Obtiene la cedula seleccionada Estimacion
export function getCedulaSeleccionadaEstimacion(cedula) { 
  return async dispatch => {
   if(cedula !== 0 || cedula !== ''){ let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoEstimacion');
    let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
    arrCedulasMuestreoArreglo = arrCedulasMuestreoArreglo.filter((bloque) => bloque.id === cedula); 
    dispatch(getCedulaSeleccionadaExito(arrCedulasMuestreoArreglo[0]));
   }  
  };  
}

// Obtiene la cedula seleccionada Floracion
export function getCedulaSeleccionadaFloracion(cedula) { 
  return async dispatch => {
   if(cedula !== 0 || cedula !== ''){ let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoFloracion');
    let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
    arrCedulasMuestreoArreglo = arrCedulasMuestreoArreglo.filter((bloque) => bloque.id === cedula); 
    dispatch(getCedulaSeleccionadaExito(arrCedulasMuestreoArreglo[0]));
   }  
  };  
}

// Obtiene la cedula seleccionada Premaduracion
export function getCedulaSeleccionadaPremaduracion(cedula) { 
  return async dispatch => {
   if(cedula !== 0 || cedula !== ''){ let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoPremaduracion');
    let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
    arrCedulasMuestreoArreglo = arrCedulasMuestreoArreglo.filter((bloque) => bloque.id === cedula); 
    dispatch(getCedulaSeleccionadaExito(arrCedulasMuestreoArreglo[0]));
   }
  
  };
  
}


const enviarFormulario = () => ({
  type: ENVIAR_MUESTREO_MERISTEMO_EXITO
});
const empezarEnvio = () => ({
  type: ENVIAR_MUESTREO_MERISTEMO,
});
const envioError = () => ({
  type: ENVIAR_MUESTREO_MERISTEMO_ERROR,
});

// Obtener Meristemos pendientes a muestrear
const getCedulasPedientes = () => ({
  type: CARGAR_MERISTEMOS 
});
const getCedulasPedientesExito = (data) => ({
  type: CARGAR_MERISTEMOS_EXITO,
  payload: data
});
const getCedulasPedientesError = (data) => ({
  type: CARGAR_MERISTEMOS_ERROR,
  payload: data
});
// Cedula seleccionada
export const getCedulaSeleccionadaExito = (data) => ({
  type: CEDULA_SELECCIONADA,
  payload: data
});

// Setear Cedula seleccionada
export const setearCedulaSeleccionada = () => ({
  type: SETEAR_CEDULA_SELECCIONADA
});
