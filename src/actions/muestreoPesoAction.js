import AsyncStorage from '@react-native-async-storage/async-storage';
import SweetAlert from 'react-native-sweet-alert';
import clienteAxios from '../config/api';
import {conexionInternet} from '../helpers/verificarConexionInternet';
import {
  ENVIAR_MUESTREO_PESO,
  ENVIAR_MUESTREO_PESO_EXITO,
  ENVIAR_MUESTREO_PESO_ERROR,
  CARGAR_MUESTREO_PESO,
  CARGAR_MUESTREO_PESO_EXITO,
  CARGAR_MUESTREO_PESO_ERROR,
} from '../types';

// Actualizar el bloque de la cedula que se trabajo
export function enviarFormularioPesoAction(cedula,id) {
  let arreglo = [];
  return async dispatch => {
    dispatch(empezarEnvio());

   
    try {    
      // Se elimina del localStorage el bloque muestreado      
      let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoPeso');
      let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      arrCedulasMuestreoArreglo = arrCedulasMuestreoArreglo.filter((bloque) => bloque.id !== id);
      await AsyncStorage.setItem('cedulasmuestreoPeso', JSON.stringify(arrCedulasMuestreoArreglo));
      dispatch(getCedulasPedientesExito(arrCedulasMuestreoArreglo));
      
      // Se almacena en el localStorage las cedulas de muestreo 
      let arrLocalStorage = await AsyncStorage.getItem('muestreoPeso');
      arreglo = JSON.parse(arrLocalStorage);
      if (arrLocalStorage) {
         // Se elimina registros duplicados
        arreglo = arreglo.filter((bloque) => bloque.id !== id);
        arreglo = [...arreglo, cedula];
        await AsyncStorage.setItem('muestreoPeso',JSON.stringify(arreglo));
      } else {
        await AsyncStorage.setItem('muestreoPeso', JSON.stringify([cedula]));
      }

      // Se verifica si posee internet, se decide si se envia a la Api o queda almacenado localStorage
      const internet = await conexionInternet();
      if (internet) {
        const jsonLocalStorage = await AsyncStorage.getItem('muestreoPeso');
        const arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.put(`/muestreoPlagas/updatePesos`, arrData);       
        
        await AsyncStorage.setItem('muestreoPeso', '');
      } 

      SweetAlert.showAlertWithOptions({
        title: 'Envio exitoso',
        subTitle:
          'El bloque de la cédula sea guardado exitosamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'success',
        cancellable: true,
      });
      // Desarrollo ver en consola los bloques que no han sido enviados
      const arrCopia = await AsyncStorage.getItem('muestreoPeso');
      const arregloCopia = JSON.parse(arrCopia);
      if(arregloCopia){
        arregloCopia.map( a => {
          console.log(a)
        })
      }


       dispatch(enviarFormulario());

    } catch (error) {
      console.log(error)
      dispatch(envioError());
      SweetAlert.showAlertWithOptions({
        title: 'Hubo un error',
        subTitle: 'Revise los datos del formulario, e intente nuevamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    }
  };
}
// Sincrnizar Fruta
export function sincronizarPesoAction() { 
  return async dispatch => {
    dispatch(empezarEnvio());
    try {

      // console.log("Preparar para Sincronizar pesos")
      const jsonLocalStorage = await AsyncStorage.getItem('muestreoPeso');
      if(jsonLocalStorage !== null){
      const arrData = JSON.parse(jsonLocalStorage);  
      await clienteAxios.put(`/muestreoPlagas/updatePesos`, arrData); 
      
       await AsyncStorage.setItem('muestreoPeso', '');
      } 
      dispatch(enviarFormulario());
    } catch (error) {
      console.log(error)
      dispatch(envioError());
    }
  
  }
}

// OBTENER CEDULAS PENDIENTES RAIZ
export function getCedulasPendientesPesoAction() { 
  return async dispatch => {
    let arrCedulasMuestreoArreglo = [];
    dispatch(getCedulasPedientes());
    try {
      const internet = await conexionInternet();
      if (internet) {
      const respuesta = await clienteAxios.get('/muestreoPlagas/cedulasPeso');
      arrCedulasMuestreoArreglo = respuesta.data; 
      } else{
        let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoPeso');
        arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      }
       await AsyncStorage.setItem('cedulasmuestreoPeso', JSON.stringify(arrCedulasMuestreoArreglo));      
      dispatch(getCedulasPedientesExito(arrCedulasMuestreoArreglo));

    } catch (error) {
      let arrCedulasMuestreo = await AsyncStorage.getItem('cedulasmuestreoPeso');
      arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      dispatch(getCedulasPedientesError(arrCedulasMuestreoArreglo))
    }
  };
}



const empezarEnvio = () => ({
  type: ENVIAR_MUESTREO_PESO,
});
const enviarFormulario = () => ({
  type: ENVIAR_MUESTREO_PESO_EXITO
});
const envioError = () => ({
  type: ENVIAR_MUESTREO_PESO_ERROR,
});

// Obtener cedulas Fruta pendientes a muestrear
const getCedulasPedientes = () => ({
  type: CARGAR_MUESTREO_PESO
});
const getCedulasPedientesExito = (data) => ({
  type: CARGAR_MUESTREO_PESO_EXITO,
  payload: data
});
const getCedulasPedientesError = (data) => ({
  type: CARGAR_MUESTREO_PESO_ERROR,
  payload:data
});
