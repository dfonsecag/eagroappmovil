import AsyncStorage from '@react-native-async-storage/async-storage';
import SweetAlert from 'react-native-sweet-alert';
import clienteAxios from '../config/api';
import {conexionInternet} from '../helpers/verificarConexionInternet';
import { CARGAR_MUESTREO_FLORACION, CARGAR_MUESTREO_FLORACION_ERROR, CARGAR_MUESTREO_FLORACION_EXITO, ENVIAR_MUESTREO_FLORACION, ENVIAR_MUESTREO_FLORACION_ERROR, ENVIAR_MUESTREO_FLORACION_EXITO } from '../types';

// Actualizar el bloque de la cedula que se trabajo
export function enviarFormularioFloracionAction(data, id) {
  let arreglo = [];
  return async dispatch => {
    dispatch(empezarEnvio());

    try {
      // Se elimina del localStorage el bloque muestreado
      let arrCedulasMuestreo = await AsyncStorage.getItem(
        'cedulasmuestreoFloracion',
      );
      let arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      arrCedulasMuestreoArreglo = arrCedulasMuestreoArreglo.filter(
        bloque => bloque.id !== id,
      );
      await AsyncStorage.setItem(
        'cedulasmuestreoFloracion',
        JSON.stringify(arrCedulasMuestreoArreglo),
      );
      dispatch(getCedulasPedientesExito(arrCedulasMuestreoArreglo));

      //Se almacena en el localStorage las cedulas de muestreo
      let arrLocalStorage = await AsyncStorage.getItem('muestreoFloracion');
      arreglo = JSON.parse(arrLocalStorage);
      if (arrLocalStorage) {
        arreglo = [...arreglo, data];
        await AsyncStorage.setItem(
          'muestreoFloracion',
          JSON.stringify(arreglo),
        );
      } else {
        await AsyncStorage.setItem(
          'muestreoFloracion',
          JSON.stringify([data]),
        );
      }

      // Se verifica si posee internet, se decide si se envia a la Api o queda almacenado localStorage
      const internet = await conexionInternet();
      if (internet) {
        const jsonLocalStorage = await AsyncStorage.getItem(
          'muestreoFloracion',
        );
        const arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.put(
          `/muestreoEstimacion/actualizarCedulasFloracion`,
          arrData,
        );
        await AsyncStorage.setItem('muestreoFloracion', '');
      }

      SweetAlert.showAlertWithOptions({
        title: 'Envio exitoso',
        subTitle: 'El bloque de la cédula sea guardado exitosamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'success',
        cancellable: true,
      });
      // Desarrollo ver en consola los bloques que no han sido enviados
      const arrCopia = await AsyncStorage.getItem('muestreoFloracion');
      const arregloCopia = JSON.parse(arrCopia);
      if (arregloCopia) {
        arregloCopia.map(a => {
          console.log(a);
        });
      }

      dispatch(enviarFormulario());
    } catch (error) {
      console.log(error);
      dispatch(envioError());
      SweetAlert.showAlertWithOptions({
        title: 'Hubo un error',
        subTitle: 'Revise los datos del formulario, e intente nuevamente',
        confirmButtonTitle: 'OK',
        confirmButtonColor: '#000',
        otherButtonTitle: 'Cancel',
        otherButtonColor: '#dedede',
        style: 'error',
        cancellable: true,
      });
    }
  };
}
// Sincronizar Estimacion
export function sincronizarFloracionAction() {
  return async dispatch => {
    dispatch(empezarEnvio());
    try {
      const jsonLocalStorage = await AsyncStorage.getItem('muestreoFloracion');
      if (jsonLocalStorage !== null) {
        const arrData = JSON.parse(jsonLocalStorage);
        await clienteAxios.put(
          `/muestreoEstimacion/actualizarCedulasFloracion`,
          arrData,
        );
        await AsyncStorage.setItem('muestreoFloracion', '');
      }

      dispatch(enviarFormulario());
    } catch (error) {
      console.log(error);
      dispatch(envioError());
    }
  };
}


// OBTENER CEDULAS PENDIENTES ESTIMACION
export function getCedulasPendientesFloracionAction() {
  return async dispatch => {
    let arrCedulasMuestreoArreglo = [];
    dispatch(getCedulasPedientes());
    try {
      const internet = await conexionInternet();
      if (internet) {
        const respuesta = await clienteAxios.get('/muestreoEstimacion/floracion');       
        arrCedulasMuestreoArreglo = respuesta.data;
      } else {
        let arrCedulasMuestreo = await AsyncStorage.getItem(
          'cedulasmuestreoFloracion',
        );
        arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      }
      await AsyncStorage.setItem(
        'cedulasmuestreoFloracion',
        JSON.stringify(arrCedulasMuestreoArreglo),
      );
      dispatch(getCedulasPedientesExito(arrCedulasMuestreoArreglo));
    } catch (error) {
      let arrCedulasMuestreo = await AsyncStorage.getItem(
        'cedulasmuestreoFloracion',
      );
      arrCedulasMuestreoArreglo = JSON.parse(arrCedulasMuestreo);
      dispatch(getCedulasPedientesError(arrCedulasMuestreoArreglo));
    }
  };
}


const empezarEnvio = () => ({
  type: ENVIAR_MUESTREO_FLORACION,
});
const enviarFormulario = () => ({
  type: ENVIAR_MUESTREO_FLORACION_EXITO,
});
const envioError = () => ({
  type: ENVIAR_MUESTREO_FLORACION_ERROR,
});

// Obtener cedulas FLORACION pendientes a muestrear
const getCedulasPedientes = () => ({
  type: CARGAR_MUESTREO_FLORACION,
});
const getCedulasPedientesExito = data => ({
  type: CARGAR_MUESTREO_FLORACION_EXITO,
  payload: data,
});
const getCedulasPedientesError = data => ({
  type: CARGAR_MUESTREO_FLORACION_ERROR,
  payload: data,
});


