

 export const menuItemsObra = [
   
    {
        name: 'Avance de Obra',
        icon: 'cube-outline',
        component: 'AvanceObraScreen'
    },
    {
        name: 'Avance de Obra Manual',
        icon: 'cube-outline',
        component: 'AvanceObraManualScreen'
    },
    
   
]