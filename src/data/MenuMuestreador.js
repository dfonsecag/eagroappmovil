

 export const MenuMuestreador = [
    {
        name: 'Muestreo Plagas en Fruta',
        icon: 'cube-outline',
        component: 'MuestreoFrutaScreen'
    },
    {
        name: 'Muestreo Plagas en Raiz',
        icon: 'cube-outline',
        component: 'MuestreoRaizScreen'
    },
    {
        name: 'Muestreo Pesos',
        icon: 'barbell-outline',
        component: 'MuestreoPesoScreen'
    },   
    {
        name: 'Muestreo Meristemos',
        icon: 'cube-outline',
        component: 'MuestreoMeristemosScreen'
    },   
    {
        name: 'Estimación de Fruta',
        icon: 'calculator-outline',
        component: 'MuestreoEstimacionScreen'
    },
    {
        name: 'Premaduración de Fruta',
        icon: 'calculator-outline',
        component: 'MuestreoPremaduracionScreen'
    },
    {
        name: 'Muestreo Floración Natural',
        icon: 'calculator-outline',
        component: 'MuestreoFloracionScreen'
    },
    {
        name: 'Muestreo Semillas',
        icon: 'cube-outline',
        component: 'MuestreoSemillaScreen'
    },
    {
        name: 'Muestreo Calidad Siembra',
        icon: 'cube-outline',
        component: 'MuestreoCalidadSiembra'
    },
    {
        name: 'Terrazas Contratistas',
        icon: 'cube-outline',
        component: 'RegistroContratistaTerrazasScreen'
    },
   
]