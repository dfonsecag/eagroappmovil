

 export const MenuAdmin = [
    
    {
        name: 'Hoja de Vida',
        icon: 'document-outline',
        component: 'HojaVidaScreen'
    },
    {
        name: 'Muestreo Plagas en Fruta',
        icon: 'cube-outline',
        component: 'MuestreoFrutaScreen'
    },
    {
        name: 'Muestreo Plagas en Raiz',
        icon: 'cube-outline',
        component: 'MuestreoRaizScreen'
    },
    {
        name: 'Muestreo Pesos',
        icon: 'barbell-outline',
        component: 'MuestreoPesoScreen'
    },
   
    {
        name: 'Muestreo Meristemos',
        icon: 'cube-outline',
        component: 'MuestreoMeristemosScreen'
    },  
    {
        name: 'Avance de Obra',
        icon: 'cube-outline',
        component: 'AvanceObraAdminScreen'
    },
    {
        name: 'Avance de Obra Manual',
        icon: 'cube-outline',
        component: 'AvanceObraManualScreen'
    },
    {
        name: 'Estimación de Fruta',
        icon: 'calculator-outline',
        component: 'MuestreoEstimacionScreen'
    },
    {
        name: 'Premaduración de Fruta',
        icon: 'calculator-outline',
        component: 'MuestreoPremaduracionScreen'
    },
    {
        name: 'Muestreo Floración Natural',
        icon: 'calculator-outline',
        component: 'MuestreoFloracionScreen'
    },
    {
        name: 'Reporte Estimación',
        icon: 'clipboard-outline',
        component: 'EstimacionScreen'
    },
    {
        name: 'Muestreo Semillas',
        icon: 'cube-outline',
        component: 'MuestreoSemillaScreen'
    },
    {
        name: 'Muestreo Calidad Siembra',
        icon: 'cube-outline',
        component: 'MuestreoCalidadSiembra'
    },
    {
        name: 'Muestreo Fruta Rechazo',
        icon: 'cube-outline',
        component: 'MuestreoFrutaRechazoScreen'
    },
    {
        name: 'Muestreo Pesos Cajas',
        icon: 'cube-outline',
        component: 'MuestreoCajasFrutasScreen'
    },
    {
        name: 'Terrazas Contratistas',
        icon: 'cube-outline',
        component: 'RegistroContratistaTerrazasScreen'
    },
    {
        name: 'Registro PH y Cloro',
        icon: 'cube-outline',
        component: 'RegistroPhCloroPlantaScreen'
    },
    
    
    
   
]