

 export const menuMuestreadorPlanta = [
   
    {
        name: 'Muestreo Fruta Rechazo',
        icon: 'document-text-outline',
        component: 'MuestreoFrutaRechazoScreen'
    },
    {
        name: 'Muestreo Pesos Cajas',
        icon: 'cube-outline',
        component: 'MuestreoCajasFrutasScreen'
    },
    {
        name: 'Registro PH y Cloro',
        icon: 'cube-outline',
        component: 'RegistroPhCloroPlantaScreen'
    },
    
   
]