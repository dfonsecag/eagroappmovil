export const MenuProduccion = [
  {
    name: 'Hoja de Vida',
    icon: 'document-outline',
    component: 'HojaVidaScreen'
},
{
    name: 'Muestreo Plagas en Fruta',
    icon: 'cube-outline',
    component: 'MuestreoFrutaScreen'
},
{
    name: 'Muestreo Plagas en Raiz',
    icon: 'cube-outline',
    component: 'MuestreoRaizScreen'
},
{
    name: 'Muestreo Pesos',
    icon: 'barbell-outline',
    component: 'MuestreoPesoScreen'
},

{
    name: 'Muestreo Meristemos',
    icon: 'cube-outline',
    component: 'MuestreoMeristemosScreen'
},
{
    name: 'Muestreo Floración Natural',
    icon: 'calculator-outline',
    component: 'MuestreoFloracionScreen'
},
{
    name: 'Avance de Obra',
    icon: 'cube-outline',
    component: 'AvanceObraAdminScreen'
},
{
    name: 'Avance de Obra Manual',
    icon: 'cube-outline',
    component: 'AvanceObraManualScreen'
},
{
    name: 'Muestreo Semillas',
    icon: 'cube-outline',
    component: 'MuestreoSemillaScreen'
},
{
    name: 'Muestreo Calidad Siembra',
    icon: 'cube-outline',
    component: 'MuestreoCalidadSiembra'
},

];
