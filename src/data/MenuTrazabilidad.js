export const MenuTrazabilidad = [
  {
    name: 'Hoja de Vida',
    icon: 'document-outline',
    component: 'HojaVidaScreen',
  },
  {
    name: 'Reporte Estimación',
    icon: 'clipboard-outline',
    component: 'EstimacionScreen',
  },
  {
    name: 'Estimación de Fruta',
    icon: 'calculator-outline',
    component: 'MuestreoEstimacionScreen',
  },
  {
    name: 'Premaduración de Fruta',
    icon: 'calculator-outline',
    component: 'MuestreoPremaduracionScreen',
  },
  {
    name: 'Muestreo Floración Natural',
    icon: 'calculator-outline',
    component: 'MuestreoFloracionScreen'
  },
 
];
